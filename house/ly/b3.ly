\version "2.14.2"
\language "english"
\pointAndClickOff
\header { title = "b3" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\score {
<<
\new Staff  {
\set Staff.instrumentName = "揚琴"
\set Staff.shortInstrumentName = "揚琴"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 4/4 \key b \dorian r4 g4 r2 | % untitled/b3 untitled/b3.t8 .25-.5; 1
  df2~ df4. r8 | % untitled/b3 untitled/b3.t8 1-1.87; 2
  r4 r8 r16 df16 r4 r8 g8 | % untitled/b3 untitled/b3.t8 2.87-3; 3
  r4 df2. | % untitled/b3 untitled/b3.t8 3.25-4; 4
  df8 r8 g8 r8 df,4:32 r8 g8 | % untitled/b3 untitled/b3.t8 4.87-5; 5
  r2 r4 r8 df8 | % untitled/b3 untitled/b3.t8 5.87-6; 6
  r2 r4 r8 r16 g16 | % untitled/b3 untitled/b3.t8 6.93-7; 7
  r16 g16 r8 r4 r4 r8 r16 g16 | % untitled/b3 untitled/b3.t8 7.93-8; 8
  r16 g16 r8 r4 r8 d,8 r8 g8 | % untitled/b3 untitled/b3.t8 8.87-9; 9
  r2 r4 r8 r16 df16 | % untitled/b3 untitled/b3.t8 9.93-10; 10
  r2 r4 r8 r16 g16 | % untitled/b3 untitled/b3.t8 10.93-11; 11
  r16 g16 r8 r4 r2 | % untitled/b3 untitled/b3.t8 11.06-11.12; 12
  r4 r8 g8 r2 \bar "|." 
} }

\new Staff  {
\set Staff.instrumentName = "箏"
\set Staff.shortInstrumentName = "箏"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 4/4 \key b \dorian r4 g4 r2 | % untitled/b3 untitled/b3.t2 .25-.5; 1
  df,8 r8 r4 df,8 r8 df,4~ | % untitled/b3 untitled/b3.t2 1.75-2.12; 2
  df,8 r8 r4 df,8 r8 df,4 | % untitled/b3 untitled/b3.t2 2.75-3; 3
  r4 df,8 r8 df,8 r8 r8 df,8 | % untitled/b3 untitled/b3.t2 3.87-4; 4
  r2 g,8 r8 r4 | % untitled/b3 untitled/b3.t2 4.5-4.62; 5
  d,,8 r8 r8 g,8 r8 g,8 r4 | % untitled/b3 untitled/b3.t2 5.62-5.75; 6
  r4 g,8 r8 r2 | % untitled/b3 untitled/b3.t2 6.25-6.37; 7
  r4 r8 df,8 d,8 df,8 d,4 | % untitled/b3 untitled/b3.t2 7.75-8; 8
  d,,8 r8 d,8 r8 r2 | % untitled/b3 untitled/b3.t2 8.25-8.37; 9
  d,8 d,,8 r8 g,8 r8 g,8 r4 | % untitled/b3 untitled/b3.t2 9.62-9.75; 10
  r4 g,8 r8 r2 | % untitled/b3 untitled/b3.t2 10.25-10.37; 11
  r4 r8 df,,8 d,,8 r8 d,4 | % untitled/b3 untitled/b3.t2 11.75-12; 12
  r4 r8 g8 r8 d,8 r4 \bar "|." 
} }

>>
}

