\version "2.14.2"
\language "english"
\pointAndClickOff
\header { title = "b6" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\score {
<<
\new StaffGroup <<
\set StaffGroup.instrumentName = "揚琴"
\set StaffGroup.shortInstrumentName = "揚琴"
\new Staff = "up" {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 4/4 \key b \dorian \change Staff = "down" b,8 \change Staff = "up" b8 <d' b'>8 r8 \change Staff = "down" b,8 \change Staff = "up" b8 <cs' a'>8 <d' b'>8 | % untitled/b6 untitled/b6.t6 .87-1; 1
 \change Staff = "down" e8 \change Staff = "up" b8 <gs' b'>8 r8 <d'' a'>8 r8 <cs'' b'>8 r8 | % untitled/b6 untitled/b6.t6 1.75-1.87; 2
 \change Staff = "down" d8 \change Staff = "up" \change Staff = "down" a8 \change Staff = "up" fs'8 \change Staff = "down" a8 \change Staff = "up" \change Staff = "down" g8 \change Staff = "up" d'8 b'8 d'8 | % untitled/b6 untitled/b6.t6 2.87-3; 3
 \change Staff = "down" c8 \change Staff = "up" \change Staff = "down" g8 \change Staff = "up" d'8 \change Staff = "down" g8 \change Staff = "up" <e' e''>8 <fs' fs''>8 <d' d''>8 <cs' cs''>8 | % untitled/b6 untitled/b6.t6 3.87-4; 4
 \change Staff = "down" b,8 \change Staff = "up" \change Staff = "down" b8 \change Staff = "up" <d' b'>8 r8 \change Staff = "down" b,8 \change Staff = "up" \change Staff = "down" b8 \change Staff = "up" <cs' a'>8 <d' b'>8 | % untitled/b6 untitled/b6.t6 4.87-5; 5
 \change Staff = "down" e8 \change Staff = "up" \change Staff = "down" b8 \change Staff = "up" <gs' b'>8 r8 <d'' a'>8 r8 <cs'' b'>8 r8 | % untitled/b6 untitled/b6.t6 5.75-5.87; 6
 \change Staff = "down" g,8 \change Staff = "up" \change Staff = "down" d8 \change Staff = "up" \change Staff = "down" a8 \change Staff = "up" \change Staff = "down" b8 \change Staff = "up" \change Staff = "down" e8 \change Staff = "up" \change Staff = "down" b8 \change Staff = "up" fs'8 g'8 | % untitled/b6 untitled/b6.t6 6.87-7; 7
 fs'4 cs''8 d''8 e''8 fs''16 e''16 d''8 cs''8 | % untitled/b6 untitled/b6.t6 7.87-8; 8
 b'4 fs'4 b8 cs'8 d'8 e'8 | % untitled/b6 untitled/b6.t6 8.87-9; 9
 fs'8 a'8 gs'4 e'8 fs'8 gs'8 a'8 | % untitled/b6 untitled/b6.t6 9.87-10; 10
 b'8 cs''8 d''4 g'8 d''8 cs''8 b'8 | % untitled/b6 untitled/b6.t6 10.87-11; 11
 d'8 e'8 fs'4 <e'' e'>8 <fs'' fs'>16 <e'' e'>16 <d'' d'>8 <cs'' cs'>8 | % untitled/b6 untitled/b6.t6 11.87-12; 12
 <b' b>8 \change Staff = "down" fs8 \change Staff = "up" <d' b'>8 r8 \change Staff = "down" b8 \change Staff = "up" \change Staff = "down" fs8 \change Staff = "up" <d' b'>8 <e' cs''>8 | % untitled/b6 untitled/b6.t6 12.87-13; 13
 \change Staff = "down" a8 \change Staff = "up" \change Staff = "down" fs8 \change Staff = "up" <fs' b'>8 r8 b8 d'8 e'8 fs'8 | % untitled/b6 untitled/b6.t6 13.87-14; 14
 \change Staff = "down" <e cs'>2 \change Staff = "up" \change Staff = "down" <a d'>4 \change Staff = "up" r8 e'8 | % untitled/b6 untitled/b6.t6 14.87-15; 15
 \change Staff = "down" <d fs'>4 \change Staff = "up" r8 b8 r4 d''8 e''8 | % untitled/b6 untitled/b6.t6 15.87-16; 16
 d''8 \change Staff = "down" e8 \change Staff = "up" \change Staff = "down" b8 \change Staff = "up" fs'8 g'8 d''8 b'8 cs''8 | % untitled/b6 untitled/b6.t6 16.87-17; 17
 b'8 \change Staff = "down" d8 \change Staff = "up" \change Staff = "down" b8 \change Staff = "up" e'8 fs'8 cs''8 b'8 a'8 | % untitled/b6 untitled/b6.t6 17.87-18; 18
 b'8 \change Staff = "down" c8 \change Staff = "up" \change Staff = "down" e8 \change Staff = "up" c'8 d'8 a'8 b'8 a'8 | % untitled/b6 untitled/b6.t6 18.87-19; 19
 b'8 fs'8 \change Staff = "down" b,8 \change Staff = "up" \change Staff = "down" fs8 \change Staff = "up" fs'8 b'8 d''8 e''8 | % untitled/b6 untitled/b6.t6 19.87-20; 20
 d''8 \change Staff = "down" e8 \change Staff = "up" \change Staff = "down" b8 \change Staff = "up" fs'8 g'8 d''8 cs''8 d''8 | % untitled/b6 untitled/b6.t6 20.87-21; 21
 cs''8 \change Staff = "down" d8 \change Staff = "up" \change Staff = "down" b8 \change Staff = "up" e'8 fs'8 cs''8 fs''8^"慢" e''8 | % untitled/b6 untitled/b6.t6 21.87-22; 22
 fs''8 g'8 b'8 cs''8 e''8 fs''8 a''8 g''8 | % untitled/b6 untitled/b6.t6 22.87-23; 23
 fs''8 b'8 fs'8 cs'8 b2 \bar "|."
} }

\new Staff = "down" \RemoveEmptyStaves {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
\override Staff.VerticalAxisGroup.remove-first = ##t
{
 \clef bass \time 4/4 \key b \dorian s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b6 untitled/b6.t6 23.5-24; 1
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b6 untitled/b6.t6 23.5-24; 2
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b6 untitled/b6.t6 23.5-24; 3
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b6 untitled/b6.t6 23.5-24; 4
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b6 untitled/b6.t6 23.5-24; 5
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b6 untitled/b6.t6 23.5-24; 6
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b6 untitled/b6.t6 23.5-24; 7
 s4 s8 s8 s8 s16 s16 s8 s8 | % untitled/b6 untitled/b6.t6 23.5-24; 8
 s4 s4 s8 s8 s8 s8 | % untitled/b6 untitled/b6.t6 23.5-24; 9
 s8 s8 s4 s8 s8 s8 s8 | % untitled/b6 untitled/b6.t6 23.5-24; 10
 s8 s8 s4 s8 s8 s8 s8 | % untitled/b6 untitled/b6.t6 23.5-24; 11
 s8 s8 s4 s8 s16 s16 s8 s8 | % untitled/b6 untitled/b6.t6 23.5-24; 12
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b6 untitled/b6.t6 23.5-24; 13
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b6 untitled/b6.t6 23.5-24; 14
 s2 s4 s8 s8 | % untitled/b6 untitled/b6.t6 23.5-24; 15
 s4 s8 s8 s4 s8 s8 | % untitled/b6 untitled/b6.t6 23.5-24; 16
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b6 untitled/b6.t6 23.5-24; 17
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b6 untitled/b6.t6 23.5-24; 18
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b6 untitled/b6.t6 23.5-24; 19
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b6 untitled/b6.t6 23.5-24; 20
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b6 untitled/b6.t6 23.5-24; 21
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b6 untitled/b6.t6 23.5-24; 22
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b6 untitled/b6.t6 23.5-24; 23
 s8 s8 s8 s8 s2 \bar "|."
} }

>>

\new Staff  {
\set Staff.instrumentName = "箏"
\set Staff.shortInstrumentName = "箏"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 4/4 \key b \dorian fs''4 b'8 cs''8 d''8 e''8 fs''8 a''8 | % untitled/b6 untitled/b6.t1 .87-1; 1
 gs''4 e''4 b'4 b'8 e''8 | % untitled/b6 untitled/b6.t1 1.87-2; 2
 d''2.:32 fs''8 e''8 | % untitled/b6 untitled/b6.t1 2.87-3; 3
 d''2 e''8 fs''8 d''8 e''8 | % untitled/b6 untitled/b6.t1 3.87-4; 4
 fs''4 b'8 cs''8 d''8 cs''8 d''8 a''8 | % untitled/b6 untitled/b6.t1 4.87-5; 5
 gs''4 e''4 b'4 b''8 a''8 | % untitled/b6 untitled/b6.t1 5.87-6; 6
 b''4.:32 b''8 a''8 b''8 d''8 e''8 | % untitled/b6 untitled/b6.t1 6.87-7; 7
 fs''2 r4 d'8 cs'8 | % untitled/b6 untitled/b6.t1 7.87-8; 8
 fs'4 b8 cs'8 d'8 e'8 fs'8 a'8 | % untitled/b6 untitled/b6.t1 8.87-9; 9
 gs'4 e'8 fs'8 gs'8 a'8 b'8 cs''8 | % untitled/b6 untitled/b6.t1 9.87-10; 10
 d''4 g'8 d''8 cs''8 b'8 d'8 e'8 | % untitled/b6 untitled/b6.t1 10.87-11; 11
 fs'4 cs'8 d'8 e'8 fs'8 d'8 cs'8 | % untitled/b6 untitled/b6.t1 11.87-12; 12
 b'2 b'8 a'8 b'8 cs''8 | % untitled/b6 untitled/b6.t1 12.87-13; 13
 a'4 d'4 b8 d'8 e'8 fs'8 | % untitled/b6 untitled/b6.t1 13.87-14; 14
 g'4 a'4 fs'4 e'8 fs'8~ | % untitled/b6 untitled/b6.t1 14.87-15.12; 15
 fs'8 b4. r4 \clef bass d'8 e'8 | % untitled/b6 untitled/b6.t1 15.87-16; 16
 d'4 b4 r4 b8 cs'8 | % untitled/b6 untitled/b6.t1 16.87-17; 17
 b4 d2 b8 a8 | % untitled/b6 untitled/b6.t1 17.87-18; 18
 b2 r4 b8 a8 | % untitled/b6 untitled/b6.t1 18.87-19; 19
 b2 r4 d'8 e'8 | % untitled/b6 untitled/b6.t1 19.87-20; 20
 d'4 b4 r4 cs'8 d'8 | % untitled/b6 untitled/b6.t1 20.87-21; 21
 cs'4 d2 \clef treble fs''8 e''8 | % untitled/b6 untitled/b6.t1 21.87-22; 22
 fs''4 r4 r4 a''8 g''8 | % untitled/b6 untitled/b6.t1 22.87-23; 23
 fs''1 \bar "|."
} }

>>
}

