\version "2.14.2"
\language "english"
\pointAndClickOff
\header { title = "score" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\score {
<<
\new StaffGroup <<
\set StaffGroup.instrumentName = "揚琴"
\set StaffGroup.shortInstrumentName = "揚琴"
\new Staff = "up" {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
\time 4/4 \key b \dorian b'4 cs''4 d''4 e''4 | % untitled/b3 untitled/b3.t8 .75-1; 1
  fs''2 r4 r8 b''8 | % untitled/b3 untitled/b3.t8 1.87-2; 2
  a''4.:32 gs''16 fs''16 e''4 d''8 cs''8 | % untitled/b3 untitled/b3.t8 2.87-3; 3
  d''8. e''16 fs''4 r2 | % untitled/b3 untitled/b3.t8 3.25-3.5; 4
  fs''8 e''16 d''16 cs''8 b'8 fs'4:32 b'8 cs''8 | % untitled/b3 untitled/b3.t8 4.87-5; 5
  d''2 r4 r8 fs''8 | % untitled/b3 untitled/b3.t8 5.87-6; 6
  e''2~:32 e''4.:32 d''16 cs''16 | % untitled/b3 untitled/b3.t8 6.93-7; 7
  d''16 cs''16 b'4. r4 r8 d''16 cs''16 | % untitled/b3 untitled/b3.t8 7.93-8; 8
  d''16 cs''16 b'4. r8 g'8 b'8 cs''8 | % untitled/b3 untitled/b3.t8 8.87-9; 9
  d''2 r4 r8 d''16 fs''16 | % untitled/b3 untitled/b3.t8 9.93-10; 10
  \acciaccatura { e''8[ fs''8 d''8] } e''8 e''4.~:32 e''4.:32 d''16 cs''16 | % untitled/b3 untitled/b3.t8 10.93-11; 11
  d''16 cs''16 b'4.~ b'2 | \time 3/4 % untitled/b3 untitled/b3.t8 11.12-12; 12
  r8 a'8 b'8 cs''8 d''8 e''8 | \time 4/4 % untitled/b3 untitled/b3.t8 12.62-12.75; 13
  fs''2~ fs''4. b''8 | % untitled/b4 untitled/b4.t7 .87-1; 14
  a''4.:32 gs''16 fs''16 e''4 d''8 cs''8 | % untitled/b4 untitled/b4.t7 1.87-2; 15
  d''8. e''16 fs''2. | % untitled/b4 untitled/b4.t7 2.25-3; 16
  b''16 a''16 b''4.~ b''2 | % untitled/b4 untitled/b4.t7 3.12-4; 17
  b''16 a''16 b''4. g''16 fs''16 g''4. | % untitled/b4 untitled/b4.t7 4.62-5; 18
  e''16 d''16 e''4. d''16 cs''16 d''8 cs''4~:32 | % untitled/b4 untitled/b4.t7 5.75-6.37; 19
  cs''4.:32 b'16 a'16 b'2~ | % untitled/b4 untitled/b4.t7 6.5-7.25; 20
  b'4 cs''4 d''4 e''4 | % untitled/b4 untitled/b4.t7 7.75-8; 21
  fs''4 b'4 e''8 fs''8 g''8 a''8 | % untitled/b4 untitled/b4.t7 8.87-9; 22
  b''4 e''4 fs''4 g''4 | % untitled/b4 untitled/b4.t7 9.75-10; 23
  a''4:32 a''8 d''8 cs''8 d''8 fs'16 cs''16 b'16 cs''16 | % untitled/b4 untitled/b4.t7 10.93-11; 24
  e'16 b'16 a'16 b'16 a''16 d''16 cs''16 d''16 e'16 cs''16 b'16 cs''16 d'16 b'16 a'16 b'16 | % untitled/b4 untitled/b4.t7 11.93-12; 25
  a''8 d''8 cs''8 e''8 r8 fs''8 e''8 d'8 | % untitled/b4 untitled/b4.t7 12.87-13; 26
  r8 a''16 d''16 fs''16 cs''16 d''8 d'8:32 a'8:32 cs''8 e''16 fs''16 | % untitled/b4 untitled/b4.t7 13.93-14; 27
  e''8 e''8 fs''8 e''8 d''16 cs''16 b'8 a''16 d''16 fs''16 cs''16 | % untitled/b4 untitled/b4.t7 14.93-15; 28
  d''8 a''16 d''16 fs''16 cs''16 d''8 r2 | % untitled/b4 untitled/b4.t7 15.37-15.5; 29
  r4 cs''4 d''4 e''4 | % untitled/b4 untitled/b4.t7 16.75-17; 30
  fs''2 r4 r8 b''8 | % untitled/b2 untitled/b2.t8 .87-1; 31
  a''4. gs''16 fs''16 e''4 d''8 cs''8 | % untitled/b2 untitled/b2.t8 1.87-2; 32
  d''8. e''16 fs''4 r2 | % untitled/b2 untitled/b2.t8 2.25-2.5; 33
  r2 r4 b'8 cs''8 | % untitled/b2 untitled/b2.t8 3.87-4; 34
  d''2 r4 r8 fs''8 | % untitled/b2 untitled/b2.t8 4.87-5; 35
  \acciaccatura { e''8[ fs''8] } e''2 r4 r16 fs''16 d''16 cs''16 | % untitled/b2 untitled/b2.t8 5.93-6; 36
  b'2 r4 e''8 d''16 cs''16 | % untitled/b2 untitled/b2.t8 6.93-7; 37
  b'1 | % untitled/b2 untitled/b2.t8 7-8; 38
  r8 a'8 b'8 cs''8 d''8 cs''8 d''8 <e'' cs''>8 | % untitled/b2 untitled/b2.t8 8.87-9; 39
  <fs'' b'>2:32 fs''4. b''8 | % untitled/b2 untitled/b2.t8 9.87-10; 40
  <a'' e''>4.:32 gs''16 fs''16 e''4 <d'' d'>8 <cs'' cs'>8 | % untitled/b2 untitled/b2.t8 10.87-11; 41
  <d'' d'>8. <e'' e'>16 <fs'' fs'>4.:32 b8 a4 | % untitled/b2 untitled/b2.t11 11.75-12; 42
  a'8 gs'16 fs'16 e'8 d'8 cs'8 b8 <b' g>8 <cs'' a>8 | % untitled/b2 untitled/b2.t8 12.87-13; 43
  <d'' b>2:32 d''4. fs''8 | % untitled/b2 untitled/b2.t8 13.87-14; 44
  \acciaccatura { e''8[ fs''8] } e''8 <e'' a'>4.:32 e''4~ e''16 fs''16 d''16 cs''16 | % untitled/b2 untitled/b2.t8 14.93-15; 45
  <d'' e'~>16:32 <e'~ cs''>16:32 <e' b'>4.:32 b'4 e''8 d''16 cs''16 | % untitled/b2 untitled/b2.t8 15.93-16; 46
  <d'' d'~>16:32 <d'~ cs''>16:32 <d' b'>4.:32 d''8 cs''8 b'8 a'8 | % untitled/b2 untitled/b2.t8 16.87-17; 47
  <b' e'>2:32 b'2 | % untitled/b2 untitled/b2.t8 17.5-18; 48
  r8 \acciaccatura { a'8[ a'8] } a'8 b'8 cs''8 d''8 cs''8 d''8 e''8 | % untitled/b2 untitled/b2.t8 18.87-19; 49
  <fs'' b'>2 r4 <b' fs''>4 | % untitled/b5 untitled/b5.t9 .75-1; 50
  <b' e''>1 | % untitled/b5 untitled/b5.t9 1-2; 51
  <g' d''>2 <e' cs''>2 | % untitled/b5 untitled/b5.t9 2.5-3; 52
  <d' b'>2 <cs' e'>4 d'8 e'8 | % untitled/b5 untitled/b5.t9 3.87-4; 53
  <b fs'>2. <b' fs''>4 | % untitled/b5 untitled/b5.t9 4.75-5; 54
  <b' e''>1 | % untitled/b5 untitled/b5.t9 5-6; 55
  <g'' d''>2 <e'' b''>2 | % untitled/b5 untitled/b5.t9 6.5-7; 56
  <cs'' b'>2 <d'' b'>2 | % untitled/b5 untitled/b5.t9 7.5-8; 57
  b''16 g''16 d''4. r2 | % untitled/b5 untitled/b5.t9 8.12-8.5; 58
  b''16 fs''16 d''16 cs''16 d''4 r2 | % untitled/b5 untitled/b5.t9 9.25-9.5; 59
  <g'' e'' b'>1 | % untitled/b5 untitled/b5.t9 10-11; 60
  <fs'' d'' b'>1 | % untitled/b5 untitled/b5.t9 11-12; 61
  <d'' b' g'>1 | % untitled/b5 untitled/b5.t9 12-13; 62
  <b' fs' e'>1 | % untitled/b5 untitled/b5.t9 13-14; 63
  <d' b' fs''>2 <cs' b' e''>2 | % untitled/b5 untitled/b5.t9 14.5-15; 64
  <b b' cs''>2 <g b' d''>4 b'8 cs''8 | % untitled/b5 untitled/b5.t9 15.87-16; 65
  \change Staff = "down" b,8 \change Staff = "up" b8 <d' b'>8 r8 \change Staff = "down" b,8 \change Staff = "up" b8 <cs' a'>8 <d' b'>8 | % untitled/b6 untitled/b6.t6 .87-1; 66
  \change Staff = "down" e8 \change Staff = "up" b8 <gs' b'>8 r8 <d'' a'>8 r8 <cs'' b'>8 r8 | % untitled/b6 untitled/b6.t6 1.75-1.87; 67
  \change Staff = "down" d8 \change Staff = "up" \change Staff = "down" a8 \change Staff = "up" fs'8 \change Staff = "down" a8 \change Staff = "up" \change Staff = "down" g8 \change Staff = "up" d'8 b'8 d'8 | % untitled/b6 untitled/b6.t6 2.87-3; 68
  \change Staff = "down" c8 \change Staff = "up" \change Staff = "down" g8 \change Staff = "up" d'8 \change Staff = "down" g8 \change Staff = "up" <e' e''>8 <fs' fs''>8 <d' d''>8 <cs' cs''>8 | % untitled/b6 untitled/b6.t6 3.87-4; 69
  \change Staff = "down" b,8 \change Staff = "up" \change Staff = "down" b8 \change Staff = "up" <d' b'>8 r8 \change Staff = "down" b,8 \change Staff = "up" \change Staff = "down" b8 \change Staff = "up" <cs' a'>8 <d' b'>8 | % untitled/b6 untitled/b6.t6 4.87-5; 70
  \change Staff = "down" e8 \change Staff = "up" \change Staff = "down" b8 \change Staff = "up" <gs' b'>8 r8 <d'' a'>8 r8 <cs'' b'>8 r8 | % untitled/b6 untitled/b6.t6 5.75-5.87; 71
  \change Staff = "down" g,8 \change Staff = "up" \change Staff = "down" d8 \change Staff = "up" \change Staff = "down" a8 \change Staff = "up" \change Staff = "down" b8 \change Staff = "up" \change Staff = "down" e8 \change Staff = "up" \change Staff = "down" b8 \change Staff = "up" fs'8 g'8 | % untitled/b6 untitled/b6.t6 6.87-7; 72
  fs'4 cs''8 d''8 e''8 fs''16 e''16 d''8 cs''8 | % untitled/b6 untitled/b6.t6 7.87-8; 73
  b'4 fs'4 b8 cs'8 d'8 e'8 | % untitled/b6 untitled/b6.t6 8.87-9; 74
  fs'8 a'8 gs'4 e'8 fs'8 gs'8 a'8 | % untitled/b6 untitled/b6.t6 9.87-10; 75
  b'8 cs''8 d''4 g'8 d''8 cs''8 b'8 | % untitled/b6 untitled/b6.t6 10.87-11; 76
  d'8 e'8 fs'4 <e'' e'>8 <fs'' fs'>16 <e'' e'>16 <d'' d'>8 <cs'' cs'>8 | % untitled/b6 untitled/b6.t6 11.87-12; 77
  <b' b>8 \change Staff = "down" fs8 \change Staff = "up" <d' b'>8 r8 \change Staff = "down" b8 \change Staff = "up" \change Staff = "down" fs8 \change Staff = "up" <d' b'>8 <e' cs''>8 | % untitled/b6 untitled/b6.t6 12.87-13; 78
  \change Staff = "down" a8 \change Staff = "up" \change Staff = "down" fs8 \change Staff = "up" <fs' b'>8 r8 b8 d'8 e'8 fs'8 | % untitled/b6 untitled/b6.t6 13.87-14; 79
  \change Staff = "down" <e cs'>2 \change Staff = "up" \change Staff = "down" <a d'>4 \change Staff = "up" r8 e'8 | % untitled/b6 untitled/b6.t6 14.87-15; 80
  \change Staff = "down" <d fs'>4 \change Staff = "up" r8 b8 r4 d''8 e''8 | % untitled/b6 untitled/b6.t6 15.87-16; 81
  d''8 \change Staff = "down" e8 \change Staff = "up" \change Staff = "down" b8 \change Staff = "up" fs'8 g'8 d''8 b'8 cs''8 | % untitled/b6 untitled/b6.t6 16.87-17; 82
  b'8 \change Staff = "down" d8 \change Staff = "up" \change Staff = "down" b8 \change Staff = "up" e'8 fs'8 cs''8 b'8 a'8 | % untitled/b6 untitled/b6.t6 17.87-18; 83
  b'8 \change Staff = "down" c8 \change Staff = "up" \change Staff = "down" e8 \change Staff = "up" c'8 d'8 a'8 b'8 a'8 | % untitled/b6 untitled/b6.t6 18.87-19; 84
  b'8 fs'8 \change Staff = "down" b,8 \change Staff = "up" \change Staff = "down" fs8 \change Staff = "up" fs'8 b'8 d''8 e''8 | % untitled/b6 untitled/b6.t6 19.87-20; 85
  d''8 \change Staff = "down" e8 \change Staff = "up" \change Staff = "down" b8 \change Staff = "up" fs'8 g'8 d''8 cs''8 d''8 | % untitled/b6 untitled/b6.t6 20.87-21; 86
  cs''8 \change Staff = "down" d8 \change Staff = "up" \change Staff = "down" b8 \change Staff = "up" e'8 fs'8 cs''8 fs''8 e''8 | % untitled/b6 untitled/b6.t6 21.87-22; 87
  fs''8 g'8 b'8 cs''8 e''8 fs''8 a''8 g''8 | % untitled/b6 untitled/b6.t6 22.87-23; 88
  fs''8 b'8 fs'8 cs'8 b2 | % untitled/b6 untitled/b6.t6 23.5-24; 89
  <fs'' b'>2 r4 <b' fs''>4 | % untitled/b5 untitled/b5.t9 .75-1; 90
  <b' e''>1 | % untitled/b5 untitled/b5.t9 1-2; 91
  <g' d''>2 <e' cs''>2 | % untitled/b5 untitled/b5.t9 2.5-3; 92
  <d' b'>2 <cs' e'>4 d'8 e'8 | % untitled/b5 untitled/b5.t9 3.87-4; 93
  <b fs'>2. <b' fs''>4 | % untitled/b5 untitled/b5.t9 4.75-5; 94
  <b' e''>1 | % untitled/b5 untitled/b5.t9 5-6; 95
  <g'' d''>2 <e'' b''>2 | % untitled/b5 untitled/b5.t9 6.5-7; 96
  <cs'' b'>2 <d'' b'>2 | % untitled/b5 untitled/b5.t9 7.5-8; 97
  b''16 g''16 d''4. r2 | % untitled/b5 untitled/b5.t9 8.12-8.5; 98
  b''16 fs''16 d''16 cs''16 d''4 r2 | % untitled/b5 untitled/b5.t9 9.25-9.5; 99
  <g'' e'' b'>1 | % untitled/b5 untitled/b5.t9 10-11; 100
  <fs'' d'' b'>1 | % untitled/b5 untitled/b5.t9 11-12; 101
  <d'' b' g'>1 | % untitled/b5 untitled/b5.t9 12-13; 102
  <b' fs' e'>1 | % untitled/b5 untitled/b5.t9 13-14; 103
  <d' b' fs''>2 <cs' b' e''>2 | % untitled/b5 untitled/b5.t9 14.5-15; 104
  <b b' cs''>2 <g b' d''>4 b'8 cs''8 \bar "|." 
} }

\new Staff = "down" \RemoveEmptyStaves {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
\override Staff.VerticalAxisGroup.remove-first = ##t
{
\time 4/4 \key b \dorian \clef bass s1 * 105 \bar "|."
} }

>>

\new Staff  {
\set Staff.instrumentName = "箏"
\set Staff.shortInstrumentName = "箏"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
\time 4/4 \key b \dorian b'4 cs''4 b'4 a'4 | % untitled/b3 untitled/b3.t2 .75-1; 1
  fs'8 b8 d'8 e'8 fs'8 e'8 fs'4 | % untitled/b3 untitled/b3.t2 1.75-2; 2
  r8 e8 b8 e'8 fs'8 a'8 fs'4 | % untitled/b3 untitled/b3.t2 2.75-3; 3
  b8 d'8 fs'8 e'8 fs'8 b8 d'8 fs'8 | % untitled/b3 untitled/b3.t2 3.87-4; 4
  a'8 gs'16( fs'16) e'8 d'8 cs'8 b8 b'8 a'8 | % untitled/b3 untitled/b3.t2 4.87-5; 5
  g4 b8 cs'8 d'8 cs'8 d'4 | % untitled/b3 untitled/b3.t2 5.75-6; 6
  r8 a8 cs'8 d'8 e'8 d'8 e'4 | % untitled/b3 untitled/b3.t2 6.75-7; 7
  e8 b8 e'8 fs'8 g'8 fs'8 g'4 | % untitled/b3 untitled/b3.t2 7.75-8; 8
  r8 d'8 g'8 a'8 b'4 b'8 a'8 | % untitled/b3 untitled/b3.t2 8.87-9; 9
  g'8 r8 b8 cs'8 d'8 cs'8 d'4 | % untitled/b3 untitled/b3.t2 9.75-10; 10
  r8 a8 cs'8 d'8 e'8 d'8 e'4 | % untitled/b3 untitled/b3.t2 10.75-11; 11
  e8 b8 e'8 fs8 g8 d'8 a'4 | \time 3/4 % untitled/b3 untitled/b3.t2 11.75-12; 12
  r8 a'8 b'8 cs''8 a'8 g'8 | \time 4/4 % untitled/b3 untitled/b3.t2 12.62-12.75; 13
  \ottava #-1 b,8 \ottava #0 b8 fs'8 e'8 fs'8 a'8 fs'4 | % untitled/b4 untitled/b4.t2 .75-1; 14
  e8 b8 e'8 b'8 e'8 fs'8 d'8 e'8 | % untitled/b4 untitled/b4.t2 1.87-2; 15
  fs'8 \ottava #-1 d8 \ottava #0 a8 d'8 fs'8 e'8 fs'8 d'8 | % untitled/b4 untitled/b4.t2 2.87-3; 16
  \ottava #-1 g,8 d8 \ottava #0 b'8 cs''8 d''8 cs''8 b'8 a'8 | % untitled/b4 untitled/b4.t2 3.87-4; 17
  e8 b8 e'8 fs'8 g'8 fs'8 e'8 d'8 | % untitled/b4 untitled/b4.t2 4.87-5; 18
  fs8 g8 b8 e'8 fs'8 g'8 b'8 e''8 | % untitled/b4 untitled/b4.t2 5.87-6; 19
  fs''4. e''8 b'8 fs'8 fs''4~\trill | % untitled/b4 untitled/b4.t2 6.75-7.25; 20
  fs''4 cs''4 b'4 a'4 | % untitled/b4 untitled/b4.t2 7.75-8; 21
  fs'4 b4 e''8 fs''8 e''8 d''8 | % untitled/b4 untitled/b4.t2 8.87-9; 22
  b'4 e'4 d'8 e'8 cs'4 | % untitled/b4 untitled/b4.t2 9.75-10; 23
  cs'8 d'8 b4 \clef bass e2 | % untitled/b4 untitled/b4.t2 10.5-11; 24
  r4 e'8 b8 e2 | % untitled/b4 untitled/b4.t2 11.5-12; 25
  \clef treble r4 cs''16 d''16 e''8 \ottava #-1 b,8 \ottava #0 cs''16 d''16 e''8 d'16 b16 | % untitled/b4 untitled/b4.t2 12.93-13; 26
  r8 cs''8 d''8 b'8 r4 cs''16 d''16 e''8 | % untitled/b4 untitled/b4.t2 13.87-14; 27
  e''16 d''16 e''8 cs''16 d''16 e''8 a'8 b'8 cs''8 d''8 | % untitled/b4 untitled/b4.t2 14.87-15; 28
  b'8 cs''8 d''8 b'8 r2 | % untitled/b4 untitled/b4.t2 15.37-15.5; 29
  r4 cs''4 b'4 a'4 | % untitled/b4 untitled/b4.t2 16.75-17; 30
  r8 b'8 d''8 e''8 fs''8 e''8 fs''8 b''8 | % untitled/b2 untitled/b2.t2 .87-1; 31
  a''8 e'8 b'8 e''8 b''8 fs''8 b''4 | % untitled/b2 untitled/b2.t2 1.75-2; 32
  d'8 a'8 d''8 e''8 d''8 b'8 a'4 | % untitled/b2 untitled/b2.t2 2.75-3; 33
  a''8 gs''16( fs''16) e''8 d''8 cs''8 b'4. | % untitled/b2 untitled/b2.t2 3.62-4; 34
  r8 g'8 b'8 cs''8 d''8 cs''8 d''8 fs''8 | % untitled/b2 untitled/b2.t2 4.87-5; 35
  e''8 a'8 cs''8 d''8 e''8 d''8 e''4 | % untitled/b2 untitled/b2.t2 5.75-6; 36
  r8 e'8 b'8 cs''8 d''8 cs''8 b'4 | % untitled/b2 untitled/b2.t2 6.75-7; 37
  r8 g'8 b'8 cs''8 d''8 cs''8 b'4 | % untitled/b2 untitled/b2.t2 7.75-8; 38
  r8 a'8 b'8 cs''8 d''8 e'8 d'8 e'8 | % untitled/b2 untitled/b2.t2 8.87-9; 39
  \ottava #-1 b,8 \ottava #0 b'8 d''8 e''8 fs''8 e''8 fs''8 b''8 | % untitled/b2 untitled/b2.t2 9.87-10; 40
  \ottava #-1 e8 \ottava #0 e'8 b'8 e''8 b''8 fs''8 b''4 | % untitled/b2 untitled/b2.t2 10.75-11; 41
  \ottava #-1 d8 \ottava #0 a'8 d''8 e''8 d''8 b'8 a'8 r8 | % untitled/b2 untitled/b2.t2 11.75-11.87; 42
  a''8 gs''16 fs''16 e''8 d''8 cs''8 b'4. | % untitled/b2 untitled/b2.t2 12.62-13; 43
  \clef bass g,4 b8 d'8 b8 a8 b8 d'8 | % untitled/b2 untitled/b2.t2 13.87-14; 44
  a,4 a8 e'8 cs'8 b8 cs'4 | % untitled/b2 untitled/b2.t2 14.75-15; 45
  e,8 b,8 b8 cs'8 d'8 cs'8 b4 | % untitled/b2 untitled/b2.t2 15.75-16; 46
  g,4 b8 cs'8 a,8 cs'8 b8 cs'8 | % untitled/b2 untitled/b2.t2 16.87-17; 47
  e,8 b,8 b8 cs'8 d'8 cs'8 b4 | % untitled/b2 untitled/b2.t2 17.75-18; 48
  \clef treble r8 a'8 b'8 cs''8 d''8 cs'8 d'8 e'8 | % untitled/b2 untitled/b2.t2 18.87-19; 49
  fs'4 b'8 \acciaccatura { a''8[ fs''8] } e''8 d''8 cs''8 d''8 a''8 | % untitled/b5 untitled/b5.t2 .87-1; 50
  gs''4 e''4 b'4 b'8 e''8 | % untitled/b5 untitled/b5.t2 1.87-2; 51
  d''2 r4 fs''8 e''8 | % untitled/b5 untitled/b5.t2 2.87-3; 52
  d''2 r8 \acciaccatura { a''8[ fs''8] } e''8 d''8 e''8 | % untitled/b5 untitled/b5.t2 3.87-4; 53
  fs''4 b'8 e''8 d''8 cs''8 d''8 a''8 | % untitled/b5 untitled/b5.t2 4.87-5; 54
  gs''4 e''4 b'4 \acciaccatura { d'''8 } b''8 a''8 | % untitled/b5 untitled/b5.t2 5.87-6; 55
  b''2 r4 a''8 cs'''8 | % untitled/b5 untitled/b5.t2 6.87-7; 56
  b''2 r2 | % untitled/b5 untitled/b5.t2 7-7.5; 57
  \acciaccatura { b'8[ d''8 e''8 fs''8 a''8] } b''2 b''8 a''8 b''8 cs'''8 | % untitled/b5 untitled/b5.t2 8.87-9; 58
  a''4 d''4 b'8 d''8 e''8 fs''8 | % untitled/b5 untitled/b5.t2 9.87-10; 59
  g''4 a''4 fs''4 e''8 fs''8 | % untitled/b5 untitled/b5.t2 10.87-11; 60
  r8 b'4. r4 b'8 e''8 | % untitled/b5 untitled/b5.t2 11.87-12; 61
  b'4 g'4 r4 a'8 cs''8 | % untitled/b5 untitled/b5.t2 12.87-13; 62
  b'4 e'2 b'8 a'8 | % untitled/b5 untitled/b5.t2 13.87-14; 63
  b'2 r4 b'8 a'8 | % untitled/b5 untitled/b5.t2 14.87-15; 64
  b'4. cs''8 d''8 cs''8 d''8 e''8 | % untitled/b5 untitled/b5.t2 15.87-16; 65
  fs''4 b'8 cs''8 d''8 e''8 fs''8 a''8 | % untitled/b6 untitled/b6.t1 .87-1; 66
  gs''4 e''4 b'4 b'8 e''8 | % untitled/b6 untitled/b6.t1 1.87-2; 67
  d''2.:32 fs''8 e''8 | % untitled/b6 untitled/b6.t1 2.87-3; 68
  d''2 e''8 fs''8 d''8 e''8 | % untitled/b6 untitled/b6.t1 3.87-4; 69
  fs''4 b'8 cs''8 d''8 cs''8 d''8 a''8 | % untitled/b6 untitled/b6.t1 4.87-5; 70
  gs''4 e''4 b'4 b''8 a''8 | % untitled/b6 untitled/b6.t1 5.87-6; 71
  b''4.:32 b''8 a''8 b''8 d''8 e''8 | % untitled/b6 untitled/b6.t1 6.87-7; 72
  fs''2 r4 d'8 cs'8 | % untitled/b6 untitled/b6.t1 7.87-8; 73
  fs'4 b8 cs'8 d'8 e'8 fs'8 a'8 | % untitled/b6 untitled/b6.t1 8.87-9; 74
  gs'4 e'8 fs'8 gs'8 a'8 b'8 cs''8 | % untitled/b6 untitled/b6.t1 9.87-10; 75
  d''4 g'8 d''8 cs''8 b'8 d'8 e'8 | % untitled/b6 untitled/b6.t1 10.87-11; 76
  fs'4 cs'8 d'8 e'8 fs'8 d'8 cs'8 | % untitled/b6 untitled/b6.t1 11.87-12; 77
  b'2 b'8 a'8 b'8 cs''8 | % untitled/b6 untitled/b6.t1 12.87-13; 78
  a'4 d'4 b8 d'8 e'8 fs'8 | % untitled/b6 untitled/b6.t1 13.87-14; 79
  g'4 a'4 fs'4 e'8 fs'8~ | % untitled/b6 untitled/b6.t1 14.87-15.12; 80
  fs'8 b4. r4 \clef bass d'8 e'8 | % untitled/b6 untitled/b6.t1 15.87-16; 81
  d'4 b4 r4 b8 cs'8 | % untitled/b6 untitled/b6.t1 16.87-17; 82
  b4 d2 b8 a8 | % untitled/b6 untitled/b6.t1 17.87-18; 83
  b2 r4 b8 a8 | % untitled/b6 untitled/b6.t1 18.87-19; 84
  b4. r8 r4 d'8 e'8 | % untitled/b6 untitled/b6.t1 19.87-20; 85
  d'4 b4 r4 cs'8 d'8 | % untitled/b6 untitled/b6.t1 20.87-21; 86
  cs'4 d2 \clef treble fs''8 e''8 | % untitled/b6 untitled/b6.t1 21.87-22; 87
  fs''4 r4 r4 a''8 g''8 | % untitled/b6 untitled/b6.t1 22.87-23; 88
  fs''1 | % untitled/b6 untitled/b6.t1 23-24; 89
  fs'4 b'8 \acciaccatura { a''8[ fs''8] } e''8 d''8 cs''8 d''8 a''8 | % untitled/b5 untitled/b5.t2 .87-1; 90
  gs''4 e''4 b'4 b'8 e''8 | % untitled/b5 untitled/b5.t2 1.87-2; 91
  d''2 r4 fs''8 e''8 | % untitled/b5 untitled/b5.t2 2.87-3; 92
  d''2 r8 \acciaccatura { a''8[ fs''8] } e''8 d''8 e''8 | % untitled/b5 untitled/b5.t2 3.87-4; 93
  fs''4 b'8 e''8 d''8 cs''8 d''8 a''8 | % untitled/b5 untitled/b5.t2 4.87-5; 94
  gs''4 e''4 b'4 \acciaccatura { d'''8 } b''8 a''8 | % untitled/b5 untitled/b5.t2 5.87-6; 95
  b''2 r4 a''8 cs'''8 | % untitled/b5 untitled/b5.t2 6.87-7; 96
  b''2 r2 | % untitled/b5 untitled/b5.t2 7-7.5; 97
  \acciaccatura { b'8[ d''8 e''8 fs''8 a''8] } b''2 b''8 a''8 b''8 cs'''8 | % untitled/b5 untitled/b5.t2 8.87-9; 98
  a''4 d''4 b'8 d''8 e''8 fs''8 | % untitled/b5 untitled/b5.t2 9.87-10; 99
  g''4 a''4 fs''4 e''8 fs''8 | % untitled/b5 untitled/b5.t2 10.87-11; 100
  r8 b'4. r4 b'8 e''8 | % untitled/b5 untitled/b5.t2 11.87-12; 101
  b'4 g'4 r4 a'8 cs''8 | % untitled/b5 untitled/b5.t2 12.87-13; 102
  b'4 e'2 b'8 a'8 | % untitled/b5 untitled/b5.t2 13.87-14; 103
  b'2 r4 b'8 a'8 | % untitled/b5 untitled/b5.t2 14.87-15; 104
  b'4. cs''8 d''8 cs''8 d''8 e''8 \bar "|." 
} }

>>
}

