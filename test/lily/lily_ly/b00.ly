\version "2.14.2"
\language "english"
\pointAndClickOff
\header {
  title = "untitled/b00"
}
notes = {
  c''8 g'4 e''16 r16 r8. g'16~ g'16 r16 e'16 d'16~ d'1~ d'1~ d'1
}
\score { << \new Staff { \clef "treble" { \key c \major \time 4/4 \notes } } >> }
