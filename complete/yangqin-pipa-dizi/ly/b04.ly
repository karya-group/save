\version "2.14.2"
\language "english"
\pointAndClickOff
\header {
  title = "pipa-kendang/b04" tagline = ""
}
\score { <<
\new StaffGroup <<
\new Staff { \clef "treble" { \key c \major \time 4/4
\set Staff.instrumentName = "dizi"
\set Staff.shortInstrumentName = "dizi"
{
  e'2\f r4 d'8 e'8 g'2 a'4 b'8 a'8~ a'4 r8 b'8~ b'8 g'4 b'8 a'8 g'8 e'4 r4 b'8
  d''8 e''2 r8 e''8 d''8 g'8 a'2 r8 d''8 b'8 e'8 g'2\mf r2 a'2 r2 e'2.\p r4
  g'2. r4 a'2. r4 d'2. a'8 g'8 e'2 r8 d'8 e'8 g'8 a'4 d''4 b'4 a'4 g'8\mf a'8
  b'8 r8 a'8 b'8 d''4 b'2 a'2 e''8\f b'8 r4 r8 a'8 r8 b'8 d''8 a'8 r4 r2 d''8
  a'8 r8 e''8~ e''8 d''4 b'8 a'8 b'8 r8 b'8 a'8 r8 d'8 e'8 g'8\mf a'8 b'8 r8
  r4 b'8 a'8 e'8 g'8 a'8 r8 r8 b'8 r8 a'8 b'8 e''8 r8 b'8 d''4 e''4 d''4 b'8
  a'8 r8 b'4. d''2\p r2 e''2 d''2 a'2 r4 r8 g'16 a'16 b'2 d''2 a'4 d''8 r8 b'4
  e''8 r8 d''1 r1 r1 e''8\f b'8 r4 r8 a'8 r8 b'8 d''8 a'8 r4 r2 d''8 a'8 r4
  e''8 b'8 d''8 a'8 r4 g'8 d''8 a'8 r8 d'8 e'8 g'1\mf r2 b'2 d''2. r8 d''16
  b'16 a'2 r8 a'4 b'8 d''2\p r2 e''2 d''2 a'2 r4 r8 g'16 a'16 b'2 d''2 a'1 r2
  r4 g'4 a'2 b'2 g'2 e'2 e'2\f r4 d'8 e'8 g'2 a'4 b'8 a'8~ a'4 r8 b'8~ b'8 g'4
  b'8 a'8 g'8 e'4 r4 b'8 d''8 e''2 r8 e''8 d''8 g'8 a'2 r8 d''8 b'8 e'8 g'2\mf
  r2 a'2 r2 e'2.\p r4 g'2. r4 a'2. r4 d'2. a'8 g'8 e'2 r8 d'8 e'8 g'8 a'4 d''4
  b'4 a'4 g'8\mf a'8 b'8 r8 a'8 b'8 d''4 b'2 a'2 g'2~\p g'8 fs'4.~ fs'2 a'4.
  r8 g'16 a'16 b'4.~ b'2 r1 r1 r1 r1 r1 g'2\mf d''4 r4 a'2 a'4 r4 e'2 a'4 r4
  a'4 r4 b'2 g'2 d''4 r4 a'2 a'4 r4 e'2 a'4 r4 a'4 r4 b'2 g'2 d''4 r4 a'2 a'4
  r4 e'2 a'4 r4 a'4 r4 b'2 g'2\p d''4 r4 a'2 a'4 r4 e'2 a'4 r4 a'4 r4 b'2
  g'2\mf d''4 r4 a'2 a'4 r4 e'2 a'4 r4 a'4 r4 b'2 g'2 d''4 r4 a'2 a'4 r4 e'2
  a'4 r4 a'4 r4 b'2 g'2\p d''4 r4 a'2 a'4 r4 e'8 e''16\f d''16 b'16 r16 d''8
  a'8 b'16 d''16 e''8 r16 d''16 cs''8 d''8 e''8 r16 b'16 g'16 a'16 b'16 r16
  a'16 b'16 a'16 g'16 e'2~ e'4~ e'8.
} } }
\new Staff { \clef "treble" { \key c \major \time 4/4
\set Staff.instrumentName = "pipa"
\set Staff.shortInstrumentName = "pipa"
{
  r8 g'8\f a'8 e'8 g'8 d'8 g'8 e'8 r8 a'8 b'8 g'8 a'8 e'8 a'8 g'8 r8 d'8 b8
  e'8 d'8 g'8 d'8 e'8 r8 g'8 a'8 e'8 g'8 d'8 g'8 e'8 r8 a'8 e'8 b'8 a'8 d''8
  a'8 b'8 r8 g'8 d'8 a'8 g'8 a'8 g'8 e'8 r8 a'8 b'8 g'8 a'8 e'8 a'8 g'8 r8 e'8
  g'8 d'8 e'8 b8 e'8 d'8 e'2 r4 d'8 e'8 g'2 a'4 a'8 b'8~ b'8 a'4 b'8~ b'8 g'4
  b'8 a'8 g'8 e'4 r4 b'8 d''8 e''2 r8 e''8 d''8 b'8 a'2 r8 d''8 b'8 e'8 g'8
  a'8 b'8 r8 a'8 b'8 d''4 fs'2 cs'2 e'8 g'8 a'8 e'8 g'8 d'8 g'8 e'8 g'8 a'8
  b'8 g'8 a'8 e'8 a'8 g'8 d'8 r8 d'8 e'8 g'8 d'8 e'8 r8 e'8 d'8 r8 e'8 d'8 b8
  d'8 e'8 g'4 r4 r2 e'8 g'8 d'8 e'8 g'8 e'8 d'8 r8 a'8 a'8 a'8 b'8 r8 a'8 b'8
  r8 b'8 a'4 b'8 a'4 a'8 b'8 g''8 d''4. r4 r8 g''8 d''8 e''8 b'8 r8 r8 e''8
  b'8 d''8 a'2~ a'4. r8 r8 e''8 b'8 r8 r8 d''8 a'8 r8 a'8. e'16 d'4 b'8. g'16
  e'4 r1 r1 r2 r8 a8 b8 d'8 e'8 g'8 a'8 e'8 g'8 d'8 g'8 e'8 g'8 a'8 b'8 g'8
  a'8 e'8 a'8 g'8 d'8 r8 d'8 e'8 g'8 d'8 e'8 r8 e'8 d'8 r8 e'8 d'8 b8 d'8 e'8
  g'8 r8 d''8 g'8 r8 d''8 g'8 r8 r8 e''8 a'8 r8 b'8 r8 e''8 a'8 r4 e''8 a'8 r8
  e''8 a'8 b'8 e''1 g''8 d''4. r4 r8 g''8 d''8 e''8 b'8 r8 r8 e''8 b'8 d''8
  a'2~ a'4. r8 r8 e''8 b'8 r8 r8 d''8 a'8 r8 a'8 r8 r4 r2 r1 fs'2 e'2 d'2 c'2
  r8 g'8 a'8 e'8 g'8 d'8 g'8 e'8 r8 a'8 b'8 g'8 a'8 e'8 a'8 g'8 r8 d'8 b8 e'8
  d'8 g'8 d'8 e'8 r8 g'8 a'8 e'8 g'8 d'8 g'8 e'8 r8 a'8 e'8 b'8 a'8 d''8 a'8
  b'8 r8 g'8 d'8 a'8 g'8 a'8 g'8 e'8 r8 a'8 b'8 g'8 a'8 e'8 a'8 g'8 r8 e'8 g'8
  d'8 e'8 b8 e'8 d'8 e'2 r4 d'8 e'8 g'2 a'4 a'8 b'8~ b'8 a'4 b'8~ b'8 g'4 b'8
  a'8 g'8 e'4 r4 b'8 d''8 e''2 r8 e''8 d''8 b'8 a'2 r8 d''8 b'8 e'8 g'8 a'8
  b'8 r8 a'8 b'8 d''4 fs'2 cs'2 b2~ b8 d'4.~ d'2 c'2~ c'8 g4.~ g2 a2~ a8 b4.~
  b1 r1 r1 r8 b'8 b'8 b'8 b'8 b'8 r4 g'4 a'4 d''4 b'4 a'4 e'4 a'4 g'4 e'4 b'4
  a'4 g'4 a'4 d''4 b'4 a'4 g'4 a'4 d''4 b'4 a'4 e'4 a'4 g'4 e'4 b'4 a'4 g'4
  a'4 d''4 b'4 a'4 g'4 a'4 d''4 b'4 a'4 e'4 a'4 g'4 e'4 b'4 a'4 g'4 a'4 d''4
  b'4 a'4 g'4 a'4 d''4 b'4 a'4 e'4 a'4 g'4 e'4 b'4 a'4 g'4 a'4 d''4 b'4 a'4
  g'4 a'4 d''4 b'4 a'4 e'4 a'4 g'4 e'4 b'4 a'4 g'4 a'4 d''4 b'4 a'4 g'4 a'4
  d''4 b'4 a'4 e'4 a'4 g'4 e'4 b'4 a'4 g'4 a'4 d''4 b'4 a'4 g'4 a'4 d''4 b'4
  a'4 e'4 a'4 g'4 e'4 b'4 a'4 g'4 a'4 d''4 b'4 a'4 e'2~ e'4~ e'8.
} } }
\new Staff { \clef "treble" { \key c \major \time 4/4
\set Staff.instrumentName = "yangqin"
\set Staff.shortInstrumentName = "yangqin"
{
  <e' b'>1\f <g' a'>2~ <g' a'>4. b'8 <a' d''>4 b'8 d''8 r8 d''8 b'4 <d' a'>1
  <e' b'>1 <a' d''>2 <a' e''>2 <g' b'>8\mf r8 d''8 e''8~ e''8 e''8 d''8 b'8 r8
  b'8\f d''4 <a'' a'>8 <b'' b'>8 <a'' a'>8 <g'' g'>8 <e' e''>8\p g'8 a'8 e'8
  g'8 d'8 g'8 e'8 r8 a'8 b'8 g'8 a'8 e'8 a'8 g'8 r8 d'8 b8 e'8 d'8 g'8 d'8 e'8
  r8 g'8 a'8 e'8 g'8 d'8 g'8 e'8 r8 a'8 e'8 b'8 a'8 d''8 a'8 b'8 r8 g'8 d'8
  a'8 g'8 a'8 g'8 e'8 r8 a'8\mf b'8 g'8 a'8 e'8 a'8 g'8 r8 e'8 g'8 d'8 e'8 b8
  e'8 d'8 <e' e''>8\f <g' b'>8 a'8 b'8 g'8 a'8 g'8 b'8 <g' d''>8 a'8 b'8 d''8
  a'8 b'8 a'8 d''8 a'8 g'8 a'8 r8 g'8 a'8 r8 g'8 r8 a'8 g'8 r8 a'8 g'8 a'8 r8
  g'4\mf b'8 g'8 a'8 g'8 b'8 a'8 e'8 g'8 a'8 b'8 d''8 b'8 a'8 g'8 e''8 d''8
  e''8 r8 d''8 e''8 r8 d''8 r8 e''8 d''8 g''8 e''8 d''8 e''4 d''8\p <a' e''>8
  b'8 d''8 <a' e''>8 b'8 d''8 b'8 <a' e''>8 d''8 b'8 <a' e''>8 d''8 <a' e''>8
  d''8 b'8 a'8 b'8 <g' d''>8 b'8 a'8 <g' d''>8 b'8 <g' d''>8 a'8 b'8 <g' d''>8
  b'8 a'8 <g' d''>8 b'8 a'8 <g' d''>8 r8 r4 r2 r1 g'4\f g'4 e'8 g'4 e'8~ e'8
  g'8 e'4 g'8 e'8 g'4 <e' e''>8 <g' b'>8 a'8 b'8 g'8 a'8 g'8 b'8 <g' d''>8 a'8
  b'8 d''8 a'8 b'8 a'8 d''8 a'8 g'8 a'8 r8 g'8 a'8 r8 g'8 r8 a'8 g'8 r8 a'8
  g'8 a'8 r8 <g' d''>8\mf a'8 b'8 <g' d''>8 a'8 b'8 <g' d''>8 b'8 <g' d''>8
  <a' e''>8 b'8 d''8 b'8 d''8 <a' e''>8 b'8 r8 b'8 <a' e''>8 d''8 b'8
  <a' e''>8 d''8 b'8 <a' e''>8~ a'4.~ a'2 d''8\p <a' e''>8 b'8 d''8 <a' e''>8
  b'8 d''8 b'8 <a' e''>8 d''8 b'8 <a' e''>8 d''8 <a' e''>8 d''8 b'8 a'8 b'8
  <g' d''>8 b'8 a'8 <g' d''>8 b'8 <g' d''>8 a'8 b'8 <g' d''>8 b'8 a'8
  <g' d''>8 b'8 a'8 <g' d''>8 a'8 b'8 <g' d''>8 a'8 b'8 <g' d''>8 r8 a'8
  <g' d''>8 r4 <g' d''>8 a'8 r4 b'8 r8 r4 b'2~ b'2 a'2 <e' b'>1\f <g' a'>2~
  <g' a'>4. b'8 <a' d''>4 b'8 d''8 r8 d''8 b'4 <d' a'>1 <e' b'>1 <a' d''>2
  <a' e''>2 <g' b'>8\mf r8 d''8 e''8~ e''8 e''8 d''8 b'8 r8 b'8\f d''4
  <a'' a'>8 <b'' b'>8 <a'' a'>8 <g'' g'>8 <e' e''>8\p g'8 a'8 e'8 g'8 d'8 g'8
  e'8 r8 a'8 b'8 g'8 a'8 e'8 a'8 g'8 r8 d'8 b8 e'8 d'8 g'8 d'8 e'8 r8 g'8 a'8
  e'8 g'8 d'8 g'8 e'8 r8 a'8 e'8 b'8 a'8 d''8 a'8 b'8 r8 g'8 d'8 a'8 g'8 a'8
  g'8 e'8 r8 a'8\mf b'8 g'8 a'8 e'8 a'8 g'8 r8 e'8 g'8 d'8 e'8 b8 e'8 d'8
  e'4\p b8 e'8~ e'8 b8 e'4 d'8 e'8 g'4 e'8 g'8 a'4 g'8 a'8 b'4 g'8 e'8 b'8 g'8
  d'8 b'8 g'8 d'8 e'8 g'8 a'8 b'8 d''8 e''8 g'8 a'8 b'8 d''8 a'8 b'8 d''8 e''8
  g'8 a'8 b'8 d''8 a'8 b'8 d''8 e''8 r4 r2 g'16\f a'16 b'8 r8 b'8 r8 b'8 r4 r1
  r1 r1 r1 g'8 g'16 a'16 b'16 g'16 a'16 b'16 d''16 a'16 b'16 d''16 b'16
  <a' e''>16 d''16 b'16 <a' e''>8 e'16 g'16 a'16 b'16 a'16 g'16 a'8 e'16 g'16
  a'16 <e' b'>16 a'16 g'16 <e' b'>8 e''16 d''16 b'16 d''16 a'16 b'16 a'16
  d''16 b'16 a'16 g'16 a'16 e'16 g'16 a'16 e''16 d''16 b'16 d''16 a'16 b'16
  d''16 b'16 g'16 a'16 b'16 a'16 g'16 b'16 a'16 g'8 g'16 a'16 b'16 g'16 a'16
  b'16 d''16 a'16 b'16 d''16 b'16 <a' e''>16 d''16 b'16 <a' e''>8 e'16 g'16
  a'16 b'16 a'16 g'16 a'8 e'16 g'16 a'16 <e' b'>16 a'16 g'16 <e' b'>8 e''16
  d''16 b'16 d''16 a'16 b'16 a'16 d''16 b'16 a'16 g'16 a'16 e'16 g'16 a'16
  e''16 d''16 b'16 d''16 a'16 b'16 d''16 b'16 g'16 a'16 b'16 a'16 g'16 b'16
  a'16 g'8\mf g'16 a'16 b'16 g'16 a'16 b'16 d''16 a'16 b'16 d''16 b'16
  <a' e''>16 d''16 b'16 <a' e''>8 r16 <a' e''>16~ <a' e''>16 r16 <a' e''>8 r8
  e'16 g'16 a'16 <e' b'>16 a'16 g'16 <e' b'>8 r8 r4 r2 r16 e''16 d''16 b'16
  d''16 a'16 b'16 d''16 b'16 g'16 a'16 b'16 a'8 r8 g'8\f g'16 a'16 b'16 g'16
  a'16 b'16 d''16 a'16 b'16 d''16 b'16 <a' e''>16 d''16 b'16 <a' e''>8 e'16
  g'16 a'16 b'16 a'16 g'16 a'8 e'16 g'16 a'16 <e' b'>16 a'16 g'16 <e' b'>8
  e''16 d''16 b'16 d''16 a'16 b'16 a'16 d''16 b'16 a'16 g'16 a'16 e'16 g'16
  a'16 e''16 d''16 b'16 d''16 a'16 b'16 d''16 b'16 g'16 a'16 b'16 a'16 g'16
  b'16 a'16 g'8 g'16 a'16 b'16 g'16 a'16 b'16 d''16 a'16 b'16 d''16 b'16
  <a' e''>16 d''16 b'16 <a' e''>8 e'16 g'16 a'16 b'16 a'16 g'16 a'8 e'16 g'16
  a'16 <e' b'>16 a'16 g'16 <e' b'>8 e''16 d''16 b'16 d''16 a'16 b'16 a'16
  d''16 b'16 a'16 g'16 a'16 e'16 g'16 a'16 e''16 d''16 b'16 d''16 a'16 b'16
  d''16 b'16 g'16 a'16 b'16 a'16 g'16 b'16 a'16 g'8 r8 a'8 a'8 d''8 d''8 b'8
  b'8 a'8 a'8 e'8 e'8 a'8 a'8 g'8 g'8 <e' b'>8 e''16 d''16 b'16 d''16 a'16
  b'16 a'16 d''16 b'16 a'16 g'16 a'16 e'16 g'16 a'16 e''16 d''16 b'16 g'16
  a'16 b'16 d''16 b'16 g'16 a'16 b'16 <a' a''>16 <b' b''>16 <a' a''>16
  <g' g''>16 e'2~ e'4~ e'8.
} } }
>>
>> }
