\version "2.14.2"
\language "english"
\pointAndClickOff
\header { title = "b1" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\score {
<<
\new Staff  {
\set Staff.instrumentName = "揚琴"
\set Staff.shortInstrumentName = "揚琴"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 4/4 \key b \minor r8 b'8 \p b'8 b'8 b'16 b'16 b'8 b'4 | % untitled/b1 untitled/b10.t5 .75-1; 1
 r8 cs''8 cs''16 d''16 d''8 d''4 r4 | % untitled/b1 untitled/b10.t5 1.5-1.75; 2
 r8 r16 b'16~ b'8 b'8 b'16 b'16 b'8 b'4 | % untitled/b1 untitled/b10.t5 2.75-3; 3
 r8 cs''8 cs''16 d''16 d''8 d''4 r4 | % untitled/b1 untitled/b10.t5 3.5-3.75; 4
 r16 fs''16 fs''8 fs''8 fs''8 fs''8 fs''8 fs''16 e''16 e''8 | % untitled/b1 untitled/b10.t5 4.87-5; 5
 e''8 e''8 e''16 d''16 d''8 d''4 r4 | % untitled/b1 untitled/b10.t5 5.5-5.75; 6
 r16 fs''16 fs''8 fs''8 fs''8 fs''8 fs''8 fs''16 e''16 e''8 | % untitled/b1 untitled/b10.t5 6.87-7; 7
 e''8 e''8 e''16 d''16 d''8 << d''2 { s8 s8 \< s8 \> s8 \! } >> | % untitled/b1 untitled/b10.t5 7.5-8; 8
 b'8 b'8 b'8 b'8 b'16 b'16 b'8 b'8 b'8 | % untitled/b1 untitled/b10.t5 8.87-9; 9
 b'16 cs''16 cs''8 cs''16 d''16 d''8 d''4 r4 | % untitled/b1 untitled/b10.t5 9.5-9.75; 10
 r8 r16 b'16~ b'8 b'8 b'16 b'16 b'8 b'8 b'8 | % untitled/b1 untitled/b10.t5 10.87-11; 11
 b'16 cs''16 cs''8 cs''16 d''16 d''8 d''4 r4 | % untitled/b1 untitled/b10.t5 11.5-11.75; 12
 r16 fs''16 fs''8 fs''8 fs''8 fs''8 fs''8 fs''16 e''16 e''8 | % untitled/b1 untitled/b10.t5 12.87-13; 13
 e''8 e''8 e''16 d''16 d''8 d''2:32 \espressivo | % untitled/b1 untitled/b10.t5 13.5-14; 14
 r16 fs''16 fs''8 fs''8 fs''8 fs''8 fs''8 fs''16 e''16 e''8 | % untitled/b1 untitled/b10.t5 14.87-15; 15
 e''8 e''8 e''16 d''16 d''8 cs''16 \< d''16 g'16 fs'16 e'16 fs'16 d''16 cs''16 \! \bar "|."
} }

\new Staff  {
\set Staff.instrumentName = "箏"
\set Staff.shortInstrumentName = "箏"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 4/4 \key b \minor
 r4 << b'2.~ { s4 \< s4 \> s4 } >> | % untitled/b1 untitled/b10.t1 .25-1.25; 1
 b'4 \! cs''4 d''2~ | % untitled/b1 untitled/b10.t1 1.5-2.5; 2
 d''2 b'2~ | % untitled/b1 untitled/b10.t1 2.5-3.25; 3
 b'4 cs''4 d''2~ | % untitled/b1 untitled/b10.t1 3.5-4.25; 4
 d''4 fs''2. | % untitled/b1 untitled/b10.t1 4.25-5; 5
 e''2 d''2~ | % untitled/b1 untitled/b10.t1 5.5-6.25; 6
 d''4 fs''2. | % untitled/b1 untitled/b10.t1 6.25-7; 7
 e''2 d''2 | % untitled/b1 untitled/b10.t1 7.5-8; 8
 b'1~ | % untitled/b1 untitled/b10.t1 8-9.25; 9
 b'4 cs''4 d''2~ | % untitled/b1 untitled/b10.t1 9.5-10.5; 10
 d''2 b'2~ | % untitled/b1 untitled/b10.t1 10.5-11.25; 11
 b'4 cs''4 d''2~ | % untitled/b1 untitled/b10.t1 11.5-12.25; 12
 d''4 fs''2. | % untitled/b1 untitled/b10.t1 12.25-13; 13
 e''2 d''2~ | % untitled/b1 untitled/b10.t1 13.5-14.25; 14
 d''4 fs''2. | % untitled/b1 untitled/b10.t1 14.25-15; 15
 e''2 r2 \bar "|."
} }

>>
}

