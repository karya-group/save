\version "2.18.2"
\language "english"
\pointAndClickOff
\include "ly/lib.ily"
\header { title = "score" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\bookpart {
\score {
<<
\new StaffGroup <<
\set StaffGroup.instrumentName = "揚琴"
\set StaffGroup.shortInstrumentName = "揚琴"
\new Staff = "up" {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 4/4 \key b \dorian b'4 cs''4 d''4 e''4 | % untitled/b3 untitled/b3.t8 .75-1; 1
 fs''2 r4 r8 b''8 | % untitled/b3 untitled/b3.t8 1.88-2; 2
 a''4. :32 gs''16 fs''16 e''4 d''8 cs''8 | % untitled/b3 untitled/b3.t8 2.88-3; 3
 d''8. e''16 fs''4 r2 | % untitled/b3 untitled/b3.t8 3.25-3.5; 4
 fs''8 e''16 d''16 cs''8 b'8 fs'4 :32 b'8 cs''8 | % untitled/b3 untitled/b3.t8 4.88-5; 5
 d''2 r4 r8 fs''8 | % untitled/b3 untitled/b3.t8 5.88-6; 6
 e''2~ :32 e''4. :32 d''16 cs''16 | % untitled/b3 untitled/b3.t8 6.94-7; 7
 d''16 cs''16 b'4. r4 r8 d''16 cs''16 | % untitled/b3 untitled/b3.t8 7.94-8; 8
 d''16 cs''16 b'4. r8 fss'8 b'8 cs''8 | % untitled/b3 untitled/b3.t8 8.88-9; 9
 d''2 r4 r8 d''16 fs''16 | % untitled/b3 untitled/b3.t8 9.94-10; 10
 \acciaccatura { e''8[ fs''8 df''8] } e''8 e''4.~ :32 e''4. :32 d''16 cs''16 | % untitled/b3 untitled/b3.t8 10.94-11; 11
 d''16 cs''16 b'4.~ b'2 | \time 3/4 % untitled/b3 untitled/b3.t8 11.12-12; 12
 r8 a'8 b'8 cs''8 d''8 e''8 | \time 4/4 % untitled/b3 untitled/b3.t8 12.62-12.75; 13
 fs''2~ fs''4. b''8 | % untitled/b4 untitled/b4.t7 .88-1; 14
 a''4. :32 gs''16 fs''16 e''4 d''8 cs''8 | % untitled/b4 untitled/b4.t7 1.88-2; 15
 d''8. e''16 fs''2. | % untitled/b4 untitled/b4.t7 2.25-3; 16
 b''16 a''16 b''4.~ b''2 | % untitled/b4 untitled/b4.t7 3.12-4; 17
 b''16 a''16 b''4. fss''16 fs''16 fss''4. | % untitled/b4 untitled/b4.t7 4.62-5; 18
 e''16 d''16 e''4. d''16 cs''16 d''8 cs''4~ :32 | % untitled/b4 untitled/b4.t7 5.75-6.38; 19
 cs''4. :32 b'16 a'16 b'2~ | % untitled/b4 untitled/b4.t7 6.5-7.25; 20
 b'4 cs''4 d''4 e''4 | % untitled/b4 untitled/b4.t7 7.75-8; 21
 fs''4 b'4 e''8 fs''8 fss''8 a''8 | % untitled/b4 untitled/b4.t7 8.88-9; 22
 b''4 e''4 fs''4 fss''4 | % untitled/b4 untitled/b4.t7 9.75-10; 23
 gss''4 :32 gss''8 d''8 cs''8 d''8 fs'16 cs''16 b'16 cs''16 | % untitled/b4 untitled/b4.t7 10.94-11; 24
 e'16 b'16 a'16 b'16 a''16 d''16 cs''16 d''16 e'16 cs''16 b'16 cs''16 d'16 b'16 a'16 b'16 | % untitled/b4 untitled/b4.t7 11.94-12; 25
 gss''8 d''8 cs''8 e''8 r8 fs''8 e''8 d'8 | % untitled/b4 untitled/b4.t7 12.88-13; 26
 r8 a''16 d''16 fs''16 cs''16 d''8 d'8 :32 a'8 :32 cs''8 e''16 fs''16 | % untitled/b4 untitled/b4.t7 13.94-14; 27
 e''8 e''8 fs''8 e''8 d''16 cs''16 b'8 a''16 d''16 fs''16 cs''16 | % untitled/b4 untitled/b4.t7 14.94-15; 28
 d''8 a''16 d''16 fs''16 cs''16 d''8 r2 | % untitled/b4 untitled/b4.t7 15.38-15.5; 29
 r4 cs''4 d''4 e''4 | % untitled/b4 untitled/b4.t7 16.75-17; 30
 fs''2 r4 r8 b''8 | % untitled/b2 untitled/b2.t8 .88-1; 31
 a''4. gs''16 fs''16 e''4 d''8 cs''8 | % untitled/b2 untitled/b2.t8 1.88-2; 32
 d''8. e''16 fs''4 r2 | % untitled/b2 untitled/b2.t8 2.25-2.5; 33
 r2 r4 b'8 cs''8 | % untitled/b2 untitled/b2.t8 3.88-4; 34
 d''2 r4 r8 fs''8 | % untitled/b2 untitled/b2.t8 4.88-5; 35
 \acciaccatura { e''8[ fs''8] } e''2 r4 r16 fs''16 d''16 cs''16 | % untitled/b2 untitled/b2.t8 5.94-6; 36
 b'2 r4 e''8 d''16 cs''16 | % untitled/b2 untitled/b2.t8 6.94-7; 37
 b'1 ^"慢" | % untitled/b2 untitled/b2.t8 7-8; 38
 r8 a'8 b'8 cs''8 d''8 cs''8 d''8 <cs'' e''>8 | % untitled/b2 untitled/b2.t8 8.88-9; 39
 <b' fs''>2 :32 fs''4. b''8 | % untitled/b2 untitled/b2.t8 9.88-10; 40
 <e'' a''>4. :32 gs''16 fs''16 e''4 <d' d''>8 <cs' cs''>8 | % untitled/b2 untitled/b2.t8 10.88-11; 41
 <d' d''>8. <e' e''>16 <fs' fs''>4. :32 b8 a4 | % untitled/b2 untitled/b2.t11 11.75-12; 42
 a'8 gs'16 fs'16 e'8 d'8 cs'8 b8 <fss b'>8 <a cs''>8 | % untitled/b2 untitled/b2.t8 12.88-13; 43
 <b d''>2 :32 d''4. fs''8 | % untitled/b2 untitled/b2.t8 13.88-14; 44
 \acciaccatura { e''8[ fs''8] } e''8 <a' e''>4. :32 e''4~ e''16 fs''16 d''16 cs''16 | % untitled/b2 untitled/b2.t8 14.94-15; 45
 <e'~ d''>16 :32 <e'~ cs''>16 :32 <e' b'>4. :32 b'4 e''8 d''16 cs''16 | % untitled/b2 untitled/b2.t8 15.94-16; 46
 <d'~ d''>16 :32 <d'~ cs''>16 :32 <d' b'>4. :32 d''8 cs''8 b'8 a'8 | % untitled/b2 untitled/b2.t8 16.88-17; 47
 <e' b'>2 :32 b'2 | % untitled/b2 untitled/b2.t8 17.5-18; 48
 r8 \acciaccatura { a'8[ a'8] } a'8 b'8 cs''8 d''8 cs''8 d''8 e''8 | % untitled/b2 untitled/b2.t8 18.88-19; 49
 <b' fs''>2 r4 <b' fs''>4 | % untitled/b5 untitled/b5.t9 .75-1; 50
 <b' e''>1 | % untitled/b5 untitled/b5.t9 1-2; 51
 <fss' d''>2 <e' cs''>2 | % untitled/b5 untitled/b5.t9 2.5-3; 52
 <d' b'>2 <cs' e'>4 d'8 e'8 | % untitled/b5 untitled/b5.t9 3.88-4; 53
 <b fs'>2. <b' fs''>4 | % untitled/b5 untitled/b5.t9 4.75-5; 54
 <b' e''>1 | % untitled/b5 untitled/b5.t9 5-6; 55
 <d'' fss''>2 <e'' b''>2 | % untitled/b5 untitled/b5.t9 6.5-7; 56
 <b' cs''>2 <b' d''>2 | % untitled/b5 untitled/b5.t9 7.5-8; 57
 b''16 fss''16 d''4. r2 | % untitled/b5 untitled/b5.t9 8.12-8.5; 58
 b''16 fs''16 d''16 cs''16 d''4 r2 | % untitled/b5 untitled/b5.t9 9.25-9.5; 59
 <b' e'' fss''>1 | % untitled/b5 untitled/b5.t9 10-11; 60
 <b' d'' fs''>1 | % untitled/b5 untitled/b5.t9 11-12; 61
 <fss' b' d''>1 | % untitled/b5 untitled/b5.t9 12-13; 62
 <e' fs' b'>1 | % untitled/b5 untitled/b5.t9 13-14; 63
 <d' b' fs''>2 <cs' b' e''>2 | % untitled/b5 untitled/b5.t9 14.5-15; 64
 <b b' cs''>2 <fss b' d''>4 b'8 cs''8 | % untitled/b5 untitled/b5.t9 15.88-16; 65
 \change Staff = "down" b,8 b8 <d' b'>8 r8 b,8 b8 <cs' a'>8 <d' b'>8 | % untitled/b6 untitled/b6.t6 .88-1; 66
 e8 b8 <gs' b'>8 r8 <a' d''>8 r8 <b' cs''>8 r8 | % untitled/b6 untitled/b6.t6 1.75-1.88; 67
 d8 a8 fs'8 a8 fss8 css'8 b'8 css'8 | % untitled/b6 untitled/b6.t6 2.88-3; 68
 bs,8 fss8 css'8 fss8 <e' e''>8 <fs' fs''>8 <d' d''>8 <cs' cs''>8 | % untitled/b6 untitled/b6.t6 3.88-4; 69
 b,8 b8 <d' b'>8 r8 b,8 b8 <cs' a'>8 <d' b'>8 | % untitled/b6 untitled/b6.t6 4.88-5; 70
 e8 b8 <gs' b'>8 r8 <a' d''>8 r8 <b' cs''>8 r8 | % untitled/b6 untitled/b6.t6 5.75-5.88; 71
 fss,8 d8 a8 b8 e8 b8 fs'8 fss'8 | % untitled/b6 untitled/b6.t6 6.88-7; 72
 fs'4 cs''8 d''8 e''8 fs''16 e''16 d''8 cs''8 | % untitled/b6 untitled/b6.t6 7.88-8; 73
 b'4 fs'4 b8 cs'8 d'8 e'8 | % untitled/b6 untitled/b6.t6 8.88-9; 74
 fs'8 a'8 gs'4 e'8 fs'8 gs'8 a'8 | % untitled/b6 untitled/b6.t6 9.88-10; 75
 b'8 cs''8 d''4 fss'8 d''8 cs''8 b'8 | % untitled/b6 untitled/b6.t6 10.88-11; 76
 d'8 e'8 fs'4 <e' e''>8 <fs' fs''>16 <e' e''>16 <d' d''>8 <cs' cs''>8 | % untitled/b6 untitled/b6.t6 11.88-12; 77
 <b b'>8 fs8 <d' b'>8 r8 b8 fs8 <d' b'>8 <e' cs''>8 | % untitled/b6 untitled/b6.t6 12.88-13; 78
 a8 fs8 <fs' b'>8 r8 b8 d'8 e'8 fs'8 | % untitled/b6 untitled/b6.t6 13.88-14; 79
 <e cs'>2 <a d'>4 r8 e'8 | % untitled/b6 untitled/b6.t6 14.88-15; 80
 <d fs'>4 r8 b8 r4 d''8 e''8 | % untitled/b6 untitled/b6.t6 15.88-16; 81
 d''8 e8 b8 fs'8 fss'8 d''8 b'8 cs''8 | % untitled/b6 untitled/b6.t6 16.88-17; 82
 b'8 d8 b8 e'8 fs'8 cs''8 b'8 a'8 | % untitled/b6 untitled/b6.t6 17.88-18; 83
 b'8 bs,8 e8 bs8 css'8 a'8 b'8 a'8 | % untitled/b6 untitled/b6.t6 18.88-19; 84
 b'8 fs'8 b,8 fs8 fs'8 b'8 d''8 e''8 | % untitled/b6 untitled/b6.t6 19.88-20; 85
 d''8 e8 b8 fs'8 fss'8 d''8 cs''8 d''8 | % untitled/b6 untitled/b6.t6 20.88-21; 86
 cs''8 d8 b8 e'8 fs'8 cs''8 fs''8 ^"慢" e''8 | % untitled/b6 untitled/b6.t6 21.88-22; 87
 fs''8 fss'8 b'8 cs''8 e''8 fs''8 a''8 fss''8 | % untitled/b6 untitled/b6.t6 22.88-23; 88
 fs''8 b'8 fs'8 cs'8 b2 | % untitled/b6 untitled/b6.t6 23.5-24; 89
 <b' fs''>2 r4 <b' fs''>4 | % untitled/b5 untitled/b5.t9 .75-1; 90
 <b' e''>1 | % untitled/b5 untitled/b5.t9 1-2; 91
 <fss' d''>2 <e' cs''>2 | % untitled/b5 untitled/b5.t9 2.5-3; 92
 <d' b'>2 <cs' e'>4 d'8 e'8 | % untitled/b5 untitled/b5.t9 3.88-4; 93
 <b fs'>2. <b' fs''>4 | % untitled/b5 untitled/b5.t9 4.75-5; 94
 <b' e''>1 | % untitled/b5 untitled/b5.t9 5-6; 95
 <d'' fss''>2 <e'' b''>2 | % untitled/b5 untitled/b5.t9 6.5-7; 96
 <b' cs''>2 <b' d''>2 | % untitled/b5 untitled/b5.t9 7.5-8; 97
 b''16 fss''16 d''4. r2 | % untitled/b5 untitled/b5.t9 8.12-8.5; 98
 b''16 fs''16 d''16 cs''16 d''4 r2 | % untitled/b5 untitled/b5.t9 9.25-9.5; 99
 <b' e'' fss''>1 | % untitled/b5 untitled/b5.t9 10-11; 100
 <b' d'' fs''>1 | % untitled/b5 untitled/b5.t9 11-12; 101
 <fss' b' d''>1 | % untitled/b5 untitled/b5.t9 12-13; 102
 <e' fs' b'>1 | % untitled/b5 untitled/b5.t9 13-14; 103
 <d' b' fs''>2 <cs' b' e''>2 | % untitled/b5 untitled/b5.t9 14.5-15; 104
 <b b' cs''>2 <fss b' d''>4 b'8 cs''8 | % untitled/b5 untitled/b5.t9 15.88-16; 105
 \change Staff = "down" b,8 b8 <d' b'>8 r8 b,8 b8 <cs' a'>8 <d' b'>8 | % untitled/b7 untitled/b7.t19 .88-1; 106
 e8 b8 <gs' b'>8 r8 <a' d''>8 r8 <b' cs''>8 r8 | % untitled/b7 untitled/b7.t19 1.75-1.88; 107
 d8 a8 fs'8 a8 fss8 css'8 b'8 css'8 | % untitled/b7 untitled/b7.t19 2.88-3; 108
 bs,8 fss8 css'8 fss8 <e' e''>8 <fs' fs''>8 <d' d''>8 <cs' cs''>8 | % untitled/b7 untitled/b7.t19 3.88-4; 109
 b,8 b8 <d' b'>8 r8 b,8 b8 <cs' a'>8 <d' b'>8 | % untitled/b7 untitled/b7.t19 4.88-5; 110
 e8 b8 <gs' b'>8 r8 <a' d''>8 r8 <b' cs''>8 r8 | % untitled/b7 untitled/b7.t19 5.75-5.88; 111
 fss,8 d8 a8 b8 e8 b8 fs'8 fss'8 | % untitled/b7 untitled/b7.t19 6.88-7; 112
 fs'4 cs''8 d''8 e''8 fs''16 e''16 d''8 cs''8 | % untitled/b7 untitled/b7.t19 7.88-8; 113
 b'4 fs'4 b8 cs'8 d'8 e'8 | % untitled/b7 untitled/b7.t19 8.88-9; 114
 fs'8 a'8 gs'4 e'8 fs'8 gs'8 a'8 | % untitled/b7 untitled/b7.t19 9.88-10; 115
 b'8 cs''8 d''4 fss'8 d''8 cs''8 b'8 | % untitled/b7 untitled/b7.t19 10.88-11; 116
 d'8 e'8 fs'4 <e' e''>8 <fs' fs''>16 <e' e''>16 <d' d''>8 <cs' cs''>8 | % untitled/b7 untitled/b7.t19 11.88-12; 117
 <b b'>8 fs8 <d' b'>8 r8 b8 fs8 <d' b'>8 <e' cs''>8 | % untitled/b7 untitled/b7.t19 12.88-13; 118
 a8 fs8 <fs' b'>8 r8 b8 d'8 e'8 fs'8 | % untitled/b7 untitled/b7.t19 13.88-14; 119
 <e cs'>2 <a d'>4 r8 e'8 | % untitled/b7 untitled/b7.t19 14.88-15; 120
 <d fs'>4 r8 b8 r4 d''8 e''8 | % untitled/b7 untitled/b7.t19 15.88-16; 121
 d''8 e8 b8 fs'8 fss'8 d''8 b'8 cs''8 | % untitled/b7 untitled/b7.t19 16.88-17; 122
 b'8 d8 b8 e'8 fs'8 cs''8 b'8 a'8 | % untitled/b7 untitled/b7.t19 17.88-18; 123
 b'8 bs,8 e8 bs8 css'8 a'8 b'8 a'8 | % untitled/b7 untitled/b7.t19 18.88-19; 124
 b'8 fs'8 b,8 fs8 fs'8 b'8 d''8 e''8 | % untitled/b7 untitled/b7.t19 19.88-20; 125
 d''8 e8 b8 fs'8 fss'8 d''8 cs''8 d''8 | % untitled/b7 untitled/b7.t19 20.88-21; 126
 cs''8 d8 b8 e'8 fs'8 cs''8 fs''8 ^"慢" e''8 | % untitled/b7 untitled/b7.t19 21.88-22; 127
 fs''8 fss'8 b'8 cs''8 e''8 fs''8 a''8 fss''8 | % untitled/b7 untitled/b7.t19 22.88-23; 128
 fs''4 cs''4 d''4 e''4 | % untitled/b7 untitled/b7.t19 23.75-24; 129
 fs''2 r4 r8 b''8 | % untitled/b8 untitled/b8.t8 .88-1; 130
 a''4. gs''16 fs''16 e''4 d''8 cs''8 | % untitled/b8 untitled/b8.t8 1.88-2; 131
 d''8. e''16 fs''4 r2 | % untitled/b8 untitled/b8.t8 2.25-2.5; 132
 r2 r4 b'8 cs''8 | % untitled/b8 untitled/b8.t8 3.88-4; 133
 d''2 r4 r8 fs''8 | % untitled/b8 untitled/b8.t8 4.88-5; 134
 \acciaccatura { e''8[ fs''8] } e''2 r4 r16 fs''16 d''16 cs''16 | % untitled/b8 untitled/b8.t8 5.94-6; 135
 b'2 r4 e''8 d''16 cs''16 | % untitled/b8 untitled/b8.t8 6.94-7; 136
 b'2. ^"慢" d''8 e''8 | % untitled/b8 untitled/b8.t8 7.88-8; 137
 d''4 b'2 cs''8 d''8 | % untitled/b8 untitled/b8.t8 8.88-9; 138
 cs''4 b'2 fs''8 e''8 | % untitled/b8 untitled/b8.t8 9.88-10; 139
 fs''8 fss'8 b'8 cs''8 e''8 fs''8 e''8 d''8 | % untitled/b8 untitled/b8.t8 10.88-11; 140
 e''8 fs'8 b'8 cs''8 d''4 d''8 e''8 | % untitled/b8 untitled/b8.t8 11.88-12; 141
 d''4 b'2 cs''8 d''8 | % untitled/b8 untitled/b8.t8 12.88-13; 142
 cs''4 b'2 cs''8 ^"慢" b'8 | % untitled/b8 untitled/b8.t8 13.88-14; 143
 cs''4 fs''8 cs''8 b'8 fs'8 e'8 cs'8 | % untitled/b8 untitled/b8.t8 14.88-15; 144
 b1 \bar "|."
} }

\new Staff = "down" \with { \RemoveAllEmptyStaves } {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \clef bass \time 4/4 \key b \dorian s4 s4 s4 s4 | % untitled/b8 untitled/b8.t8 15-16; 1
 s2 s4 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 2
 s4. s16 s16 s4 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 3
 s8. s16 s4 s2 | % untitled/b8 untitled/b8.t8 15-16; 4
 s8 s16 s16 s8 s8 s4 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 5
 s2 s4 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 6
 s2 s4. s16 s16 | % untitled/b8 untitled/b8.t8 15-16; 7
 s16 s16 s4. s4 s8 s16 s16 | % untitled/b8 untitled/b8.t8 15-16; 8
 s16 s16 s4. s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 9
 s2 s4 s8 s16 s16 | % untitled/b8 untitled/b8.t8 15-16; 10
 s8 s4. s4. s16 s16 | % untitled/b8 untitled/b8.t8 15-16; 11
 s16 s16 s4. s2 | \time 3/4 % untitled/b8 untitled/b8.t8 15-16; 12
 s8 s8 s8 s8 s8 s8 | \time 4/4 % untitled/b8 untitled/b8.t8 15-16; 13
 s2 s4. s8 | % untitled/b8 untitled/b8.t8 15-16; 14
 s4. s16 s16 s4 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 15
 s8. s16 s2. | % untitled/b8 untitled/b8.t8 15-16; 16
 s16 s16 s4. s2 | % untitled/b8 untitled/b8.t8 15-16; 17
 s16 s16 s4. s16 s16 s4. | % untitled/b8 untitled/b8.t8 15-16; 18
 s16 s16 s4. s16 s16 s8 s4 | % untitled/b8 untitled/b8.t8 15-16; 19
 s4. s16 s16 s2 | % untitled/b8 untitled/b8.t8 15-16; 20
 s4 s4 s4 s4 | % untitled/b8 untitled/b8.t8 15-16; 21
 s4 s4 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 22
 s4 s4 s4 s4 | % untitled/b8 untitled/b8.t8 15-16; 23
 s4 s8 s8 s8 s8 s16 s16 s16 s16 | % untitled/b8 untitled/b8.t8 15-16; 24
 s16 s16 s16 s16 s16 s16 s16 s16 s16 s16 s16 s16 s16 s16 s16 s16 | % untitled/b8 untitled/b8.t8 15-16; 25
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 26
 s8 s16 s16 s16 s16 s8 s8 s8 s8 s16 s16 | % untitled/b8 untitled/b8.t8 15-16; 27
 s8 s8 s8 s8 s16 s16 s8 s16 s16 s16 s16 | % untitled/b8 untitled/b8.t8 15-16; 28
 s8 s16 s16 s16 s16 s8 s2 | % untitled/b8 untitled/b8.t8 15-16; 29
 s4 s4 s4 s4 | % untitled/b8 untitled/b8.t8 15-16; 30
 s2 s4 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 31
 s4. s16 s16 s4 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 32
 s8. s16 s4 s2 | % untitled/b8 untitled/b8.t8 15-16; 33
 s2 s4 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 34
 s2 s4 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 35
 s2 s4 s16 s16 s16 s16 | % untitled/b8 untitled/b8.t8 15-16; 36
 s2 s4 s8 s16 s16 | % untitled/b8 untitled/b8.t8 15-16; 37
 s1 | % untitled/b8 untitled/b8.t8 15-16; 38
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 39
 s2 s4. s8 | % untitled/b8 untitled/b8.t8 15-16; 40
 s4. s16 s16 s4 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 41
 s8. s16 s4. s8 s4 | % untitled/b8 untitled/b8.t8 15-16; 42
 s8 s16 s16 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 43
 s2 s4. s8 | % untitled/b8 untitled/b8.t8 15-16; 44
 s8 s4. s4 s16 s16 s16 s16 | % untitled/b8 untitled/b8.t8 15-16; 45
 s16 s16 s4. s4 s8 s16 s16 | % untitled/b8 untitled/b8.t8 15-16; 46
 s16 s16 s4. s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 47
 s2 s2 | % untitled/b8 untitled/b8.t8 15-16; 48
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 49
 s2 s4 s4 | % untitled/b8 untitled/b8.t8 15-16; 50
 s1 | % untitled/b8 untitled/b8.t8 15-16; 51
 s2 s2 | % untitled/b8 untitled/b8.t8 15-16; 52
 s2 s4 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 53
 s2. s4 | % untitled/b8 untitled/b8.t8 15-16; 54
 s1 | % untitled/b8 untitled/b8.t8 15-16; 55
 s2 s2 | % untitled/b8 untitled/b8.t8 15-16; 56
 s2 s2 | % untitled/b8 untitled/b8.t8 15-16; 57
 s16 s16 s4. s2 | % untitled/b8 untitled/b8.t8 15-16; 58
 s16 s16 s16 s16 s4 s2 | % untitled/b8 untitled/b8.t8 15-16; 59
 s1 | % untitled/b8 untitled/b8.t8 15-16; 60
 s1 | % untitled/b8 untitled/b8.t8 15-16; 61
 s1 | % untitled/b8 untitled/b8.t8 15-16; 62
 s1 | % untitled/b8 untitled/b8.t8 15-16; 63
 s2 s2 | % untitled/b8 untitled/b8.t8 15-16; 64
 s2 s4 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 65
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 66
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 67
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 68
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 69
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 70
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 71
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 72
 s4 s8 s8 s8 s16 s16 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 73
 s4 s4 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 74
 s8 s8 s4 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 75
 s8 s8 s4 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 76
 s8 s8 s4 s8 s16 s16 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 77
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 78
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 79
 s2 s4 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 80
 s4 s8 s8 s4 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 81
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 82
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 83
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 84
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 85
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 86
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 87
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 88
 s8 s8 s8 s8 s2 | % untitled/b8 untitled/b8.t8 15-16; 89
 s2 s4 s4 | % untitled/b8 untitled/b8.t8 15-16; 90
 s1 | % untitled/b8 untitled/b8.t8 15-16; 91
 s2 s2 | % untitled/b8 untitled/b8.t8 15-16; 92
 s2 s4 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 93
 s2. s4 | % untitled/b8 untitled/b8.t8 15-16; 94
 s1 | % untitled/b8 untitled/b8.t8 15-16; 95
 s2 s2 | % untitled/b8 untitled/b8.t8 15-16; 96
 s2 s2 | % untitled/b8 untitled/b8.t8 15-16; 97
 s16 s16 s4. s2 | % untitled/b8 untitled/b8.t8 15-16; 98
 s16 s16 s16 s16 s4 s2 | % untitled/b8 untitled/b8.t8 15-16; 99
 s1 | % untitled/b8 untitled/b8.t8 15-16; 100
 s1 | % untitled/b8 untitled/b8.t8 15-16; 101
 s1 | % untitled/b8 untitled/b8.t8 15-16; 102
 s1 | % untitled/b8 untitled/b8.t8 15-16; 103
 s2 s2 | % untitled/b8 untitled/b8.t8 15-16; 104
 s2 s4 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 105
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 106
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 107
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 108
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 109
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 110
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 111
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 112
 s4 s8 s8 s8 s16 s16 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 113
 s4 s4 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 114
 s8 s8 s4 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 115
 s8 s8 s4 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 116
 s8 s8 s4 s8 s16 s16 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 117
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 118
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 119
 s2 s4 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 120
 s4 s8 s8 s4 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 121
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 122
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 123
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 124
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 125
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 126
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 127
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 128
 s4 s4 s4 s4 | % untitled/b8 untitled/b8.t8 15-16; 129
 s2 s4 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 130
 s4. s16 s16 s4 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 131
 s8. s16 s4 s2 | % untitled/b8 untitled/b8.t8 15-16; 132
 s2 s4 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 133
 s2 s4 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 134
 s2 s4 s16 s16 s16 s16 | % untitled/b8 untitled/b8.t8 15-16; 135
 s2 s4 s8 s16 s16 | % untitled/b8 untitled/b8.t8 15-16; 136
 s2. s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 137
 s4 s2 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 138
 s4 s2 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 139
 s8 s8 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 140
 s8 s8 s8 s8 s4 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 141
 s4 s2 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 142
 s4 s2 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 143
 s4 s8 s8 s8 s8 s8 s8 | % untitled/b8 untitled/b8.t8 15-16; 144
 s1 \bar "|."
} }

>>

\new Staff  {
\set Staff.instrumentName = "箏"
\set Staff.shortInstrumentName = "箏"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 4/4 \key b \dorian b'4 cs''4 b'4 a'4 | % untitled/b3 untitled/b3.t2 .75-1; 1
 fs'8 b8 d'8 e'8 fs'8 e'8 fs'4 | % untitled/b3 untitled/b3.t2 1.75-2; 2
 r8 e8 b8 e'8 fs'8 a'8 fs'4 | % untitled/b3 untitled/b3.t2 2.75-3; 3
 b8 d'8 fs'8 e'8 fs'8 b8 d'8 fs'8 | % untitled/b3 untitled/b3.t2 3.88-4; 4
 a'8 gs'16 ( fs'16 ) e'8 d'8 cs'8 b8 b'8 a'8 | % untitled/b3 untitled/b3.t2 4.88-5; 5
 fss4 b8 cs'8 d'8 cs'8 d'4 | % untitled/b3 untitled/b3.t2 5.75-6; 6
 r8 a8 cs'8 d'8 e'8 d'8 e'4 | % untitled/b3 untitled/b3.t2 6.75-7; 7
 e8 b8 e'8 fs'8 fss'8 fs'8 fss'4 | % untitled/b3 untitled/b3.t2 7.75-8; 8
 r8 d'8 fss'8 a'8 b'4 b'8 a'8 | % untitled/b3 untitled/b3.t2 8.88-9; 9
 fss'8 r8 b8 cs'8 d'8 cs'8 d'4 | % untitled/b3 untitled/b3.t2 9.75-10; 10
 r8 a8 cs'8 d'8 e'8 d'8 e'4 | % untitled/b3 untitled/b3.t2 10.75-11; 11
 e8 b8 e'8 fs8 fss8 d'8 a'4 | \time 3/4 % untitled/b3 untitled/b3.t2 11.75-12; 12
 r8 a'8 b'8 cs''8 a'8 fss'8 | \time 4/4 % untitled/b3 untitled/b3.t2 12.62-12.75; 13
 \ottava #-1 b,8 \ottava #0 b8 fs'8 e'8 fs'8 a'8 fs'4 | % untitled/b4 untitled/b4.t2 .75-1; 14
 e8 b8 e'8 b'8 e'8 fs'8 d'8 e'8 | % untitled/b4 untitled/b4.t2 1.88-2; 15
 fs'8 \ottava #-1 d8 \ottava #0 a8 d'8 fs'8 e'8 fs'8 d'8 | % untitled/b4 untitled/b4.t2 2.88-3; 16
 \ottava #-1 fss,8 d8 \ottava #0 b'8 cs''8 d''8 cs''8 b'8 a'8 | % untitled/b4 untitled/b4.t2 3.88-4; 17
 e8 b8 e'8 fs'8 fss'8 fs'8 e'8 d'8 | % untitled/b4 untitled/b4.t2 4.88-5; 18
 fs8 fss8 b8 e'8 fs'8 fss'8 b'8 e''8 | % untitled/b4 untitled/b4.t2 5.88-6; 19
 fs''4. e''8 b'8 fs'8 fs''4~ \trill | % untitled/b4 untitled/b4.t2 6.75-7.25; 20
 fs''4 cs''4 b'4 a'4 | % untitled/b4 untitled/b4.t2 7.75-8; 21
 fs'4 b4 e''8 fs''8 e''8 d''8 | % untitled/b4 untitled/b4.t2 8.88-9; 22
 b'4 e'4 d'8 e'8 cs'4 | % untitled/b4 untitled/b4.t2 9.75-10; 23
 cs'8 d'8 b4 \clef bass e2 | % untitled/b4 untitled/b4.t2 10.5-11; 24
 r4 e'8 b8 e2 | % untitled/b4 untitled/b4.t2 11.5-12; 25
 \clef treble r4 cs''16 d''16 e''8 \ottava #-1 b,8 \ottava #0 cs''16 d''16 e''8 d'16 b16 | % untitled/b4 untitled/b4.t2 12.94-13; 26
 r8 cs''8 d''8 b'8 r4 cs''16 d''16 e''8 | % untitled/b4 untitled/b4.t2 13.88-14; 27
 e''16 d''16 e''8 cs''16 d''16 e''8 a'8 b'8 cs''8 d''8 | % untitled/b4 untitled/b4.t2 14.88-15; 28
 b'8 cs''8 d''8 b'8 r2 | % untitled/b4 untitled/b4.t2 15.38-15.5; 29
 r4 cs''4 b'4 a'4 | % untitled/b4 untitled/b4.t2 16.75-17; 30
 r8 b'8 d''8 e''8 fs''8 e''8 fs''8 b''8 | % untitled/b2 untitled/b2.t2 .88-1; 31
 a''8 e'8 b'8 e''8 b''8 fs''8 b''4 | % untitled/b2 untitled/b2.t2 1.75-2; 32
 d'8 a'8 d''8 e''8 d''8 b'8 a'4 | % untitled/b2 untitled/b2.t2 2.75-3; 33
 a''8 gs''16 ( fs''16 ) e''8 d''8 cs''8 b'4. | % untitled/b2 untitled/b2.t2 3.62-4; 34
 r8 fss'8 b'8 cs''8 d''8 cs''8 d''8 fs''8 | % untitled/b2 untitled/b2.t2 4.88-5; 35
 e''8 a'8 cs''8 d''8 e''8 d''8 e''4 | % untitled/b2 untitled/b2.t2 5.75-6; 36
 r8 e'8 b'8 cs''8 d''8 cs''8 b'4 | % untitled/b2 untitled/b2.t2 6.75-7; 37
 r8 fss'8 b'8 cs''8 d''8 cs''8 b'4 | % untitled/b2 untitled/b2.t2 7.75-8; 38
 r8 a'8 b'8 cs''8 d''8 e'8 d'8 e'8 | % untitled/b2 untitled/b2.t2 8.88-9; 39
 \ottava #-1 b,8 \ottava #0 b'8 d''8 e''8 fs''8 e''8 fs''8 b''8 | % untitled/b2 untitled/b2.t2 9.88-10; 40
 \ottava #-1 e8 \ottava #0 e'8 b'8 e''8 b''8 fs''8 b''4 | % untitled/b2 untitled/b2.t2 10.75-11; 41
 \ottava #-1 d8 \ottava #0 a'8 d''8 e''8 d''8 b'8 a'8 r8 | % untitled/b2 untitled/b2.t2 11.75-11.88; 42
 a''8 gs''16 fs''16 e''8 d''8 cs''8 b'4. | % untitled/b2 untitled/b2.t2 12.62-13; 43
 \clef bass fss,4 b8 d'8 b8 a8 b8 d'8 | % untitled/b2 untitled/b2.t2 13.88-14; 44
 a,4 a8 e'8 cs'8 b8 cs'4 | % untitled/b2 untitled/b2.t2 14.75-15; 45
 e,8 b,8 b8 cs'8 d'8 cs'8 b4 | % untitled/b2 untitled/b2.t2 15.75-16; 46
 fss,4 b8 cs'8 a,8 cs'8 b8 cs'8 | % untitled/b2 untitled/b2.t2 16.88-17; 47
 e,8 b,8 b8 cs'8 d'8 cs'8 b4 | % untitled/b2 untitled/b2.t2 17.75-18; 48
 \clef treble r8 a'8 b'8 cs''8 d''8 cs'8 d'8 e'8 | % untitled/b2 untitled/b2.t2 18.88-19; 49
 fs'4 b'8 \acciaccatura { a''8[ fs''8] } e''8 d''8 cs''8 d''8 a''8 | % untitled/b5 untitled/b5.t2 .88-1; 50
 gs''4 e''4 b'4 b'8 e''8 | % untitled/b5 untitled/b5.t2 1.88-2; 51
 d''2 r4 fs''8 e''8 | % untitled/b5 untitled/b5.t2 2.88-3; 52
 d''2 r8 \acciaccatura { a''8[ fs''8] } e''8 d''8 e''8 | % untitled/b5 untitled/b5.t2 3.88-4; 53
 fs''4 b'8 e''8 d''8 cs''8 d''8 a''8 | % untitled/b5 untitled/b5.t2 4.88-5; 54
 gs''4 e''4 b'4 b''8 a''8 | % untitled/b5 untitled/b5.t2 5.88-6; 55
 b''2 r4 a''8 cs'''8 | % untitled/b5 untitled/b5.t2 6.88-7; 56
 b''2 r2 | % untitled/b5 untitled/b5.t2 7-7.5; 57
 \acciaccatura { b'8[ d''8 e''8 fs''8 a''8] } b''2 b''8 gss''8 b''8 cs'''8 | % untitled/b5 untitled/b5.t2 8.88-9; 58
 gss''4 d''4 b'8 d''8 e''8 fs''8 | % untitled/b5 untitled/b5.t2 9.88-10; 59
 fss''4 a''4 fs''4 e''8 fs''8 | % untitled/b5 untitled/b5.t2 10.88-11; 60
 r8 b'4. r4 b'8 e''8 | % untitled/b5 untitled/b5.t2 11.88-12; 61
 b'4 fss'4 r4 gss'8 cs''8 | % untitled/b5 untitled/b5.t2 12.88-13; 62
 b'4 e'2 b'8 a'8 | % untitled/b5 untitled/b5.t2 13.88-14; 63
 b'2 r4 b'8 a'8 | % untitled/b5 untitled/b5.t2 14.88-15; 64
 b'4. cs''8 d''8 cs''8 d''8 e''8 | % untitled/b5 untitled/b5.t2 15.88-16; 65
 fs''4 b'8 cs''8 d''8 e''8 fs''8 a''8 | % untitled/b6 untitled/b6.t1 .88-1; 66
 gs''4 e''4 b'4 b'8 e''8 | % untitled/b6 untitled/b6.t1 1.88-2; 67
 d''2. :32 fs''8 e''8 | % untitled/b6 untitled/b6.t1 2.88-3; 68
 d''2 e''8 fs''8 d''8 e''8 | % untitled/b6 untitled/b6.t1 3.88-4; 69
 fs''4 b'8 cs''8 d''8 cs''8 d''8 a''8 | % untitled/b6 untitled/b6.t1 4.88-5; 70
 gs''4 e''4 b'4 b''8 a''8 | % untitled/b6 untitled/b6.t1 5.88-6; 71
 b''4. :32 b''8 a''8 b''8 d''8 e''8 | % untitled/b6 untitled/b6.t1 6.88-7; 72
 fs''2 r4 d'8 cs'8 | % untitled/b6 untitled/b6.t1 7.88-8; 73
 fs'4 b8 cs'8 d'8 e'8 fs'8 a'8 | % untitled/b6 untitled/b6.t1 8.88-9; 74
 gs'4 e'8 fs'8 gs'8 a'8 b'8 cs''8 | % untitled/b6 untitled/b6.t1 9.88-10; 75
 d''4 fss'8 d''8 cs''8 b'8 d'8 e'8 | % untitled/b6 untitled/b6.t1 10.88-11; 76
 fs'4 cs'8 d'8 e'8 fs'8 d'8 cs'8 | % untitled/b6 untitled/b6.t1 11.88-12; 77
 b'2 b'8 gss'8 b'8 cs''8 | % untitled/b6 untitled/b6.t1 12.88-13; 78
 gss'4 d'4 b8 d'8 e'8 fs'8 | % untitled/b6 untitled/b6.t1 13.88-14; 79
 fss'4 a'4 fs'4 e'8 fs'8~ | % untitled/b6 untitled/b6.t1 14.88-15.12; 80
 fs'8 b4. r4 \clef bass d'8 e'8 | % untitled/b6 untitled/b6.t1 15.88-16; 81
 d'4 b4 r4 b8 cs'8 | % untitled/b6 untitled/b6.t1 16.88-17; 82
 b4 d2 b8 a8 | % untitled/b6 untitled/b6.t1 17.88-18; 83
 b2 r4 b8 a8 | % untitled/b6 untitled/b6.t1 18.88-19; 84
 b2 r4 d'8 e'8 | % untitled/b6 untitled/b6.t1 19.88-20; 85
 d'4 b4 r4 cs'8 d'8 | % untitled/b6 untitled/b6.t1 20.88-21; 86
 cs'4 d2 \clef treble fs''8 e''8 | % untitled/b6 untitled/b6.t1 21.88-22; 87
 fs''4 r2 a''8 fss''8 | % untitled/b6 untitled/b6.t1 22.88-23; 88
 fs''1 | % untitled/b6 untitled/b6.t1 23-24; 89
 fs'4 b'8 \acciaccatura { a''8[ fs''8] } e''8 d''8 cs''8 d''8 a''8 | % untitled/b5 untitled/b5.t2 .88-1; 90
 gs''4 e''4 b'4 b'8 e''8 | % untitled/b5 untitled/b5.t2 1.88-2; 91
 d''2 r4 fs''8 e''8 | % untitled/b5 untitled/b5.t2 2.88-3; 92
 d''2 r8 \acciaccatura { a''8[ fs''8] } e''8 d''8 e''8 | % untitled/b5 untitled/b5.t2 3.88-4; 93
 fs''4 b'8 e''8 d''8 cs''8 d''8 a''8 | % untitled/b5 untitled/b5.t2 4.88-5; 94
 gs''4 e''4 b'4 b''8 a''8 | % untitled/b5 untitled/b5.t2 5.88-6; 95
 b''2 r4 a''8 cs'''8 | % untitled/b5 untitled/b5.t2 6.88-7; 96
 b''2 r2 | % untitled/b5 untitled/b5.t2 7-7.5; 97
 \acciaccatura { b'8[ d''8 e''8 fs''8 a''8] } b''2 b''8 gss''8 b''8 cs'''8 | % untitled/b5 untitled/b5.t2 8.88-9; 98
 gss''4 d''4 b'8 d''8 e''8 fs''8 | % untitled/b5 untitled/b5.t2 9.88-10; 99
 fss''4 a''4 fs''4 e''8 fs''8 | % untitled/b5 untitled/b5.t2 10.88-11; 100
 r8 b'4. r4 b'8 e''8 | % untitled/b5 untitled/b5.t2 11.88-12; 101
 b'4 fss'4 r4 gss'8 cs''8 | % untitled/b5 untitled/b5.t2 12.88-13; 102
 b'4 e'2 b'8 a'8 | % untitled/b5 untitled/b5.t2 13.88-14; 103
 b'2 r4 b'8 a'8 | % untitled/b5 untitled/b5.t2 14.88-15; 104
 b'4. cs''8 d''8 cs''8 d''8 e''8 | % untitled/b5 untitled/b5.t2 15.88-16; 105
 fs''4 b'8 cs''8 d''8 e''8 fs''8 a''8 | % untitled/b7 untitled/b7.t13 .88-1; 106
 gs''4 e''4 b'4 b'8 e''8 | % untitled/b7 untitled/b7.t13 1.88-2; 107
 d''2. :32 fs''8 e''8 | % untitled/b7 untitled/b7.t13 2.88-3; 108
 d''2 e''8 fs''8 d''8 e''8 | % untitled/b7 untitled/b7.t13 3.88-4; 109
 fs''4 b'8 cs''8 d''8 cs''8 d''8 a''8 | % untitled/b7 untitled/b7.t13 4.88-5; 110
 gs''4 e''4 b'4 b''8 a''8 | % untitled/b7 untitled/b7.t13 5.88-6; 111
 b''4. :32 b''8 a''8 b''8 d''8 e''8 | % untitled/b7 untitled/b7.t13 6.88-7; 112
 fs''2 r4 d'8 cs'8 | % untitled/b7 untitled/b7.t13 7.88-8; 113
 fs'4 b8 cs'8 d'8 e'8 fs'8 a'8 | % untitled/b7 untitled/b7.t13 8.88-9; 114
 gs'4 e'8 fs'8 gs'8 a'8 b'8 cs''8 | % untitled/b7 untitled/b7.t13 9.88-10; 115
 d''4 fss'8 d''8 cs''8 b'8 d'8 e'8 | % untitled/b7 untitled/b7.t13 10.88-11; 116
 fs'4 cs'8 d'8 e'8 fs'8 d'8 cs'8 | % untitled/b7 untitled/b7.t13 11.88-12; 117
 b'2 b'8 gss'8 b'8 cs''8 | % untitled/b7 untitled/b7.t13 12.88-13; 118
 gss'4 d'4 b8 d'8 e'8 fs'8 | % untitled/b7 untitled/b7.t13 13.88-14; 119
 fss'4 a'4 fs'4 e'8 fs'8~ | % untitled/b7 untitled/b7.t13 14.88-15.12; 120
 fs'8 b4. r4 \clef bass d'8 e'8 | % untitled/b7 untitled/b7.t13 15.88-16; 121
 d'4 b4 r4 b8 cs'8 | % untitled/b7 untitled/b7.t13 16.88-17; 122
 b4 d2 b8 a8 | % untitled/b7 untitled/b7.t13 17.88-18; 123
 b2 r4 b8 a8 | % untitled/b7 untitled/b7.t13 18.88-19; 124
 b2 r4 d'8 e'8 | % untitled/b7 untitled/b7.t13 19.88-20; 125
 d'4 b4 r4 cs'8 d'8 | % untitled/b7 untitled/b7.t13 20.88-21; 126
 cs'4 d2 \clef treble fs''8 e''8 | % untitled/b7 untitled/b7.t13 21.88-22; 127
 fs''4 r2 a''8 fss''8 | % untitled/b7 untitled/b7.t13 22.88-23; 128
 fs''8 b'8 fs'8 cs'8 b2 | % untitled/b7 untitled/b7.t13 23.5-24; 129
 r8 b'8 d''8 e''8 fs''8 e''8 fs''8 b''8 | % untitled/b8 untitled/b8.t3 .88-1; 130
 a''8 e'8 b'8 e''8 b''8 fs''8 b''4 | % untitled/b8 untitled/b8.t3 1.75-2; 131
 d'8 a'8 d''8 e''8 d''8 b'8 a'4 | % untitled/b8 untitled/b8.t3 2.75-3; 132
 a''8 gs''16 ( fs''16 ) e''8 d''8 cs''8 b'4. | % untitled/b8 untitled/b8.t3 3.62-4; 133
 r8 fss'8 b'8 cs''8 d''8 cs''8 d''8 fs''8 | % untitled/b8 untitled/b8.t3 4.88-5; 134
 e''8 a'8 cs''8 d''8 e''8 d''8 e''4 | % untitled/b8 untitled/b8.t3 5.75-6; 135
 r8 e'8 b'8 cs''8 d''8 cs''8 b'4 | % untitled/b8 untitled/b8.t3 6.75-7; 136
 r8 fss'8 b'4 cs''4 d'8 e'8 | % untitled/b8 untitled/b8.t3 7.88-8; 137
 d'8 \clef bass e8 b8 \clef treble fs'8 fss'4 fs'4 | % untitled/b8 untitled/b8.t3 8.75-9; 138
 \clef bass r8 d8 b8 \clef treble e'8 fs'4 fs'8 e'8 | % untitled/b8 untitled/b8.t3 9.88-10; 139
 fs'4 e'2 e'8 d'8 | % untitled/b8 untitled/b8.t3 10.88-11; 140
 e'4 d'2 d'8 e'8 | % untitled/b8 untitled/b8.t3 11.88-12; 141
 d'8 \clef bass e8 b8 \clef treble fs'8 fss'4 fs'4 | % untitled/b8 untitled/b8.t3 12.75-13; 142
 \clef bass r8 d8 b8 \clef treble e'8 fs'4 cs'8 b8 | % untitled/b8 untitled/b8.t3 13.88-14; 143
 cs'2~ :32 cs'4~ :32 cs'8. :32 r16 | % untitled/b8 untitled/b8.t3 14-14.94; 144
 b1 \bar "|."
} }

>>
}
}

