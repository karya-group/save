\version "2.14.2"
\language "english"
\pointAndClickOff
\header { title = "b8" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\score {
<<
\new StaffGroup <<
\set StaffGroup.instrumentName = "揚琴"
\set StaffGroup.shortInstrumentName = "揚琴"
\new Staff = "up" {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 4/4 \key b \minor d'''16 cs'''16 b''16 fs''16 d''16 cs''16 b'16 <cs'' g''>16 d''16 r16 d''16 cs''16 b'16 fs''16 fs''16 cs''16 | % untitled/b8 untitled/b8.t5 .93-1; 1
 d''16 r16 fs''16 <cs'' g''>16 d''16 fs''16 r16 <cs'' g''>16 r16 <d'' fs''>16 e''16 d''16 a''8 g''16 fs''16 | % untitled/b8 untitled/b8.t5 1.93-2; 2
 e''8 cs''16 b'16 cs''8 cs''8 g''8 d''16 cs''16 d''16 g''8 cs''16~ | % untitled/b8 untitled/b8.t5 2.93-3.06; 3
 cs''16 fs'16 fs''8 cs''8 fs'8 r4 e''8 cs''8 \bar "|."
} }

\new Staff = "down" \RemoveEmptyStaves {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
\override Staff.VerticalAxisGroup.remove-first = ##t
{
 \clef bass \time 4/4 \key b \minor s16 s16 s16 s16 s16 s16 s16 s16 s16 s16 s16 s16 s16 s16 s16 s16 | % untitled/b8 untitled/b8.t5 3.87-4; 1
 s16 s16 s16 s16 s16 s16 s16 s16 s16 s16 s16 s16 s8 s16 s16 | % untitled/b8 untitled/b8.t5 3.87-4; 2
 s8 s16 s16 s8 s8 s8 s16 s16 s16 s8 s16 | % untitled/b8 untitled/b8.t5 3.87-4; 3
 s16 s16 s8 s8 s8 s4 s8 s8 \bar "|."
} }

>>

\new Staff  {
\set Staff.instrumentName = "箏"
\set Staff.shortInstrumentName = "箏"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 4/4 \key b \minor b,2 fs2 | % untitled/pokok untitled/pokok.t2 .5-1; 1
 b2 d2 | % untitled/pokok untitled/pokok.t2 1.5-2; 2
 e2:32 g4~ g8.~ <g fs~>16 | % untitled/pokok untitled/pokok.t2 2.5-3; 3
 fs8 \clef treble fs'8 e'8 d'8 r4 fs'8 fs8 \bar "|."
} }

>>
}

