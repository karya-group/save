\version "2.14.2"
\language "english"
\header { title = "order" tagline = "" }
\score { <<

\new PianoStaff <<
\new Staff {\set Staff.instrumentName = "c1"
\set Staff.shortInstrumentName = "c1"
{
  \clef treble \key d \major \time 5/4 r8 a''4.\f e''2\mf fs''8.\p a''16 | g''4\mf <d''' g''>4\p <d''' g''>4\mf <d''' g''>4 <c''' f''>4\p | e''4\mf cs''4\p a'4 e''4 g''4\mf | d'''4\f c'''4\mf g''4\p a''4\mf b''4 | % 0
  <e' e''>4~ <e' b' e''>4~ <b' g'' e''>4~ <g'' b'' e''>4~ <b'' d''' d''>4\f | <a' c''' c''>4~\mf <c''' a'>4~ <a' c'' f'>4~ <c'' g'' c''>4~ <g'' b'' f''>4\f | <a''' a''>4\mf <e''' e''>8\p <fs''' fs''>8 <g''' g''>4\mf <a''' a''>4 <b''' b''>4\f | <c'''' c'''>4 <a''' a''>8\p <b''' b''>8 <c'''' c'''>4\mf <d'''' d'''>4 <e'''' e'''>4\f | % 4
  <e'''' e'''>4 <g''' c'''>4\mf <e''' g''>4\p <ds''' fs''>4 <b'' gs''>4\mf | <c''' a''>4 <a'' e''>4 <e'' c''>4 <a'' b'>4 <e'' a'>4 | <b'' g''>4 <a'' d''>4 <d'' b'>4 <a'' a'>4 <d'' g'>4 | <c''' a'>4\f <a'' f'>4\mf <f'' a'>4 <d''' gs'>4 <e'' a'>4\f | % 8
  c'''2.\mf b''4 a''4~ | a''4 e''4\p c''4 b'4~ b'4 | a'4 e'4 e''4\mf d''4 c''4 | <e'' c'''>4 <c'' a''>16 r16 <d'' b''>16 r16 <e'' c''' c'''>4 <f'' d''' b''>4 <g'' e''' a''>8.~ a''16~ | % 12
  <a'' a'' f'''>4 <f'' d'''>16 r16 <g'' e'''>16 r16 <a'' f''' c'''>4 <as'' g''' as''>4 <c''' a''' a''>8 r8 | <c''' a'''>4\p <a'' f'''>4\mf <f'' d'''>4 <g'' cs'''>4 <a'' d'''>4 | <d'' e'' a'>4~ <e'' a' e''>4~ <e'' a' a''>4 <d'' g' d''>4~ <g' d'' g''>4 | <d'' a' e''>4~ <a' e'' e''>4~ <a' e'' a''>4 <d'' f' d''>4~ f'4 | % 16
  r4 r4 r4 r4 r4 | % 20
} }
\new Staff {\set Staff.instrumentName = "c1"
\set Staff.shortInstrumentName = "c1"
{
  \clef treble \key d \major \time 5/4 <cs' e' a'>1~\arpeggio\p <cs' e' a'>4\arpeggio | <c' f' a'>1~\arpeggio\mf <c' f' a'>8\arpeggio r8 | <cs' e' a'>1~\arpeggio\p <cs' e' a'>4\arpeggio | <c' e' g'>1~\mf <c' e' g'>4 | % 0
  <e' b g'>1~\arpeggio <e' b g'>4\arpeggio | <e' f' a>1~ <e' f' a>4 | <cs' e' a'>1~\arpeggio\p <cs' e' a'>4\arpeggio | <c' f' a'>1 r8 <e' c' g'>8~\arpeggio\mf | % 4
  <e' c' g'>2~\arpeggio <e' c' g'>8\arpeggio r8 <ds' fs' b>8. r16 <e' gs' b>8. r16 | <a e' c''>1~ <a e' c''>4 | <e' g b'>1~ <e' g b'>4 | <f d' d''>2.\arpeggio <e b d'>4~ <e b d'>4 | % 8
  a,4~ <a, e>4~ <a, e c'>4~ <a, e c' b>4~ <a, e c' b a>4~ | <e c' b a a,>4~\p <c' b a a, e>4~ <b a a, e d'>4~\mf <a a, e d' f>4~\p <a, e d' f c>8~\mf <e d' f c g,>8~\p | <e d' f c a,>4~ <d' f c a, e>4~ <f c a, e c'>4~\mf <c a, e c' b>4~\p <a, e c' b a>4~\mf | <e c' b a a,>4~\p <c' b a a, e>4~ <b a a, e d'>4~\mf <a a, e d' f>4~\p <a, e d' f c>8~\mf <e d' f c g,>8\p | % 12
  d,4~\mf <d, as,>4~ <d, as, g>4~ <d, as, g f>4~ <d, as, g f e>8 r8 | as,4~\p <as, f>4~ <as, f f'>4~ <f f' a, e'>4~ <f f' a, e' d'>4 | a,4~\mf <a, e>4~ <a, e c'>4~ <a, c' e b>4~ <a, c' e b a>4~ | <c' e b a a,>4~\p <c' b a a, e>4~ <b a a, e d'>4~\mf <a a, d' f b>4~\p <a a, d' f>8~ <a d' f>8~ | % 16
  <a d' f>4~ <a d'>4~ a2 r4 | % 20
} }
>>
>> }
