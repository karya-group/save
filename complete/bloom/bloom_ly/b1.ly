\version "2.14.2"
\language "english"
\header { title = "b1" tagline = "" }
\score { <<

\new PianoStaff <<
\new Staff {\set Staff.instrumentName = "c1"
\set Staff.shortInstrumentName = "c1"
{
  \clef treble \key c \major \time 4/4 <a''' a''>16\mf <e''' e'' fs''' fs''>1\p <g''' g''>16\mf <a''' a''>16 <b''' b''>16\f <c'''' c'''>16 <a''' a'' b''' b''>1\p <c'''' c'''>16\mf <d'''' d'''>16 <e'''' e'''>16\f <e'''' e'''>16 <g''' c'''>16\mf <e''' g''>16\p <ds''' fs''>16 <b'' gs''>16\mf <c''' a''>16 | <a'' e''>16 <e'' c''>16 <a'' b'>16 <e'' a'>16 <b'' g''>16 <a'' d''>16 <d'' b' a'' a'>16 r16 <d'' g' c''' a'>16 r16 <a'' f' f'' a'>16 r16 <d''' gs' e'' a'>16 r16 r8 | % 0
} }
\new Staff {\set Staff.instrumentName = "c1"
\set Staff.shortInstrumentName = "c1"
{
  \clef treble \key c \major \time 4/4 <cs' e' a'>4~\arpeggio\p <cs' e' a'>16\arpeggio <c' f' a'>8.~ <c' f' a'>16 <e' c' g'>8.\arpeggio\mf r16 <ds' fs' b>16 <e' gs' b>16 <a e' c''>16~ | <a e' c''>4 <e' g b'>4~ <e' g b' f d' d''>16~ <f d' d''>8\arpeggio r16 <e b d'>8 r8 | % 0
} }
>>
>> }
