\version "2.14.2"
\language "english"
\header { title = "b0" tagline = "" }
\score { <<

\new Staff {\set Staff.instrumentName = "c1"
\set Staff.shortInstrumentName = "c1"
{
  \clef treble \key c \major \time 4/4 a''4\mf r4 r2 | % 0
} }
>> }
