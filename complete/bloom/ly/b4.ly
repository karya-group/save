\version "2.14.2"
\language "english"
\header { title = "b4" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\score {
<<
\new PianoStaff <<
\set PianoStaff.instrumentName = "piano"
\set PianoStaff.shortInstrumentName = ""
\new Staff = "up" {
\set Staff.pedalSustainStyle = #'mixed
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 4/4 \key c \major <a,,~ a''>4 <a,,~ b''>4 <a,,~ e'''>4 <a,,~ a'' a>4 | % 1
  <a,,~ d''' g>4 <a,,~ a'' es~>4 <a,,~ es~ b''>4 <a,,~ es es'''>4 | % 2
  <a,, a'' e>4 <g, d''' d>4 <a,,~ a'' cs~>4 <a,,~ cs~ b''>4 | % 3
  <a,,~ cs e'''>4 <a,,~ a'' d>4 <a,, d''' e>4 <bs,,~ a'' g>4 | % 4
  <bs,,~ b''~ es~>8 <bs,,~ b''~ es>32 <bs,,~ b''>16. <bs,,~ g''' e>4 <bs,,~ a''~ d~>8 <bs,,~ a''~ d>32 <bs,,~ a''>16. <bs,,~ es'''~ e~>8 <bs,,~ es'''~ e>32 <bs,, es'''>16. | % 5
  <e,~ e'' b>4 <e,~ b'' d'>4 <e,~ e''' g'>4 <e,~ e'' bs'' bs'>4 | % 6
  <e,~ b'' b'' b'>4 <e, d'' a''~ a'~>4 <<
  \new Voice { \voiceTwo
  <a'' a'>4 } >> \oneVoice
  <d,~ a''>4 <d,~ e''' g'' g'>4 | % 7
  <d, d'' bs'' bs'>4 <g, a'' b'' b'>4 r2 \bar "|." } }
\new Staff = "down" {
\set Staff.pedalSustainStyle = #'mixed
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 4/4 \key c \major \arpeggioArrowUp<cs'~ e'~ a'~>1\arpeggio | % 1
  <cs' e' a'>4 <bs~ es'~ a'~>2. | % 2
  <bs es' a'>2 \arpeggioArrowUp<cs'~ e'~ a'~>2\arpeggio | % 3
  <cs' e' a'>2. <bs~ e'~ g'~>4 | % 4
  <bs e' g'>1 | % 5
  <e'~ b~ g'~>1 | % 6
  <e' b g'>4 \arpeggioArrowUp<e'~ es'~ a~>2.\arpeggio | % 7
  <e' es' a>2 r2 \bar "|." } }
>>
>>
}

