\version "2.14.2"
\language "english"
\header { title = "bloom" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\score {
<<
\new PianoStaff <<
\set PianoStaff.instrumentName = "piano"
\set PianoStaff.shortInstrumentName = ""
\new Staff = "up" {
\set Staff.pedalSustainStyle = #'mixed
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 5/4 \key a \minor r8 a''8~ a''4 e''4~ e''4 fs''4 | % 1
  \acciaccatura { a''8 } g''4 <d''' g''>4 <d''' g''>4 <d''' g''>4 <c''' f''>4 | % 2
  e''4 cs''4 a'4 e''4 g''4 | % 3
  d'''4 c'''4 g''4 a''4 b''4 | % 4
  <e' e''>4 <b' e''>4 <g'' e''>4 <b'' e''>4 <d''' d''>4 | % 5
  <a' c''' c''>4 a'4 <c'' f'>4 <g'' c''>4 <b'' f''>4 | % 6
  \ottava #1 <a''' a''>4 <e''' e''>8 <fs''' fs''>8 <g''' g''>4 <a''' a''>4 <b''' b''>4 | % 7
  \ottava #2 <c'''' c'''>4 <a''' a''>8 <b''' b''>8 <c'''' c'''>4 <d'''' d'''>4 <e'''' e'''>4 | % 8
  <e'''' e'''>4 \ottava #1 <g''' c'''>4 <e''' g''>4 <ds''' fs''>4 \ottava #0 <b'' gs''>4 | % 9
  <c''' a''>4 <a'' e''>4 <e'' c''>4 <a'' b'>4 <e'' a'>4 | % 10
  <b'' g''>4 <a'' d''>4 <d'' b'>4 <a'' a'>4 <d'' g'>4 | % 11
  <c''' a'>4 <a'' f'>4 <f'' a'>4 <d''' gs'>4 <e'' a'>4 | % 12
  c'''2. b''4 a''4~ | % 13
  a''4 e''4 c''4 b'2 | % 14
  a'4 e'4 e''4 d''4 c''4 | % 15
  <e'' c'''>4 <c'' a''>8 <d'' b''>8 <e'' c'''>4 <f'' d''' b''>4 <g'' e''' a''>4 | % 16
  <a'' f'''>4 <f'' d'''>8 <g'' e'''>8 <a'' f''' c'''>4 <bf'' g'''>4 <c''' a''' a''>8 r8 | % 17
  <c''' a'''>4 <a'' f'''>4 <f'' d'''>4 <g'' cs'''>4 <a'' d'''>4 | % 18
  <<
  { \voiceOne
  d''4 e''4 a''4 r4 g''4 | % 19
  d''4 e''4 a''4 r4 g''8 f''8 | % 20
  d''4 e''4 a''4 d''4 g''4 | % 21
  d''4 e''4 a''4 d''4 g''4 | % 22
  f''4 g''4 c'''4 f''4 bf''4 | % 23
  f''4 g''4 c'''4 d'''4 e'''4 | % 24
  a''4 b''4 e'''4 a''4 d'''4 | % 25
  a''4 b''4 f'''4 a''4 d'''4 | % 26
  a''4 b''4 e'''4 a''4 d'''4 | % 27
  a''4 b''4 g'''4 a''4 f'''4 | % 28
  e''4 <b'' d'>4 <e''' g'>4 <e'' c''' c''>4 <b'' b'>4 | % 29
  <d'' a'' a'>2 <g'' g'>4 <c''' c''>4 <b'' b'>4 | % 30
  a''4 e''8-. fs''8-. g''4 a''4 b''4 | % 31
  c'''4 a''8-. b''8-. c'''4 d'''4 e'''4-. | % 32
  e'''4 c'''4 g''4 fs''4-. gs''4-. | % 33
  <c''' a''>2 \acciaccatura { b''8[ c'''8] } b''4 g''4 f''4 | % 34
  e''4 d''4 f''4 e''4 d''4 | % 35
  } \new Voice { \voiceTwo
  <e'' a'>2. <g' d''>2 | % 19
  <a' e''>2. <f' d''>4 r4 | % 20
  <e' a'>2. <d' g'>2 | % 21
  <a e'>4 <e c'>8 <g d'>8 <a e'>4 <a f'>4 <b g'>4 | % 22
  <c' a'>2. <as g'>2 | % 23
  <d' a'>2. c'8 g'8~ g'4 | % 24
  \arpeggioArrowUp<cs'~ e'~ a'~>2.\arpeggio <cs' e' a'>2 | % 25
  <c'~ f'~ a'~>2. <c' f' a'>2 | % 26
  \arpeggioArrowUp<cs'~ e'~ a'~>2.\arpeggio <cs' e' a'>2 | % 27
  <c'~ e'~ g'~>2. <c' e' g'>2 | % 28
  <e'~ b~ g'~>2. <e' b g'>2 | % 29
  \arpeggioArrowUp<e'~ f'~ a~>2.\arpeggio <e' f' a>2 | % 30
  \arpeggioArrowUp<cs'~ e'~ a'~>2.\arpeggio <cs' e' a'>2 | % 31
  <c'~ f'~ a'~>2. <c' f' a'>2 | % 32
  \arpeggioArrowUp<e' c' g'>2.\arpeggio <ds' fs' b>4-. <e' gs' b>4-. | % 33
  <a~ e'~ c''~>2. <a e' c''>2 | % 34
  <e'~ g~ b'~>2. <e' g b'>2 | % 35
  } >> \oneVoice
  c''4 b'4 g''4 e''4 gs'4 | % 36
  a'4 c''4 <b' e'>4 g'4 <e'' a'>4 | % 37
  <g'' b'>2 d''4 b'4 a'4 | % 38
  <e'' cs''>4 fs''2 <cs'' gs'>8 <d'' a'>8 <cs'' gs'>4 | % 39
  <g'' b'>4 <c'' a''>2 e''4 d''16 e''16 d''8 | % 40
  <g' b'>4 a'4 <d'~ e'~>4 <d' e'>4 <c' g'>4 | % 41
  <ds' a'>4 <fs' fs''>4 <a' a''>4 <c'' c'''>4 <fs'' fs'''>4 | % 42
  <a'' a'''>4 <a g'>4 e'4 bf'8 fs'8 g4 | % 43
  <a'' a'''>4 <b'' a'>8 a'''8 e'''4 <ds''' ds'' a'''>8 fs'8 g4 | % 44
  <cs' a'>4 <a fs'>4 <e' cs''>4 <d' a'>4 <ds' c''>4 | % 45
  <a'' a'''>4 <a g'>4 e'4 bf'8 fs'8 g4 | % 46
  <a'' a'''>4 <a g'>8 a''8 e'4 <bf' a''>8 fs'8 g4 | % 47
  bf'8 fs'8 g2~ g2 | % 48
  c''4 a'8-. b'8-. c''4 d''4 e''4 | % 49
  f''4 d''8-. e''8-. f''4 g''4 a''4-. | % 50
  a''4 f''4 d''4 c''4 ds''4 | % 51
  <<
  { \voiceOne
  r2 c'''4 c'''2 | % 52
  c'''4 c'''2 c'''4 c'''4 | % 53
  b''4 b''2 b''4 b''4 | % 54
  } \new Voice { \voiceTwo
  e''2.~ e''2~ | % 52
  e''2.~ e''2 | % 53
  d''2.~ d''2 | % 54
  } >> \oneVoice
  r4 d'''4 d'''4 c'''4 c'''4 | % 55
  b''4 c'''4 d'''4 e'''4 c'''4 | % 56
  b''4 e''4 d''4 a''4 c''4 | % 57
  <b'' e''>4 <e'' c''>4 <d'' a'>4 <a'' d''>4 <c'' g'>4 | % 58
  <e'~ b'~>2. <e' b'~>2 | % 59
  <b' b''>4 e'''2 b'''4 g''8 f''8 | % 60
  <<
  { \voiceOne
  a''4 e''4 fs''4 a''4 b''4 | % 61
  } \new Voice { \voiceTwo
  e''4 cs''4 a'4 fs'4 d'4 | % 61
  } >> \oneVoice
  c'''4 g''4 d''4 d'''8 c'''8~ c'''4 | % 62
  a''4 e''4 fs''4 a''4 b''4 | % 63
  <<
  { \voiceOne
  a''4 g''4 d''4 e''4 c''4 | % 64
  b'4 r2 r2 | % 65
  } \new Voice { \voiceTwo
  r2. r4 g'4 | % 64
  e'4 g4 a4 c'4 b4 | % 65
  } >> \oneVoice
  a2.~ a2 | % 66
  <<
  { \voiceOne
  a'4 c''4 b'4 g'4 e''4 | % 67
  g''2 d''4 b'4 e''4 | % 68
  a''4 e''8-. g''8-. a''4 b''4 c'''4 | % 69
  } \new Voice { \voiceTwo
  r2 e'4~ e'4 a'4 | % 67
  b'2 r4 r4 a'4 | % 68
  c''4 r2 r2 | % 69
  } >> \oneVoice
  d'''4 g''8-. c'''8-. d'''4 e'''4 f'''4 | % 70
  <<
  { \voiceOne
  <e'' c''' e'''>4 <c'' a''>8-. <d'' b''>8-. <e'' c'''>4 <f'' d'''>4 <g'' e'''>4 | % 71
  <a'' f'''>4 <f'' d'''>8-. <g'' e'''>8-. <a'' f'''>4 <bf'' g'''>4 <c''' a'''>4 | % 72
  } \new Voice { \voiceTwo
  r2 a4 f8 g8 a4 | % 71
  as4 c'4 d'4 as8 c'8 d'4 | % 72
  } >> \oneVoice
  <a'' a''' e'>4 f'4 c''4 c'''4 f''4 | % 73
  c''4 g'4 d'4 \clef bass g4 d4 | % 74
  a,4 g,4 d,4~ d,2 \bar "|." 
} }

\new Staff = "down" {
\set Staff.pedalSustainStyle = #'mixed
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 5/4 \key a \minor \arpeggioArrowUp<cs'~ e'~ a'~>2.\arpeggio <cs' e' a'>2 | % 1
  \arpeggioArrowUp<c'~ f'~ a'~>2.\arpeggio <c' f' a'>2 | % 2
  \arpeggioArrowUp<cs'~ e'~ a'~>2.\arpeggio <cs' e' a'>2 | % 3
  <c'~ e'~ g'~>2. <c' e' g'>2 | % 4
  \arpeggioArrowUp<e'~ b~ g'~>2.\arpeggio \sustainOn <e' b g'>2 | % 5
  <e'~ f'~ a~>2. \sustainOff\sustainOn <e' f' a>2 \sustainOff | % 6
  \arpeggioArrowUp<cs'~ e'~ a'~>2.\arpeggio <cs' e' a'>2 | % 7
  <c'~ f'~ a'~>2. <c' f' a'>4. \arpeggioArrowUp<e'~ c'~ g'~>8\arpeggio | % 8
  <e' c' g'>2. <ds' fs' b>4-. <e' gs' b>4-. | % 9
  <a~ e'~ c''~>2. <a e' c''>2 | % 10
  <e'~ g~ b'~>2. <e' g b'>2 | % 11
  \arpeggioArrowUp<f d' d''>2.\arpeggio \clef bass <e b d'>2 | % 12
  \clef bass a,4 \sustainOn e4 c'4 b4 a4 | % 13
  a,4 e4 d'4 f4 c8 g,8 | % 14
  a,4 e4 c'4 \sustainOff b4 a4 | % 15
  a,4 \sustainOn e4 d'4 f4 c8 g,8 | % 16
  d,4 \sustainOff\sustainOn as,4 g4 f4 e4 | % 17
  as,4 \sustainOff\sustainOn f4 f'4 <a, e'>4 d'4 | % 18
  a,4 \sustainOff\sustainOn e4 c'4 <e b>4 a4 | % 19
  a,4 \sustainOff\sustainOn e4 d'4 <f b>4 c'8 g,8 | % 20
  a,4 \sustainOff\sustainOn e4 c'4 b4 a4 | % 21
  a,4 \sustainOff\sustainOn e4 d'4 f4 c8 g,8 | % 22
  d,4 \sustainOff\sustainOn as,4 as4 f4 ds4 | % 23
  as,4 \sustainOff\sustainOn f4 f'4 <a, e'>4 d'4 \sustainOff | % 24
  <<
  { \voiceOne
  r2. a4 g4 | % 25
  f2. e4 d4 | % 26
  cs2. d4 e4 | % 27
  g4 f4-. e4 d4-. e4-. | % 28
  } \new Voice { \voiceTwo
  a,,2.~ a,,2~ | % 25
  a,,2.~ a,,4 g,4 | % 26
  a,,2.~ a,,2 | % 27
  c,2.~ c,2 | % 28
  } >> \oneVoice
  e,2.~ e,2 | % 29
  r4 d,2~ d,4 g,4 | % 30
  a,2.~ a,2~ | % 31
  a,2.~ a,4 g,4-. | % 32
  f,2. ds,4-. e,4-. | % 33
  a,2.~ a,2~ | % 34
  a,2.~ a,2 | % 35
  \arpeggioArrowUp<f d' f'>2.\arpeggio \arpeggioArrowUp<e b d'>2\arpeggio | % 36
  a,4 \sustainOn e4 c'4 b4 a4 | % 37
  a,4 \sustainOff\sustainOn e4 b4 g4 a8 g,8 | % 38
  fs,4 \sustainOff\sustainOn cs4 a4 gs4 fs4 | % 39
  a,4 \sustainOff\sustainOn e4 c'4 b4 a4 | % 40
  a,4 \sustainOff\sustainOn e4 b4 g4 a8 g,8 | % 41
  fs,4 \sustainOff\sustainOn cs4 a4 gs4 fs4 \sustainOff | % 42
  a,4 \sustainOn e4 c'4 ds'4 a4 | % 43
  a,4 \sustainOff\sustainOn e4 b4 g4 a8 g,8 | % 44
  fs,4 \sustainOff\sustainOn cs4 a4 gs4 fs4 | % 45
  a,4 \sustainOff\sustainOn e4 c'4 ds'4 <g, a>4 | % 46
  a,2. \sustainOff\sustainOn g,2~ | % 47
  g,2 \sustainOff\sustainOn \clef treble d'4 g'4 <bf' ds'>4 \sustainOff | % 48
  \clef treble <a e' a'>2 <e'~ b'~ g''~>4 <e' b' g''>2 | % 49
  <a f' c'' f>2. \clef bass e4 d4-. | % 50
  <f' c>2. <b~ d' a,>8 <b e'~ b,~>8 <e' b, e>4 | % 51
  \arpeggioArrowUp<a,~ a~ e'~>2.\arpeggio \sustainOn <a,~ a~ e'~>2 | % 52
  <a,~ a~ e'~>2. <a, a e'>2 | % 53
  \arpeggioArrowUp<a,~ f'~ a'~>2.\arpeggio \sustainOff\sustainOn <a, f' a'>4 e'4 \sustainOff | % 54
  d'4 f'4 g'4 <g e' d'>2 | % 55
  <f a d' g'>2. <e b g' b'>2 | % 56
  \arpeggioArrowUp<a~ e'~ e''~ a,~>2.\arpeggio <a e' e'' a,>2 | % 57
  <a~ f'~ f''~>2. <a f' f''>2 | % 58
  \arpeggioArrowUp<a,~ e''~ a~>2.\arpeggio <a,~ e''~ a~>2 | % 59
  <a,~ e''~ a~>2. <a, e'' a>2 | % 60
  \arpeggioArrowUp<cs'~ e'~ a'~>2.\arpeggio <cs' e' a'>2 | % 61
  <<
  { \voiceOne
  <c'~ f'~ a'~>2. <c' f' a'>2 | % 62
  \arpeggioArrowUp<cs'~ e'~ a'~>2.\arpeggio <cs' e' a'>2 | % 63
  <c'~ e'~ g'~>2. <c' e' g'>2 | % 64
  } \new Voice { \voiceTwo
  r4 a4 f4 d4 c4 | % 62
  cs4 a,4 cs4 e4 a4 | % 63
  g2 g,4 g4 r4 | % 64
  } >> \oneVoice
  <e' b g'>2 <c'~ f'~ a'~>4 <c' f' a'>4 <g' d' g>4 | % 65
  <<
  { \voiceOne
  \arpeggioArrowUp<e~ a~ e'~>2.\arpeggio <e a e'>2 | % 66
  } \new Voice { \voiceTwo
  r4 a,4 a,,4~ a,,2 | % 66
  } >> \oneVoice
  a,4 \sustainOn e'4 g'4 b4 d'4 | % 67
  a,4 \sustainOff\sustainOn g4 d'4 a4 f4 | % 68
  <a~ a,~ e'~ e~>2. \sustainOff <a a, e' e>2 | % 69
  <f,~ c~ g~ c'~>2. <f, c g c'>4 e,4 | % 70
  <a,~ a~ e'~ a'~>2. <a, a e' a'>2 | % 71
  d,4 \sustainOn as,4 g4 f4 <e, e>4 | % 72
  a,4 \sustainOff\sustainOn a,,2~ a,,2~ | % 73
  a,,2.~ a,,2~ | % 74
  a,,2.~ a,,2 \bar "|." 
} }

>>

>>
\header { piece = "I" }
}

\score {
<<
\new PianoStaff <<
\set PianoStaff.instrumentName = "piano"
\set PianoStaff.shortInstrumentName = ""
\new Staff = "up" {
\set Staff.pedalSustainStyle = #'mixed
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 6/4 \key d \minor a''8 gs''8 a''8 f''8 e''8 f''8 d''8 cs''8 d''8 a'8 e''8 gs''8 | % 1
  a''8 gs''8 a''8 f''8 e''8 f''8 d''8 cs''8 d''8 a''8 d''8 g''8 | % 2
  bf''8 a''8 bf''8 g''8 fs''8 g''8 d''8 cs''8 d''8 bf'8 g'8 d'8 | % 3
  bf''8 a''8 bf''8 g''8 fs''8 g''8 d''8 cs''8 d''8 bf'8 g'8 a'8 | % 4
  e''8 ds''8 e''8 cs''8 b'8 cs''8 a'8 gs'8 a'8 a8 cs'8 g'8 | % 5
  f''8 e''8 f''8 d''8 cs''8 d''8 bf'8 a'8 bf'8 f''8 g''8 gs''8 | % 6
  a''8 gs''8 a''8 f''8 e''8 f''8 <d'' a''>8 <cs'' gs''>8 <d'' a''>8 <a' f''>8 <gs' e''>8 <a' f''>8 | % 7
  g''8 fs''8 g''8 d''8 cs''8 d''8 <bf' g''>8 <a' fs''>8 <bf' g''>8 <g' d''>8 <fs' cs''>8 <g' d''>8 | % 8
  cs''8 b'8 cs''8 a'8 gs'8 a'8 <g'' cs''>8 <fs'' b'>8 <g'' cs''>8 <e'' a'>8 <d'' gs'>8 <e'' a'>8 | % 9
  f''8 e''8 f''8 d''8 cs''8 d''8 <a' a'' f''>8 <gs' gs'' e''>8 <a' a'' f''>8 <f' f'' a'>8 <d' d'' gs'>8 <a a'>8 | % 10
  <<
  { \voiceOne
  <f'' a''>8 <e'' g''>8 <f'' a''>8 <d'' f''>8 <cs'' e''>8 <d'' f''>8 <e'' g''>8 <d'' fs''>8 <e'' g''>8 e''8 f''8 g''8 | % 11
  <a'' cs'''>8 <gs'' b''>8 <a'' cs'''>8 <e'' a''>8 <ds'' gs''>8 <e'' a''~>8 a''2. | % 12
  } \new Voice { \voiceTwo
  bf'2. a'2~ a'8 g'16 f'16 | % 11
  e'1. | % 12
  } >> \oneVoice
  f''8 d''8 a'8 <d'' f''>8 <a' d''>8 <f' a'>8 <a' d'' f''>8 <f' a' a''>8 <d' f' d'''>8 <cs' a' g''>8 <d' f' a'>8 <a cs'' g''>8 | % 13
  \ottava #1 a''8 gs''8 a''8 bf''8 a''8 bf''8 <a'' f'''>8 <gs'' e'''>8 <a'' f'''>8 <f'' d'''>8 <e'' cs'''>8 <f'' d'''>8 | % 14
  <bf'' e'''>8 <a'' ds'''>8 <bf'' e'''>8 <c''' g'''>8 <bf'' f'''>8 <c''' g'''>8 <bf'' d'''>8 <a'' cs'''>8 <bf'' d'''>8 <g'' bf''>8 <d'' g''>8 <bf' f''>8 | % 15
  \ottava #0 <a'' e''>8 e''8 d''8 c''8 b'8 a'8 g''8 d''8 c''8 bf'8 a'8 g'8 | % 16
  e''8 ds''8 e''8 <g'' e''>8 <fs'' ds''>8 <g'' e''>8 <bf'' g''>8 <a'' fs''>8 <bf'' g''>8 <cs''' a''>8 <b'' gs''>8 <cs''' a''>8 | % 17
  <<
  { \voiceOne
  <d''' f'''>2. } \new Voice { \voiceTwo
  a''8 gs''8 a''8 f''8 e''8 f''8 } >> \oneVoice
  d''8 cs''8 d''8 a'8 e''8 gs''8 | % 18
  a''8 gs''8 a''8 f''8 e''8 f''8 d''8 cs''8 d''8 a''8 d''8 g''8 | % 19
  bf''8 a''8 bf''8 g''8 fs''8 g''8 <d'' bf''>8 <cs'' a''>8 <d'' bf''>8 <bf' g''>8 <g' fs''>8 <d' g''>8 | % 20
  bf''8 a''8 bf''8 g''8 fs''8 g''8 <d'' bf''>8 <cs'' a''>8 <d'' bf''>8 <bf' g''>8 <g' fs''>8 <a' g''>8 | % 21
  e''8 ds''8 e''8 cs''8 b'8 cs''8 <a' e''>8 <gs' d''>8 <a' e''>8 <e' cs''>8 <cs' b'>8 <g' cs''>8 | % 22
  f''8 e''8 f''8 d''8 cs''8 d''8 <gs' gs''>8 <fs' fs''>8 <gs' gs''>8 <a' a''>8 <gs' gs''>8 <a' a''>8 | % 23
  <<
  { \voiceOne
  a''4 d'''4 a''4 c'''4 bf''4 a''4 | % 24
  g''4 a''4 bf''4 g''2 d''4 | % 25
  g''4 cs'''4 g''4 a''4 e''4 g''4 | % 26
  f''4 g''4 a''4 f''2. | % 27
  f''4 g''4 a''4 g''4 a''4 bf''4 | % 28
  \acciaccatura { gs''8 } a''4-. e''4 d''4 cs''4 d''4 e''4 | % 29
  a''4 g''2~ g''2. | % 30
  a'''8 gs'''8 a'''8 e'''8 d'''8 e'''8 cs'''8 b''8 cs'''8 a''8 gs''8 a''8 | % 31
  } \new Voice { \voiceTwo
  r2. a''8 gs''8 a''8 f''8 e''8 f''8 | % 24
  r2. d''8 df''8 d''8 bf'8 a'8 bf'8 | % 25
  r2. e''8 d''8 e''8 e'8 d'8 r8 | % 26
  r2. <a' a''>8 <gs' gs''>8 <a' a''>8 <f' f''>8 <d' d''>8 <a a'>8 | % 27
  r1 r4 g'8 f'8 | % 28
  <e' cs''>1. | % 29
  a'4 b'4 cs''4 g'4 a'4 bf'4 | % 30
  \acciaccatura { bs8 } cs'4 d'4 e'4 a4 b4 cs'4-. | % 31
  } >> \oneVoice
  d'''8 bf''8 a''8 bf''8 f''8 bf''8 a''8 gs''8 a''8 e''8 d''8 e''8 | % 32
  cs''8 b'8 cs''8 f'8 e'8 f'8 cs''8 b'8 cs''8 a'8 gs'8 a'8 | % 33
  e'8 ds'8 e'8 <cs'' a>8 <b' gs>8 <cs'' a>8 <a' e>8 <gs' ds>8 <a' e>8 <e' df>8 <ds' c>8 <e' df>8 | % 34
  <<
  { \voiceOne
  <d''' f'''>2. <d''' e'''>2 g'''4 | % 35
  <d''' f'''>2 a'''4 <d''' gs'''>4 <e''' a'''>4 <d''' as'''>4 | % 36
  <cs'''~ e'''~>2 <cs''' e'''>8 g'''16 a'''16 bf'''4 a'''4 g'''4 | % 37
  <a'' e'''>4 f'''16 e'''16 ds'''16 e'''16~ e'''4 e'''4 f'''4 g'''4 | % 38
  \arpeggioArrowUp<cs''' e''' a'''>2.\arpeggio <d''' g'''>2. | % 39
  \arpeggioArrowUp<d''' a'' f'''>2.\arpeggio <f'' d'''>2 <e'' e'''>4 | % 40
  f'''2. } \new Voice { \voiceTwo
  a''8 gs''8 a''8 f''8 e''8 f''8 d''8 cs''8 d''8 a'8 e''8 gs''8 | % 35
  a''8 gs''8 a''8 f''8 e''8 f''8 d''8 cs''8 d''8 a''8 d''8 g''8 | % 36
  bf''8 a''8 bf''8 g''8 fs''8 g''8 <d'' bf''>8 <cs'' a''>8 <d'' bf''>8 <bf' g''>8 <g' fs''>8 <d' g''>8 | % 37
  bf''8 a''8 bf''8 g''8 fs''8 g''8 <d'' bf''>8 <cs'' a''>8 <d'' bf''>8 <bf' g''>8 <g' fs''>8 <a' g''>8 | % 38
  e''8 ds''8 e''8 cs''8 b'8 cs''8 <a' e''>8 <gs' d''>8 <a' e''>8 <e' cs''>8 <cs' b'>8 <g' cs''>8 | % 39
  f''8 e''8 f''8 d''8 cs''8 d''8 <gs' gs''>8 <fs' fs''>8 <gs' gs''>8 <a' a''>8 <gs' gs''>8 <a' a''>8 | % 40
  f''8 d''8 cs''8 d''8 a'8 f'8 } >> \oneVoice
  g''8 e''8 cs''8 d''8 a'8 f'8 | % 41
  a''8 f''8 e''8 f''8 d''8 a'8 bf''8 g''8 fs''8 g''8 e''8 d''8 | % 42
  bf''8 g''8 fs''8 g''8 d''8 bf'8 a''8 f''8 e''8 f''8 d''8 a'8 | % 43
  a''8 e''8 d''8 e''8 cs''8 a'8 g''8 d''8 cs''8 d''8 bf'8 g'8 | % 44
  f''8 d''8 cs''8 d''8 a'8 f'8 f''8 d''8 cs''8 d''8 bf'8 d''8 | % 45
  e''8 cs''8 b'8 cs''8 a'8 cs''8 e'8 d''8 f'8 d''8 e'8 cs''8 | % 46
  <<
  { \voiceOne
  r4 d'''4 a''4 c'''4 bf''4 a''4 | % 47
  } \new Voice { \voiceTwo
  \arpeggioArrowUp<a' f' d''>1.\arpeggio | % 47
  } >> \oneVoice
  g''4 a''4 bf''4 g''2 d''4 | % 48
  g''4 cs'''4 g''4 a''4 e''4 g''4 | % 49
  <<
  { \voiceOne
  f''4 g''4 a''4 f''2. | % 50
  } \new Voice { \voiceTwo
  r2. <a' a''>8 <gs' gs''>8 <a' a''>8 <f' f''>8 <d' d''>8 <a a'>8 | % 50
  } >> \oneVoice
  f''4 g''4 a''4 g''4 a''4 bf''4 | % 51
  \acciaccatura { gs''8 } a''4-. e''4 d''4 cs''4 d''4 e''4 | % 52
  \acciaccatura { gs'8 } a'4 e'4 d'4 cs'4 d'4 e'4 | % 53
  <<
  { \voiceOne
  a'4 b'4 cs''4 f''4 e''4 g''4 | % 54
  } \new Voice { \voiceTwo
  a1. | % 54
  } >> \oneVoice
  <<
  { \voiceOne
  f''1. | % 55
  } \new Voice { \voiceTwo
  as4 c'4 d'4 g'4 f'4 c''8 bf'8 | % 55
  } >> \oneVoice
  <<
  { \voiceOne
  r2. f'''8 d'''8 c'''8 d'''8 c'''8 a''8 | % 56
  } \new Voice { \voiceTwo
  d''4 c''4 b'4 c''4 b'4 a'4 | % 56
  } \new Voice { \voiceFour
  a'1. | % 56
  } >> \oneVoice
  <<
  { \voiceOne
  g''8 a''8 b''8 g''8 f''8 e''8 d''8 f''8 d''8 c''8 d''8 g''8 | % 57
  } \new Voice { \voiceTwo
  b'4 a'4 g'4 a'4 f'4 g'4 | % 57
  } >> \oneVoice
  <<
  { \voiceOne
  a''8 f''8 e''8 f''8 d''8 f''8 g''8 e''8 d''8 e''8 r4 | % 58
  } \new Voice { \voiceTwo
  a'8 f'8 e'8 f'8 d'8 f'8 g'8 e'8 d'8 e'8 cs'8 e'8 | % 58
  } >> \oneVoice
  <<
  { \voiceOne
  a''8 a''8~ a''4 a''4 g''4 f''4 e''4 | % 59
  } \new Voice { \voiceTwo
  f'8 d'8 cs'8 d'8 a8 d'8 g'8 d'8 cs'8 d'8 as8 d'8 | % 59
  } >> \oneVoice
  <<
  { \voiceOne
  f''4 a''4 d'''4 e'''2 g'''4 | % 60
  } \new Voice { \voiceTwo
  a'8 f'8 e'8 f'8 d'8 a'8 bf'8 g'8 fs'8 g'8 d'8 g'8 | % 60
  } >> \oneVoice
  <<
  { \voiceOne
  e'''2 d'''8 e'''8 f'''4 e'''4 d'''4 | % 61
  } \new Voice { \voiceTwo
  cs''8 a'8 gs'8 a'8 e''8 g''8 a''8 e''8 d''8 e''8 a'8 e''8 | % 61
  } >> \oneVoice
  <<
  { \voiceOne
  f'''4 e'''4 d'''4 a''2 c'''8 bf''8 | % 62
  } \new Voice { \voiceTwo
  f''8 d''8 cs''8 d''8 a'8 d''8 g''8 d''8 cs''8 d''8 bf'8 d''8 | % 62
  } >> \oneVoice
  <<
  { \voiceOne
  r8 f''8 e''8 f''8 d''8 a''8 bf''8 g''8 fs''8 g''8 d''8 g''8 | % 63
  } \new Voice { \voiceTwo
  a''2 f''16 g''16 f''16 d''16 e''2 d''4 | % 63
  } >> \oneVoice
  <<
  { \voiceOne
  cs'''8 a''8 gs''8 a''8 e'''8 g'''8 a'''8 e'''8 d'''8 e'''8 a''8 e'''8 | % 64
  } \new Voice { \voiceTwo
  cs''4 d''4 e''4 bf'4 a'4-. g'4 | % 64
  } >> \oneVoice
  <<
  { \voiceOne
  a''4 d'''4 a''4 c'''4 bf''4 a''4 | % 65
  } \new Voice { \voiceTwo
  a''8 gs''8 a''8 f''8 e''8 f''8 <d'' a''>8 <cs'' gs''>8 <d'' a''>8 <a' f''>8 <gs' e''>8 <a' f''>8 | % 65
  } >> \oneVoice
  <<
  { \voiceOne
  r4 a''4 bf''4 g''2 d''4 | % 66
  } \new Voice { \voiceTwo
  g''8 fs''8 g''8 d''8 cs''8 d''8 <bf' g''>8 <a' fs''>8 <bf' g''>8 <g' d''>8 <fs' cs''>8 <g' d''>8 | % 66
  } >> \oneVoice
  <<
  { \voiceOne
  g''4 cs'''4 g''4 a''4 e''4 g''4 | % 67
  } \new Voice { \voiceTwo
  cs''8 b'8 cs''8 a'8 gs'8 a'8 <e'' cs''>8 <d'' bf'>8 <cs'' a'>8 <a' e'>8 <b' f'>8 <cs'' g'>8 | % 67
  } >> \oneVoice
  <<
  { \voiceOne
  f''4 g''4 a''4 r2. | % 68
  } \new Voice { \voiceTwo
  f''8 e''8 f''8 d''8 cs''8 d''8 <a'' f''>8 <gs'' e''>8 <a'' f''>8 <f'' a'>8 <d'' gs'>8 a'8 | % 68
  } >> \oneVoice
  <f'' a''>8 <e'' g''>8 <f'' a''>8 <d'' f''>8 <cs'' e''>8 <d'' f''>8 <e'' g''>8 <d'' fs''>8 <e'' g''>8 <a' e''>8 <gs' f''>8 <a' g''>16 gs''16 | % 69
  <<
  { \voiceOne
  <a'' cs'''>8 <gs'' b''>8 <a'' cs'''>8 <e'' a''>8 <ds'' gs''>8 <e'' a''>8 cs''8 cs'''8 d''8 d'''8 e''8 e'''8 | % 70
  } \new Voice { \voiceTwo
  e'2. a'8 gs'8 a'8 e'8 ds'8 e'8 | % 70
  } >> \oneVoice
  <f'' d''>8 <d'' d'''>8 a'8 <d'' f''>8 <a' d''>8 <f' a'>8 <d'' f''>8 <a' a''>8 <f' d'''>8 <a' g''>8 <f' a'>8 <cs'' g''>8 | % 71
  <<
  { \voiceOne
  \acciaccatura { a''8[ c'''8 e'''8 f'''8] } a'''1. } \new Voice { \voiceTwo
  <d'' a''>1. } >> \oneVoice
  \bar "|." 
} }

\new Staff = "down" {
\set Staff.pedalSustainStyle = #'mixed
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 6/4 \key d \minor \clef bass R4*6 | % 1
  R4*6 | % 2
  R4*6 | % 3
  R4*6 | % 4
  R4*6 | % 5
  R4*6 | % 6
  <f' d' d>1. | % 7
  <d g' as>1. | % 8
  <cs a g'>1. | % 9
  <d~ a~ f'>2. <d a>2. | % 10
  <as d' g,>2. <a e' e,>2. | % 11
  a,,2. r2. | % 12
  d,1. | % 13
  <d d'>1. | % 14
  <cs cs'>2. <g, g>2. | % 15
  <a, a>2. <as, bf>4. <e,~ e~>8 <e, e>4 | % 16
  <a, a>1. | % 17
  d,4-. <f d'>4-. <f d'>4-. a,,4-. <g cs'>4-. <g cs'>4-. | % 18
  d,4-. <f d'>4-. <f d'>4-. <<
  { \voiceOne
  r4 <g cs'>4 <f' a>4 | % 19
  } \new Voice { \voiceTwo
  bf,,2 a,,4 | % 19
  } >> \oneVoice
  bf,,4-. <g bf>4-. <g bf>4-. g,,4-. <e bf>4-. <e bf>4-. | % 20
  bf,,4-. <d bf>4-. <d bf>4-. g,4-. <bf c'>4-. <bf c'>4-. | % 21
  a,4-. <e df'>4-. <e df'>4-. e,4-. <f df'>4-. <f df'>4-. | % 22
  bf,4-. <f d'>4-. <f d'>4-. df4-. <e a>4-. <e bf>8 f,16 e,16 | % 23
  <<
  { \voiceOne
  r4 <a f'>4-. <gs e'>8 <a f'>8 r4 <d bf>4-. <d a>16 c'16 bf8 | % 24
  r4 <d' bf'>4-. <cs' a'>8 <d' bf'>8 r4 <as f'>4-. r4 | % 25
  r4 <g e'>4-. <d' f'>8 <e' gs'>8 r4 <a' f'>8 <bf' g'>8 r8 <e'~ gs'~>8 | % 26
  <e' gs'>4 <a f'>4-. <f d'>4-. r2. | % 27
  r4 <g bf>4-. r4 r4 <g bf>4-. } \new Voice { \voiceTwo
  d,4-. r2 g,2. | % 24
  g4-. r2 d4-. r4 d,4 | % 25
  cs,4-. a4-. r4 a,,2. | % 26
  d,4-. r2 r2. | % 27
  g,4-. d4-. g,4 gs,4-. d4-. } >> \oneVoice
  r4 | % 28
  a,1. | % 29
  <f' d'' as,>1. | % 30
  <e' cs'' e''>4 a,4-. <cs' e' a'>4-. a'4-. <b' b e' g'>4 <cs'' a cs' f'>4 | % 31
  <a, e'>4-. <cs' e' a'>4-. r4 a,4-. <cs' e' a'>4-. r4 | % 32
  \acciaccatura { af,8 } g,4-. <b e' gs'>4-. a,4-. <cs' e' a'>4-. <cs' e' a'>4-. a,,4-. | % 33
  <cs' e' a'>4-. r2 a,,4 b,,4 cs,4 | % 34
  d,4-. <f d'>4-. <f d'>4-. a,,4-. <g cs'>4-. <g cs'>4-. | % 35
  d,4-. <f d'>4-. <f d'>4-. <<
  { \voiceOne
  r4 <g cs'>4 <f' a>4 | % 36
  } \new Voice { \voiceTwo
  bf,,2 a,,4 | % 36
  } >> \oneVoice
  bf,,4-. <g as>4-. <g as>4-. g,,4-. <e as>4-. <e as>4-. | % 37
  as,,4-. <d as>4-. <d as>4-. g,4-. <as c'>4-. <as c'>4-. | % 38
  a,4-. <e cs'>4-. <e cs'>4-. e,4-. <f cs'>4-. <f cs'>4-. | % 39
  as,4-. <f d'>4-. <f d'>4-. cs4-. <e a>4-. <e as>4-. | % 40
  \clef treble d'2. e'2 cs'4 | % 41
  d'4 e'4 f'4 bf'4 a'4 g'4 | % 42
  g4 a4 bf4 a'4 g'4 f'4 | % 43
  \clef bass e4 f4 g4 bf4 a4 g4 | % 44
  <<
  { \voiceOne
  \clef treble d'2. f'2 g'4 | % 45
  a'2. g'2 bf'4 | % 46
  \clef bass r4 <a f'>4-. <gs e'>8 <a f'>8 r4 <d bf>4-. <d a>16 c'16 bf8 | % 47
  r4 <d' bf'>4-. <cs' a'>8 <d' bf'>8 r4 <as f'>4-. r4 | % 48
  r4 <g e'>4-. <d' f'>8 <e' gs'>8 r4 <a' f'>8 <bf' g'>8 r8 <e'~ gs'~>8 | % 49
  <e' gs'>4 <a f'>4-. <f d'>4-. r2. | % 50
  r4 <g bf>4-. r4 r4 <g bf>4-. r4 | % 51
  r4 <a e g>4-. r4 <as, g d>4-. r4 <g, e g>4 | % 52
  } \new Voice { \voiceTwo
  a2. bf2. | % 45
  cs'4 d'4 e'4 a2. | % 46
  d,4-. r2 g,2. | % 47
  g4-. r2 d4-. r4 d,4 | % 48
  cs,4-. r2 a,,2. | % 49
  d,4-. r2 r2. | % 50
  g,4-. d4-. g,4 gs,4-. d4-. gs,4 | % 51
  a,1. | % 52
  } >> \oneVoice
  <a, e cs>1. | % 53
  \arpeggioArrowUp<a, f d'>1.\arpeggio | % 54
  \arpeggioArrowUp<g, d as>1.\arpeggio | % 55
  <<
  { \voiceOne
  d'2. f'2. | % 56
  g'2. e'4 d'4 b4 | % 57
  a2. g2 e4 | % 58
  r4 <a f'>4-. <a f'>4-. r4 <as g' e'>4-. <as g' e'>4-. | % 59
  r4 <a f'>4-. <a f'>4-. r4 <cs' bf'>4-. <bf' cs'>4-. | % 60
  a2. as2 cs'4 | % 61
  \clef treble r4 <a' f''>4-. <a' f''>4-. r4 <bf' g'' e''>4-. <bf' g'' e''>4-. | % 62
  r4 <a' f''>4-. <a' f''>4-. r4 <cs'' bf''>4-. <bf'' cs''>4-. | % 63
  a'2. cs'4 d'4-. e'4 | % 64
  } \new Voice { \voiceTwo
  <d, a, f>1. | % 56
  d,1~ d,4 c,4 | % 57
  d,1. | % 58
  d2. e2 g4 | % 59
  f2. g2 as4 | % 60
  r4 <a, e>4-. <a, e>4-. r4 <g, d>4-. <g, d>4-. | % 61
  d'2. e'2 g'4 | % 62
  f'2. g'2 bf'4 | % 63
  r4 <a e'>4-. <a e'>4-. r2. | % 64
  } >> \oneVoice
  \clef bass <f' d' d>1. | % 65
  <d g' as>1. | % 66
  <cs a g'>1. | % 67
  <d a f'>2. \clef treble a'8 gs'8 a'8 f'8 d'8 a8 | % 68
  <as d' bf'>2. <a~ e'~ a'~>2 <a e' a'>8 g'16 f'16 | % 69
  \clef bass a,,2. <<
  { \voiceOne
  r2. | % 70
  r2. a'8 f'8 d'8 cs'8 d'8 a8 | % 71
  d1. } \new Voice { \voiceTwo
  r2. | % 70
  d,1.~ | % 71
  d,1. } >> \oneVoice
  \bar "|." 
} }

>>

>>
\header { piece = "II" }
}

