\version "2.14.2"
\language "english"
\header { title = "b3" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\score {
<<
\new PianoStaff <<
\set PianoStaff.instrumentName = "piano"
\set PianoStaff.shortInstrumentName = ""
\new Staff = "up" {
\set Staff.pedalSustainStyle = #'mixed
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 4/4 \key c \major <<
  { \voiceOne
  d''4 e''4 a''4 r4 | % 1
  g''4 d''4 e''4 a''4 | % 2
  r4 g''8 es''8 d''4 e''4 | % 3
  a''4 d''4 g''4 d''4 | % 4
  e''4 a''4 d''4 g''4 | % 5
  es''4 g''4 bs''4 es''4 | % 6
  as''4 es''4 g''4 bs''4 | % 7
  d'''4 e'''4 } \new Voice { \voiceTwo
  <e'' a'>2. <g'~ d''~>4 | % 1
  <g' d''>4 <a' e''>2. | % 2
  <es' d''>4 r4 <e'~ a'~>2 | % 3
  <e' a'>4 <d' g'>2 <a e'>4 | % 4
  <e bs>8 <g d'>8 <a e'>4 <a es'>4 <b g'>4 | % 5
  <bs a'>2. <as~ g'~>4 | % 6
  <as g'>4 <d' a'>2. | % 7
  bs8 g'4. } >> \oneVoice
  r2 \bar "|." } }
\new Staff = "down" {
\set Staff.pedalSustainStyle = #'mixed
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 4/4 \key c \major \ottava #-1 a,4 \sustainOn e4 bs4 <e b>4 | % 1
  a4 a,4 \sustainOff\sustainOn e4 d'4 | % 2
  <es b>4 bs8 g,8 a,4 \sustainOff\sustainOn e4 | % 3
  bs4 b4 a4 a,4 \sustainOff\sustainOn | % 4
  e4 d'4 es4 bs,8 g,8 | % 5
  d,4 \sustainOff\sustainOn as,4 as4 es4 | % 6
  ds4 as,4 \sustainOff\sustainOn es4 es'4 | % 7
  <a, e'>4 d'4 r2 \bar "|." } }
>>
>>
}

