\version "2.14.2"
\language "english"
\header { title = "b1" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\score {
<<
\new PianoStaff <<
\set PianoStaff.instrumentName = "piano"
\set PianoStaff.shortInstrumentName = ""
\new Staff = "up" {
\set Staff.pedalSustainStyle = #'mixed
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 4/4 \key c \major \ottava #1 <a''' a''>4 <e''' e''>8 <fs''' fs''>8 <g''' g''>4 <a''' a''>4 | % 1
  <b''' b''>4 <bs''' bs''>4 <a''' a''>8 <b''' b''>8 <bs''' bs''>4 | % 2
  <d'''' d'''>4 <e'''' e'''>4 <e'''' e'''>4 <g''' bs''>4 | % 3
  <e''' g''>4 <ds''' fs''>4 <b'' gs''>4 \ottava #0 <bs'' a''>4 | % 4
  <a'' e''>4 <e'' bs'>4 <a'' b'>4 <e'' a'>4 | % 5
  <b'' g''>4 <a'' d''>4 <d'' b'>4 <a'' a'>4 | % 6
  <d'' g'>4 <bs'' a'>4 <a'' es'>4 <es'' a'>4 | % 7
  <d''' gs'>4 <e'' a'>4 r2 \bar "|." } }
\new Staff = "down" {
\set Staff.pedalSustainStyle = #'mixed
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 4/4 \key c \major \arpeggioArrowUp<cs'~ e'~ a'~>1\arpeggio | % 1
  <cs' e' a'>4 <bs~ es'~ a'~>2. | % 2
  <bs es' a'>4 r8 \arpeggioArrowUp<e'~ bs~ g'~>8\arpeggio <e'~ bs~ g'~>2 | % 3
  <e' bs g'>8 r8 <ds'~ fs'~ b~>8 <ds' fs' b>32 r32 r16 <e'~ gs'~ b~>8 <e' gs' b>32 r32 r16 <a~ e'~ bs'~>4 | % 4
  <a e' bs'>1 | % 5
  <e'~ g~ b'~>1 | % 6
  <e' g b'>4 \arpeggioArrowUp<es d' d''>2.\arpeggio | % 7
  <e b d'>2 r2 \bar "|." } }
>>
>>
}

