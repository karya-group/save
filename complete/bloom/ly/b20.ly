\version "2.14.2"
\language "english"
\header { title = "b20" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\score {
<<
\new PianoStaff <<
\set PianoStaff.instrumentName = "piano"
\set PianoStaff.shortInstrumentName = ""
\new Staff = "up" {
\set Staff.pedalSustainStyle = #'mixed
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 4/4 \key c \major <<
  { \voiceOne
  f'''2. } \new Voice { \voiceTwo
  f''8 d''8 cs''8 d''8 a'8 f'8 } >> \oneVoice
  g''8 e''8 | % 1
  cs''8 d''8 a'8 f'8 a''8 f''8 e''8 f''8 | % 2
  d''8 a'8 bf''8 g''8 fs''8 g''8 e''8 d''8 | % 3
  bf''8 g''8 fs''8 g''8 d''8 bf'8 a''8 f''8 | % 4
  e''8 f''8 d''8 a'8 a''8 e''8 d''8 e''8 | % 5
  cs''8 a'8 g''8 d''8 cs''8 d''8 bf'8 g'8 | % 6
  f''8 d''8 cs''8 d''8 a'8 f'8 f''8 d''8 | % 7
  cs''8 d''8 bf'8 d''8 e''8 cs''8 b'8 cs''8 | % 8
  a'8 cs''8 e'8 d''8 f'8 d''8 e'8 cs''8 \bar "|." 
} }

\new Staff = "down" {
\set Staff.pedalSustainStyle = #'mixed
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 4/4 \key c \major \clef treble d'2. e'4~ | % 1
  e'4 cs'4 d'4 e'4 | % 2
  f'4 bf'4 a'4 g'4 | % 3
  g4 a4 bf4 a'4 | % 4
  g'4 f'4 \clef bass e4 f4 | % 5
  g4 bf4 a4 g4 | % 6
  <<
  { \voiceOne
  \clef treble d'2. f'4~ | % 7
  f'4 g'4 a'2~ | % 8
  a'4 g'2 bf'4 } \new Voice { \voiceTwo
  a2. bf4~ | % 7
  bf2 cs'4 d'4 | % 8
  e'4 a2. } >> \oneVoice
  \bar "|." 
} }

>>

>>
}

