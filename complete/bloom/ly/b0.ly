\version "2.14.2"
\language "english"
\header { title = "b0" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\score {
<<
\new PianoStaff <<
\set PianoStaff.instrumentName = "piano"
\set PianoStaff.shortInstrumentName = ""
\new Staff = "up" {
\set Staff.pedalSustainStyle = #'mixed
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 4/4 \key c \major r8 a''4. e''2 | % 1
  fs''4 \acciaccatura { a''8 } g''4 <d''' g''>4 <d''' g''>4 | % 2
  <d''' g''>4 <c''' f''>4 e''4 cs''4 | % 3
  a'4 e''4 g''4 d'''4 | % 4
  bs''4 g''4 a''4 b''4 | % 5
  <e' e''>4 <b' e''>4 <g'' e''>4 <b'' e''>4 | % 6
  <d''' d''>4 <a' c''' c''>4 a'4 <c'' f'>4 | % 7
  <g'' c''>4 <b'' f''>4 r2 \bar "|." } }
\new Staff = "down" {
\set Staff.pedalSustainStyle = #'mixed
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 4/4 \key c \major \arpeggioArrowUp<cs'~ e'~ a'~>1\arpeggio | % 1
  <cs' e' a'>4 \arpeggioArrowUp<c'~ f'~ a'~>2.\arpeggio | % 2
  <c' f' a'>2 \arpeggioArrowUp<df'~ e'~ a'~>2\arpeggio | % 3
  <df' e' a'>2. <c'~ e'~ g'~>4 | % 4
  <c' e' g'>1 | % 5
  \arpeggioArrowUp<e'~ b~ g'~>1\arpeggio \sustainOn | % 6
  <e' b g'>4 <e'~ f'~ a~>2. \sustainOff\sustainOn | % 7
  <e' f' a>2 \sustainOff r2 \bar "|." } }
>>
>>
}

