\version "2.14.2"
\language "english"
\header { title = "b13" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\score {
<<
\new Staff  {
\set Staff.instrumentName = "piano"
\set Staff.pedalSustainStyle = #'mixed
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 4/4 \key c \major a''8 gs''8 a''8 f''8 e''8 f''8 d''8 cs''8 | % 1
  d''8 a'8 e''8 gs''8 a''8 gs''8 a''8 f''8 | % 2
  e''8 f''8 d''8 cs''8 d''8 a''8 d''8 g''8 | % 3
  bf''8 a''8 bf''8 g''8 fs''8 g''8 d''8 cs''8 | % 4
  d''8 bf'8 g'8 d'8 bf''8 a''8 bf''8 g''8 | % 5
  fs''8 g''8 d''8 cs''8 d''8 bf'8 g'8 a'8 | % 6
  e''8 ds''8 e''8 cs''8 b'8 cs''8 a'8 gs'8 | % 7
  a'8 a8 cs'8 g'8 f''8 e''8 f''8 d''8 | % 8
  cs''8 d''8 bf'8 a'8 bf'8 f''8 g''8 gs''8 \bar "|." 
} }

>>
}

