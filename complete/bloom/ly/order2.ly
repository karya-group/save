\version "2.14.2"
\language "english"
\header { title = "order2" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\score {
<<
\new PianoStaff <<
\set PianoStaff.instrumentName = "piano"
\set PianoStaff.shortInstrumentName = ""
\new Staff = "up" {
\set Staff.pedalSustainStyle = #'mixed
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 5/4 \key a \minor r8 a''8~ a''4 e''4~ e''4 fs''4 | % 1
  \acciaccatura { a''8 } g''4 <d''' g''>4 <d''' g''>4 <d''' g''>4 <c''' f''>4 | % 2
  e''4 cs''4 a'4 e''4 g''4 | % 3
  d'''4 c'''4 g''4 a''4 b''4 | % 4
  <e' e''>4 <b' e''>4 <g'' e''>4 <b'' e''>4 <d''' d''>4 | % 5
  <a' c''' c''>4 a'4 <c'' f'>4 <g'' c''>4 <b'' f''>4 | % 6
  \ottava #1 <a''' a''>4 <e''' e''>8 <fs''' fs''>8 <g''' g''>4 <a''' a''>4 <b''' b''>4 | % 7
  \ottava #2 <c'''' c'''>4 <a''' a''>8 <b''' b''>8 <c'''' c'''>4 <d'''' d'''>4 <e'''' e'''>4 | % 8
  <e'''' e'''>4 \ottava #1 <g''' c'''>4 <e''' g''>4 <ds''' fs''>4 \ottava #0 <b'' gs''>4 | % 9
  <c''' a''>4 <a'' e''>4 <e'' c''>4 <a'' b'>4 <e'' a'>4 | % 10
  <b'' g''>4 <a'' d''>4 <d'' b'>4 <a'' a'>4 <d'' g'>4 | % 11
  <c''' a'>4 <a'' f'>4 <f'' a'>4 <d''' gs'>4 <e'' a'>4 | % 12
  c'''2. b''4 a''4~ | % 13
  a''4 e''4 c''4 b'2 | % 14
  a'4 e'4 e''4 d''4 c''4 | % 15
  <e'' c'''>4 <c'' a''>8 <d'' b''>8 <e'' c'''>4 <f'' d''' b''>4 <g'' e''' a''>4 | % 16
  <a'' f'''>4 <f'' d'''>8 <g'' e'''>8 <a'' f''' c'''>4 <as'' g'''>4 <c''' a''' a''>8 r8 | % 17
  <c''' a'''>4 <a'' f'''>4 <f'' d'''>4 <g'' cs'''>4 <a'' d'''>4 | % 18
  <<
  { \voiceOne
  d''4 e''4 a''4 r4 g''4 | % 19
  d''4 e''4 a''4 r4 g''8 f''8 | % 20
  d''4 e''4 a''4 d''4 g''4 | % 21
  d''4 e''4 a''4 d''4 g''4 | % 22
  f''4 g''4 c'''4 f''4 as''4 | % 23
  f''4 g''4 c'''4 d'''4 e'''4 | % 24
  a''4 b''4 e'''4 a''4 d'''4 | % 25
  a''4 b''4 f'''4 a''4 d'''4 | % 26
  a''4 b''4 e'''4 a''4 d'''4 | % 27
  a''4 b''4 g'''4 a''4 f'''4 | % 28
  e''4 <b'' d'>4 <e''' g'>4 <e'' c''' c''>4 <b'' b'>4 | % 29
  <d'' a'' a'>2 <g'' g'>4 <c''' c''>4 <b'' b'>4 | % 30
  a''4 e''8-. fs''8-. g''4 a''4 b''4 | % 31
  c'''4 a''8-. b''8-. c'''4 d'''4 e'''4-. | % 32
  e'''4 c'''4 g''4 fs''4-. gs''4-. | % 33
  <c''' a''>2 \acciaccatura { b''8[ c'''8] } b''4 g''4 f''4 | % 34
  e''4 d''4 f''4 e''4 d''4 | % 35
  c''4 b'4 g''4 e''4 gs'4 | % 36
  } \new Voice { \voiceTwo
  <e'' a'>2. <g' d''>2 | % 19
  <a' e''>2. <f' d''>4 r4 | % 20
  <e' a'>2. <d' g'>2 | % 21
  <a e'>4 <e c'>8 <g d'>8 <a e'>4 <a f'>4 <b g'>4 | % 22
  <c' a'>2. <as g'>2 | % 23
  <d' a'>2. c'8 g'8~ g'4 | % 24
  \arpeggioArrowUp<cs'~ e'~ a'~>2.\arpeggio <cs' e' a'>2 | % 25
  <c'~ f'~ a'~>2. <c' f' a'>2 | % 26
  \arpeggioArrowUp<cs'~ e'~ a'~>2.\arpeggio <cs' e' a'>2 | % 27
  <c'~ e'~ g'~>2. <c' e' g'>2 | % 28
  <e'~ b~ g'~>2. <e' b g'>2 | % 29
  \arpeggioArrowUp<e'~ f'~ a~>2.\arpeggio <e' f' a>2 | % 30
  \arpeggioArrowUp<cs'~ e'~ a'~>2.\arpeggio <cs' e' a'>2 | % 31
  <c'~ f'~ a'~>2. <c' f' a'>2 | % 32
  \arpeggioArrowUp<e' c' g'>2.\arpeggio <ds' fs' b>4-. <e' gs' b>4-. | % 33
  <a~ e'~ c''~>2. <a e' c''>2 | % 34
  <e'~ g~ b'~>2. <e' g b'>2 | % 35
  R2. R2 | % 36
  } >> \oneVoice
  a'4 c''4 <b' e'>4 g'4 <e'' a'>4 | % 37
  <g'' b'>2 d''4 b'4 a'4 | % 38
  <e'' cs''>4 fs''2 <cs'' gs'>8 <d'' a'>8 <cs'' gs'>4 | % 39
  <g'' b'>4 <c'' a''>2 e''4 d''16 e''16 d''8 | % 40
  <g' b'>4 a'4 <d'~ e'~>4 <d' e'>4 <c' g'>4 | % 41
  <ds' a'>4 <fs' fs''>4 <a' a''>4 <c'' c'''>4 <fs'' fs'''>4 | % 42
  <a'' a'''>4 <a g'>4 e'4 as'8 fs'8 g4 | % 43
  <a'' a'''>4 <b'' a'>8 a'''8 e'''4 <ds''' ds'' a'''>8 fs'8 g4 | % 44
  <cs' a'>4 <a fs'>4 <e' cs''>4 <d' a'>4 <ds' c''>4 | % 45
  <a'' a'''>4 <a g'>4 e'4 as'8 fs'8 g4 | % 46
  <a'' a'''>4 <a g'>8 a''8 e'4 <as' a''>8 fs'8 g4 | % 47
  as'8 fs'8 g2~ g2 | % 48
  <<
  { \voiceOne
  R2. R2 | % 49
  R2. R2 | % 50
  R2. R2 | % 51
  r2 c'''4 c'''2 | % 52
  c'''4 c'''2 c'''4 c'''4 | % 53
  b''4 b''2 b''4 b''4 | % 54
  } \new Voice { \voiceTwo
  c''4 a'8-. b'8-. c''4 d''4 e''4 | % 49
  f''4 d''8-. e''8-. f''4 g''4 a''4-. | % 50
  a''4 f''4 d''4 c''4 ds''4 | % 51
  e''2.~ e''2~ | % 52
  e''2.~ e''2 | % 53
  d''2.~ d''2 | % 54
  } >> \oneVoice
  r4 d'''4 d'''4 c'''4 c'''4 | % 55
  b''4 c'''4 d'''4 e'''4 c'''4 | % 56
  b''4 e''4 d''4 a''4 c''4 | % 57
  <b'' e''>4 <e'' c''>4 <d'' a'>4 <a'' d''>4 <c'' g'>4 | % 58
  <e'~ b'~>2. <e' b'~>2 | % 59
  <b' b''>4 e'''2 b'''4 g''8 f''8 | % 60
  <<
  { \voiceOne
  a''4 e''4 fs''4 a''4 b''4 | % 61
  c'''4 g''4 d''4 d'''8 c'''8~ c'''4 | % 62
  a''4 e''4 fs''4 a''4 b''4 | % 63
  a''4 g''4 d''4 e''4 c''4 | % 64
  b'4 r2 r2 | % 65
  R2. R2 | % 66
  a'4 c''4 b'4 g'4 e''4 | % 67
  g''2 d''4 b'4 e''4 | % 68
  a''4 e''8-. g''8-. a''4 b''4 c'''4 | % 69
  d'''4 g''8-. c'''8-. d'''4 e'''4 f'''4 | % 70
  <e'' c''' e'''>4 <c'' a''>8-. <d'' b''>8-. <e'' c'''>4 <f'' d'''>4 <g'' e'''>4 | % 71
  <a'' f'''>4 <f'' d'''>8-. <g'' e'''>8-. <a'' f'''>4 <as'' g'''>4 <c''' a'''>4 | % 72
  } \new Voice { \voiceTwo
  e''4 cs''4 a'4 fs'4 d'4 | % 61
  R2. R2 | % 62
  R2. R2 | % 63
  r2. r4 g'4 | % 64
  e'4 g4 a4 c'4 b4 | % 65
  a2.~ a2 | % 66
  r2 e'4~ e'4 a'4 | % 67
  b'2 r4 r4 a'4 | % 68
  c''4 r2 r2 | % 69
  R2. R2 | % 70
  r2 a4 f8 g8 a4 | % 71
  as4 c'4 d'4 as8 c'8 d'4 | % 72
  } >> \oneVoice
  <a'' a''' e'>4 f'4 c''4 c'''4 f''4 | % 73
  c''4 g'4 d'4 \clef bass g4 d4 | % 74
  a,4 g,4 d,4~ d,2 \bar "|." } }
\new Staff = "down" {
\set Staff.pedalSustainStyle = #'mixed
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 5/4 \key a \minor \arpeggioArrowUp<cs'~ e'~ a'~>2.\arpeggio <cs' e' a'>2 | % 1
  \arpeggioArrowUp<c'~ f'~ a'~>2.\arpeggio <c' f' a'>2 | % 2
  \arpeggioArrowUp<cs'~ e'~ a'~>2.\arpeggio <cs' e' a'>2 | % 3
  <c'~ e'~ g'~>2. <c' e' g'>2 | % 4
  \arpeggioArrowUp<e'~ b~ g'~>2.\arpeggio \sustainOn <e' b g'>2 | % 5
  <e'~ f'~ a~>2. \sustainOff\sustainOn <e' f' a>2 \sustainOff | % 6
  \arpeggioArrowUp<cs'~ e'~ a'~>2.\arpeggio <cs' e' a'>2 | % 7
  <c'~ f'~ a'~>2. <c' f' a'>4. \arpeggioArrowUp<e'~ c'~ g'~>8\arpeggio | % 8
  <e' c' g'>2. <ds' fs' b>4-. <e' gs' b>4-. | % 9
  <a~ e'~ c''~>2. <a e' c''>2 | % 10
  <e'~ g~ b'~>2. <e' g b'>2 | % 11
  \arpeggioArrowUp<f d' d''>2.\arpeggio \clef bass <e b d'>2 | % 12
  \clef bass a,4 \sustainOn e4 c'4 b4 a4 | % 13
  a,4 e4 d'4 f4 c8 g,8 | % 14
  a,4 e4 c'4 \sustainOff b4 a4 | % 15
  a,4 \sustainOn e4 d'4 f4 c8 g,8 | % 16
  d,4 \sustainOff\sustainOn as,4 g4 f4 e4 | % 17
  as,4 \sustainOff\sustainOn f4 f'4 <a, e'>4 d'4 | % 18
  a,4 \sustainOff\sustainOn e4 c'4 <e b>4 a4 | % 19
  a,4 \sustainOff\sustainOn e4 d'4 <f b>4 c'8 g,8 | % 20
  a,4 \sustainOff\sustainOn e4 c'4 b4 a4 | % 21
  a,4 \sustainOff\sustainOn e4 d'4 f4 c8 g,8 | % 22
  d,4 \sustainOff\sustainOn as,4 as4 f4 ds4 | % 23
  as,4 \sustainOff\sustainOn f4 f'4 <a, e'>4 d'4 \sustainOff | % 24
  <<
  { \voiceOne
  r2. a4 g4 | % 25
  f2. e4 d4 | % 26
  cs2. d4 e4 | % 27
  g4 f4-. e4 d4-. e4-. | % 28
  R2. R2 | % 29
  R2. R2 | % 30
  } \new Voice { \voiceTwo
  a,,2.~ a,,2~ | % 25
  a,,2.~ a,,4 g,4 | % 26
  a,,2.~ a,,2 | % 27
  c,2.~ c,2 | % 28
  e,2.~ e,2 | % 29
  r4 d,2~ d,4 g,4 | % 30
  } >> \oneVoice
  a,2.~ a,2~ | % 31
  a,2.~ a,4 g,4-. | % 32
  f,2. ds,4-. e,4-. | % 33
  a,2.~ a,2~ | % 34
  a,2.~ a,2 | % 35
  <<
  \new Voice { \voiceTwo
  \arpeggioArrowUp<f d' f'>2.\arpeggio \arpeggioArrowUp<e b d'>2\arpeggio | % 36
  } >> \oneVoice
  a,4 \sustainOn e4 c'4 b4 a4 | % 37
  a,4 \sustainOff\sustainOn e4 b4 g4 a8 g,8 | % 38
  fs,4 \sustainOff\sustainOn cs4 a4 gs4 fs4 | % 39
  a,4 \sustainOff\sustainOn e4 c'4 b4 a4 | % 40
  a,4 \sustainOff\sustainOn e4 b4 g4 a8 g,8 | % 41
  fs,4 \sustainOff\sustainOn cs4 a4 gs4 fs4 \sustainOff | % 42
  a,4 \sustainOn e4 c'4 ds'4 a4 | % 43
  a,4 \sustainOff\sustainOn e4 b4 g4 a8 g,8 | % 44
  fs,4 \sustainOff\sustainOn cs4 a4 gs4 fs4 | % 45
  a,4 \sustainOff\sustainOn e4 c'4 ds'4 <g, a>4 | % 46
  a,2. \sustainOff\sustainOn g,2~ | % 47
  g,2 \sustainOff\sustainOn \clef treble d'4 g'4 <as' ds'>4 \sustainOff | % 48
  \clef treble <a e' a'>2 <e'~ b'~ g''~>4 <e' b' g''>2 | % 49
  <a f' c'' f>2. \clef bass e4 d4-. | % 50
  <f' c>2. <b~ d' a,>8 <b e'~ b,~>8 <e' b, e>4 | % 51
  \arpeggioArrowUp<a,~ a~ e'~>2.\arpeggio \sustainOn <a,~ a~ e'~>2 | % 52
  <a,~ a~ e'~>2. <a, a e'>2 | % 53
  \arpeggioArrowUp<a,~ f'~ a'~>2.\arpeggio \sustainOff\sustainOn <a, f' a'>4 e'4 \sustainOff | % 54
  d'4 f'4 g'4 <g e' d'>2 | % 55
  <f a d' g'>2. <e b g' b'>2 | % 56
  \arpeggioArrowUp<a~ e'~ e''~ a,~>2.\arpeggio <a e' e'' a,>2 | % 57
  <a~ f'~ f''~>2. <a f' f''>2 | % 58
  \arpeggioArrowUp<a,~ e''~ a~>2.\arpeggio <a,~ e''~ a~>2 | % 59
  <a,~ e''~ a~>2. <a, e'' a>2 | % 60
  <<
  { \voiceOne
  \arpeggioArrowUp<cs'~ e'~ a'~>2.\arpeggio <cs' e' a'>2 | % 61
  <c'~ f'~ a'~>2. <c' f' a'>2 | % 62
  \arpeggioArrowUp<cs'~ e'~ a'~>2.\arpeggio <cs' e' a'>2 | % 63
  <c'~ e'~ g'~>2. <c' e' g'>2 | % 64
  <e' b g'>2 <c'~ f'~ a'~>4 <c' f' a'>4 <g' d' g>4 | % 65
  \arpeggioArrowUp<e~ a~ e'~>2.\arpeggio <e a e'>2 | % 66
  } \new Voice { \voiceTwo
  R2. R2 | % 61
  r4 a4 f4 d4 c4 | % 62
  cs4 a,4 cs4 e4 a4 | % 63
  g2 g,4 g4 r4 | % 64
  R2. R2 | % 65
  r4 a,4 a,,4~ a,,2 | % 66
  } >> \oneVoice
  a,4 \sustainOn e'4 g'4 b4 d'4 | % 67
  a,4 \sustainOff\sustainOn g4 d'4 a4 f4 | % 68
  <a~ a,~ e'~ e~>2. \sustainOff <a a, e' e>2 | % 69
  <f,~ c~ g~ c'~>2. <f, c g c'>4 e,4 | % 70
  <a,~ a~ e'~ a'~>2. <a, a e' a'>2 | % 71
  d,4 \sustainOn as,4 g4 f4 <e, e>4 | % 72
  a,4 \sustainOff\sustainOn a,,2~ a,,2~ | % 73
  a,,2.~ a,,2~ | % 74
  a,,2.~ a,,2 \bar "|." } }
>>
>>
}

