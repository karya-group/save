\version "2.18.2"
\language "english"
\pointAndClickOff
\include "ly/lib.ily"
\header { title = "b2" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\bookpart {
\score {
<<
\new Staff  {
\set Staff.instrumentName = "vln1"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 3/4 \key c \major R4*3 | % 1
 R4*3 | % 2
 R4*3 | % 3
 R4*3 | % 4
 R4*3 | % 5
 R4*3 | % 6
 R4*3 | % 7
 r2 c''4 \p | % untitled/b2 untitled/b2.t2 23-24; 8
 c'''4( f''4 e''4 | % untitled/b2 untitled/b2.t2 26-27; 9
 d''2) bf''4( | % untitled/b2 untitled/b2.t2 29-30; 10
 c''4) a''4( g''4 | % untitled/b2 untitled/b2.t2 32-33; 11
 bf'4) g''4( e''4) | % untitled/b2 untitled/b2.t2 35-35.95; 12
 a''2.~ | % untitled/b2 untitled/b2.t2 36-47; 13
 a''2.~ | % untitled/b2 untitled/b2.t2 36-47; 14
 a''2.~ | % untitled/b2 untitled/b2.t2 36-47; 15
 a''2 g''4( | % untitled/b2 untitled/b2.t2 47-48; 16
 c'''4 f''4 e''4) | % untitled/b2 untitled/b2.t2 50-51; 17
 bf''4( e''4 d''4) | % untitled/b2 untitled/b2.t2 53-54; 18
 a''2.~ | % untitled/b2 untitled/b2.t2 54-59; 19
 a''2 r4 | % untitled/b2 untitled/b2.t2 54-59; 20
 d'2.~ | % untitled/b2 untitled/b2.t2 60-87; 21
 d'2.~ | % untitled/b2 untitled/b2.t2 60-87; 22
 d'2.~ | % untitled/b2 untitled/b2.t2 60-87; 23
 d'2.~ | % untitled/b2 untitled/b2.t2 60-87; 24
 d'2.~ | % untitled/b2 untitled/b2.t2 60-87; 25
 d'2.~ | % untitled/b2 untitled/b2.t2 60-87; 26
 d'2.~ | % untitled/b2 untitled/b2.t2 60-87; 27
 d'2.~ | % untitled/b2 untitled/b2.t2 60-87; 28
 d'2. | % untitled/b2 untitled/b2.t2 60-87; 29
 d'2. \trill | % untitled/b2 untitled/b2.t2 87-90; 30
 d'2. \trill | % untitled/b2 untitled/b2.t2 90-93; 31
 \repeat tremolo 12 { d'32( f'32) } | % untitled/b2 untitled/b2.t2 90-93; 32
 \repeat tremolo 12 { d'32( f'32) } | % untitled/b2 untitled/b2.t2 90-93; 33
 d'2. \trill | % untitled/b2 untitled/b2.t2 99-102; 34
 d'2. \trill | % untitled/b2 untitled/b2.t2 102-105; 35
 d'2. \trill | % untitled/b2 untitled/b2.t2 105-108; 36
 \repeat tremolo 12 { d'32( f'32) } | % untitled/b2 untitled/b2.t2 105-108; 37
 \repeat tremolo 12 { d'32( f'32) } | % untitled/b2 untitled/b2.t2 105-108; 38
 \repeat tremolo 12 { d'32( f'32) } | % untitled/b2 untitled/b2.t2 105-108; 39
 R4*3 | % untitled/b2 untitled/b2.t2 105-108; 40
 R4*3 | % untitled/b2 untitled/b2.t2 105-108; 41
 R4*3 | % untitled/b2 untitled/b2.t2 105-108; 42
 R4*3 | % untitled/b2 untitled/b2.t2 105-108; 43
 r4. a'''8~ a'''4 \bar "|."
} }

\new Staff  {
\set Staff.instrumentName = "vln2"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 3/4 \key c \major bf'2.~( ^"nv" | % untitled/b2 untitled/b2.t9 0-16; 1
 bf'2.~ | % untitled/b2 untitled/b2.t9 0-16; 2
 bf'2.~ | % untitled/b2 untitled/b2.t9 0-16; 3
 bf'2.~ | % untitled/b2 untitled/b2.t9 0-16; 4
 bf'2.~ | % untitled/b2 untitled/b2.t9 0-16; 5
 bf'4\glissando a'2~ | % untitled/b2 untitled/b2.t9 16-19; 6
 a'4) r4 d'4^"vib" | % untitled/b2 untitled/b2.t9 20-21; 7
 c''4( f'4 e'4 | % untitled/b2 untitled/b2.t9 23-24; 8
 d'2) bf'4( | % untitled/b2 untitled/b2.t9 26-27; 9
 c'4) a'4( g'4 | % untitled/b2 untitled/b2.t9 29-30; 10
 bf4) g'4( e'4) | % untitled/b2 untitled/b2.t9 32-32.95; 11
 a2.~ | % untitled/b2 untitled/b2.t9 33-37; 12
 a4 bf16( a16~ a8) a4 | % untitled/b2 untitled/b2.t9 38-39; 13
 a4 g4 a4 | % untitled/b2 untitled/b2.t9 41-42; 14
 bf16( a16~ a8) bf16( a16~ a8) a4 | % untitled/b2 untitled/b2.t9 44-45; 15
 a4 c'4\glissando a4 | % untitled/b2 untitled/b2.t9 47-47.95; 16
 g4 bf16( a16~ a8) a4 | % untitled/b2 untitled/b2.t9 50-51; 17
 a4 g4 a4 | % untitled/b2 untitled/b2.t9 53-54; 18
 c'16( d'16~ d'8) a4 g4 | % untitled/b2 untitled/b2.t9 56-57; 19
 c'4 a4 g16( a16~ a8) | % untitled/b2 untitled/b2.t9 59.25-60; 20
 c'8( d'8) a4 g4 | % untitled/b2 untitled/b2.t9 62-63; 21
 c'8( d'8 g8 a8 c'8) r8 | % untitled/b2 untitled/b2.t9 65-65.5; 22
 c'4 d'4 a4 | % untitled/b2 untitled/b2.t9 68-69; 23
 d'4 c'4 a4 | % untitled/b2 untitled/b2.t9 71-72; 24
 g4 r4 c'4 | % untitled/b2 untitled/b2.t9 74-75; 25
 a4 g4 r4 | % untitled/b2 untitled/b2.t9 76-77; 26
 c'4 r4 a4 | % untitled/b2 untitled/b2.t9 80-81; 27
 r4 c'4 a4 | % untitled/b2 untitled/b2.t9 83-84; 28
 g4 r4 c'4 | % untitled/b2 untitled/b2.t9 86-87; 29
 d'4 a4 d'4 | % untitled/b2 untitled/b2.t9 89-90; 30
 c'4 a4 g4 | % untitled/b2 untitled/b2.t9 92-93; 31
 r4 c'4 a4 | % untitled/b2 untitled/b2.t9 95-96; 32
 g4 d'4 c'4 | % untitled/b2 untitled/b2.t9 98-99; 33
 r4 a4 r4 | % untitled/b2 untitled/b2.t9 100-101; 34
 c'8 r8 a8 r8 c'8 d'8 | % untitled/b2 untitled/b2.t9 104.5-105; 35
 a8 d'8 c'8 a8 g8 r8 | % untitled/b2 untitled/b2.t9 107-107.5; 36
 d'2 c'4 | % untitled/b2 untitled/b2.t9 110-111; 37
 r4 g2 | % untitled/b2 untitled/b2.t9 112-114; 38
 r8 d'8~ d'8 a'8 a'4 | % untitled/b2 untitled/b2.t9 116-117; 39
 d'4 d'4 r4 | % untitled/b2 untitled/b2.t9 118-119; 40
 f'8 e'8 g'8 e'16 g'16 f'16 r16 e'16 a'16 | % untitled/b2 untitled/b2.t9 122.75-122.98; 41
 g'16 r16 e'16 a'16 g'16 r16 e'16 c''16 a'16 r16 e'16 r16 | % untitled/b2 untitled/b2.t9 125.5-125.75; 42
 e''16 c''16 r16 f''16 e''16 r16 g''16 e''16 r16 e''16 g''16 r16 | % untitled/b2 untitled/b2.t9 128.5-128.75; 43
 e''16 g''16 r16 g''16 e''16 r16 g''16 e''16 g''16 r16 r8 \bar "|."
} }

\new Staff  {
\set Staff.instrumentName = "vla"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 3/4 \key c \major r4 d4^"pizz." \p d4 | % untitled/b2 untitled/b2.t19 2-3; 1
 r2 d4 | % untitled/b2 untitled/b2.t19 5-6; 2
 d4 r2 | % untitled/b2 untitled/b2.t19 6-7; 3
 d4 d4 r4 | % untitled/b2 untitled/b2.t19 10-11; 4
 r4 d4 d4 | % untitled/b2 untitled/b2.t19 14-15; 5
 r2 d4 | % untitled/b2 untitled/b2.t19 17-18; 6
 d4 d2 | % untitled/b2 untitled/b2.t19 19-21; 7
 R4*3 | % untitled/b2 untitled/b2.t19 19-21; 8
 r4 g4 d'4 | % untitled/b2 untitled/b2.t19 26-27; 9
 a'4 r4 g'4 | % untitled/b2 untitled/b2.t19 29-30; 10
 r4 d4( ^"arco" a4 | % untitled/b2 untitled/b2.t19 32-33; 11
 d'4 ef'4) d'4( | % untitled/b2 untitled/b2.t19 35-36; 12
 c'4 d4 a4) | % untitled/b2 untitled/b2.t19 38-38.95; 13
 d'4( e'4 d'4 | % untitled/b2 untitled/b2.t19 41-42; 14
 c'4) d4( a4 | % untitled/b2 untitled/b2.t19 44-45; 15
 d'4 e'4 d'4) | % untitled/b2 untitled/b2.t19 47-47.95; 16
 c'4( a4 d4 | % untitled/b2 untitled/b2.t19 50-51; 17
 a4) d'4( e'4 | % untitled/b2 untitled/b2.t19 53-54; 18
 c'4 a4) d4( | % untitled/b2 untitled/b2.t19 56-57; 19
 a4 d'4 e'4 | % untitled/b2 untitled/b2.t19 59-60; 20
 d'4) a4( d4 | % untitled/b2 untitled/b2.t19 62-63; 21
 a4 d'4 e'4~ | % untitled/b2 untitled/b2.t19 65-73; 22
 e'2.~ | % untitled/b2 untitled/b2.t19 65-73; 23
 e'2.~ | % untitled/b2 untitled/b2.t19 65-73; 24
 e'4) r2 | % untitled/b2 untitled/b2.t19 65-73; 25
 \once \override Glissando.style = #'dashed-line \once \set glissandoMap = #'((1 . 1)) <d'~ g'\harmonic>2\glissando <d' d''\harmonic>4 | % untitled/b2 untitled/b2.t19 75-78; 26
 R4*3 | % untitled/b2 untitled/b2.t19 75-78; 27
 R4*3 | % untitled/b2 untitled/b2.t19 75-78; 28
 R4*3 | % untitled/b2 untitled/b2.t19 75-78; 29
 R4*3 | % untitled/b2 untitled/b2.t19 75-78; 30
 <d''~ a''~\harmonic>2. | % untitled/b2 untitled/b2.t19 90-95.97; 31
 <d'' a''\harmonic>2. | % untitled/b2 untitled/b2.t19 90-95.97; 32
 R4*3 | % untitled/b2 untitled/b2.t19 90-95.97; 33
 R4*3 | % untitled/b2 untitled/b2.t19 90-95.97; 34
 R4*3 | % untitled/b2 untitled/b2.t19 90-95.97; 35
 R4*3 | % untitled/b2 untitled/b2.t19 90-95.97; 36
 d'4 a8 d'8 c'4 | % untitled/b2 untitled/b2.t19 110-111; 37
 a8 c'8 g8 a8 c'8 d'8 | % untitled/b2 untitled/b2.t19 113.5-113.97; 38
 a8 d'8 c'8 a8 g8 r8 | % untitled/b2 untitled/b2.t19 116-116.5; 39
 c'8 a8 g8 r8 a8 g8 | % untitled/b2 untitled/b2.t19 119.5-119.97; 40
 c'16 a16 g16 r16 a8 g16 d'16 c'16 a16 g16 r16 | % untitled/b2 untitled/b2.t19 122.5-122.75; 41
 d'16 c'16 a16 g16 d'16 c'16 a16 g16 f'16 d'16 c'16 a16 | % untitled/b2 untitled/b2.t19 125.75-125.98; 42
 c'16 a'16 f'16 e'16 c''16 a'16 g'16 d''16 bf'16 r16 c''16 d''16 | % untitled/b2 untitled/b2.t19 128.75-128.98; 43
 r16 c''16 d''16 r16 d''16 c''16 r16 d''16 c''16 r16 r8 \bar "|."
} }

\new Staff  {
\set Staff.instrumentName = "cello"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 3/4 \key c \major \clef bass r4 d,4( \p a,4 | % untitled/b2 untitled/b2.t27 2-3; 1
 d4 ef4) d4( | % untitled/b2 untitled/b2.t27 5-6; 2
 a,4 d,4 a,4) | % untitled/b2 untitled/b2.t27 8-8.95; 3
 d4( ef4 d4 | % untitled/b2 untitled/b2.t27 11-12; 4
 a,4) d,4( a,4 | % untitled/b2 untitled/b2.t27 14-15; 5
 d4 ef4) d4( | % untitled/b2 untitled/b2.t27 17-18; 6
 a,4) d,2^"pizz." | % untitled/b2 untitled/b2.t27 19-21; 7
 R4*3 | % untitled/b2 untitled/b2.t27 19-21; 8
 R4*3 | % untitled/b2 untitled/b2.t27 19-21; 9
 \tuplet 3/2 { f8 g8 c'8 } r4 \tuplet 3/2 { ef8 f8 bf8 } | % untitled/b2 untitled/b2.t27 29.5-29.75; 10
 r4 d4 a4 | % untitled/b2 untitled/b2.t27 32-33; 11
 d'4 c'4 g4 | % untitled/b2 untitled/b2.t27 35-36; 12
 a4 bf4 d4 | % untitled/b2 untitled/b2.t27 38-39; 13
 a4 c'4 \clef treble a'4 | % untitled/b2 untitled/b2.t27 41-42; 14
 g'4 bf4 g'4 | % untitled/b2 untitled/b2.t27 44-45; 15
 e'4 a2 | % untitled/b2 untitled/b2.t27 46-48; 16
 r2 e''4( ^"arco" | % untitled/b2 untitled/b2.t27 50-51; 17
 bf'4 e''4 d''4) | % untitled/b2 untitled/b2.t27 53-53.95; 18
 \clef bass <d'~ a'~\harmonic>2. | % untitled/b2 untitled/b2.t27 54-60.97; 19
 <d'~ a'~\harmonic>2. | % untitled/b2 untitled/b2.t27 54-60.97; 20
 <d' a'\harmonic>4 \once \override Glissando.style = #'dashed-line \once \set glissandoMap = #'((1 . 1)) <a~ d'\harmonic>2\glissando | % untitled/b2 untitled/b2.t27 61-64; 21
 <a a'\harmonic>4 r2 | % untitled/b2 untitled/b2.t27 61-64; 22
 R4*3 | % untitled/b2 untitled/b2.t27 61-64; 23
 r4 f'2~ | % untitled/b2 untitled/b2.t27 70-81; 24
 f'2.~ | % untitled/b2 untitled/b2.t27 70-81; 25
 f'2.~ | % untitled/b2 untitled/b2.t27 70-81; 26
 f'2. | % untitled/b2 untitled/b2.t27 70-81; 27
 ef'2.~ | % untitled/b2 untitled/b2.t27 81-86.5; 28
 ef'2~ ef'8 r8 | % untitled/b2 untitled/b2.t27 81-86.5; 29
 \tuplet 3/2 { f'8 d'8 f'8 } d'16 f'16 d'16 f'16 \tuplet 3/2 { d'16 f'16 d'16 f'16 d'16 f'16 } | % untitled/b2 untitled/b2.t27 89.62-89.75; 30
 \repeat tremolo 12 { d'32( f'32) } | % untitled/b2 untitled/b2.t27 89.62-89.75; 31
 \repeat tremolo 12 { d'32( f'32) } | % untitled/b2 untitled/b2.t27 89.62-89.75; 32
 \repeat tremolo 12 { d'32( f'32) } | % untitled/b2 untitled/b2.t27 89.62-89.75; 33
 d'2. \trill | % untitled/b2 untitled/b2.t27 99-102; 34
 d'2. \trill | % untitled/b2 untitled/b2.t27 102-105; 35
 d'2. \trill | % untitled/b2 untitled/b2.t27 105-108; 36
 \repeat tremolo 12 { d'32( f'32) } | % untitled/b2 untitled/b2.t27 105-108; 37
 \repeat tremolo 12 { d'32( f'32) } | % untitled/b2 untitled/b2.t27 105-108; 38
 \repeat tremolo 12 { d'32( f'32) } | % untitled/b2 untitled/b2.t27 105-108; 39
 R4*3 | % untitled/b2 untitled/b2.t27 105-108; 40
 R4*3 | % untitled/b2 untitled/b2.t27 105-108; 41
 R4*3 | % untitled/b2 untitled/b2.t27 105-108; 42
 R4*3 | % untitled/b2 untitled/b2.t27 105-108; 43
 R4*3 \bar "|."
} }

>>
}
}

