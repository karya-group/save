\version "2.18.2"
\language "english"
\pointAndClickOff
\include "ly/lib.ily"
\header { title = "b1" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\bookpart {
\score {
<<
\new Staff  {
\set Staff.instrumentName = "vln1"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 3/4 \key c \major \clef treble R4*3 | % 1
 R4*3 | % 2
 r2 \ottava #1 a'''4~ \p | % untitled/b1 untitled/b1.t1 8-11.5; 3
 a'''2~ a'''8 \ottava #0 <d''~ a''~\harmonic>8 | % untitled/b1 untitled/b1.t1 11.5-15; 4
 <d'' a''\harmonic>2. | % untitled/b1 untitled/b1.t1 11.5-15; 5
 \ottava #1 g'''4 f'''4( ef'''4 | % untitled/b1 untitled/b1.t1 17-18; 6
 f'''4) g'''4( a'''4~ | % untitled/b1 untitled/b1.t1 20-23.5; 7
 a'''2~ a'''8) c'''8 | % untitled/b1 untitled/b1.t1 23.5-24; 8
 a'''2 bf'''4( | % untitled/b1 untitled/b1.t1 26-27; 9
 ef'''4 g'''4) bf'''4( | % untitled/b1 untitled/b1.t1 29-30; 10
 c''''4) a'''2 | % untitled/b1 untitled/b1.t1 31-33; 11
 bf'''4( g'''4 bf'''4) | % untitled/b1 untitled/b1.t1 35-36; 12
 f'''4( e'''4 g'''4) | % untitled/b1 untitled/b1.t1 38-39; 13
 f'''8( g'''8) e'''2~ | % untitled/b1 untitled/b1.t1 40-42.5; 14
 e'''8 c'''8( d'''8 bf''8~ bf''8 a''8) | % untitled/b1 untitled/b1.t1 44.5-45; 15
 \ottava #0 <d''~ a''~\harmonic>2 \f <d'' a''\harmonic>8 a''8 | % untitled/b1 untitled/b1.t1 47.5-48; 16
 a''2 g''4( | % untitled/b1 untitled/b1.t1 50-51; 17
 f''4) ef''4( f''4 | % untitled/b1 untitled/b1.t1 53-54; 18
 g''4 a''4. bf''8) | % untitled/b1 untitled/b1.t1 56.5-57; 19
 a''2 c''4 | % untitled/b1 untitled/b1.t1 59-60; 20
 c'''4( f''4 e''4 | % untitled/b1 untitled/b1.t1 62-63; 21
 d''2) bf''4( | % untitled/b1 untitled/b1.t1 65-66; 22
 c''4) a''4( g''4 | % untitled/b1 untitled/b1.t1 68-69; 23
 bf'4) g''4( e''4) | % untitled/b1 untitled/b1.t1 71-72; 24
 bf'8 \p a'8( c''8 a'8 bf'8) a'8( | % untitled/b1 untitled/b1.t1 74.5-75; 25
 c''8 a'8 bf'8) r8 r4 | % untitled/b1 untitled/b1.t1 76-76.5; 26
 a'2.^"pizz." | % untitled/b1 untitled/b1.t1 78-81; 27
 g'16( ^"arco" a'16) e'8-. g'8-. d'16( e'16) r4 | % untitled/b1 untitled/b1.t1 82.75-82.98; 28
 a'2^"pizz." g'16( ^"arco" a'16) e'8-. | % untitled/b1 untitled/b1.t1 86.5-87; 29
 g'8-. d'16( e'16) g'16( a'16 e'16 a'16) g'16( e'16) d'8-. | % untitled/b1 untitled/b1.t1 89.5-90; 30
 g'16( e'16) d'8-. e'16( d'16) a'8-. e'16( a'16 g'16 e'16) | % untitled/b1 untitled/b1.t1 92.75-93; 31
 e'2.~ \trill | % untitled/b1 untitled/b1.t1 93-98; 32
 e'2 \trill e''4~ \trill | % untitled/b1 untitled/b1.t1 98-102; 33
 e''2. \trill | % untitled/b1 untitled/b1.t1 98-102; 34
 r4 \f \ottava #1 a'''2~ | % untitled/b1 untitled/b1.t1 103-108; 35
 a'''2. | % untitled/b1 untitled/b1.t1 103-108; 36
 a'''2. \f | % untitled/b1 untitled/b1.t1 108-111; 37
 \ottava #0 r4 \p \once \override Glissando.style = #'dashed-line \once \set glissandoMap = #'((1 . 1)) <a'~ d''\harmonic>2\glissando | % untitled/b1 untitled/b1.t1 112-114; 38
 <a' a''\harmonic>4 g''4( \mf a''4~ | % untitled/b1 untitled/b1.t1 116-119.5; 39
 a''2~ a''8) c''8 | % untitled/b1 untitled/b1.t1 119.5-120; 40
 a''2 bf''4 | % untitled/b1 untitled/b1.t1 122-123; 41
 c''4( g''4 c'''4) | % untitled/b1 untitled/b1.t1 125-126; 42
 d'''4( c'''4 bf''4) | % untitled/b1 untitled/b1.t1 128-129; 43
 a''4( bf''2~ | % untitled/b1 untitled/b1.t1 130-133; 44
 bf''4) g''4 bf''4 | % untitled/b1 untitled/b1.t1 134-135; 45
 e'''4 a''4 f'''4~ | % untitled/b1 untitled/b1.t1 137-143; 46
 f'''2.~ | % untitled/b1 untitled/b1.t1 137-143; 47
 f'''2 f''4~ \p | % untitled/b1 untitled/b1.t1 143-148; 48
 f''2.~ | % untitled/b1 untitled/b1.t1 143-148; 49
 f''4 e''4 g''4 | % untitled/b1 untitled/b1.t1 149-149.95; 50
 f''8 g''8 e''2 | % untitled/b1 untitled/b1.t1 151-153; 51
 r8 c''8( d''8) bf'8~( bf'8 a'8) \f \bar "|."
} }

\new Staff  {
\set Staff.instrumentName = "vln2"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 3/4 \key c \major R4*3 | % untitled/b1 untitled/b1.t1 155.5-156; 1
 R4*3 | % untitled/b1 untitled/b1.t1 155.5-156; 2
 r2 \ottava #1 a'''4~ \p | % untitled/b1 untitled/b1.t10 8-12; 3
 a'''2. | % untitled/b1 untitled/b1.t10 8-12; 4
 \ottava #0 <d'' a''\harmonic>2. | % untitled/b1 untitled/b1.t10 12-15; 5
 \ottava #1 g'''4 f'''4( ef'''4 | % untitled/b1 untitled/b1.t10 17-18; 6
 f'''4) g'''4( a'''4~ | % untitled/b1 untitled/b1.t10 20-23.5; 7
 a'''2~ a'''8) c'''8 | % untitled/b1 untitled/b1.t10 23.5-24; 8
 a'''2 bf''4( | % untitled/b1 untitled/b1.t10 26-27; 9
 ef'''4 g''4) c'''4( | % untitled/b1 untitled/b1.t10 29-30; 10
 f'''4) a'''2 | % untitled/b1 untitled/b1.t10 31-33; 11
 bf''4( g'''4 d'''4) | % untitled/b1 untitled/b1.t10 35-35.95; 12
 f''4( e'''4 g''4) | % untitled/b1 untitled/b1.t10 38-38.95; 13
 f'''4 e'''2~ | % untitled/b1 untitled/b1.t10 40-42.5; 14
 e'''8 c'''8( d'''8 bf''8~ bf''8 a''8) | % untitled/b1 untitled/b1.t10 44.5-45; 15
 \ottava #0 d'4( \f a'4 d''4 | % untitled/b1 untitled/b1.t10 47-48; 16
 ef''4) d''4( a'4 | % untitled/b1 untitled/b1.t10 50-51; 17
 d'4 a'4) d''4( | % untitled/b1 untitled/b1.t10 53-54; 18
 ef''4 d''4 a'4) | % untitled/b1 untitled/b1.t10 56-56.95; 19
 R4*3 | % untitled/b1 untitled/b1.t10 56-56.95; 20
 R4*3 | % untitled/b1 untitled/b1.t10 56-56.95; 21
 R4*3 | % untitled/b1 untitled/b1.t10 56-56.95; 22
 \arpeggioArrowUp <g' a' e''>2\arpeggio ^"pizz." r4 | % untitled/b1 untitled/b1.t10 66-68; 23
 R4*3 | % untitled/b1 untitled/b1.t10 66-68; 24

 a'4 r4
 \once \override Glissando.style = #'dashed-line
 \once \set glissandoMap = #'((1 . 1))
 <a'~ a''\harmonic>4\glissando ^"arco" | % untitled/b1 untitled/b1.t10 74-75; 25
 <a' d''\harmonic>8 r8 bf'8( \p a'8 c''8) a'8( | % untitled/b1 untitled/b1.t10 77.5-78; 26

 bf'8 a'8 c''8) a'8( bf'8 a'8 | % untitled/b1 untitled/b1.t10 80.5-81; 27
 c''8) a'8( bf'8 a'8 c''8) a'8( | % untitled/b1 untitled/b1.t10 83.5-84; 28
 bf'8 a'8 c''8) a'8( bf'8 a'8 | % untitled/b1 untitled/b1.t10 86.5-87; 29
 c''8) a'8( bf'8 a'8 c''8) a'8( | % untitled/b1 untitled/b1.t10 89.5-90; 30
 bf'8 a'8 c''8) a'8( bf'8 a'8 | % untitled/b1 untitled/b1.t10 92.5-93; 31
 c''8) a'8( bf'8 a'8 c''8) a'8( | % untitled/b1 untitled/b1.t10 95.5-96; 32
 bf'8 a'8 c''8) a'8( bf'8 a'8 | % untitled/b1 untitled/b1.t10 98.5-99; 33
 c''8) a'8( bf'8 a'8 c''8) a'8 | % untitled/b1 untitled/b1.t10 101.5-102; 34
 r4 \f <d''~ a''~\harmonic>2 \p | % untitled/b1 untitled/b1.t10 103-107.97; 35
 <d'' a''\harmonic>2. | % untitled/b1 untitled/b1.t10 103-107.97; 36
 \repeat tremolo 12 { d'32( a'32) } | % untitled/b1 untitled/b1.t10 103-107.97; 37
 \repeat tremolo 12 { d'32( a'32) } | % untitled/b1 untitled/b1.t10 103-107.97; 38
 \repeat tremolo 12 { d'32( a'32) } | % untitled/b1 untitled/b1.t10 103-107.97; 39
 \repeat tremolo 12 { d'32( a'32) } | % untitled/b1 untitled/b1.t10 103-107.97; 40
 \repeat tremolo 12 { d'32( a'32) } | % untitled/b1 untitled/b1.t10 103-107.97; 41
 \repeat tremolo 12 { d'32( a'32) } | % untitled/b1 untitled/b1.t10 103-107.97; 42
 \repeat tremolo 12 { d'32( a'32) } | % untitled/b1 untitled/b1.t10 103-107.97; 43
 \repeat tremolo 12 { d'32( a'32) } | % untitled/b1 untitled/b1.t10 103-107.97; 44
 \repeat tremolo 8 { d'32( a'32) } \tuplet 3/2 { d'16( a'16 d'16 a'16 d'16 a'16 } | % untitled/b1 untitled/b1.t10 134.62-134.75; 45
 \tuplet 3/2 { d'16 a'16 d'16 a'16 d'16 a'16 } d'16 a'16 d'16 a'16 d'16 a'16 d'16 a'16 | % untitled/b1 untitled/b1.t10 137.75-138; 46
 \tuplet 3/2 { d'8 a'8 d'8 } a'8 d'8) d''4( | % untitled/b1 untitled/b1.t10 140-141; 47
 a'4 d'4 a'4) | % untitled/b1 untitled/b1.t10 143-143.95; 48
 d''4( ef''4 d''4 | % untitled/b1 untitled/b1.t10 146-147; 49
 a'4 c'4 g'4 | % untitled/b1 untitled/b1.t10 149-150; 50
 d''4 c''4) f'4( | % untitled/b1 untitled/b1.t10 152-153; 51
 d'4 g4 d'4) \bar "|."
} }

\new Staff  {
\set Staff.instrumentName = "vla"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 3/4 \key c \major \clef alto d'4( \p a'4 d''4 | % untitled/b1 untitled/b1.t22 2-3; 1
 ef''4) d''4( a'4 | % untitled/b1 untitled/b1.t22 5-6; 2
 d'4 a'4) d''4( | % untitled/b1 untitled/b1.t22 8-9; 3
 ef''4 d''4 a'4) | % untitled/b1 untitled/b1.t22 11-12; 4
 d'4( a'4 d''4 | % untitled/b1 untitled/b1.t22 14-15; 5
 ef''4) d''4( a'4 | % untitled/b1 untitled/b1.t22 17-18; 6
 d'4 a'4) d''4( | % untitled/b1 untitled/b1.t22 20-21; 7
 ef''4 d''4 a'4) | % untitled/b1 untitled/b1.t22 23-24; 8
 d'4( a'4 d''4 | % untitled/b1 untitled/b1.t22 26-27; 9
 ef''4) d''4( a'4 | % untitled/b1 untitled/b1.t22 29-30; 10
 d'4 a'4) d''4( | % untitled/b1 untitled/b1.t22 32-33; 11
 ef''4 d''4 a'4) | % untitled/b1 untitled/b1.t22 35-36; 12
 c'4( g'4 d''4 | % untitled/b1 untitled/b1.t22 38-39; 13
 c''4) f'4( d'4 | % untitled/b1 untitled/b1.t22 41-42; 14
 g4 d'4) bf'4 | % untitled/b1 untitled/b1.t22 44-45; 15
 d'4^"pizz." \f a'4 d''4 | % untitled/b1 untitled/b1.t22 47-48; 16
 ef''4 d''4 a'4 | % untitled/b1 untitled/b1.t22 50-51; 17
 d'4 a'4 d''4 | % untitled/b1 untitled/b1.t22 53-54; 18
 ef''4 d''4 a'4 | % untitled/b1 untitled/b1.t22 56-57; 19
 <d' d''>2 r4 | % untitled/b1 untitled/b1.t22 57-59; 20
 R4*3 | % untitled/b1 untitled/b1.t22 57-59; 21
 r4 g4 d'4 | % untitled/b1 untitled/b1.t22 65-66; 22
 bf'2 r4 | % untitled/b1 untitled/b1.t22 66-68; 23
 r8 f8~ f8 d'8~ d'8 r8 | % untitled/b1 untitled/b1.t22 70.5-71.5; 24
 a'8( ^"arco" \p bf'8 a'8 c''8) a'8( bf'8 | % untitled/b1 untitled/b1.t22 74.5-75; 25
 a'8 c''8) a'8( bf'8 a'8 c''8) | % untitled/b1 untitled/b1.t22 77.5-78; 26
 a'8( bf'8 a'8 c''8) a'8( bf'8 | % untitled/b1 untitled/b1.t22 80.5-81; 27
 a'8 c''8) a'8( bf'8 a'8 c''8) | % untitled/b1 untitled/b1.t22 83.5-84; 28
 a'8( bf'8 a'8 c''8) a'8( bf'8 | % untitled/b1 untitled/b1.t22 86.5-87; 29
 a'8 c''8) a'8( bf'8 a'8 c''8) | % untitled/b1 untitled/b1.t22 89.5-90; 30
 a'8( bf'8 a'8 c''8) a'8( bf'8 | % untitled/b1 untitled/b1.t22 92.5-93; 31
 a'8 c''8) a'8( bf'8 a'8 c''8) | % untitled/b1 untitled/b1.t22 95.5-96; 32
 a'8( bf'8 a'8 c''8) a'8( bf'8 | % untitled/b1 untitled/b1.t22 98.5-99; 33
 a'8 c''8) a'8( bf'8 a'8 c''8) | % untitled/b1 untitled/b1.t22 101.5-102; 34
 r4 \f <d'~ ef''~>2 \p | % untitled/b1 untitled/b1.t22 103-108; 35
 <d' ef''>2. | % untitled/b1 untitled/b1.t22 103-108; 36
 \repeat tremolo 12 { d'32( ef''32) } | % untitled/b1 untitled/b1.t22 103-108; 37
 \repeat tremolo 12 { d'32( ef''32) } | % untitled/b1 untitled/b1.t22 103-108; 38
 \repeat tremolo 12 { d'32( ef''32) } | % untitled/b1 untitled/b1.t22 103-108; 39
 \repeat tremolo 12 { d'32( ef''32) } | % untitled/b1 untitled/b1.t22 103-108; 40
 \repeat tremolo 12 { d'32( ef''32) } | % untitled/b1 untitled/b1.t22 103-108; 41
 \repeat tremolo 12 { d'32( ef''32) } | % untitled/b1 untitled/b1.t22 103-108; 42
 <d'~ d''~\trill>2. | % untitled/b1 untitled/b1.t22 126-132; 43
 <d' d''\trill>2. | % untitled/b1 untitled/b1.t22 126-132; 44
 \tuplet 3/2 { d''16( ef''16 d''16 ef''16 d''16 ef''16 } \tuplet 3/2 { d''16 ef''16 d''16 ef''16 d''16 ef''16 } d''16 ef''16 d''16 ef''16 | % untitled/b1 untitled/b1.t22 134.75-135; 45
 d''16 ef''16 \tuplet 3/2 { d''8 ef''8 d''8 } ef''8 d''8 ef''8 | % untitled/b1 untitled/b1.t22 137.5-138; 46
 d''8 ef''8) d''4( a'4 | % untitled/b1 untitled/b1.t22 140-141; 47
 d'4 a'4) d''4( | % untitled/b1 untitled/b1.t22 143-144; 48
 ef''4 d''4 a'4) | % untitled/b1 untitled/b1.t22 146-146.95; 49
 c'4( g'4 d''4 | % untitled/b1 untitled/b1.t22 149-150; 50
 c''4) f'4( d'4 | % untitled/b1 untitled/b1.t22 152-153; 51
 g4 d'4 bf'4) \bar "|."
} }

\new Staff  {
\set Staff.instrumentName = "cello"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 3/4 \key c \major \clef bass R4*3 | % untitled/b1 untitled/b1.t22 155-156; 1
 R4*3 | % untitled/b1 untitled/b1.t22 155-156; 2
 R4*3 | % untitled/b1 untitled/b1.t22 155-156; 3
 R4*3 | % untitled/b1 untitled/b1.t22 155-156; 4
 R4*3 | % untitled/b1 untitled/b1.t22 155-156; 5
 R4*3 | % untitled/b1 untitled/b1.t22 155-156; 6
 r8 a8~^"pizz." \p a8 d,8~ d,4 | % untitled/b1 untitled/b1.t33 19.5-21; 7
 R4*3 | % untitled/b1 untitled/b1.t33 19.5-21; 8
 R4*3 | % untitled/b1 untitled/b1.t33 19.5-21; 9
 r4 ef4 bf,4 | % untitled/b1 untitled/b1.t33 29-30; 10
 g,4 r2 | % untitled/b1 untitled/b1.t33 30-31; 11
 a4 d,2 | % untitled/b1 untitled/b1.t33 34-36; 12
 r4 e4 d4 | % untitled/b1 untitled/b1.t33 38-39; 13
 g,4 r2 | % untitled/b1 untitled/b1.t33 39-40; 14
 r8 g,8~ g,8 a8~ a8 r8 | % untitled/b1 untitled/b1.t33 43.5-44.5; 15
 d,2. \f | % untitled/b1 untitled/b1.t33 45-48; 16
 R4*3 | % untitled/b1 untitled/b1.t33 45-48; 17
 \clef treble bf'4^"arco" a'4 c'4 | % untitled/b1 untitled/b1.t33 53-54; 18
 c''4 f''4 g''4 | % untitled/b1 untitled/b1.t33 56-57; 19
 a''2 c''4 | % untitled/b1 untitled/b1.t33 59-60; 20
 c''4( f''4 e''4 | % untitled/b1 untitled/b1.t33 62-63; 21
 d''2) bf'4( | % untitled/b1 untitled/b1.t33 65-66; 22
 c''4) a''4( g''4 | % untitled/b1 untitled/b1.t33 68-69; 23
 bf'4) g''4( e''4) | % untitled/b1 untitled/b1.t33 71-71.95; 24
 a'4. \p <d'~ a'~\harmonic>8:32 <d'~ a'~\harmonic>4:32 | % untitled/b1 untitled/b1.t33 73.5-76.5; 25
 <d' a'\harmonic>4.:32 r8 \p r4 | % untitled/b1 untitled/b1.t33 73.5-76.5; 26
 a'2.~ | % untitled/b1 untitled/b1.t33 78-83; 27
 a'2 r4 | % untitled/b1 untitled/b1.t33 78-83; 28
 a'2~ a'8 r8 | % untitled/b1 untitled/b1.t33 84-86.5; 29
 \clef bass e'8^"pizz." a,8~ a,4 r8 e'8 | % untitled/b1 untitled/b1.t33 89.5-90; 30
 a,8 a,8~ a,4 \clef treble f'8( ^"arco" e'8 | % untitled/b1 untitled/b1.t33 92.5-93; 31
 g'8 e'8) f'8( e'16 a'16 g'8 e'8) | % untitled/b1 untitled/b1.t33 95.5-95.97; 32
 d'16( e'16) a'8-. g'16( a'16) e'8-. g'8-. d'16( e'16) | % untitled/b1 untitled/b1.t33 98.75-99; 33
 g'8-. e'8-. d'16( e'16) a'8-. g'16( a'16 e'16 g'16) | % untitled/b1 untitled/b1.t33 101.75-101.98; 34
 r4 \f <d'~ a'~>2 \p | % untitled/b1 untitled/b1.t33 103-108; 35
 <d' a'>2. | % untitled/b1 untitled/b1.t33 103-108; 36
 a'2. | % untitled/b1 untitled/b1.t33 108-111; 37
 g''4( \mf f''4 ef''4 | % untitled/b1 untitled/b1.t33 113-114; 38
 f''4) g'4( a'4~ | % untitled/b1 untitled/b1.t33 116-119.5; 39
 a'2~ a'8) c''8 | % untitled/b1 untitled/b1.t33 119.5-120; 40
 d''2. | % untitled/b1 untitled/b1.t33 120-123; 41
 c''2~ c''8 bf'8 | % untitled/b1 untitled/b1.t33 125.5-126; 42
 d''4( g''2) | % untitled/b1 untitled/b1.t33 127-129; 43
 a''4( bf'2~ | % untitled/b1 untitled/b1.t33 130-133; 44
 bf'4) g''4 d''4 | % untitled/b1 untitled/b1.t33 134-135; 45
 e''4 a'2~ | % untitled/b1 untitled/b1.t33 136-140; 46
 a'2 f''4~ \p | % untitled/b1 untitled/b1.t33 140-144; 47
 f''2. | % untitled/b1 untitled/b1.t33 140-144; 48
 R4*3 | % untitled/b1 untitled/b1.t33 140-144; 49
 R4*3 | % untitled/b1 untitled/b1.t33 140-144; 50
 R4*3 | % untitled/b1 untitled/b1.t33 140-144; 51
 \clef bass r8 c'8( d'8) bf8~( bf8 a8) \f \bar "|."
} }

>>
}
}

