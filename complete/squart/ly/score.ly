\version "2.18.2"
\language "english"
\pointAndClickOff
\include "ly/lib.ily"
\header { title = "score" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\bookpart {
\score {
<<
\new Staff  {
\set Staff.instrumentName = "vln1"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 3/4 \key c \major \clef treble R4*3 | % 1
 R4*3 | % 2
 r2 a'''4~ \p | % untitled/b1 untitled/b1.t1 8-11.5; 3
 a'''4~ a'''4. <a'~ d''~\harmonic>8 | % untitled/b1 untitled/b1.t1 11.5-15; 4
 <a' d''\harmonic>2. | % untitled/b1 untitled/b1.t1 11.5-15; 5
 g'''4 f'''4 ( ef'''4 | % untitled/b1 untitled/b1.t1 17-18; 6
 f'''4 ) g'''4 ( a'''4~ | % untitled/b1 untitled/b1.t1 20-23.5; 7
 a'''4~ a'''4. ) c'''8 | % untitled/b1 untitled/b1.t1 23.5-24; 8
 a'''2 bf'''4 ( | % untitled/b1 untitled/b1.t1 26-27; 9
 ef'''4 g'''4 ) bf'''4 ( | % untitled/b1 untitled/b1.t1 29-30; 10
 c''''4 ) a'''2 | % untitled/b1 untitled/b1.t1 31-33; 11
 bf'''4 ( g'''4 bf'''4 ) | % untitled/b1 untitled/b1.t1 35-36; 12
 f'''4 ( e'''4 g'''4 ) | % untitled/b1 untitled/b1.t1 38-39; 13
 f'''8 ( g'''8 ) e'''2~ \< | % untitled/b1 untitled/b1.t1 40-42.5; 14
 e'''8 c'''8 ( d'''8 bf''4 a''8 ) | % untitled/b1 untitled/b1.t1 44.5-45; 15
 <a'~ d''~\harmonic>4 \f <a' d''\harmonic>4. a''8 | % untitled/b1 untitled/b1.t1 47.5-48; 16
 a''2 g''4 ( | % untitled/b1 untitled/b1.t1 50-51; 17
 f''4 ) ef''4 ( f''4 | % untitled/b1 untitled/b1.t1 53-54; 18
 g''4 a''4. bf''8 ) | % untitled/b1 untitled/b1.t1 56.5-57; 19
 a''2 c''4 | % untitled/b1 untitled/b1.t1 59-60; 20
 c'''4 ( f''4 e''4 | % untitled/b1 untitled/b1.t1 62-63; 21
 d''2 ) bf''4 ( | % untitled/b1 untitled/b1.t1 65-66; 22
 c''4 ) a''4 ( g''4 | % untitled/b1 untitled/b1.t1 68-69; 23
 bf'4 ) \> g''4 ( e''4 ) | % untitled/b1 untitled/b1.t1 71-72; 24
 \textSpannerDown \override TextSpanner.bound-details.left.text = \markup { "accel" } \tempo \markup { \concat {
    ( \smaller \general-align #Y #DOWN \note #"8" #1
      " = "
      \smaller \general-align #Y #DOWN \note #"4" #1
    )
} } bf'8 \p \startTextSpan a'8 ( c''8 a'8 bf'8 ) a'8 ( | % untitled/b1 untitled/b1.t1 74.5-75; 25
 c''8 a'8 bf'8 ) r8 r4 | % untitled/b1 untitled/b1.t1 76-76.5; 26
 a'2. ^"pizz." | % untitled/b1 untitled/b1.t1 78-81; 27
 g'16 ( ^"arco" a'16 ) e'8 -. g'8 -. d'16 ( e'16 ) r4 | % untitled/b1 untitled/b1.t1 82.75-82.98; 28
 a'2 ^"pizz." g'16 ( ^"arco" a'16 ) e'8 -. | % untitled/b1 untitled/b1.t1 86.5-87; 29
 g'8 -. d'16 ( e'16 ) g'16 ( a'16 e'16 a'16 ) g'16 ( e'16 ) d'8 -. | % untitled/b1 untitled/b1.t1 89.5-90; 30
 g'16 ( e'16 ) d'8 -. e'16 ( d'16 ) a'8 -. e'16 ( \< a'16 g'16 e'16 ) | % untitled/b1 untitled/b1.t1 92.75-93; 31
 \tempo \markup { \concat {
    ( \smaller \general-align #Y #DOWN \note #"8" #1
      " = "
      \smaller \general-align #Y #DOWN \note #"8" #1
    )
} } \pitchedTrill e'2.~ \startTrillSpan f' | % untitled/b1 untitled/b1.t1 93-98; 32
 e'2 \stopTrillSpan \pitchedTrill e''4~ \startTrillSpan f'' | % untitled/b1 untitled/b1.t1 98-102; 33
 e''2. \stopTrillSpan | % untitled/b1 untitled/b1.t1 98-102; 34
 \tempo "a tempo" r4 \f \stopTextSpan a'''2~ \< | % untitled/b1 untitled/b1.t1 103-108; 35
 a'''2. | % untitled/b1 untitled/b1.t1 103-108; 36
 a'''2. \f \> | % untitled/b1 untitled/b1.t1 108-111; 37
 r4 \p \once \override Glissando.style = #'dashed-line \once \set glissandoMap = #'((1 . 1)) <a'~ d''\harmonic>2 \glissando | % untitled/b1 untitled/b1.t1 112-114; 38
 <a' a''\harmonic>4 g''4 ( \mf a''4~ | % untitled/b1 untitled/b1.t1 116-119.5; 39
 a''4~ a''4. ) c''8 | % untitled/b1 untitled/b1.t1 119.5-120; 40
 a''2 bf''4 | % untitled/b1 untitled/b1.t1 122-123; 41
 c''4 ( g''4 c'''4 ) | % untitled/b1 untitled/b1.t1 125-126; 42
 d'''4 ( c'''4 bf''4 ) | % untitled/b1 untitled/b1.t1 128-129; 43
 a''4 ( bf''2~ | % untitled/b1 untitled/b1.t1 130-133; 44
 bf''4 ) g''4 bf''4 | % untitled/b1 untitled/b1.t1 134-135; 45
 e'''4 a''4 f'''4~ \> | % untitled/b1 untitled/b1.t1 137-143; 46
 f'''2.~ | % untitled/b1 untitled/b1.t1 137-143; 47
 f'''2 f''4~ \p | % untitled/b1 untitled/b1.t1 143-148; 48
 f''2.~ | % untitled/b1 untitled/b1.t1 143-148; 49
 f''4 e''4 ( g''4 ) | % untitled/b1 untitled/b1.t1 149-150; 50
 f''8 ( g''8 ) e''2 | % untitled/b1 untitled/b1.t1 151-153; 51
 r8 c''8 ( \< d''8 ) bf'4 ( a'8 ) | % untitled/b1 untitled/b1.t1 155.5-156; 52
 R4*3 \f | % untitled/b1 untitled/b1.t1 155.5-156; 53
 R4*3 | % untitled/b1 untitled/b1.t1 155.5-156; 54
 R4*3 | % untitled/b1 untitled/b1.t1 155.5-156; 55
 R4*3 | % untitled/b1 untitled/b1.t1 155.5-156; 56
 R4*3 | % untitled/b1 untitled/b1.t1 155.5-156; 57
 R4*3 | % untitled/b1 untitled/b1.t1 155.5-156; 58
 R4*3 | % untitled/b1 untitled/b1.t1 155.5-156; 59
 r2 c''4 \p | % untitled/b2 untitled/b2.t2 23-24; 60
 c'''4 ( f''4 e''4 | % untitled/b2 untitled/b2.t2 26-27; 61
 d''2 ) bf''4 ( | % untitled/b2 untitled/b2.t2 29-30; 62
 c''4 ) a''4 ( g''4 | % untitled/b2 untitled/b2.t2 32-33; 63
 bf'4 ) g''4 ( e''4 ) | % untitled/b2 untitled/b2.t2 35-35.95; 64
 a''2.~ | % untitled/b2 untitled/b2.t2 36-47; 65
 a''2.~ | % untitled/b2 untitled/b2.t2 36-47; 66
 a''2.~ | % untitled/b2 untitled/b2.t2 36-47; 67
 a''2 g''4 ( | % untitled/b2 untitled/b2.t2 47-48; 68
 c'''4 f''4 e''4 ) | % untitled/b2 untitled/b2.t2 50-51; 69
 bf''4 ( e''4 d''4 ) | % untitled/b2 untitled/b2.t2 53-54; 70
 a''2.~ | % untitled/b2 untitled/b2.t2 54-59; 71
 a''2 r4 | % untitled/b2 untitled/b2.t2 54-59; 72
 d'2.~ | % untitled/b2 untitled/b2.t2 60-87; 73
 d'2.~ | % untitled/b2 untitled/b2.t2 60-87; 74
 d'2.~ | % untitled/b2 untitled/b2.t2 60-87; 75
 d'2.~ | % untitled/b2 untitled/b2.t2 60-87; 76
 d'2.~ | % untitled/b2 untitled/b2.t2 60-87; 77
 d'2.~ | % untitled/b2 untitled/b2.t2 60-87; 78
 d'2.~ | % untitled/b2 untitled/b2.t2 60-87; 79
 d'2.~ | % untitled/b2 untitled/b2.t2 60-87; 80
 d'2. | % untitled/b2 untitled/b2.t2 60-87; 81
 \repeat tremolo 12 { d'32( ef'32) } | % untitled/b2 untitled/b2.t2 60-87; 82
 \repeat tremolo 12 { d'32( ef'32) } | % untitled/b2 untitled/b2.t2 60-87; 83
 \repeat tremolo 12 { d'32( f'32) } | % untitled/b2 untitled/b2.t2 60-87; 84
 \repeat tremolo 12 { d'32( \< f'32) } | % untitled/b2 untitled/b2.t2 60-87; 85
 \repeat tremolo 12 { \textSpannerDown \override TextSpanner.bound-details.left.text = \markup { "accel" } \tempo \markup { \concat {
    ( \smaller \general-align #Y #DOWN \note #"8" #1
      " = "
      \smaller \general-align #Y #DOWN \note #"4" #1
    )
} } d'32( \f \> \startTextSpan ef'32) } | % untitled/b2 untitled/b2.t2 60-87; 86
 \repeat tremolo 12 { d'32( ef'32) } | % untitled/b2 untitled/b2.t2 60-87; 87
 \repeat tremolo 12 { d'32( \mp ef'32) } | % untitled/b2 untitled/b2.t2 60-87; 88
 \repeat tremolo 12 { \tempo \markup { \concat {
    ( \smaller \general-align #Y #DOWN \note #"8" #1
      " = "
      \smaller \general-align #Y #DOWN \note #"8" #1
    )
} } d'32( ef'32) } | % untitled/b2 untitled/b2.t2 60-87; 89
 \repeat tremolo 12 { d'32( ef'32) } | % untitled/b2 untitled/b2.t2 60-87; 90
 \repeat tremolo 12 { d'32( \mf f'32) } | % untitled/b2 untitled/b2.t2 60-87; 91
 \repeat tremolo 12 { d'32( f'32) } | % untitled/b2 untitled/b2.t2 60-87; 92
 \repeat tremolo 12 { d'32( f'32) } | % untitled/b2 untitled/b2.t2 60-87; 93
 \tempo \markup { \concat {
    ( \smaller \general-align #Y #DOWN \note #"8" #1
      " = "
      \smaller \general-align #Y #DOWN \note #"16" #1
    )
} } R4*3 | % untitled/b2 untitled/b2.t2 60-87; 94
 \tempo "a tempo" R4*3 \stopTextSpan | % untitled/b2 untitled/b2.t2 60-87; 95
 R4*3 | % untitled/b2 untitled/b2.t2 60-87; 96
 R4*3 | % untitled/b2 untitled/b2.t2 60-87; 97
 r4. a'''8~ \p a'''4 | % untitled/b2 untitled/b2.t2 136.5-138; 98
 a'''2.~ \f | % untitled/b3 untitled/b3.t1 0-7; 99
 a'''2.~ | % untitled/b3 untitled/b3.t1 0-7; 100
 a'''4 g'''4 f'''4 | % untitled/b3 untitled/b3.t1 8-9; 101
 ef'''4 f'''4 g'''4 | % untitled/b3 untitled/b3.t1 11-12; 102
 a'''4~ a'''4. a'''8 -^ | % untitled/b3 untitled/b3.t1 14.5-15; 103
 a'''2.~ -^ \ff | % untitled/b3 untitled/b3.t1 15-18.5; 104
 a'''8 -^ a'''8~ a'''4 g'''4 | % untitled/b3 untitled/b3.t1 20-21; 105
 f'''4 ef'''4 f'''4 | % untitled/b3 untitled/b3.t1 23-24; 106
 g'''4 a'''2~ | % untitled/b3 untitled/b3.t1 25-29; 107
 a'''2 c''4 | % untitled/b3 untitled/b3.t1 29-30; 108
 c'''4 f''4 e''4 | % untitled/b3 untitled/b3.t1 32-33; 109
 d''2 bf''4 | % untitled/b3 untitled/b3.t1 35-36; 110
 c''4 \> a''4 g''4 | % untitled/b3 untitled/b3.t1 38-39; 111
 bf'4 g''4 e''4 | % untitled/b3 untitled/b3.t1 41-42; 112
 a'2.~ \mp | % untitled/b3 untitled/b3.t1 42-48; 113
 a'2. | % untitled/b3 untitled/b3.t1 42-48; 114
 bf'4 a'4 g'4 | % untitled/b3 untitled/b3.t1 50-51; 115
 c''4 f'4 e'4 | % untitled/b3 untitled/b3.t1 53-54; 116
 g'2.~ | % untitled/b3 untitled/b3.t1 54-59; 117
 g'2 a''4~ \p | % untitled/b3 untitled/b3.t1 59-65; 118
 a''2.~ | % untitled/b3 untitled/b3.t1 59-65; 119
 a''2 g''4 | % untitled/b3 untitled/b3.t1 65-66; 120
 f''4 ( ef''4 f''4 ) | % untitled/b3 untitled/b3.t1 68-69; 121
 g''4 ( a''4. ) c''8 | % untitled/b3 untitled/b3.t1 71.5-72; 122
 a''4 ( bf''4 ) f''4 ( | % untitled/b3 untitled/b3.t1 74-75; 123
 e'''4 ) g''4 ( bf''4 ) | % untitled/b3 untitled/b3.t1 77-78; 124
 a'''2. | % untitled/b3 untitled/b3.t1 78-81; 125
 r4 g'''4 ( f'''4 ) | % untitled/b3 untitled/b3.t1 83-84; 126
 e'''4 ( f'''4 g'''4 ) | % untitled/b3 untitled/b3.t1 86-87; 127
 e'''2 d'''4 ( | % untitled/b3 untitled/b3.t1 89-90; 128
 a''4 ) e''2 | % untitled/b3 untitled/b3.t1 91-93; 129
 f''4 e''4 g''4 | % untitled/b3 untitled/b3.t1 95-96; 130
 f''8 ( g''8 ) e''2~ | % untitled/b3 untitled/b3.t1 97-99.5; 131
 e''8 c''8 ( \< d''8 b'4 a'8 ) | % untitled/b3 untitled/b3.t1 101.5-102; 132
 R4*3 \f | % untitled/b3 untitled/b3.t1 101.5-102; 133
 a''2.~ ^"nv" \p | % untitled/b3 untitled/b3.t1 105-111; 134
 a''2. | % untitled/b3 untitled/b3.t1 105-111; 135
 a'''2.~ | % untitled/b3 untitled/b3.t1 111-118; 136
 \textSpannerDown \override TextSpanner.bound-details.left.text = \markup { "rit. poco a poco" } a'''2.~ \startTextSpan | % untitled/b3 untitled/b3.t1 111-118; 137
 a'''4 <a'~ a''~\harmonic>2 | % untitled/b3 untitled/b3.t1 118-128; 138
 <a'~ a''~\harmonic>2. | % untitled/b3 untitled/b3.t1 118-128; 139
 <a'~ a''~\harmonic>2. | % untitled/b3 untitled/b3.t1 118-128; 140
 <a' a''\harmonic>2 r4 | % untitled/b3 untitled/b3.t1 118-128; 141
 a'4 ^"vib" r2 | % untitled/b3 untitled/b3.t1 129-130; 142
 r4 a'4 r4 | % untitled/b3 untitled/b3.t1 133-134; 143
 r2 a'4 | % untitled/b3 untitled/b3.t1 137-138; 144
 R4*3 \stopTextSpan | % untitled/b3 untitled/b3.t1 137-138; 145
 a'4 \pp r2 \bar "|."
} }

\new Staff  {
\set Staff.instrumentName = "vln2"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 3/4 \key c \major R4*3 | % untitled/b3 untitled/b3.t1 141-142; 1
 R4*3 | % untitled/b3 untitled/b3.t1 141-142; 2
 r2 a'''4~ \p | % untitled/b1 untitled/b1.t10 8-12; 3
 a'''2. | % untitled/b1 untitled/b1.t10 8-12; 4
 <a' d''\harmonic>2. | % untitled/b1 untitled/b1.t10 12-15; 5
 g'''4 f'''4 ( ef'''4 | % untitled/b1 untitled/b1.t10 17-18; 6
 f'''4 ) g'''4 ( a'''4~ | % untitled/b1 untitled/b1.t10 20-23.5; 7
 a'''4~ a'''4. ) c'''8 | % untitled/b1 untitled/b1.t10 23.5-24; 8
 a'''2 bf''4 ( | % untitled/b1 untitled/b1.t10 26-27; 9
 ef'''4 g''4 ) c'''4 ( | % untitled/b1 untitled/b1.t10 29-30; 10
 f'''4 ) a'''2 | % untitled/b1 untitled/b1.t10 31-33; 11
 bf''4 ( g'''4 d'''4 ) | % untitled/b1 untitled/b1.t10 35-35.95; 12
 f''4 ( e'''4 g''4 ) | % untitled/b1 untitled/b1.t10 38-38.95; 13
 f'''4 e'''2~ \< | % untitled/b1 untitled/b1.t10 40-42.5; 14
 e'''8 c'''8 ( d'''8 bf''4 a''8 ) | % untitled/b1 untitled/b1.t10 44.5-45; 15
 d'4 ( \f a'4 d''4 | % untitled/b1 untitled/b1.t10 47-48; 16
 ef''4 ) d''4 ( a'4 | % untitled/b1 untitled/b1.t10 50-51; 17
 d'4 a'4 ) d''4 ( | % untitled/b1 untitled/b1.t10 53-54; 18
 ef''4 d''4 a'4 ) | % untitled/b1 untitled/b1.t10 56-56.95; 19
 R4*3 | % untitled/b1 untitled/b1.t10 56-56.95; 20
 R4*3 | % untitled/b1 untitled/b1.t10 56-56.95; 21
 R4*3 | % untitled/b1 untitled/b1.t10 56-56.95; 22
 \arpeggioArrowUp <g' a' e''>2 \arpeggio ^"pizz." r4 | % untitled/b1 untitled/b1.t10 66-68; 23
 R4*3 | % untitled/b1 untitled/b1.t10 66-68; 24
 \textSpannerDown \override TextSpanner.bound-details.left.text = \markup { "accel" } a'4 \startTextSpan r4 \once \override Glissando.style = #'dashed-line \once \set glissandoMap = #'((1 . 1)) <a'~ a''\harmonic>4 \glissando ^"arco" | % untitled/b1 untitled/b1.t10 74-75; 25
 <a' d''\harmonic>8 r8 bf'8 ( \p a'8 c''8 ) a'8 ( | % untitled/b1 untitled/b1.t10 77.5-78; 26
 bf'8 a'8 c''8 ) a'8 ( bf'8 a'8 | % untitled/b1 untitled/b1.t10 80.5-81; 27
 c''8 ) a'8 ( bf'8 a'8 c''8 ) a'8 ( | % untitled/b1 untitled/b1.t10 83.5-84; 28
 bf'8 a'8 c''8 ) a'8 ( bf'8 a'8 | % untitled/b1 untitled/b1.t10 86.5-87; 29
 c''8 ) a'8 ( bf'8 a'8 c''8 ) a'8 ( | % untitled/b1 untitled/b1.t10 89.5-90; 30
 bf'8 a'8 c''8 ) a'8 ( bf'8 \< a'8 | % untitled/b1 untitled/b1.t10 92.5-93; 31
 c''8 ) a'8 ( bf'8 a'8 c''8 ) a'8 ( | % untitled/b1 untitled/b1.t10 95.5-96; 32
 bf'8 a'8 c''8 ) a'8 ( bf'8 a'8 | % untitled/b1 untitled/b1.t10 98.5-99; 33
 c''8 ) a'8 ( bf'8 a'8 c''8 ) a'8 | % untitled/b1 untitled/b1.t10 101.5-102; 34
 r4 \f \stopTextSpan <a'~ d''~\harmonic>2 \p | % untitled/b1 untitled/b1.t10 103-108; 35
 <a' d''\harmonic>2. | % untitled/b1 untitled/b1.t10 103-108; 36
 \repeat tremolo 12 { d'32( a'32) } | % untitled/b1 untitled/b1.t10 103-108; 37
 \repeat tremolo 12 { d'32( a'32) } | % untitled/b1 untitled/b1.t10 103-108; 38
 \repeat tremolo 12 { d'32( a'32) } | % untitled/b1 untitled/b1.t10 103-108; 39
 \repeat tremolo 12 { d'32( a'32) } | % untitled/b1 untitled/b1.t10 103-108; 40
 \repeat tremolo 12 { d'32( a'32) } | % untitled/b1 untitled/b1.t10 103-108; 41
 \repeat tremolo 12 { d'32( a'32) } | % untitled/b1 untitled/b1.t10 103-108; 42
 \repeat tremolo 12 { d'32( a'32) } | % untitled/b1 untitled/b1.t10 103-108; 43
 \repeat tremolo 12 { d'32( a'32) } | % untitled/b1 untitled/b1.t10 103-108; 44
 \repeat tremolo 8 { d'32( a'32) } \tuplet 6/4 { d'16 ( a'16 d'16 a'16 d'16 a'16 } | % untitled/b1 untitled/b1.t10 103-108; 45
 \tuplet 6/4 { d'16 a'16 d'16 a'16 d'16 a'16 } d'16 a'16 d'16 a'16 d'16 a'16 d'16 a'16 | % untitled/b1 untitled/b1.t10 137.75-138; 46
 \tuplet 3/2 { d'8 a'8 d'8 } a'8 d'8 ) d''4 ( | % untitled/b1 untitled/b1.t10 140-141; 47
 a'4 d'4 a'4 ) | % untitled/b1 untitled/b1.t10 143-144; 48
 d''4 ( ef''4 d''4 | % untitled/b1 untitled/b1.t10 146-147; 49
 a'4 c'4 g'4 | % untitled/b1 untitled/b1.t10 149-150; 50
 d''4 c''4 ) f'4 ( | % untitled/b1 untitled/b1.t10 152-153; 51
 d'4 -\markup \italic "(no crescendo)" g4 d'4 ) | % untitled/b1 untitled/b1.t10 155-156; 52
 bf'2.~ ( ^"nv" | % untitled/b2 untitled/b2.t9 0-16; 53
 bf'2.~ | % untitled/b2 untitled/b2.t9 0-16; 54
 bf'2.~ | % untitled/b2 untitled/b2.t9 0-16; 55
 bf'2.~ | % untitled/b2 untitled/b2.t9 0-16; 56
 bf'2.~ | % untitled/b2 untitled/b2.t9 0-16; 57
 bf'4 \glissando a'2~ | % untitled/b2 untitled/b2.t9 16-19; 58
 a'4 ) r4 d'4 ^"vib" | % untitled/b2 untitled/b2.t9 20-21; 59
 c''4 ( f'4 e'4 | % untitled/b2 untitled/b2.t9 23-24; 60
 d'2 ) bf'4 ( | % untitled/b2 untitled/b2.t9 26-27; 61
 c'4 ) a'4 ( g'4 | % untitled/b2 untitled/b2.t9 29-30; 62
 bf4 ) g'4 ( e'4 ) | % untitled/b2 untitled/b2.t9 32-33; 63
 a2.~ | % untitled/b2 untitled/b2.t9 33-37; 64
 a4 bf16 ( a16~ a8 ) a4 | % untitled/b2 untitled/b2.t9 38-39; 65
 a4 g4 a4 | % untitled/b2 untitled/b2.t9 41-42; 66
 bf16 ( a16~ a8 ) bf16 ( a16~ a8 ) a4 | % untitled/b2 untitled/b2.t9 44-45; 67
 a4 c'4 \glissando a4 | % untitled/b2 untitled/b2.t9 47-48; 68
 g4 bf16 ( a16~ a8 ) a4 | % untitled/b2 untitled/b2.t9 50-51; 69
 a4 g4 a4 | % untitled/b2 untitled/b2.t9 53-54; 70
 c'16 ( d'16~ d'8 ) a4 g4 | % untitled/b2 untitled/b2.t9 56-57; 71
 c'4 a4 g16 ( a16~ a8 ) | % untitled/b2 untitled/b2.t9 59.25-60; 72
 c'8 ( d'8 ) a4 g4 | % untitled/b2 untitled/b2.t9 62-63; 73
 c'8 ( d'8 g8 a8 c'8 ) r8 | % untitled/b2 untitled/b2.t9 65-65.5; 74
 c'4 ( d'4 ) a4 ( | % untitled/b2 untitled/b2.t9 68-69; 75
 d'4 ) c'4 ( a4 | % untitled/b2 untitled/b2.t9 71-72; 76
 g4 ) r4 c'4 ( | % untitled/b2 untitled/b2.t9 74-75; 77
 a4 g4 ) r4 | % untitled/b2 untitled/b2.t9 76-77; 78
 c'4 r4 a4 | % untitled/b2 untitled/b2.t9 80-81; 79
 r4 c'4 ( a4 | % untitled/b2 untitled/b2.t9 83-84; 80
 g4 ) r4 c'4 ( | % untitled/b2 untitled/b2.t9 86-87; 81
 d'4 ) a4 ( d'4 ) | % untitled/b2 untitled/b2.t9 89-90; 82
 c'4 ( a4 g4 ) | % untitled/b2 untitled/b2.t9 92-93; 83
 r4 c'4 ( a4 | % untitled/b2 untitled/b2.t9 95-96; 84
 g4 ) d'4 ( c'4 ) | % untitled/b2 untitled/b2.t9 98-99; 85
 \textSpannerDown \override TextSpanner.bound-details.left.text = \markup { "accel" } r8 \startTextSpan a8 r8 c'8 ( d'8 ) a8 | % untitled/b2 untitled/b2.t9 101.5-102; 86
 r8 g8 ( a8 ) c'8 r8 a8 ( | % untitled/b2 untitled/b2.t9 104.5-105; 87
 c'8 ) g8 ( a8 c'8 d'8 ) a8 | % untitled/b2 untitled/b2.t9 107.5-108; 88
 d'8 ( \< c'8 ) r8 a8 r8 d'8 ( | % untitled/b2 untitled/b2.t9 110.5-111; 89
 a8 d'8 ) c'8 ( a8 g4 ) -. | % untitled/b2 untitled/b2.t9 113-114; 90
 d'2 ^"pizz." \f c'4 | % untitled/b2 untitled/b2.t9 116-117; 91
 r4 g2 | % untitled/b2 untitled/b2.t9 118-120; 92
 r8 d'4 a8 g4 | % untitled/b2 untitled/b2.t9 122-123; 93
 c'4 g4 r4 | % untitled/b2 untitled/b2.t9 124-125; 94
 f'8 ( ^"arco" \stopTextSpan e'8 ) g'8 ( e'16 g'16 f'8 ) -. e'16 ( a'16 | % untitled/b2 untitled/b2.t9 128.75-129; 95
 g'8 ) -. e'16 ( a'16 g'8 ) -. e'16 ( c''16 a'8 ) -. e'8 -. | % untitled/b2 untitled/b2.t9 131.5-132; 96
 e''16 ( c''8 ) -. f''16 ( e''8 ) -. g''16 ( e''16~ -. e''16 ) -. e''16 ( g''8 ) -. | % untitled/b2 untitled/b2.t9 134.5-135; 97
 e''16 ( g''8 ) -. g''16 ( e''8 ) -. g''16 ( e''16 g''8 ) -. r8 | % untitled/b2 untitled/b2.t9 137-137.5; 98
 a''8 \f bf''8 a''8 bf''8 a''8 bf''8 | % untitled/b3 untitled/b3.t8 2.5-3; 99
 a''8 g''8 a''8 bf''8 a''8 g''8 | % untitled/b3 untitled/b3.t8 5.5-6; 100
 a''8 g''8 a''8 bf''8 a''8 c'''8 | % untitled/b3 untitled/b3.t8 8.5-9; 101
 a''8 bf''8 a''8 bf''8 a''8 bf''8 | % untitled/b3 untitled/b3.t8 11.5-12; 102
 a''8 g''8 a''8 bf''8 a''8 g''8 | % untitled/b3 untitled/b3.t8 14.5-15; 103
 d'''8 \ff a''8 d'''8 a''8 d'''8 a''8 | % untitled/b3 untitled/b3.t8 17.5-18; 104
 d'''8 a''8 d'''8 a''8 d'''8 a''8 | % untitled/b3 untitled/b3.t8 20.5-21; 105
 c'''8 a''8 c'''8 a''8 c'''8 a''8 | % untitled/b3 untitled/b3.t8 23.5-24; 106
 d'''8 a''8 d'''8 a''8 d'''8 a''8 | % untitled/b3 untitled/b3.t8 26.5-27; 107
 a''8 bf''8 a''8 bf''8 a''8 bf''8 | % untitled/b3 untitled/b3.t8 29.5-30; 108
 a''8 g''8 a''8 bf''8 a''8 g''8 | % untitled/b3 untitled/b3.t8 32.5-33; 109
 f''8 g''8 f''8 g''8 f''8 g''8 | % untitled/b3 untitled/b3.t8 35.5-36; 110
 f''8 \> e''8 f''8 g''8 f''8 e''8 | % untitled/b3 untitled/b3.t8 38.5-39; 111
 e''8 f''8 e''8 f''8 e''8 d''8 | % untitled/b3 untitled/b3.t8 41.5-42; 112
 r4 \mp e''8 ( f''8 e''8 f''8 ) | % untitled/b3 untitled/b3.t8 44.5-45; 113
 e''8 ( d''8 ) e''8 ( f''8 g''8 f''8 | % untitled/b3 untitled/b3.t8 47.5-48; 114
 g''8 f''8 ) e''8 ( d''8 e''8 d''8 ) | % untitled/b3 untitled/b3.t8 50.5-51; 115
 e''8 ( f''8 e''8 f''8 ) e''8 ( d''8 | % untitled/b3 untitled/b3.t8 53.5-54; 116
 e''8 d''8 e''8 d''8 ) e''8 ( f''8 | % untitled/b3 untitled/b3.t8 56.5-57; 117
 e''8 d''8 ) e''8 ( d''8 e''8 d''8 ) | % untitled/b3 untitled/b3.t8 59.5-60; 118
 \tuplet 3/2 { e''8 ( \p d''8 e''8 } d''16 e''16 d''16 e''16 \tuplet 6/4 { d''16 e''16 d''16 e''16 d''16 e''16 ) } | % untitled/b3 untitled/b3.t8 61.75-62; 119
 d''2.~ \trill | % untitled/b3 untitled/b3.t8 63-69; 120
 d''2. \trill | % untitled/b3 untitled/b3.t8 63-69; 121
 d''2.~ \trill | % untitled/b3 untitled/b3.t8 69-75; 122
 d''2. \trill | % untitled/b3 untitled/b3.t8 69-75; 123
 d''2. \trill | % untitled/b3 untitled/b3.t8 75-78; 124
 \tuplet 6/4 { d''16 ( e''16 d''16 e''16 d''16 e''16 } \tuplet 6/4 { d''16 e''16 d''16 e''16 d''16 e''16 } d''16 e''16 d''16 e''16 ) | % untitled/b3 untitled/b3.t8 80.75-81; 125
 d''16 ( e''16 \tuplet 3/2 { d''8 e''8 d''8 } e''8 d''8 e''8 ) | % untitled/b3 untitled/b3.t8 83.5-84; 126
 d''8 ( e''8 ) d''4 ( a'4 ) | % untitled/b3 untitled/b3.t8 86-87; 127
 d'4 ( a'4 d''4 | % untitled/b3 untitled/b3.t8 89-90; 128
 e''4 ) d''4 ( a'4 | % untitled/b3 untitled/b3.t8 92-93; 129
 c'4 g'4 ) d''4 ( | % untitled/b3 untitled/b3.t8 95-96; 130
 c''4 f'4 d'4 ) | % untitled/b3 untitled/b3.t8 98-99; 131
 g4 ( \< d'4 b'4 ) \f | % untitled/b3 untitled/b3.t8 101-102; 132
 c''2.~ \p | % untitled/b3 untitled/b3.t8 102-110; 133
 c''2.~ | % untitled/b3 untitled/b3.t8 102-110; 134
 c''2 b'4~ | % untitled/b3 untitled/b3.t8 110-116; 135
 b'2.~ | % untitled/b3 untitled/b3.t8 110-116; 136
 \textSpannerDown \override TextSpanner.bound-details.left.text = \markup { "rit. poco a poco" } b'2 \startTextSpan g'4~ | % untitled/b3 untitled/b3.t8 116-123; 137
 g'2.~ | % untitled/b3 untitled/b3.t8 116-123; 138
 g'2. | % untitled/b3 untitled/b3.t8 116-123; 139
 e'2.~ ^"nv" | % untitled/b3 untitled/b3.t8 123-128; 140
 e'2 r4 | % untitled/b3 untitled/b3.t8 123-128; 141
 e'4 ^"pizz." r2 | % untitled/b3 untitled/b3.t8 129-130; 142
 r4 e'4 r4 | % untitled/b3 untitled/b3.t8 133-134; 143
 r2 e'4 | % untitled/b3 untitled/b3.t8 137-138; 144
 R4*3 \stopTextSpan | % untitled/b3 untitled/b3.t8 137-138; 145
 e'4 \pp r2 \bar "|."
} }

\new Staff  {
\set Staff.instrumentName = "vla"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 3/4 \key c \major \clef alto d'4 ( \p a'4 d''4 | % untitled/b1 untitled/b1.t22 2-3; 1
 ef''4 ) d''4 ( a'4 | % untitled/b1 untitled/b1.t22 5-6; 2
 d'4 a'4 ) d''4 ( | % untitled/b1 untitled/b1.t22 8-9; 3
 ef''4 d''4 a'4 ) | % untitled/b1 untitled/b1.t22 11-12; 4
 d'4 ( a'4 d''4 | % untitled/b1 untitled/b1.t22 14-15; 5
 ef''4 ) d''4 ( a'4 | % untitled/b1 untitled/b1.t22 17-18; 6
 d'4 a'4 ) d''4 ( | % untitled/b1 untitled/b1.t22 20-21; 7
 ef''4 d''4 a'4 ) | % untitled/b1 untitled/b1.t22 23-24; 8
 d'4 ( a'4 d''4 | % untitled/b1 untitled/b1.t22 26-27; 9
 ef''4 ) d''4 ( a'4 | % untitled/b1 untitled/b1.t22 29-30; 10
 d'4 a'4 ) d''4 ( | % untitled/b1 untitled/b1.t22 32-33; 11
 ef''4 d''4 a'4 ) | % untitled/b1 untitled/b1.t22 35-36; 12
 c'4 ( g'4 d''4 | % untitled/b1 untitled/b1.t22 38-39; 13
 c''4 ) f'4 ( \< d'4 | % untitled/b1 untitled/b1.t22 41-42; 14
 g4 d'4 ) bf'4 | % untitled/b1 untitled/b1.t22 44-45; 15
 d'4 ^"pizz." \f a'4 d''4 | % untitled/b1 untitled/b1.t22 47-48; 16
 ef''4 d''4 a'4 | % untitled/b1 untitled/b1.t22 50-51; 17
 d'4 a'4 d''4 | % untitled/b1 untitled/b1.t22 53-54; 18
 ef''4 d''4 a'4 | % untitled/b1 untitled/b1.t22 56-57; 19
 <d' d''>2 r4 | % untitled/b1 untitled/b1.t22 57-59; 20
 R4*3 | % untitled/b1 untitled/b1.t22 57-59; 21
 r4 g4 d'4 | % untitled/b1 untitled/b1.t22 65-66; 22
 bf'2 r4 | % untitled/b1 untitled/b1.t22 66-68; 23
 r8 \> f4 d'4 r8 | % untitled/b1 untitled/b1.t22 70.5-71.5; 24
 \textSpannerDown \override TextSpanner.bound-details.left.text = \markup { "accel" } a'8 ( ^"arco" \p \startTextSpan bf'8 a'8 c''8 ) a'8 ( bf'8 | % untitled/b1 untitled/b1.t22 74.5-75; 25
 a'8 c''8 ) a'8 ( bf'8 a'8 c''8 ) | % untitled/b1 untitled/b1.t22 77.5-78; 26
 a'8 ( bf'8 a'8 c''8 ) a'8 ( bf'8 | % untitled/b1 untitled/b1.t22 80.5-81; 27
 a'8 c''8 ) a'8 ( bf'8 a'8 c''8 ) | % untitled/b1 untitled/b1.t22 83.5-84; 28
 a'8 ( bf'8 a'8 c''8 ) a'8 ( bf'8 | % untitled/b1 untitled/b1.t22 86.5-87; 29
 a'8 c''8 ) a'8 ( bf'8 a'8 c''8 ) | % untitled/b1 untitled/b1.t22 89.5-90; 30
 a'8 ( bf'8 a'8 c''8 ) a'8 ( \< bf'8 | % untitled/b1 untitled/b1.t22 92.5-93; 31
 a'8 c''8 ) a'8 ( bf'8 a'8 c''8 ) | % untitled/b1 untitled/b1.t22 95.5-96; 32
 a'8 ( bf'8 a'8 c''8 ) a'8 ( bf'8 | % untitled/b1 untitled/b1.t22 98.5-99; 33
 a'8 c''8 ) a'8 ( bf'8 a'8 c''8 ) | % untitled/b1 untitled/b1.t22 101.5-102; 34
 r4 \f \stopTextSpan <d'~ ef''~>2 \p | % untitled/b1 untitled/b1.t22 103-108; 35
 <d' ef''>2. | % untitled/b1 untitled/b1.t22 103-108; 36
 \repeat tremolo 12 { d'32( ef''32) } | % untitled/b1 untitled/b1.t22 103-108; 37
 \repeat tremolo 12 { d'32( ef''32) } | % untitled/b1 untitled/b1.t22 103-108; 38
 \repeat tremolo 12 { d'32( ef''32) } | % untitled/b1 untitled/b1.t22 103-108; 39
 \repeat tremolo 12 { d'32( ef''32) } | % untitled/b1 untitled/b1.t22 103-108; 40
 \repeat tremolo 12 { d'32( ef''32) } | % untitled/b1 untitled/b1.t22 103-108; 41
 \repeat tremolo 12 { d'32( ef''32) } | % untitled/b1 untitled/b1.t22 103-108; 42
 \pitchedTrill <d'~ d''~>2. \startTrillSpan ef'' | % untitled/b1 untitled/b1.t22 126-132; 43
 <d' d''>2. \stopTrillSpan | % untitled/b1 untitled/b1.t22 126-132; 44
 \tuplet 6/4 { d''16 ( ef''16 d''16 ef''16 d''16 ef''16 } \tuplet 6/4 { d''16 ef''16 d''16 ef''16 d''16 ef''16 } d''16 ef''16 d''16 ef''16 | % untitled/b1 untitled/b1.t22 134.75-135; 45
 d''16 ef''16 \tuplet 3/2 { d''8 ef''8 d''8 } ef''8 d''8 ef''8 | % untitled/b1 untitled/b1.t22 137.5-138; 46
 d''8 ef''8 ) d''4 ( a'4 | % untitled/b1 untitled/b1.t22 140-141; 47
 d'4 a'4 ) d''4 ( | % untitled/b1 untitled/b1.t22 143-144; 48
 ef''4 d''4 a'4 ) | % untitled/b1 untitled/b1.t22 146-147; 49
 c'4 ( g'4 d''4 | % untitled/b1 untitled/b1.t22 149-150; 50
 c''4 ) f'4 ( d'4 | % untitled/b1 untitled/b1.t22 152-153; 51
 g4 \< d'4 bf'4 ) | % untitled/b1 untitled/b1.t22 155-156; 52
 r4 \f d4 ^"pizz." \p d4 | % untitled/b2 untitled/b2.t19 2-3; 53
 r2 d4 | % untitled/b2 untitled/b2.t19 5-6; 54
 d4 r2 | % untitled/b2 untitled/b2.t19 6-7; 55
 d4 d4 r4 | % untitled/b2 untitled/b2.t19 10-11; 56
 r4 d4 d4 | % untitled/b2 untitled/b2.t19 14-15; 57
 r2 d4 | % untitled/b2 untitled/b2.t19 17-18; 58
 d4 d2 | % untitled/b2 untitled/b2.t19 19-21; 59
 R4*3 | % untitled/b2 untitled/b2.t19 19-21; 60
 r4 g4 d'4 | % untitled/b2 untitled/b2.t19 26-27; 61
 a'4 r4 g'4 | % untitled/b2 untitled/b2.t19 29-30; 62
 r4 d4 ( ^"arco" a4 | % untitled/b2 untitled/b2.t19 32-33; 63
 d'4 ef'4 ) d'4 ( | % untitled/b2 untitled/b2.t19 35-36; 64
 c'4 d4 a4 ) | % untitled/b2 untitled/b2.t19 38-39; 65
 d'4 ( e'4 d'4 | % untitled/b2 untitled/b2.t19 41-42; 66
 c'4 ) d4 ( a4 | % untitled/b2 untitled/b2.t19 44-45; 67
 d'4 e'4 d'4 ) | % untitled/b2 untitled/b2.t19 47-48; 68
 c'4 ( a4 d4 | % untitled/b2 untitled/b2.t19 50-51; 69
 a4 ) d'4 ( e'4 | % untitled/b2 untitled/b2.t19 53-54; 70
 c'4 a4 ) d4 ( | % untitled/b2 untitled/b2.t19 56-57; 71
 a4 d'4 e'4 | % untitled/b2 untitled/b2.t19 59-60; 72
 d'4 ) a4 ( d4 | % untitled/b2 untitled/b2.t19 62-63; 73
 a4 d'4 e'4~ | % untitled/b2 untitled/b2.t19 65-73; 74
 e'2.~ | % untitled/b2 untitled/b2.t19 65-73; 75
 e'2.~ | % untitled/b2 untitled/b2.t19 65-73; 76
 e'4 ) r2 | % untitled/b2 untitled/b2.t19 65-73; 77
 \once \override Glissando.style = #'dashed-line \once \set glissandoMap = #'((1 . 1)) <d'~ g'\harmonic>2 \glissando <d' d''\harmonic>4 | % untitled/b2 untitled/b2.t19 75-78; 78
 <d'~ d''~\harmonic>2. | % untitled/b2 untitled/b2.t19 78-87; 79
 <d'~ d''~\harmonic>2. \> | % untitled/b2 untitled/b2.t19 78-87; 80
 <d' d''\harmonic>2. | % untitled/b2 untitled/b2.t19 78-87; 81
 R4*3 \! | % untitled/b2 untitled/b2.t19 78-87; 82
 \clef treble <a' d''\harmonic>2 \< <a'~ a''~\harmonic>4 \f \> | % untitled/b2 untitled/b2.t19 92-99; 83
 <a'~ a''~\harmonic>2. | % untitled/b2 untitled/b2.t19 92-99; 84
 <a' a''\harmonic>2. | % untitled/b2 untitled/b2.t19 92-99; 85
 \textSpannerDown \override TextSpanner.bound-details.left.text = \markup { "accel" } R4*3 \p \startTextSpan | % untitled/b2 untitled/b2.t19 92-99; 86
 R4*3 | % untitled/b2 untitled/b2.t19 92-99; 87
 R4*3 | % untitled/b2 untitled/b2.t19 92-99; 88
 \clef alto r8 c8 ^"pizz." r8 a8 r4 | % untitled/b2 untitled/b2.t19 109.5-110; 89
 d4 c4 g4 | % untitled/b2 untitled/b2.t19 113-114; 90
 d'4 ^"arco" \f a8 ( d'8 ) c'4 | % untitled/b2 untitled/b2.t19 116-117; 91
 a8 ( c'8 ) g8 ( a8 ) c'8 ( d'8 ) | % untitled/b2 untitled/b2.t19 119.5-119.97; 92
 a8 ( d'8 ) c'8 ( a8 g8 ) r8 | % untitled/b2 untitled/b2.t19 122-122.5; 93
 c'8 ( a8 g8 ) r8 a8 ( g8 ) | % untitled/b2 untitled/b2.t19 125.5-126; 94
 c'16 ( \stopTextSpan a16 g8 ) -. a8 g16 ( d'16 ) c'16 ( a16 g8 ) -. | % untitled/b2 untitled/b2.t19 128.5-129; 95
 d'16 ( c'16 a16 g16 ) d'16 ( c'16 a16 g16 ) f'16 ( d'16 c'16 a16 ) | % untitled/b2 untitled/b2.t19 131.75-132; 96
 c'16 ( a'16 f'16 e'16 ) c''16 ( a'16 g'16 ) d''16 ( bf'8 ) -. c''16 ( d''16~ -. | % untitled/b2 untitled/b2.t19 134.75-135.25; 97
 d''16 ) -. c''16 ( d''8 ) -. d''16 ( c''8 ) -. d''16 ( c''8 ) -. r8 | % untitled/b2 untitled/b2.t19 137-137.5; 98
 d''16 \f g''8 d''16~ d''16 g''8 d''16~ d''16 g''8 d''16~ | % untitled/b3 untitled/b3.t18 2.75-3.25; 99
 d''16 e''8 d''16~ d''16 g''8 d''16~ d''16 e''8 d''16~ | % untitled/b3 untitled/b3.t18 5.75-6.25; 100
 d''16 e''8 d''16~ d''16 g''8 d''16~ d''16 ef''8 d''16~ | % untitled/b3 untitled/b3.t18 8.75-9.25; 101
 d''16 ef''8 d''16~ d''16 g''8 d''16~ d''16 g''8 d''16~ | % untitled/b3 untitled/b3.t18 11.75-12.25; 102
 d''16 ef''8 d''16~ d''16 g''8 d''16~ d''16 ef''8 d''16~ | % untitled/b3 untitled/b3.t18 14.75-15.25; 103
 d''16 \ff bf''8 d''16~ d''16 bf''8 d''16~ d''16 bf''8 d''16~ | % untitled/b3 untitled/b3.t18 17.75-18.25; 104
 d''16 bf''8 d''16~ d''16 bf''8 d''16~ d''16 bf''8 d''16~ | % untitled/b3 untitled/b3.t18 20.75-21.25; 105
 d''16 bf''8 d''16~ d''16 bf''8 d''16~ d''16 bf''8 d''16~ | % untitled/b3 untitled/b3.t18 23.75-24.25; 106
 d''16 bf''8 d''16~ d''16 bf''8 d''16~ d''16 bf''8 d''16~ | % untitled/b3 untitled/b3.t18 26.75-27.25; 107
 d''16 g''8 d''16~ d''16 g''8 d''16~ d''16 g''8 d''16~ | % untitled/b3 untitled/b3.t18 29.75-30.25; 108
 d''16 e''8 d''16~ d''16 g''8 d''16~ d''16 e''8 d''16~ | % untitled/b3 untitled/b3.t18 32.75-33.25; 109
 d''16 e''8 d''16~ d''16 e''8 d''16~ d''16 c''8 d''16~ | % untitled/b3 untitled/b3.t18 35.75-36.25; 110
 d''16 \> c''8 d''16~ d''16 c''8 d''16~ d''16 bf'8 d''16~ | % untitled/b3 untitled/b3.t18 38.75-39.25; 111
 d''16 a'8 d''16~ d''16 a'8 d''16~ d''16 a'8 d''16 | % untitled/b3 untitled/b3.t18 41.75-42; 112
 R4*3 \mp | % untitled/b3 untitled/b3.t18 41.75-42; 113
 R4*3 | % untitled/b3 untitled/b3.t18 41.75-42; 114
 bf4 a'4 g'4 | % untitled/b3 untitled/b3.t18 50-51; 115
 c''4 f'4 e'4 | % untitled/b3 untitled/b3.t18 53-54; 116
 g'2 \acciaccatura { a'8 } e'4~ \p | % untitled/b3 untitled/b3.t18 56-58; 117
 e'4 \acciaccatura { a'8 } g'2 | % untitled/b3 untitled/b3.t18 58-60; 118
 \acciaccatura { bf'8 } a'2 a'4~ | % untitled/b3 untitled/b3.t18 62-65; 119
 \clef treble a'2 g''4 | % untitled/b3 untitled/b3.t18 65-66; 120
 f''4 ( ef''4 f''4 ) | % untitled/b3 untitled/b3.t18 68-69; 121
 g'4 ( a'4. ) c''8 | % untitled/b3 untitled/b3.t18 71.5-72; 122
 a''4 ( bf'4 ) f''4 ( | % untitled/b3 untitled/b3.t18 74-75; 123
 e''4 ) g''4 ( bf''4 ) | % untitled/b3 untitled/b3.t18 77-78; 124
 a''2.~ | % untitled/b3 untitled/b3.t18 78-87; 125
 a''2.~ | % untitled/b3 untitled/b3.t18 78-87; 126
 a''2. | % untitled/b3 untitled/b3.t18 78-87; 127
 r4 g''4 ( f''4 ) | % untitled/b3 untitled/b3.t18 89-90; 128
 e''4 ( f''4 g''4 ) | % untitled/b3 untitled/b3.t18 92-93; 129
 e''4 ( d''2 ) | % untitled/b3 untitled/b3.t18 94-96; 130
 R4*3 | % untitled/b3 untitled/b3.t18 94-96; 131
 \clef alto r8 c'8 ( \< d'8 b4 a8 ) | % untitled/b3 untitled/b3.t18 101.5-102; 132
 r4 \f d4 ( \p a4 | % untitled/b3 untitled/b3.t18 104-105; 133
 d'4 e'4 ) d'4 ( | % untitled/b3 untitled/b3.t18 107-108; 134
 a4 d4 a4 ) | % untitled/b3 untitled/b3.t18 110-111; 135
 d'4 ( e'4 d'4 | % untitled/b3 untitled/b3.t18 113-114; 136
 \textSpannerDown \override TextSpanner.bound-details.left.text = \markup { "rit. poco a poco" } a4 ) \startTextSpan d4 ( a4 | % untitled/b3 untitled/b3.t18 116-117; 137
 d'4 e'4 d'4 | % untitled/b3 untitled/b3.t18 119-120; 138
 a4 ) d4 ( a4 | % untitled/b3 untitled/b3.t18 122-123; 139
 d'4 e'4 d'4 | % untitled/b3 untitled/b3.t18 125-126; 140
 a4 ) d4 ( a4 | % untitled/b3 untitled/b3.t18 128-129; 141
 d'4 e'4 d'4 | % untitled/b3 untitled/b3.t18 131-132; 142
 a4 ) d4 ( a4 | % untitled/b3 untitled/b3.t18 134-135; 143
 d'4 e'4 d'4 | % untitled/b3 untitled/b3.t18 137-138; 144
 a4 ) \stopTextSpan r2 | % untitled/b3 untitled/b3.t18 138-139; 145
 R4*3 \bar "|."
} }

\new Staff  {
\set Staff.instrumentName = "cello"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 3/4 \key c \major \clef bass R4*3 | % untitled/b3 untitled/b3.t18 138-139; 1
 R4*3 | % untitled/b3 untitled/b3.t18 138-139; 2
 R4*3 | % untitled/b3 untitled/b3.t18 138-139; 3
 R4*3 | % untitled/b3 untitled/b3.t18 138-139; 4
 R4*3 | % untitled/b3 untitled/b3.t18 138-139; 5
 R4*3 | % untitled/b3 untitled/b3.t18 138-139; 6
 r8 a4 ^"pizz." \p d,8~ d,4 | % untitled/b1 untitled/b1.t33 19.5-21; 7
 R4*3 | % untitled/b1 untitled/b1.t33 19.5-21; 8
 R4*3 | % untitled/b1 untitled/b1.t33 19.5-21; 9
 r4 ef4 bf,4 | % untitled/b1 untitled/b1.t33 29-30; 10
 g,4 r2 | % untitled/b1 untitled/b1.t33 30-31; 11
 a4 d,2 | % untitled/b1 untitled/b1.t33 34-36; 12
 r4 e4 d4 | % untitled/b1 untitled/b1.t33 38-39; 13
 g,4 r2 | % untitled/b1 untitled/b1.t33 39-40; 14
 r8 g,4 a4 r8 | % untitled/b1 untitled/b1.t33 43.5-44.5; 15
 d,2. \f | % untitled/b1 untitled/b1.t33 45-48; 16
 R4*3 | % untitled/b1 untitled/b1.t33 45-48; 17
 \clef treble bf'4 ^"arco" a'4 c'4 | % untitled/b1 untitled/b1.t33 53-54; 18
 c''4 f''4 g''4 | % untitled/b1 untitled/b1.t33 56-57; 19
 a''2 c''4 | % untitled/b1 untitled/b1.t33 59-60; 20
 c''4 ( f''4 e''4 | % untitled/b1 untitled/b1.t33 62-63; 21
 d''2 ) bf'4 ( | % untitled/b1 untitled/b1.t33 65-66; 22
 c''4 ) a''4 ( g''4 | % untitled/b1 untitled/b1.t33 68-69; 23
 bf'4 ) \> g''4 ( e''4 ) | % untitled/b1 untitled/b1.t33 71-71.95; 24
 \textSpannerDown \override TextSpanner.bound-details.left.text = \markup { "accel" } a'4. \p \startTextSpan <a~ d'~\harmonic>8 :32 \< <a~ d'~\harmonic>4 :32 | % untitled/b1 untitled/b1.t33 73.5-76.5; 25
 <a d'\harmonic>4. :32 \> r8 \p r4 | % untitled/b1 untitled/b1.t33 73.5-76.5; 26
 a'2. | % untitled/b1 untitled/b1.t33 78-81; 27
 R4*3 | % untitled/b1 untitled/b1.t33 78-81; 28
 a'2 r4 | % untitled/b1 untitled/b1.t33 84-86; 29
 \clef bass e'8 ^"pizz." a,8~ a,4 r8 e'8 | % untitled/b1 untitled/b1.t33 89.5-90; 30
 a,8 a,8~ a,4 \clef treble f'8 ( ^"arco" \< e'8 | % untitled/b1 untitled/b1.t33 92.5-93; 31
 g'8 e'8 ) f'8 ( e'16 a'16 g'8 e'8 ) | % untitled/b1 untitled/b1.t33 95.5-95.97; 32
 d'16 ( e'16 ) a'8 -. g'16 ( a'16 ) e'8 -. g'8 -. d'16 ( e'16 ) | % untitled/b1 untitled/b1.t33 98.75-99; 33
 g'8 -. e'8 -. d'16 ( e'16 ) a'8 -. g'16 ( a'16 ) e'16 ( g'16 ) | % untitled/b1 untitled/b1.t33 101.75-101.98; 34
 r4 \f \stopTextSpan <d'~ a'~>2 \p | % untitled/b1 untitled/b1.t33 103-108; 35
 <d' a'>2. | % untitled/b1 untitled/b1.t33 103-108; 36
 a'2. | % untitled/b1 untitled/b1.t33 108-111; 37
 g''4 ( \mf f''4 ef''4 | % untitled/b1 untitled/b1.t33 113-114; 38
 f''4 ) g'4 ( a'4~ | % untitled/b1 untitled/b1.t33 116-119.5; 39
 a'4~ a'4. ) c''8 | % untitled/b1 untitled/b1.t33 119.5-120; 40
 d''2. | % untitled/b1 untitled/b1.t33 120-123; 41
 c''4~ c''4. bf'8 | % untitled/b1 untitled/b1.t33 125.5-126; 42
 d''4 ( g''2 ) | % untitled/b1 untitled/b1.t33 127-129; 43
 a''4 ( bf'2~ | % untitled/b1 untitled/b1.t33 130-133; 44
 bf'4 ) g''4 d''4 | % untitled/b1 untitled/b1.t33 134-135; 45
 e''4 a'2~ \> | % untitled/b1 untitled/b1.t33 136-140; 46
 a'2 f''4~ \p | % untitled/b1 untitled/b1.t33 140-144; 47
 f''2. | % untitled/b1 untitled/b1.t33 140-144; 48
 R4*3 | % untitled/b1 untitled/b1.t33 140-144; 49
 R4*3 | % untitled/b1 untitled/b1.t33 140-144; 50
 R4*3 | % untitled/b1 untitled/b1.t33 140-144; 51
 \clef bass r8 c'8 ( \< d'8 ) bf4 ( a8 ) | % untitled/b1 untitled/b1.t33 155.5-156; 52
 \clef bass r4 \f d,4 ( \p a,4 | % untitled/b2 untitled/b2.t27 2-3; 53
 d4 ef4 ) d4 ( | % untitled/b2 untitled/b2.t27 5-6; 54
 a,4 d,4 a,4 ) | % untitled/b2 untitled/b2.t27 8-8.95; 55
 d4 ( ef4 d4 | % untitled/b2 untitled/b2.t27 11-12; 56
 a,4 ) d,4 ( a,4 | % untitled/b2 untitled/b2.t27 14-15; 57
 d4 ef4 ) d4 ( | % untitled/b2 untitled/b2.t27 17-18; 58
 a,4 ) d,2 ^"pizz." | % untitled/b2 untitled/b2.t27 19-21; 59
 R4*3 | % untitled/b2 untitled/b2.t27 19-21; 60
 R4*3 | % untitled/b2 untitled/b2.t27 19-21; 61
 \tuplet 3/2 { f8 g8 c'8 } r4 \tuplet 3/2 { ef8 f8 bf8 } | % untitled/b2 untitled/b2.t27 19-21; 62
 r4 d4 a4 | % untitled/b2 untitled/b2.t27 32-33; 63
 d'4 c'4 g4 | % untitled/b2 untitled/b2.t27 35-36; 64
 a4 bf4 d4 | % untitled/b2 untitled/b2.t27 38-39; 65
 a4 c'4 \clef treble a'4 | % untitled/b2 untitled/b2.t27 41-42; 66
 g'4 bf4 g'4 | % untitled/b2 untitled/b2.t27 44-45; 67
 e'4 a2 | % untitled/b2 untitled/b2.t27 46-48; 68
 r2 e''4 ( ^"arco" | % untitled/b2 untitled/b2.t27 50-51; 69
 bf'4 e''4 d''4 ) | % untitled/b2 untitled/b2.t27 53-54; 70
 \clef bass <a~ d'~\harmonic>2. | % untitled/b2 untitled/b2.t27 54-61; 71
 <a~ d'~\harmonic>2. | % untitled/b2 untitled/b2.t27 54-61; 72
 <a d'\harmonic>4 \once \override Glissando.style = #'dashed-line \once \set glissandoMap = #'((1 . 1)) <a~ d'\harmonic>2 \glissando | % untitled/b2 untitled/b2.t27 61-64; 73
 <a a'\harmonic>4 r2 | % untitled/b2 untitled/b2.t27 61-64; 74
 R4*3 | % untitled/b2 untitled/b2.t27 61-64; 75
 \clef tenor r4 d'2 \glissando | % untitled/b2 untitled/b2.t27 70-72; 76
 f'2.~ | % untitled/b2 untitled/b2.t27 72-81; 77
 f'2.~ | % untitled/b2 untitled/b2.t27 72-81; 78
 f'2. \glissando | % untitled/b2 untitled/b2.t27 72-81; 79
 ef'2.~ | % untitled/b2 untitled/b2.t27 81-86.5; 80
 ef'4~ ef'4. r8 | % untitled/b2 untitled/b2.t27 81-86.5; 81
 \tuplet 3/2 { f'8 ( d'8 f'8 } d'16 f'16 d'16 f'16 \tuplet 6/4 { d'16 f'16 d'16 f'16 d'16 f'16 ) } | % untitled/b2 untitled/b2.t27 88.75-89; 82
 \repeat tremolo 12 { d'32( f'32) } | % untitled/b2 untitled/b2.t27 88.75-89; 83
 \repeat tremolo 12 { d'32( f'32) } | % untitled/b2 untitled/b2.t27 88.75-89; 84
 \repeat tremolo 12 { d'32( \< f'32) } | % untitled/b2 untitled/b2.t27 88.75-89; 85
 \repeat tremolo 12 { \textSpannerDown \override TextSpanner.bound-details.left.text = \markup { "accel" } d'32( \f \> \startTextSpan ef'32) } | % untitled/b2 untitled/b2.t27 88.75-89; 86
 \repeat tremolo 12 { d'32( ef'32) } | % untitled/b2 untitled/b2.t27 88.75-89; 87
 \repeat tremolo 12 { c'32( \mp ef'32) } | % untitled/b2 untitled/b2.t27 88.75-89; 88
 \repeat tremolo 12 { c'32( ef'32) } | % untitled/b2 untitled/b2.t27 88.75-89; 89
 \repeat tremolo 12 { c'32( ef'32) } | % untitled/b2 untitled/b2.t27 88.75-89; 90
 \repeat tremolo 12 { d'32( \mf f'32) } | % untitled/b2 untitled/b2.t27 88.75-89; 91
 \repeat tremolo 12 { d'32( f'32) } | % untitled/b2 untitled/b2.t27 88.75-89; 92
 \repeat tremolo 12 { d'32( f'32) } | % untitled/b2 untitled/b2.t27 88.75-89; 93
 R4*3 | % untitled/b2 untitled/b2.t27 88.75-89; 94
 R4*3 \stopTextSpan | % untitled/b2 untitled/b2.t27 88.75-89; 95
 R4*3 | % untitled/b2 untitled/b2.t27 88.75-89; 96
 R4*3 | % untitled/b2 untitled/b2.t27 88.75-89; 97
 R4*3 | % untitled/b2 untitled/b2.t27 88.75-89; 98
 R4*3 | % untitled/b2 untitled/b2.t27 88.75-89; 99
 R4*3 | % untitled/b2 untitled/b2.t27 88.75-89; 100
 R4*3 | % untitled/b2 untitled/b2.t27 88.75-89; 101
 R4*3 | % untitled/b2 untitled/b2.t27 88.75-89; 102
 R4*3 | % untitled/b2 untitled/b2.t27 88.75-89; 103
 \clef treble d'2. -^ \ff | % untitled/b3 untitled/b3.t26 15-18; 104
 d'2. | % untitled/b3 untitled/b3.t26 18-21; 105
 d''4 ef''4 f''4 | % untitled/b3 untitled/b3.t26 23-24; 106
 g'4 a'2~ | % untitled/b3 untitled/b3.t26 25-29; 107
 a'2 c''4 | % untitled/b3 untitled/b3.t26 29-30; 108
 c''4 f''4 e''4 | % untitled/b3 untitled/b3.t26 32-33; 109
 d''2 bf'4 | % untitled/b3 untitled/b3.t26 35-36; 110
 c''4 \> a''4 g''4 | % untitled/b3 untitled/b3.t26 38-39; 111
 bf'4 g''4 e''4 | % untitled/b3 untitled/b3.t26 41-42; 112
 a'2.~ \mp | % untitled/b3 untitled/b3.t26 42-49; 113
 a'2.~ | % untitled/b3 untitled/b3.t26 42-49; 114
 a'4 a'4 g'4~ | % untitled/b3 untitled/b3.t26 50-52; 115
 g'4 f'4 e'4 | % untitled/b3 untitled/b3.t26 53-54; 116
 d'2.~ \p | % untitled/b3 untitled/b3.t26 54-66; 117
 d'2.~ | % untitled/b3 untitled/b3.t26 54-66; 118
 d'2.~ | % untitled/b3 untitled/b3.t26 54-66; 119
 d'2. | % untitled/b3 untitled/b3.t26 54-66; 120
 \repeat tremolo 12 { d'32( b'32) } | % untitled/b3 untitled/b3.t26 54-66; 121
 \repeat tremolo 12 { d'32( g'32) } | % untitled/b3 untitled/b3.t26 54-66; 122
 \repeat tremolo 12 { d'32( a'32) } | % untitled/b3 untitled/b3.t26 54-66; 123
 \repeat tremolo 12 { d'32( bf'32) } | % untitled/b3 untitled/b3.t26 54-66; 124
 \repeat tremolo 8 { d'32( bf'32) } \tuplet 6/4 { d'16 ( bf'16 d'16 bf'16 d'16 bf'16 ) } | % untitled/b3 untitled/b3.t26 54-66; 125
 \tuplet 6/4 { d'16 ( bf'16 d'16 bf'16 d'16 bf'16 } d'16 bf'16 d'16 bf'16 d'16 bf'16 d'16 bf'16 ) | % untitled/b3 untitled/b3.t26 83.75-83.98; 126
 \tuplet 3/2 { d'8 ( bf'8 d'8 } bf'8 d'8 ) d''4 ( | % untitled/b3 untitled/b3.t26 86-87; 127
 a'4 ) d'4 ( a'4 | % untitled/b3 untitled/b3.t26 89-90; 128
 d''4 e''4 ) d''4 ( | % untitled/b3 untitled/b3.t26 92-93; 129
 a'4 c'4 g'4 ) | % untitled/b3 untitled/b3.t26 95-96; 130
 d''4 ( c''4 f'4 | % untitled/b3 untitled/b3.t26 98-99; 131
 d'4 ) g4 ( d'4 ) | % untitled/b3 untitled/b3.t26 101-102; 132
 \clef bass r4 d,4 ^"pizz." \p d,4 | % untitled/b3 untitled/b3.t26 104-105; 133
 r2 d,4 | % untitled/b3 untitled/b3.t26 107-108; 134
 d,4 r2 | % untitled/b3 untitled/b3.t26 108-109; 135
 d,4 d,4 r4 | % untitled/b3 untitled/b3.t26 112-113; 136
 \textSpannerDown \override TextSpanner.bound-details.left.text = \markup { "rit. poco a poco" } r4 \startTextSpan d,4 d,4 | % untitled/b3 untitled/b3.t26 116-117; 137
 r2 d,4 | % untitled/b3 untitled/b3.t26 119-120; 138
 R4*3 | % untitled/b3 untitled/b3.t26 119-120; 139
 d,4 \pp r4. d,8 | % untitled/b3 untitled/b3.t26 125.5-126; 140
 R4*3 | % untitled/b3 untitled/b3.t26 125.5-126; 141
 R4*3 | % untitled/b3 untitled/b3.t26 125.5-126; 142
 R4*3 | % untitled/b3 untitled/b3.t26 125.5-126; 143
 R4*3 | % untitled/b3 untitled/b3.t26 125.5-126; 144
 R4*3 \stopTextSpan | % untitled/b3 untitled/b3.t26 125.5-126; 145
 R4*3 \bar "|."
} }

>>
}
}

