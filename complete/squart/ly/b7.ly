\version "2.18.2"
\language "english"
\pointAndClickOff
\include "ly/lib.ily"
\header { title = "b7" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\bookpart {
\score {
<<
\new Staff  {
\set Staff.instrumentName = "vln1"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 6/8 \key c \major <d''~ g''~>2. \ff | % untitled/b7 untitled/b7.t5 0-5; 1
 <d''~ g''~>2. | % untitled/b7 untitled/b7.t5 0-5; 2
 <d''~ g''~>2. | % untitled/b7 untitled/b7.t5 0-5; 3
 <d'' g''>4 <d'~ g'~>8 \mf <d'~ g'~>4. | % untitled/b7 untitled/b7.t5 5-10; 4
 <d'~ g'~>2. | % untitled/b7 untitled/b7.t5 5-10; 5
 <d'~ g'~>2. | % untitled/b7 untitled/b7.t5 5-10; 6
 <d' g'>2 <c'~ g'~>4 \< | \time 5/8 % untitled/b7 untitled/b7.t5 10-13.25; 7
 <c'~ g'~>4. <c'~ g'~>4 | \time 6/8 % untitled/b7 untitled/b7.t5 10-13.25; 8
 <c' g'>2. | % untitled/b7 untitled/b7.t5 10-13.25; 9
 <c'~ a'~>2. | % untitled/b7 untitled/b7.t5 13.25-16; 10
 <c'~ a'~>2 <c' a'>8 <f'~ c''~>8 | % untitled/b7 untitled/b7.t5 16-18.5; 11
 <f'~ c''~>2. | % untitled/b7 untitled/b7.t5 16-18.5; 12
 <f' c''>4. <a'~ d''~>4. | % untitled/b7 untitled/b7.t5 18.5-20; 13
 <a' d''>4. <c''~ bf''~>4. | % untitled/b7 untitled/b7.t5 20-21.5; 14
 <c'' bf''>4. <d''~ a''~>4. \! \ff | % untitled/b7 untitled/b7.t5 21.5-23.75; 15
 <d'' a''>2. | % untitled/b7 untitled/b7.t5 21.5-23.75; 16
 a''2.~\trill \mf | % untitled/b7 untitled/b7.t5 23.75-31.25; 17
 \textSpannerDown \override TextSpanner.bound-details.left.text = \markup { "rit." } a''2.~\trill \startTextSpan | % untitled/b7 untitled/b7.t5 23.75-31.25; 18
 a''2.~\trill | % untitled/b7 untitled/b7.t5 23.75-31.25; 19
 a''2.~\trill | % untitled/b7 untitled/b7.t5 23.75-31.25; 20
 a''2.\trill \stopTextSpan | % untitled/b7 untitled/b7.t5 23.75-31.25; 21
 a''2.~ \> | % untitled/b7 untitled/b7.t5 31.25-35.75; 22
 a''2.~ | % untitled/b7 untitled/b7.t5 31.25-35.75; 23
 a''2. \! \alNiente \bar "|."
} }

\new Staff  {
\set Staff.instrumentName = "vln2"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 6/8 \key c \major d''8-> \ff d''8-. d''8-. d''8-> d''8-. d''8-. | % untitled/b7 untitled/b7.t16 1.25-1.5; 1
 d''4-> d''8-. d''8-. d''8-> d''8-. | % untitled/b7 untitled/b7.t16 2.75-3; 2
 d''8-. d''4-> r8 d''8-. d''8-. | % untitled/b7 untitled/b7.t16 4.25-4.5; 3
 d''4-> r8 r8 g'8-. \mf g'8-. | % untitled/b7 untitled/b7.t16 5.75-6; 4
 g'8-.-> g'8-. g'8-. g'4-.-> d''8-. | % untitled/b7 untitled/b7.t16 7.25-7.5; 5
 d''8-. d''8-. f''8-.-> ef''8-. ef''8-. c''8-. | % untitled/b7 untitled/b7.t16 8.75-9; 6
 d''8-.-> d''8-. ef''8-. f''8-.-> ef''8-. \< ef''8-. | \time 5/8 % untitled/b7 untitled/b7.t16 10.25-10.5; 7
 d''8-. ef''8-.-> d''8-. d''8-. d''8-. | \time 6/8 % untitled/b7 untitled/b7.t16 11.5-11.75; 8
 b'8-.-> c''8-. d''8-. f''8-.-> f''8-. f''8-. | % untitled/b7 untitled/b7.t16 13-13.25; 9
 ef''8-.-> ef''8-. ef''8-. b'8-.-> c''8-. d''8-. | % untitled/b7 untitled/b7.t16 14.5-14.75; 10
 f''8-.-> f''8-. f''8-. g''4-.-> g''8-. | % untitled/b7 untitled/b7.t16 16-16.25; 11
 b'8-.-> c''8-. d''8-. f''8-.-> f''8-. f''8-. | % untitled/b7 untitled/b7.t16 17.5-17.75; 12
 d''8-.-> d''8-. d''8-. f''8-.-> f''4-. | % untitled/b7 untitled/b7.t16 18.75-19.25; 13
 d''8-.-> d''8-. d''8-. g''4-.-> g''8-. | % untitled/b7 untitled/b7.t16 20.5-20.75; 14
 d''8-.-> d''8-. d''8-. f''8-.-> \! \ff f''8-. d''8-.-> | % untitled/b7 untitled/b7.t16 22-22.25; 15
 d''8-. g''4-.-> g''8-. d''8-.-> d''8-. | % untitled/b7 untitled/b7.t16 23.5-23.75; 16
 d''8-. bf''8-.-> a''8-. a''8-. bf''8-.-> a''8-. | % untitled/b7 untitled/b7.t16 25-25.25; 17
 \textSpannerDown \override TextSpanner.bound-details.left.text = \markup { "rit." } r8 \startTextSpan a''8-. \> a''8-. d''8-.-> a''8-. a''8-. | % untitled/b7 untitled/b7.t16 26.5-26.75; 18
 d''8-.-> a''8-. a''8-. d'''8-\flageolet-. \! \mp a''8-. a''8-. | % untitled/b7 untitled/b7.t16 28-28.25; 19
 d'''4-\flageolet-. a''8-. d'''4.~-\flageolet | % untitled/b7 untitled/b7.t16 29-35.75; 20
 d'''2.~-\flageolet \stopTextSpan | % untitled/b7 untitled/b7.t16 29-35.75; 21
 d'''2.~-\flageolet \> | % untitled/b7 untitled/b7.t16 29-35.75; 22
 d'''2.~-\flageolet | % untitled/b7 untitled/b7.t16 29-35.75; 23
 d'''2.-\flageolet \! \alNiente \bar "|."
} }

\new Staff  {
\set Staff.instrumentName = "vla"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
\clef alto
{
 \time 6/8 \key c \major \clef treble g'4. \ff d''4.^"pizz." | % untitled/b7 untitled/b7.t25 .75-1.5; 1
 d''2 ef''4~ | % untitled/b7 untitled/b7.t25 2.5-3.25; 2
 ef''8 c''4~ c''4. | % untitled/b7 untitled/b7.t25 3.25-4.5; 3
 d''2 r4 | % untitled/b7 untitled/b7.t25 4.5-5.5; 4
 g'4. \mf g4.~ | % untitled/b7 untitled/b7.t25 6.75-8; 5
 g4 ef''8~ ef''4. | % untitled/b7 untitled/b7.t25 8-9; 6
 d''4. f''4.~ \< | \time 5/8 % untitled/b7 untitled/b7.t25 9.75-11; 7
 f''4 ef''8~ ef''4 | \time 6/8 % untitled/b7 untitled/b7.t25 11-11.75; 8
 c''4. \glissando ef''4. | % untitled/b7 untitled/b7.t25 12.5-13.25; 9
 d''4. c''4. \glissando | % untitled/b7 untitled/b7.t25 14-14.75; 10
 f''4. ef''4. | % untitled/b7 untitled/b7.t25 15.5-16.25; 11
 d''4. c''4. | % untitled/b7 untitled/b7.t25 17-17.75; 12
 b'4. a'4. | % untitled/b7 untitled/b7.t25 18.5-19.25; 13
 g'4. f'4.~^"arco" | % untitled/b7 untitled/b7.t25 20-21.5; 14
 f'4. ef'4.~ \! \ff | % untitled/b7 untitled/b7.t25 21.5-24; 15
 ef'2.~ | % untitled/b7 untitled/b7.t25 21.5-24; 16
 ef'8 bf4~ bf4.~ | % untitled/b7 untitled/b7.t25 24-26; 17
 \textSpannerDown \override TextSpanner.bound-details.left.text = \markup { "rit." } bf4. \startTextSpan c'4.~^"porta" \mf | % untitled/b7 untitled/b7.t25 26-32; 18
 c'2.~ \> | % untitled/b7 untitled/b7.t25 26-32; 19
 c'2.~ | % untitled/b7 untitled/b7.t25 26-32; 20
 c'2.~ | % untitled/b7 untitled/b7.t25 26-32; 21
 c'4. \stopTextSpan \! \alNiente r4. | % untitled/b7 untitled/b7.t25 26-32; 22
 R8*6 | % untitled/b7 untitled/b7.t25 26-32; 23
 R8*6 \bar "|."
} }

\new Staff  {
\set Staff.instrumentName = "cello"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
\clef bass
{
 \time 6/8 \key c \major \clef tenor g'2.~-^ \ff | % untitled/b7 untitled/b7.t33 0-4; 1
 g'2.~-^ | % untitled/b7 untitled/b7.t33 0-4; 2
 g'2-^ r4 | % untitled/b7 untitled/b7.t33 0-4; 3
 r4 \clef bass d16( \mf g16 d16 g,16 c,4~ | % untitled/b7 untitled/b7.t33 5.5-10; 4
 c,2.~ | % untitled/b7 untitled/b7.t33 5.5-10; 5
 c,2.~ | % untitled/b7 untitled/b7.t33 5.5-10; 6
 c,2) f,4~ \< | \time 5/8 % untitled/b7 untitled/b7.t33 10-12.5; 7
 f,4.~ f,4~ | \time 6/8 % untitled/b7 untitled/b7.t33 10-12.5; 8
 f,4. ef,4.~ | % untitled/b7 untitled/b7.t33 12.5-16; 9
 ef,2.~ | % untitled/b7 untitled/b7.t33 12.5-16; 10
 ef,2~ ef,8 d,8~ | % untitled/b7 untitled/b7.t33 16-18.5; 11
 d,2.~ | % untitled/b7 untitled/b7.t33 16-18.5; 12
 d,4. c,4.~ | % untitled/b7 untitled/b7.t33 18.5-21.5; 13
 c,2.~ | % untitled/b7 untitled/b7.t33 18.5-21.5; 14
 c,4. c4.~ \! \ff | % untitled/b7 untitled/b7.t33 21.5-32; 15
 c2.~ | % untitled/b7 untitled/b7.t33 21.5-32; 16
 c2.~ | % untitled/b7 untitled/b7.t33 21.5-32; 17
 \textSpannerDown \override TextSpanner.bound-details.left.text = \markup { "rit." } c2.~ \startTextSpan | % untitled/b7 untitled/b7.t33 21.5-32; 18
 c2.~ \> | % untitled/b7 untitled/b7.t33 21.5-32; 19
 c2.~ | % untitled/b7 untitled/b7.t33 21.5-32; 20
 c2.~ | % untitled/b7 untitled/b7.t33 21.5-32; 21
 c4. \stopTextSpan \! \alNiente r4. | % untitled/b7 untitled/b7.t33 21.5-32; 22
 R8*6 | % untitled/b7 untitled/b7.t33 21.5-32; 23
 R8*6 \bar "|."
} }

>>
}
}

