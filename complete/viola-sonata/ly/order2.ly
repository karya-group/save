\version "2.14.2"
\language "english"
\header { title = "order2" tagline = "" }
\score { <<

\new Staff  {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\set Staff.instrumentName = "viola"
\set Staff.shortInstrumentName = ""
{
\time 4/4 \key c \major \clef treble e''4^"pizz." \f f'4 d''4 e'4 | % 1
  cs''4 d'4 b'4 cs'4 | % 2
  e''4 f'4 f''4 e'4 | % 3
  c''4 ef'4 bf'4 d'4 | % 4
  \clef alto c'1~^"arco" \p | % 5
  c'16 bf'16( c'16) a'16( df'16) g'16 r8 c'2 | % 6
  bf'4( c'4) a'4( bf4) | % 7
  g'4( a4) gf'4( g4) | % 8
  a'4-+ \f r8 a'8_~ \pp <a'_~ bf'>2 | % 9
  <a'_~ b'!^~>2 <a'_~ b'>8 <a'_~ c''~>4. | % 10
  <a' c''>2 <gs' cs''>2 \mf | % 11
  b'2 as'2~( | % 12
  as'8 b8) gs'2 as'8( b8) | % 13
  gs'8( as8) g'8( af8) f'8( g8) r8 b'8 | % 14
  cs''2 \> ds''2 | % 15
  e''2~( \p e''8 fs'8) ds''4( | % 16
  e'8) cs''8( ds'8) bs'8~ bs'4. cs''16(-- ds''16)-- | % 17
  e''4^"pizz." \f fs'4 ds''4 e'4 | % 18
  cs''4 ds'4 bs'4 cs'4 | % 19
  e''4 fs'4 ds''4 e'4 | % 20
  cs''4 ds'4 bs'4 cs'4 | % 21
  gs2^"arco" \p \< fs4 b8( a8) | % 22
  ds8( e8 fs8) <c a>8( df8 gs8 fs8 f8) | % 23
  e2 \f <gs e'>4 <cs cs'>4 | % 24
  <c g>16 \ff <e'~ c''~>8. <e' c''>4 <<
  { \voiceOne
  bf'8( af'8) g'4 | % 25
  } \new Voice { \voiceTwo
  d'4 bf8( a8) | % 25
  } >> \oneVoice
  <c g>16 \mf <g ef'>8. bf'16( af'16 g'8)-. g'16( f'16 ef'8)-. ef'16( df'16 b16 a16) | % 26
  g1~ \p | % 27
  g16 ef'16( g16) d'16( a16) df'16 r8 g2 | % 28
  ef'4( f4) d'4( ef4) | % 29
  c'4( d4) b4( c4) | % 30
  a'4-+ \f r8 a'8_~ \pp <a'_~ bf'>2 | % 31
  <a'_~ b'!^~>2 <a'_~ b'>8 <a'_~ c''~>4. | % 32
  <a' c''>2 <gs' cs''>2 \mf | % 33
  b'2 as'2~( | % 34
  as'8 b8) gs'2 as'8( b8) | % 35
  gs'8( as8) g'8( af8) f'8( g8) r8 b'8 | % 36
  cs''2 \> ds''2 | % 37
  e''2 \p ef''2 | % 38
  df''4 e''8 c''8 af'8 d'8 df'16( \mf b'8.) | % 39
  c'16( \> bf'8.) b16( a'8.) r4 c'4~ \p | % 40
  c'4 g'8.( \mf \> bf'16) gf'8.( a'16) r4 | % 41
  c'1 \p | % 42
  c'4.( b8) c'4.( bf8) | % 43
  a2 d8 a4( bf8) | % 44
  d8( a8 bf8 c'8) bf8( a4 d8) | % 45
  bf8( a4 d8) c'8( bf8) bf'8( a8) | % 46
  g16 \< g8. ef'16 ef'8. f16 f8. d'16 d'8. | % 47
  ef16 ef8. c'16 c'8. d16 d8. b16 b8. | % 48
  g16 \f g8. ef'16 ef'8. f16 f8. d'16 d'8. | % 49
  ef16 ef8. c'16 c'8. d16 d8. bf16 bf8. | % 50
  c16 c8. a16 a8. d16 d8. bf16 bf8. | % 51
  ef16 ef8. c'16 c'8 a16 <g~ d'~>2 | % 52
  <g d'>1 | % 53
  <g d'>16 <d' a'>8. <c g>16 <g f'>8. <ef g>16 <g ef'>8. <c g>16 <g d'>8. | % 54
  c16( a16 c'16 f'16 \clef treble bf'16 f''16 g''16 c''16) f''8 ef''8 d''8 c''8 | % 55
  d''4 f''8 ef''8 d''2 | % 56
  \clef alto d'2 \p c'4( d'8 ef'8) | % 57
  bf4( a4) bf4( a4) | % 58
  d2 e4( f8 g8) | % 59
  bf8( g'8) a4 b8( f'8) af8( gs'8) | % 60
  a'2 bf'8( a'8) c''8( b'8) | % 61
  bf'4( a'8 g'8) b8( cs'8 e'8 g'8) | % 62
  <<
  { \voiceOne
  a'4. r8 bf'8 r8 a'8( g'8) | % 63
  f'4 r4 a'4.( bf'8) | % 64
  d''4.( cs''8) } \new Voice { \voiceTwo
  f'32( \f g'32 f'32 e'32 f'4 e'8) d'8( f'8) df'4 | % 63
  bf8( g8) a8( bf8) f'8( e'8 g'4~ | % 64
  g'8) e'8( f'4) } >> \oneVoice
  <d'~ d''~>2 \> | \time 2/4 % 65
  <d' d''>2 | \time 4/4 % 66
  <c g>16 \f <e'~ c''~>8. <e' c''>4 <<
  { \voiceOne
  bf'8( af'8) g'4 | % 67
  } \new Voice { \voiceTwo
  d'4 bf8( a8) | % 67
  } >> \oneVoice
  <c g>16 <g ef'>8. bf'16( af'16 g'8)-. g'16( f'16 ef'8)-. ef'16( df'16 b16 a16) | % 68
  <c g>16 <g ef'>8. <g ef'>16 <ef' c''>8. <c g>16 <g ef'>8. <g ef'>16 <ef' c''>8. | % 69
  \clef treble ef'16 g'16 c''16 g'16 ef''16 c''16 g'16 g''16 ef''16 c''16 g'16 ef''16 c''16 g'16 ef'16 af'16 | % 70
  <b af'>16 <af' ef''>8. af''16( g''16 f''8)-. f''16( ef''16 d''8)-. d''16( c''16 b'16 a'16) | % 71
  af'4-- \ff g'4-- f'4-- ef'4-- | % 72
  c'8 g'8 d''8 g''8 c'''4-- b''4-- | % 73
  bf''4-- a''4-- af''4-- g''4-- | % 74
  f'8 af'8 c''8 f''8 g''4-- gf''4-- | % 75
  f''4-- e''4-- ef''4-- d''4-- | % 76
  c''4-. \clef alto <g c>16 <g ef'>8.-. c4-. r4 } }

\new PianoStaff <<
\set PianoStaff.instrumentName = "piano"
\set PianoStaff.shortInstrumentName = ""

\new Staff = "up" {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 4/4 \key c \major e''16 \mf e''8.-. f'16 f'8.-. d''16 d''8.-. e'16 e'8.-. | % 1
  cs''16 cs''8.-. d'16 d'8.-. b'16 b'8.-. cs'16 cs'8.-. | % 2
  e''16 e''8.-. f'16 f'8.-. d''16 d''8.-. e'16 e'8.-. | % 3
  bf'16 bf'8.-. c'16 c'8.-. a'16 a'8.-. bf16 bf8.-. | % 4
  a1~ \p | % 5
  a2 a2~ | % 6
  a1~ | % 7
  a4 c'''16 bf''16 e''16 f''16 a''4~ a''8. bf''16 | % 8
  a''2 r16 g''16 c''16 df''16 f''4~ | % 9
  f''4~ f''16 e''16 b'16 c''16 ef''4~ ef''16 d''16 a'16 e'16 | % 10
  cs'16 f'16 a'16 fs''16~ fs''4 \arpeggioArrowUp<as' fs'' cs'''>2\arpeggio \mf | % 11
  <b' ds'' g''>2 <b'~ ds''~ fs''~>2 | % 12
  <b' ds'' fs''>2. \times 2/3 { as''16 gs''16 ds''16 } fs''8~ | % 13
  fs''4 \times 2/3 { e''16 ds''16 g'16 } cs''8~ cs''4. b'8 | % 14
  as'2 \> fs'2 | % 15
  gs'1~ \p | % 16
  gs'1 | % 17
  e''16 \mf e''8.-. fs'16 fs'8.-. ds''16 ds''8.-. e'16 e'8.-. | % 18
  cs''16 cs''8.-. ds'16 ds'8.-. c''16 c''8.-. cs'16 cs'8.-. | % 19
  e''16 e''8.-. fs'16 fs'8.-. ds''16 ds''8.-. e'16 e'8.-. | % 20
  cs''16 cs''8.-. ds'16 ds'8.-. c''16 c''8.-. cs'16 cs'8.-. | % 21
  e''16 \< e''8.-. fs'16 fs'8.-. ds''16 ds''8.-. e'16 e'8.-. | % 22
  cs''16 \mf cs''8.-. ds'16 ds'8.-. <a' c''>16 <a' c''>8.-. r16 cs''16 c'''16 f''16 | % 23
  \times 4/6 { cs'''16 \f e''16 cs''16 \change Staff = "down" c''16 e'16 cs'16 } \times 4/5 { \change Staff = "up" e''16 cs''16 bs'16 gs'16 e'16 } r4 \clef bass \times 2/3 { e,16 cs16 gs,16 } cs,8 | % 24
  \clef treble <c'' g'' d'''>2. \ff <g'' ef'''>8 <fs'' d'''>8 | % 25
  <f''! df'''>2 \mf \arpeggioArrowUp<ef'' gf'' b''>2\arpeggio | % 26
  <bf~ g'~>1 \p | % 27
  <bf g'>2 <bf~ g'~>2 | % 28
  <bf~ g'~>1 | % 29
  <bf g'>4 as''16 gs''16 d''16 ds''16 fs''4~ fs''8. g''16 | % 30
  a''2 r16 g''16 c''16 df''16 f''4~ | % 31
  f''4~ f''16 e''16 b'16 c''16 ef''4~ ef''16 d''16 a'16 e'16 | % 32
  cs'16 f'16 a'16 fs''16~ fs''4 \arpeggioArrowUp<as' fs'' cs'''>2\arpeggio \mf | % 33
  <b' ds'' g''>2 <b'~ ds''~ fs''~>2 | % 34
  <b' ds'' fs''>2. \times 2/3 { as''16 gs''16 ds''16 } fs''8~ | % 35
  fs''4 \times 2/3 { e''16 ds''16 g'16 } cs''8~ cs''4 r8 b'8 | % 36
  as'2 \> fs'2 \! | % 37
  <<
  { \voiceOne
  r4 \times 2/3 { e'''16 ds'''16 as''16 } d'''8~ d'''4 \times 2/3 { cs'''16 b''16 g''16 } as''8 | % 38
  } \new Voice { \voiceTwo
  gs'1 \p | % 38
  } >> \oneVoice
  \times 4/6 { b''16 as''16 e''16 a''16 gs''16 cs''16 } g''8 e''8 c''8 f'8 gs'8. \mf \> b'16 | % 39
  g'8. bf'16 fs'8. a'16 r4 a4~ \p | % 40
  a4 c'16 \mf e''16 \> bf'16 g'16 b16 d''16 af'16 d'16 r4 | % 41
  a4 \p g'4 f'4 g'8 af'8 | % 42
  ef'8 ef''8 d'4 ef'8 ef''8 df'8 b8 | % 43
  c'16 c'8.-. a'16 a'8.-. bf16 bf8.-. g'16 g'8.-. | % 44
  a16 a8.-. gf'16 gf'8.-. c'16 c'8.-. a'16 a'8.-. | % 45
  bf16 bf8.-. g'16 g'8.-. a16 a8.-. gf'16 gf'8.-. | % 46
  \clef bass g16 \< g8. ef'16 ef'8. f16 f8. d'16 d'8. | % 47
  ef16 ef8. c'16 c'8. d16 d8. b16 b8. | % 48
  \clef treble g'16 \f d''16 a''16 g''16 d'''16 g''16 a''16 c''16 g''16 d''16 f''16 a''16 bf'16 g''16 a'16 fs''16 | % 49
  g'16 ef''16 a''16 g''16 d'''16 ef''16 a''16 g''16 ef''16 d'''16 f''16 c'''16 ef''16 bf''16 d''16 a''16 | % 50
  g'16 d''16 a''16 g''16 d'''16 f''16 a''16 g''16 \ottava #1 g'''16 d'''16 c'''16 f'''16 bf''16 f'''16 a''16 f'''16 | % 51
  g''16 bf''16 g'''16 f'''16 bf'''16 ef'''16 d'''16 a'''16 g'''8 as''8 f'''8 a''8 | % 52
  ef'''8 g''8 d'''8 f''8 c'''8 ef''8 bf''8 d''8 | % 53
  \ottava #0 g''8 bf'8 f''8 a'8 ef''8 g'8 d''8 f'8 | % 54
  c''8 ef'8 bf'8 d'8 a'8 c'8 g'8 bf8 | % 55
  <a g'>4 a'8 c'8 <g' g''>16 ef''16 bf'16 g'16 ef'16 c'16 g16 ef16 | % 56
  R1 | % 57
  R1 | % 58
  e''16 \p e''8.-. f'16 f'8.-. d''16 d''8.-. e'16 e'8.-. | % 59
  df''16 df''8.-. d'16 d'8.-. b'16 b'8.-. df'16 df'8.-. | % 60
  e''16 e''8.-. f'16 f'8.-. d''16 d''8.-. e'16 e'8.-. | % 61
  df''16 df''8.-. d'16 d'8.-. b'16 b'8.-. df'16 df'8.-. | % 62
  e''16 \f e''8.-. f'16 f'8.-. d''16 d''8.-. e'16 e'8.-. | % 63
  df''16 df''8.-. d'16 d'8.-. b'16 b'8.-. df'16 df'8.-. | % 64
  d''16 d''8.-. f'16 f'8.-. a'16 a'8.-. a16 a8.-. | \time 2/4 % 65
  f'16 f'8.-. r4 | \time 4/4 % 66
  <c'' g'' d'''>2. <g'' ef'''>8 <d''' gf''>8 | % 67
  <f'' df'''>2 \arpeggioArrowUp<ds'' fs'' b''>2\arpeggio | % 68
  \clef bass c,16 g,16 d16 c16 g16 c16 d16 c'16 f16 g16 ef'16 f16 d'16 ef16 c'16 d16 | % 69
  c16 g16 d'16 c'16 \clef treble g'16 d'16 c''16 ef'16 g'16 ef''16 f'16 d''16 ef'16 c''16 d'16 b'16 | % 70
  <ef' b'>2 \arpeggioArrowUp<c'' ef'' af'' c'''>2\arpeggio | % 71
  <b af'>4 \ff <c' g'>4 <af c' f'>4 <g c' g'>4 | % 72
  <c' f' a'>4 <d' f' b'>4 <ef' g' c''>4 <d' g' b'>4 | % 73
  <d' g' bf'>4 <c' f' a'>4 <c' f' af'>4 <c' ef' g'>4 | % 74
  <c' d' f'>4 <c' ef'>4 <b d'>4 <b ef'>4 | % 75
  <c' f'>4 <c' g'>4 <c' a'>4 <d' b'>4 | % 76
  <ef' c''>4-. <g g'>4-. g4-. r4 } }

\new Staff = "down" {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 4/4 \key c \major \clef bass R1 | % 1
  r2 r8 \ottava #-1 \times 2/3 { d,,16 cs,16 f,,16 } d,,8 \ottava #0 r8 | % 2
  R1 | % 3
  r2 r8 \ottava #-1 \times 2/3 { d,,16 d,16 a,,16 } d,,8 \ottava #0 r8 | % 4
  \ottava #-1 <d,,~ fs,~>1 | % 5
  <d,, fs,>2 <d,,~ fs,~>2 | % 6
  <d,,~ fs,~>1 | % 7
  <d,, fs,>1 | % 8
  \ottava #0 R1 | % 9
  R1 | % 10
  r2 \arpeggioArrowUp<ds as f'>2\arpeggio | % 11
  <b, fs cs'>2 <gs,~ ds~ as~>2 | % 12
  <gs, ds as>1 | % 13
  R1 | % 14
  <cs' b, fs>2 <fs as gs, ds>2 | % 15
  <ds~ cs,~ gs,~>1 | % 16
  <ds cs, gs,>1 | % 17
  R1 | % 18
  R1 | % 19
  \times 2/3 { cs,16 cs16 e,16 } cs,8 r4 gs2 | % 20
  fs4 gs8 a8 e8 e'8 ds4 | % 21
  \times 2/3 { cs16 cs'16 e16 } cs8 \times 2/3 { gs16 ds'16 bs16 } fs8 \times 2/3 { e16 cs'16 gs16 } ds8 <g b>8 <f a>8 | % 22
  <ds fs>8 <e gs>8 <fs a>8 c'8 df'4 df'16 df'8.-. | % 23
  \clef treble r2 \clef bass \times 4/6 { cs'16 gs16 cs16 cs'16 gs16 cs16 } r4 | % 24
  <c, g, e>1 | % 25
  <c, g, ef>1 | % 26
  <c,~ e~>1 | % 27
  <c, e>2 <c,~ e~>2 | % 28
  <c,~ e~>1 | % 29
  <c, e>1 | % 30
  R1 | % 31
  R1 | % 32
  r2 \arpeggioArrowUp<ds as f'>2\arpeggio | % 33
  <b, fs cs'>2 <gs,~ ds~ as~>2 | % 34
  <gs, ds as>1 | % 35
  R1 | % 36
  <cs' b, fs>2 <fs as gs, ds>2 | % 37
  <gs, ds cs,>1 | % 38
  R1 | % 39
  r2 r4 \ottava #-1 <fs,~ d,,~>4 | % 40
  <fs, d,,>2. r4 | % 41
  <fs,~ d,,~>1 | % 42
  <fs, d,,>1 | % 43
  <g,~ g,,~ d,~>1 | % 44
  <g,~ g,,~ d,~>1 | % 45
  <g, g,, d,>1 | % 46
  <g,,~ g,~>1 | % 47
  <g,, g,>1 | % 48
  \ottava #0 g16 g8. ef'16 ef'8. f16 f8. d'16 d'8. | % 49
  ef16 ef8. c'16 c'8. d16 d8. bf16 bf8. | % 50
  c16 c8. a16 a8. bf,16 bf,8. g16 g8. | % 51
  c16 c8. a8. \ottava #-1 a,16 g,2~ | % 52
  <g, f,, c,>1 | % 53
  g,2~ <g, f,, c,>2 | % 54
  <f, c as,,>1 | % 55
  <d, g, g,,>4 <c,, c,>4 <g,, d, g,>2 | % 56
  \ottava #0 <g,~ d~>1 | % 57
  <g, d>1 | % 58
  \ottava #-1 <d,~ d,,~ a,,~>1 | % 59
  <d, d,, a,,>1 | % 60
  \ottava #0 R1 | % 61
  R1 | % 62
  R1 | % 63
  R1 | % 64
  R1 | \time 2/4 % 65
  r4 d16 d8.-. | \time 4/4 % 66
  <c, g, e>1 | % 67
  <g, ds c,>1 | % 68
  r2 r2 | % 69
  r2 r2 | % 70
  \ottava #-1 <af,, ef, b,>1 | % 71
  \ottava #0 <b, ef af af,>4 <c ef g g,>4 <f, c f>4 <ef, c>4 | % 72
  <d, d>4 <g, d>4 <ef, c>4 <g, d>4 | % 73
  <bf, g>4 <c a>4 <ef af>4 <ef g>4 | % 74
  f4 fs4 g4 gf4 | % 75
  <af f>4 e4 ef4 <g d>4 | % 76
  <g ef' c>4-. <g, ef c,>4-. \ottava #-1 <c,, c,>4-. r4 } }
>>
>> }
