\version "2.14.2"
\language "english"
\pointAndClickOff
\header { title = "mvt1" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\score {
<<
\new Staff  {
\set Staff.instrumentName = "viola"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 6/8 \key c \major \clef alto R8*6 | % 1
  R8*6 | % 2
  R8*6 | % 3
  R8*6 | % 4
  R8*6 | % 5
  R8*6 | % 6
  R8*6 | % 7
  R8*6 | % 8
  R8*6 | % 9
  R8*6 | % 10
  R8*6 | % 11
  R8*6 | % 12
  R8*6 | % 13
  R8*6 | % 14
  R8*6 | % 15
  R8*6 | % 16
  R8*6 | % 17
  R8*6 | % 18
  R8*6 | % 19
  R8*6 | % 20
  R8*6 | % 21
  R8*6 | % 22
  r4. cs8-. \mf <a e'>8-. <a e'>8-. | % viola-sonata/vla1 viola-sonata/vla1.t2 68.5-69; 23
  cs8-. <a e'>8-. <a e'>8-. d16( a16) <d' f'>8-. <d' a'>8-. | % viola-sonata/vla1 viola-sonata/vla1.t2 71.5-72; 24
  as8-. <f' as'>8-. g8-. <d' g'>8-. ds8-. <as ds'>8-. | % viola-sonata/vla1 viola-sonata/vla1.t2 74.5-75; 25
  a16( b16 cs'16 d'16 e'16 f'16 g'4) bf'8 | % viola-sonata/vla1 viola-sonata/vla1.t2 77.5-78; 26
  a'16( gs'16 a'16 e'16 cs'16 d'16) e'16( d'16 e'16 cs'16 a16 as16) | % viola-sonata/vla1 viola-sonata/vla1.t2 80.75-81; 27
  a16( e'16 a'16 gs'16 a'16 b'16) cs''8. d''16~ d''8 | % viola-sonata/vla1 viola-sonata/vla1.t2 83.25-84; 28
  cs''4. d''8. \ff f''16~ f''8 | % viola-sonata/vla1 viola-sonata/vla1.t2 86.25-87; 29
  e''4.:32 r4. | % viola-sonata/vla1 viola-sonata/vla1.t2 87-88.5; 30
  <d' a'>2( <d'' a''>4)-\flageolet | % viola-sonata/vla1 viola-sonata/vla1.t2 92-93; 31
  r4. cs8-. \mf <a e'>8-. <a e'>8-. | % viola-sonata/vla1 viola-sonata/vla1.t2 95.5-96; 32
  cs8-. <a e'>8-. <a e'>8-. d16( a16) <d' f'>8-. <d' a'>8-. | % viola-sonata/vla1 viola-sonata/vla1.t2 98.5-99; 33
  cs16-. a16-. <e' cs''>8-. <e' cs''>8-. \times 4/5 { d''16-. a'16-. f'16-. e'16-. d'16-. } <a f'>8 | % viola-sonata/vla1 viola-sonata/vla1.t2 101.5-102; 34
  a'16( \p bf'16 a'8) c''16( bf'16 a'4) r8 | % viola-sonata/vla1 viola-sonata/vla1.t2 103.5-104.43; 35
  a'16( \mf gs'16 a'16 f'16 d'16 e'16) f'16( e'16 f'16 d'16 a16 as16) | % viola-sonata/vla1 viola-sonata/vla1.t2 107.75-108; 36
  c'16( a16 fs16 ef'16 d'16 c'16) bf16( a16 bf16 g16 d16 g16) | % viola-sonata/vla1 viola-sonata/vla1.t2 110.75-111; 37
  f4( g8) a8( d'8 f'8) | % viola-sonata/vla1 viola-sonata/vla1.t2 113.5-114; 38
  af'8( \> g'8 f'8) e'8( d'8 cs'8) \p | % viola-sonata/vla1 viola-sonata/vla1.t2 116.5-117; 39
  d'8-. \f <a f'>8-. <a f'>8-. f8-. <a d'>8-. <a d'>8-. | % viola-sonata/vla1 viola-sonata/vla1.t2 119.5-120; 40
  <d' a'>2( <d'' a''>4)-\flageolet | % viola-sonata/vla1 viola-sonata/vla1.t2 122-123; 41
  a'8^"pizz." \fp f'8 \< d'8 a8 f8 d8 | % viola-sonata/vla1 viola-sonata/vla1.t2 125.5-126; 42
  cs8 \f a8 \mf a8 cs8 a8 a8 | % viola-sonata/vla1 viola-sonata/vla1.t2 128.5-129; 43
  d8 <a f'>8 <a f'>8 c8 <a e'>8 <a e'>8 | % viola-sonata/vla1 viola-sonata/vla1.t2 131.5-132; 44
  <f c'>8 <c' af'>8 <c' af'>8 <ef c'>8 <c' g'>8 <c' g'>8 | % viola-sonata/vla1 viola-sonata/vla1.t2 134.5-135; 45
  \clef treble <ef' bf'>8 <bf' gf''>8 <bf' gf''>8 bf''8 f''8 df''8 | % viola-sonata/vla1 viola-sonata/vla1.t2 137.5-138; 46
  \clef alto e'2.^"arco" \sfz | % viola-sonata/vla1 viola-sonata/vla1.t2 138-141; 47
  a'16( \f gs'16 a'16 f'16 d'16 e'16) f'16( e'16 f'16 d'16 a16 bf16) | % viola-sonata/vla1 viola-sonata/vla1.t2 143.75-144; 48
  c'16( a16 fs16 ef'16 d'16 c'16) bf16( a16 bf16 g16 d16 g16) | % viola-sonata/vla1 viola-sonata/vla1.t2 146.75-147; 49
  f4( \mf g8) \< a8 d'8 f'8 | % viola-sonata/vla1 viola-sonata/vla1.t2 149.5-150; 50
  c''8( \f bf'8 a'8) bf'16( a'16 g'8 d'8) | % viola-sonata/vla1 viola-sonata/vla1.t2 152.5-153; 51
  b'16( a'16 b'16 g'16 d'16 g'16) ef'8 g'8 c''8 | % viola-sonata/vla1 viola-sonata/vla1.t2 155.5-156; 52
  ef''8( d''8 c''8) ef''16( d''16 c''16 b'16 c''8) | % viola-sonata/vla1 viola-sonata/vla1.t2 158.5-159; 53
  a'16( a16) g'16( bf16) gf'16( c'16) ef'16( g16) d'16( a16) c'16( fs16) | % viola-sonata/vla1 viola-sonata/vla1.t2 161.75-162; 54
  ef'2 r4 | % viola-sonata/vla1 viola-sonata/vla1.t2 162-164; 55
  d'4. c'4. | % viola-sonata/vla1 viola-sonata/vla1.t2 166.5-168; 56
  bf2. | % viola-sonata/vla1 viola-sonata/vla1.t2 168-171; 57
  \ottava #1 g''4.^"pizz." \ottava #0 <g g'>4. \bar "|." 
} }

\new PianoStaff <<
\set PianoStaff.instrumentName = ""
\set PianoStaff.shortInstrumentName = ""
\new Staff = "up" {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 6/8 \key c \major a''16( \f gs''16 a''16 f''16 d''16 e''16) f''16( e''16 f''16 d''16 a'16 bf'16) | % viola-sonata/pno1 viola-sonata/pno1.t26 2.75-3; 1
  c''16( a'16 fs'16 ef''16 d''16 c''16) bf'16( a'16 bf'16 g'16 d'16 g'16) | % viola-sonata/pno1 viola-sonata/pno1.t26 5.75-6; 2
  f'4( g'8) a'8( d''8 f''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 8.5-9; 3
  c'''8( bf''8 a''8) bf''16( a''16 g''8 d''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 11.5-12; 4
  b''16( a''16 b''16 g''16 d''16 g''16) ef''8( g''8 c'''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 14.5-15; 5
  ef'''8( d'''8 c'''8) ef'''16( d'''16 c'''16 b''16 c'''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 17.5-18; 6
  d'''16( c'''16 b''8 g''8) d'''16( b''16 g''16 d''16 b'16 g'16) | % viola-sonata/pno1 viola-sonata/pno1.t26 20.75-21; 7
  c''4~ \mf c''16 a'16( bf'4~ bf'16) g'16 | % viola-sonata/pno1 viola-sonata/pno1.t26 23.75-24; 8
  a'16( \< d''16 cs''16 d''16 a'16 fs'16) e'16( g'16 fs'16 g'16 e'16 c'16) | % viola-sonata/pno1 viola-sonata/pno1.t26 26.75-27; 9
  d'8 \f b'8 g''8 ef'8 c''8 g''8 | % viola-sonata/pno1 viola-sonata/pno1.t26 29.5-30; 10
  fs''4.( g''4.) | % viola-sonata/pno1 viola-sonata/pno1.t26 31.5-33; 11
  g''16( d''16 bf'16 a'16 bf'16 d''16) bf''16( g''16 d''16 cs''16 d''16 g''16) | % viola-sonata/pno1 viola-sonata/pno1.t26 35.75-36; 12
  \ottava #1 g'''4 f'''8 ef'''8 d'''8 c'''8 | % viola-sonata/pno1 viola-sonata/pno1.t26 38.5-39; 13
  \ottava #0 bf''8 a''8 g''8 cs'''8( \p a''16 g''16 a''16 e'''16) | % viola-sonata/pno1 viola-sonata/pno1.t26 41.75-42; 14
  cs'''8( a''16 g''16 a''16 e'''16) f'''8( e'''8 d'''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 44.5-45; 15
  c'''8( bf''8 a''8) g''8( f''8 e''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 47.5-48; 16
  f''8( e''8 d''8) \stemUp g''8( f''8 e''8) \stemNeutral | % viola-sonata/pno1 viola-sonata/pno1.t26 50.5-51; 17
  d''2 a'16( a''16 d''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 53.5-54; 18
  f''8 \f e''8 d''8 c''8 bf'8 a'8 | % viola-sonata/pno1 viola-sonata/pno1.t26 56.5-57; 19
  gs'16 \p fs'16 gs'16 b'16 e'16 b'16 gs'16 fs'16 gs'16 b'16 e'16 b'16 | % viola-sonata/pno1 viola-sonata/pno1.t26 59.75-60; 20
  c''16( \< b'16 c''16 a'16 e'16 a'16) c''16( b'16 c''16 a'16 e'16 a'16) | % viola-sonata/pno1 viola-sonata/pno1.t26 62.75-63; 21
  e''16( ds''16 e''16 c''16 a'16 c''16) <e'' a''>4 \f a''8 \p | % viola-sonata/pno1 viola-sonata/pno1.t26 65.5-66; 22
  bf''16( a''16 g''16 f''16 e''16 d''16) cs''4. | % viola-sonata/pno1 viola-sonata/pno1.t26 67.5-69; 23
  a'4 a'16( bf'16 a'8) c''16( bf'16 a'8) | % viola-sonata/pno1 viola-sonata/pno1.t26 71.5-72; 24
  f''16( e''16 d''8) g''16( f''16 e''8) bf''16( a''16 g''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 74.5-75; 25
  cs'''16( b''16 a''16 g''16 f''16 e''16 ds''4) g''8^"melody" | % viola-sonata/pno1 viola-sonata/pno1.t26 77.5-78; 26
  a''8 \acciaccatura { bf''8[ c'''8] } bf''4~ bf''8 \acciaccatura { d'''8[ c'''8] } bf''8. gs''16 | % viola-sonata/pno1 viola-sonata/pno1.t26 80.75-81; 27
  <e'' a''>8 \acciaccatura { f''8[ g''8] } f''4 r8 \times 4/6 { \acciaccatura { a''8[ g''8] } f''16 e''16 d''16 c''16 as'16 gs'16 } | % viola-sonata/pno1 viola-sonata/pno1.t26 81.5-82.5; 28
  a'4. f'8. \ff d'16~ d'8 | % viola-sonata/pno1 viola-sonata/pno1.t26 86.25-87; 29
  \ottava #1 <a'' a''' e''' cs'''>4.:32 r4. | % viola-sonata/pno1 viola-sonata/pno1.t26 87-88.5; 30
  d'''16( cs'''16 d'''16 a''16 f''16 a''16) f'''16( e'''16 f'''16 d'''16 a''16 d'''16) | % viola-sonata/pno1 viola-sonata/pno1.t26 92.75-93; 31
  \ottava #0 bf''16( \> a''16 g''16 f''16 e''16 d''16 cs''4.) | % viola-sonata/pno1 viola-sonata/pno1.t26 94.5-95.93; 32
  a'4 \p a'16( bf'16 a'8) c''16( bf'16 a'16 g'16 | % viola-sonata/pno1 viola-sonata/pno1.t26 98.75-99; 33
  a'8) c''16( bf'16 a'16 g'16 f'16 g'16 a'4) | % viola-sonata/pno1 viola-sonata/pno1.t26 101-101.93; 34
  <a' f' cs'' f''>4. r4. | % viola-sonata/pno1 viola-sonata/pno1.t26 102-103.5; 35
  R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t26 102-103.5; 36
  R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t26 102-103.5; 37
  a'16( gs'16 a'16 f'16 d'16 e'16) f'16( e'16 f'8 a16 bf16) | % viola-sonata/pno1 viola-sonata/pno1.t26 113.75-114; 38
  d'16( \> e'16) af'16( g'16) bf'16( a'16) e''16( d''16) f''16( e''16) bf''16( \p a''16) | % viola-sonata/pno1 viola-sonata/pno1.t26 116.75-117; 39
  d'''8 \f r4 r4. | % viola-sonata/pno1 viola-sonata/pno1.t26 117-117.5; 40
  \ottava #1 \acciaccatura { d''''8[ a'''8] } d'''16( cs'''16 d'''16 a''16 f''16 a''16) f'''16( e'''16 f'''16 d'''16 a''16 d'''16) | % viola-sonata/pno1 viola-sonata/pno1.t26 122.75-123; 41
  a'''16 \fp e'''16 \< f'''16 cs'''16 d'''16 gs''16 \ottava #0 a''16 e''16 f''16 cs''16 d''16 a'16 \f | % viola-sonata/pno1 viola-sonata/pno1.t26 125.75-126; 42
  cs''8( \p a''16 g''16 a''16 e'''16) cs'''8( a''16 g''16 a''16 e'''16) | % viola-sonata/pno1 viola-sonata/pno1.t26 128.75-129; 43
  \ottava #1 f'''8( e'''8 d'''8) e'''8( f'''8 g'''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 131.5-132; 44
  af'''8( g'''8 f'''8) ef'''8( d'''8 c'''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 134.5-135; 45
  gf'''8( f'''8 ef'''8) df'''8( c'''8 bf''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 137.5-138; 46
  <g'' bf'' d'''>4. <a'' cs''' e'''>4. | % viola-sonata/pno1 viola-sonata/pno1.t26 139.5-141; 47
  <f''' a'' d'''>4. \f <d''' f'' a''>4. | % viola-sonata/pno1 viola-sonata/pno1.t26 142.5-144; 48
  <d''' a'' c'''>8. <ef'''~ a''~ c'''~>16 <ef''' a'' c'''>8 <bf'' d''' a''>8. <d'''~ g''~ bf''~>16 <d''' g'' bf''>8 | % viola-sonata/pno1 viola-sonata/pno1.t26 146.25-147; 49
  \ottava #0 <a'' g''>8( <f'' a''>4) <bf'' e''>8( <a'' f''>8) \times 2/3 { d''16 e''16 g''16 } | % viola-sonata/pno1 viola-sonata/pno1.t26 149-149.5; 50
  a''8( g''8 fs''8) d''16( ef''16 d''8 g''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 152.5-153; 51
  d''16( e''?16 d''8 g''8) c''16( d''16 c''8 g''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 155.5-156; 52
  a'16( bf'16 a'8 ef''8) a'16( b'16 a'8 ef''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 158.5-159; 53
  a'16 b'16 a'8 ef''8 a'16 c''16 a'8 d''8 | % viola-sonata/pno1 viola-sonata/pno1.t26 161.5-162; 54
  <ef' bf' ef''>2 r4 | % viola-sonata/pno1 viola-sonata/pno1.t26 162-164; 55
  <d' bf' d''>4. <d' a' d''>4. | % viola-sonata/pno1 viola-sonata/pno1.t26 166.5-168; 56
  <bf' d'' g'>2. | % viola-sonata/pno1 viola-sonata/pno1.t26 168-171; 57
  <g''' g'' bf''>8 r4 <g'' bf'' bf' d''>8 r4 \bar "|." 
} }

\new Staff = "down" {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 6/8 \key c \major R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t26 172.5-173; 1
  R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t26 172.5-173; 2
  a'16( gs'16 a'16 f'16 d'16 e'16) f'16( e'16 f'16 d'16 a16 bf16) | % viola-sonata/pno1 viola-sonata/pno1.t2 8.75-9; 3
  \clef bass c'16( a16 fs16 ef'16 d'16 c'16) bf16( a16 bf16 g16 d16 g16) | % viola-sonata/pno1 viola-sonata/pno1.t2 11.75-12; 4
  f4( g8) ef16( d16 ef16 c16 g,16 c16) | % viola-sonata/pno1 viola-sonata/pno1.t2 14.75-15; 5
  g16( fs16 g16 ef16 c16 ef16) c'16( b16 c'16 g16 ef16 g16) | % viola-sonata/pno1 viola-sonata/pno1.t2 17.75-18; 6
  g,16( d16 g16 b16 d'16 g'16) g8( b8 c'16 d'16) | % viola-sonata/pno1 viola-sonata/pno1.t2 20.75-21; 7
  \clef treble ef'16( g'16 fs'16 g'16 ef'16 c'16) d'16( g'16 fs'16 g'16 d'16 bf16) | % viola-sonata/pno1 viola-sonata/pno1.t2 23.75-24; 8
  d'4~ d'16 b16( c'4~ c'16) a16 | % viola-sonata/pno1 viola-sonata/pno1.t2 26.75-27; 9
  b16 d'16 g'16 fs'16 g'16 d'16 c'16 ef'16 g'16 fs'16 g'16 a16 | % viola-sonata/pno1 viola-sonata/pno1.t2 29.75-30; 10
  \clef bass d'16( d16 e16 fs16 g16 a16) \clef treble b16( c'16 d'16 e'16 fs'16 g'16) | % viola-sonata/pno1 viola-sonata/pno1.t2 32.75-33; 11
  a'16( bf'16 g'16 d'16 a16 bf16) \clef bass g16( d16 cs16 d16 bf,16 g,16) | % viola-sonata/pno1 viola-sonata/pno1.t2 35.75-36; 12
  g4 <f f'>8 <ef ef'>8 <d d'>8 <c c'>8 | % viola-sonata/pno1 viola-sonata/pno1.t2 38.5-39; 13
  <bf, bf>8 <a, a>8 <g, g>8 cs,16 cs16 <a, e>16 bf,16 <a, e>16 <g a>16 | % viola-sonata/pno1 viola-sonata/pno1.t2 41.75-42; 14
  cs,16 cs16 <a, e>16 bf,16 <a, e>16 <g a>16 d,16 d16 <a, f>16 e16 <a, f>16 a16 | % viola-sonata/pno1 viola-sonata/pno1.t2 44.75-45; 15
  g,16 d16 <g d'>16 cs'16 <bf d'>16 g'16 c16 g16 <c' e'>16 b16 <c' g'>16 e'16 | % viola-sonata/pno1 viola-sonata/pno1.t2 47.75-48; 16
  <d' f'>8( <a e'>8 <f d'>8) c16_( g16 c'16 \change Staff = "up" e'16 g'16 c''16) | % viola-sonata/pno1 viola-sonata/pno1.t2 50.75-51; 17
  \change Staff = "down" a,16( d16 f16 a16 d'16 f'16) d,16( a,16 d16 f16 a16 d'16) | % viola-sonata/pno1 viola-sonata/pno1.t2 53.75-54; 18
  <f f'>8 <e e'>8 <d d'>8 <c c'>8 <bf, bf>8 <a, a>8 | % viola-sonata/pno1 viola-sonata/pno1.t2 56.5-57; 19
  e,16 fs,16 e,16 b,16 <e gs>16 b,16 e16 ds16 e16 b,16 gs,16 b,16 | % viola-sonata/pno1 viola-sonata/pno1.t2 59.75-60; 20
  <c' c>8 <b b,>8 <a, a>8 <g g,>8 <f f,>8 <e, e>8 | % viola-sonata/pno1 viola-sonata/pno1.t2 62.5-63; 21
  <d d,>8 <c c,>8 <b,, b,>8 <a,, a,>4 r8 | % viola-sonata/pno1 viola-sonata/pno1.t2 64.5-65.5; 22
  R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t2 64.5-65.5; 23
  R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t2 64.5-65.5; 24
  R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t2 64.5-65.5; 25
  R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t2 64.5-65.5; 26
  a,8-. <e cs'>8-. <e cs'>8-. a,8-. <f d'>8-. <f d'>8-. | % viola-sonata/pno1 viola-sonata/pno1.t2 80.5-81; 27
  a,8-. <e cs'>8-. <e cs'>8-. a,8-. <d' f>8-. <d' f>8-. | % viola-sonata/pno1 viola-sonata/pno1.t2 83.5-84; 28
  <e cs' a,>4. <d a f,>8. <a,~ f~ d,~>16 <a, f d,>8 | % viola-sonata/pno1 viola-sonata/pno1.t2 86.25-87; 29
  \ottava #-1 <a,, a,>4. \ottava #0 r4. | % viola-sonata/pno1 viola-sonata/pno1.t2 87-88.5; 30
  d,16 a,16 <a d>16 gs16 <a f>16 d'16 f,16 d16 <f a>16 e16 <d' f>16 a'16 | % viola-sonata/pno1 viola-sonata/pno1.t2 92.75-93; 31
  \clef treble bf'16( a'16 g'16 f'16 e'16 d'16 cs'4.) | % viola-sonata/pno1 viola-sonata/pno1.t2 94.5-95.93; 32
  \clef bass a4 a16( bf16 a8) c'16( bf16 a16 bf16 | % viola-sonata/pno1 viola-sonata/pno1.t2 98.75-99; 33
  a8) c'16( bf16 a16 bf16 a16 bf16 a4) | % viola-sonata/pno1 viola-sonata/pno1.t2 101-101.93; 34
  <f, cs a>4. a16 \mf g16 f16 e16 d16 bf,16 | % viola-sonata/pno1 viola-sonata/pno1.t2 104.75-105; 35
  g,16 g16 <bf d'>16 cs'16 <g d'>16 g'16 a,16 d16 <f a>16 e16 <f d'>16 d16 | % viola-sonata/pno1 viola-sonata/pno1.t2 107.75-108; 36
  c16 a16 c'16 b16 c'16 fs'16 bf,16 d16 g16 bf16 d'16 g'16 | % viola-sonata/pno1 viola-sonata/pno1.t2 110.75-111; 37
  d,8-. <f a,>8-. <f a,>8-. <a, f>8-. <a d>8-. d'8-. | % viola-sonata/pno1 viola-sonata/pno1.t2 113.5-114; 38
  <b f'>8( <as e'>8 <a d'>8) <g e'>8( <f d'>8 <e cs'>8) | % viola-sonata/pno1 viola-sonata/pno1.t2 116.5-117; 39
  d,16 a,16 <d a>16 gs16 <f a>16 d'16 f,16 d16 <a f>16 e16 <d' f>16 a'16 | % viola-sonata/pno1 viola-sonata/pno1.t2 119.75-120; 40
  d,16 a,16 <d a>16 gs16 <f a>16 d'16 f,16 d16 <f a>16 e16 <f d'>16 a'16 | % viola-sonata/pno1 viola-sonata/pno1.t2 122.75-123; 41
  \clef treble a'16( a''16) f'16( f''16) d'16( d''16) \clef bass a16( a'16) f16( f'16) d16( d'16) | % viola-sonata/pno1 viola-sonata/pno1.t2 125.75-126; 42
  cs,16 cs16 <a, e>16 bf,16 <a, e>16 <g a>16 cs,16 cs16 <a, e>16 bf,16 <a, e>16 <g a>16 | % viola-sonata/pno1 viola-sonata/pno1.t2 128.75-129; 43
  d,16 d16 <a, f>16 e16 <a, f>16 a16 c,16 c16 <a, e>16 d16 <c e>16 a16 | % viola-sonata/pno1 viola-sonata/pno1.t2 131.75-132; 44
  f,16 f16 <af c'>16 g16 <af f'>16 c'16 c,16 c16 <ef c'>16 d16 <g c'>16 g'16 | % viola-sonata/pno1 viola-sonata/pno1.t2 134.75-135; 45
  ef,16( ef16) df,16( df16) c,16( c16) bf,,16( bf,16 f16 bf16 df'16 f'16) | % viola-sonata/pno1 viola-sonata/pno1.t2 137.75-138; 46
  g'16( e'16 d'16 bf16 g16 e16) e'16( cs'16 a16 g16 e16 cs16) | % viola-sonata/pno1 viola-sonata/pno1.t2 140.75-141; 47
  <d d'>4. r4. | % viola-sonata/pno1 viola-sonata/pno1.t2 141-142.5; 48
  R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t2 141-142.5; 49
  \clef treble a'16 gs'16 a'16 f'16 d'16 e'16 f'16 e'16 f'16 d'16 a16 bf16 | % viola-sonata/pno1 viola-sonata/pno1.t2 149.75-150; 50
  \clef bass c'16 a16 fs16 ef'16 d'16 c'16 bf16 a16 bf16 g16 d16 g16 | % viola-sonata/pno1 viola-sonata/pno1.t2 152.75-153; 51
  f4( g8) ef16 d16 ef16 c16 g,16 c16 | % viola-sonata/pno1 viola-sonata/pno1.t2 155.75-156; 52
  g16 fs16 g16 ef16 c16 ef16 c'16 b16 c'16 g16 ef16 g16 | % viola-sonata/pno1 viola-sonata/pno1.t2 158.75-159; 53
  ef'16 d'16 ef'16 c'16 g16 c'16 fs'16 e'16 fs'16 d'16 a16 d'16 | % viola-sonata/pno1 viola-sonata/pno1.t2 161.75-162; 54
  <ef g bf df'>2 r4 | % viola-sonata/pno1 viola-sonata/pno1.t2 162-164; 55
  <d g bf>4. <d fs a>4. | % viola-sonata/pno1 viola-sonata/pno1.t2 166.5-168; 56
  g,16( d16 g16 bf,16 d16 bf16) d'16( g16 d'16 \change Staff = "up" bf'16 g'16 d''16) | % viola-sonata/pno1 viola-sonata/pno1.t2 170.75-171; 57
  \change Staff = "down" <bf, g, bf d>8 r4 \ottava #-1 <g,, bf,, d, bf,>8 r4 \bar "|." 
} }

>>

>>
}

