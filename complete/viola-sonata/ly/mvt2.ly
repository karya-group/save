\version "2.14.2"
\language "english"
\pointAndClickOff
\header { title = "mvt2" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\score {
<<
\new Staff  {
\set Staff.instrumentName = "viola"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 4/4 \key c \major \clef treble e''4^"pizz." \f f'4 d''4 e'4 | % viola-sonata/vla2 viola-sonata/b2.t38 3-4; 1
  cs''4 d'4 b'4 cs'4 | % viola-sonata/vla2 viola-sonata/b2.t38 7-8; 2
  e''4 f'4 f''4 e'4 | % viola-sonata/vla2 viola-sonata/b2.t38 11-12; 3
  c''4 ef'4 bf'4 d'4 | % viola-sonata/vla2 viola-sonata/b2.t38 15-16; 4
  \clef alto c'1~^"arco" \p | % viola-sonata/vla2 viola-sonata/b2.t38 16-20.25; 5
  c'16 bf'16( c'16) a'16( df'16) g'16 r8 c'2 | % viola-sonata/vla2 viola-sonata/b2.t38 22-24; 6
  bf'4( c'4) a'4( bf4) | % viola-sonata/vla2 viola-sonata/b2.t38 27-28; 7
  g'4( a4) gf'4( g4) | % viola-sonata/vla2 viola-sonata/b2.t38 31-32; 8
  a'4-+ \f r8 a'8_~^"nv" \pp <a'_~ bf'>2 | % viola-sonata/vla2 viola-sonata/b2.t41 33.5-42; 9
  <a'_~ b'!^~>2 <a'_~ b'>8 <a'_~ c''~>4. | % viola-sonata/vla2 viola-sonata/b2.t41 33.5-42; 10
  <a' c''>2 <gs' cs''>2^"vib" \mf | % viola-sonata/vla2 viola-sonata/b2.t38 42-44; 11
  b'2 as'2~( | % viola-sonata/vla2 viola-sonata/b2.t38 46-48.5; 12
  as'8 b8) gs'2 as'8( b8) | % viola-sonata/vla2 viola-sonata/b2.t38 51.5-52; 13
  gs'8( as8) g'8( af8) f'8( g8) r8 b'8 | % viola-sonata/vla2 viola-sonata/b2.t38 55.5-56; 14
  cs''2 \> ds''2 | % viola-sonata/vla2 viola-sonata/b2.t38 58-60; 15
  e''2~( \p e''8 fs'8) ds''4( | % viola-sonata/vla2 viola-sonata/b2.t38 63-64; 16
  e'8) cs''8( ds'8) bs'8~ bs'4. cs''16(-- ds''16)-- | % viola-sonata/vla2 viola-sonata/b2.t38 67.75-68; 17
  e''4^"pizz." \f fs'4 ds''4 e'4 | % viola-sonata/vla2 viola-sonata/b2.t38 71-72; 18
  cs''4 ds'4 bs'4 cs'4 | % viola-sonata/vla2 viola-sonata/b2.t38 75-76; 19
  e''4 fs'4 ds''4 e'4 | % viola-sonata/vla2 viola-sonata/b2.t38 79-80; 20
  cs''4 ds'4 bs'4 cs'4 | % viola-sonata/vla2 viola-sonata/b2.t38 83-84; 21
  gs2^"arco" \p \< fs4 b8( a8) | % viola-sonata/vla2 viola-sonata/b2.t38 87.5-88; 22
  ds8( e8 fs8) <c a>8( df8 gs8 fs8 f8) | % viola-sonata/vla2 viola-sonata/b2.t38 91.5-92; 23
  e2 \f <gs e'>4 <cs cs'>4 | % viola-sonata/vla2 viola-sonata/b2.t38 95-96; 24
  <c g>16 \ff <e'~ c''~>8. <e' c''>4 <<
  { \voiceOne
  bf'8( af'8) g'4 | % viola-sonata/vla2 viola-sonata/b2.t38 99-100; 25
  } \new Voice { \voiceTwo
  d'4 bf8( a8) | % viola-sonata/vla2 viola-sonata/b2.t41 99.5-100; 25
  } >> \oneVoice
  <c g>16 \mf <g ef'>8. bf'16( af'16 g'8)-. g'16( f'16 ef'8)-. ef'16( df'16 b16 a16) | % viola-sonata/vla2 viola-sonata/b2.t38 103.75-104; 26
  g1~ \p | % viola-sonata/vla2 viola-sonata/b2.t38 104-108.25; 27
  g16 ef'16( g16) d'16( a16) df'16 r8 g2 | % viola-sonata/vla2 viola-sonata/b2.t38 110-112; 28
  ef'4( f4) d'4( ef4) | % viola-sonata/vla2 viola-sonata/b2.t38 115-116; 29
  c'4( d4) b4( c4) | % viola-sonata/vla2 viola-sonata/b2.t38 119-120; 30
  a'4-+ \f r8 a'8_~^"nv" \pp <a'_~ bf'>2 | % viola-sonata/vla2 viola-sonata/b2.t41 121.5-130; 31
  <a'_~ b'!^~>2 <a'_~ b'>8 <a'_~ c''~>4. | % viola-sonata/vla2 viola-sonata/b2.t41 121.5-130; 32
  <a' c''>2 <gs' cs''>2^"vib" \mf | % viola-sonata/vla2 viola-sonata/b2.t38 130-132; 33
  b'2 as'2~( | % viola-sonata/vla2 viola-sonata/b2.t38 134-136.5; 34
  as'8 b8) gs'2 as'8( b8) | % viola-sonata/vla2 viola-sonata/b2.t38 139.5-140; 35
  gs'8( as8) g'8( af8) f'8( g8) r8 b'8 | % viola-sonata/vla2 viola-sonata/b2.t38 143.5-144; 36
  cs''2 \> ds''2 | % viola-sonata/vla2 viola-sonata/b2.t38 146-148; 37
  e''2 \p ef''2 | % viola-sonata/vla2 viola-sonata/b2.t38 150-152; 38
  df''4 e''8 c''8 af'8 d'8 df'16( \mf b'8.) | % viola-sonata/vla2 viola-sonata/b2.t38 155.25-156; 39
  c'16( \> bf'8.) b16( a'8.) r4 c'4~ \p | % viola-sonata/vla2 viola-sonata/b2.t38 159-161; 40
  c'4 g'8.( \mf \> bf'16) gf'8.( a'16) r4 | % viola-sonata/vla2 viola-sonata/b2.t38 162.75-163; 41
  c'1 \p | % viola-sonata/vla2 viola-sonata/b2.t38 164-168; 42
  c'4.( b8) c'4.( bf8) | % viola-sonata/vla2 viola-sonata/b2.t38 171.5-172; 43
  a2 d8 a4( bf8) | % viola-sonata/vla2 viola-sonata/b2.t38 175.5-176; 44
  d8( a8 bf8 c'8) bf8( a4 d8) | % viola-sonata/vla2 viola-sonata/b2.t38 179.5-180; 45
  bf8( a4 d8) c'8( bf8) bf'8( a8) | % viola-sonata/vla2 viola-sonata/b2.t38 183.5-184; 46
  g16 \< g8. ef'16 ef'8. f16 f8. d'16 d'8. | % viola-sonata/vla2 viola-sonata/b2.t38 187.25-188; 47
  ef16 ef8. c'16 c'8. d16 d8. b16 b8. | % viola-sonata/vla2 viola-sonata/b2.t38 191.25-192; 48
  g16 \f g8. ef'16 ef'8. f16 f8. d'16 d'8. | % viola-sonata/vla2 viola-sonata/b2.t38 195.25-196; 49
  ef16 ef8. c'16 c'8. d16 d8. bf16 bf8. | % viola-sonata/vla2 viola-sonata/b2.t38 199.25-200; 50
  c16 c8. a16 a8. d16 d8. bf16 bf8. | % viola-sonata/vla2 viola-sonata/b2.t38 203.25-204; 51
  ef16 ef8. c'16 c'8 a16 <g~ d'~>2 | % viola-sonata/vla2 viola-sonata/b2.t38 206-212; 52
  <g d'>1 | % viola-sonata/vla2 viola-sonata/b2.t38 206-212; 53
  <g d'>16 <d' a'>8. <c g>16 <g f'>8. <ef g>16 <g ef'>8. <c g>16 <g d'>8. | % viola-sonata/vla2 viola-sonata/b2.t38 215.25-216; 54
  c16( a16 c'16 f'16 \clef treble bf'16 f''16 g''16 c''16) f''8 ef''8 d''8 c''8 | % viola-sonata/vla2 viola-sonata/b2.t38 219.5-220; 55
  d''4 f''8 ef''8 d''2 | % viola-sonata/vla2 viola-sonata/b2.t38 222-224; 56
  \clef alto d'2 \p c'4( d'8 ef'8) | % viola-sonata/vla2 viola-sonata/b2.t38 227.5-228; 57
  bf4( a4) bf4( a4) | % viola-sonata/vla2 viola-sonata/b2.t38 231-232; 58
  d2 e4( f8 g8) | % viola-sonata/vla2 viola-sonata/b2.t38 235.5-236; 59
  bf8( g'8) a4 b8( f'8) af8( gs'8) | % viola-sonata/vla2 viola-sonata/b2.t38 239.5-240; 60
  a'2 bf'8( a'8) c''8( b'8) | % viola-sonata/vla2 viola-sonata/b2.t38 243.5-244; 61
  bf'4( a'8 g'8) b8( cs'8 e'8 g'8) | % viola-sonata/vla2 viola-sonata/b2.t38 247.5-248; 62
  <<
  { \voiceOne
  a'4. \f r8 bf'8 r8 a'8( g'8) | % viola-sonata/vla2 viola-sonata/b2.t41 251.5-252; 63
  f'4 r4 a'4.( bf'8) | % viola-sonata/vla2 viola-sonata/b2.t41 255.5-256; 64
  d''4.( cs''8) } \new Voice { \voiceTwo
  f'32( g'32 f'32 e'32 f'4 e'8) d'8( f'8) df'4 | % viola-sonata/vla2 viola-sonata/b2.t38 251-252; 63
  bf8( g8) a8( bf8) f'8( e'8 g'4~ | % viola-sonata/vla2 viola-sonata/b2.t38 255-256.5; 64
  g'8) e'8( f'4) } >> \oneVoice
  <d'~ d''~>2 \> | \time 2/4 % viola-sonata/vla2 viola-sonata/b2.t38 258-262; 65
  <d' d''>2 | \time 4/4 % viola-sonata/vla2 viola-sonata/b2.t38 258-262; 66
  <c g>16 \f <e'~ c''~>8. <e' c''>4 <<
  { \voiceOne
  bf'8( af'8) g'4 | % viola-sonata/vla2 viola-sonata/b2.t38 265-266; 67
  } \new Voice { \voiceTwo
  d'4 bf8( a8) | % viola-sonata/vla2 viola-sonata/b2.t41 265.5-266; 67
  } >> \oneVoice
  <c g>16 <g ef'>8. bf'16( af'16 g'8)-. g'16( f'16 ef'8)-. ef'16( df'16 b16 a16) | % viola-sonata/vla2 viola-sonata/b2.t38 269.75-270; 68
  <c g>16 <g ef'>8. <g ef'>16 <ef' c''>8. <c g>16 <g ef'>8. <g ef'>16 <ef' c''>8. | % viola-sonata/vla2 viola-sonata/b2.t38 273.25-274; 69
  \clef treble ef'16 g'16 c''16 g'16 ef''16 c''16 g'16 g''16 ef''16 c''16 g'16 ef''16 c''16 g'16 ef'16 af'16 | % viola-sonata/vla2 viola-sonata/b2.t38 277.75-278; 70
  <b af'>16 <af' ef''>8. af''16( g''16 f''8)-. f''16( ef''16 d''8)-. d''16( c''16 b'16 a'16) | % viola-sonata/vla2 viola-sonata/b2.t38 281.75-282; 71
  af'4-- \ff g'4-- f'4-- ef'4-- | % viola-sonata/vla2 viola-sonata/b2.t38 285-286; 72
  c'8 g'8 d''8 g''8 c'''4-- b''4-- | % viola-sonata/vla2 viola-sonata/b2.t38 289-290; 73
  bf''4-- a''4-- af''4-- g''4-- | % viola-sonata/vla2 viola-sonata/b2.t38 293-294; 74
  f'8 af'8 c''8 f''8 g''4-- gf''4-- | % viola-sonata/vla2 viola-sonata/b2.t38 297-298; 75
  f''4-- e''4-- ef''4-- d''4-- | % viola-sonata/vla2 viola-sonata/b2.t38 301-302; 76
  c''4-. \clef alto <g c>16( <g ef'>8.)-. c4-. r4 \bar "|." 
} }

\new PianoStaff <<
\set PianoStaff.instrumentName = ""
\set PianoStaff.shortInstrumentName = ""
\new Staff = "up" {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 4/4 \key c \major e''16 \mf e''8.-. f'16 f'8.-. d''16 d''8.-. e'16 e'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 3.25-4; 1
  cs''16 cs''8.-. d'16 d'8.-. b'16 b'8.-. cs'16 cs'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 7.25-8; 2
  e''16 e''8.-. f'16 f'8.-. d''16 d''8.-. e'16 e'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 11.25-12; 3
  bf'16 bf'8.-. c'16 c'8.-. a'16 a'8.-. bf16 bf8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 15.25-16; 4
  a1~ \p | % viola-sonata/pno2 viola-sonata/b2.t23 16-22; 5
  a2 a2~ | % viola-sonata/pno2 viola-sonata/b2.t23 22-29; 6
  a1~ | % viola-sonata/pno2 viola-sonata/b2.t23 22-29; 7
  a4 c'''16( bf''16 e''16 f''16) a''4~( a''8. bf''16) | % viola-sonata/pno2 viola-sonata/b2.t23 31.75-32; 8
  a''2 r16 g''16( c''16 df''16 f''4~ | % viola-sonata/pno2 viola-sonata/b2.t23 35-37.25; 9
  f''4~ f''16) e''16( b'16 c''16 ef''4~ ef''16) d''16 a'16 e'16 | % viola-sonata/pno2 viola-sonata/b2.t23 39.75-40; 10
  cs'16 f'16 a'16 fs''16~ fs''4 \arpeggioArrowUp<as' fs'' cs'''>2\arpeggio \mf | % viola-sonata/pno2 viola-sonata/b2.t23 42-44; 11
  <b' ds'' g''>2 <b'~ ds''~ fs''~>2 | % viola-sonata/pno2 viola-sonata/b2.t23 46-51; 12
  <b' ds'' fs''>2. \times 2/3 { as''16( gs''16 ds''16 } fs''8~ | % viola-sonata/pno2 viola-sonata/b2.t23 51.5-53; 13
  fs''4) \times 2/3 { e''16( ds''16 g'16 } cs''8~ cs''4.) b'8 | % viola-sonata/pno2 viola-sonata/b2.t23 55.5-56; 14
  as'2 \> fs'2 | % viola-sonata/pno2 viola-sonata/b2.t23 58-60; 15
  gs'1~ \p | % viola-sonata/pno2 viola-sonata/b2.t23 60-68; 16
  gs'1 | % viola-sonata/pno2 viola-sonata/b2.t23 60-68; 17
  e''16 \mf e''8.-. fs'16 fs'8.-. ds''16 ds''8.-. e'16 e'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 71.25-72; 18
  cs''16 cs''8.-. ds'16 ds'8.-. c''16 c''8.-. cs'16 cs'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 75.25-76; 19
  e''16 e''8.-. fs'16 fs'8.-. ds''16 ds''8.-. e'16 e'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 79.25-80; 20
  cs''16 cs''8.-. ds'16 ds'8.-. c''16 c''8.-. cs'16 cs'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 83.25-84; 21
  e''16 \< e''8.-. fs'16 fs'8.-. ds''16 ds''8.-. e'16 e'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 87.25-88; 22
  cs''16 \mf cs''8.-. ds'16 ds'8.-. <a' c''>16 <a' c''>8.-. r16 cs''16 c'''16 f''16 | % viola-sonata/pno2 viola-sonata/b2.t23 91.75-92; 23
  \times 4/6 { cs'''16( \f e''16 cs''16 \change Staff = "down" c''16 e'16 cs'16) } \times 4/5 { \change Staff = "up" e''16( cs''16 bs'16 gs'16 e'16) } r4 \clef bass \times 2/3 { e,16( cs16 gs,16 } cs,8) | % viola-sonata/pno2 viola-sonata/b2.t23 95.5-96; 24
  \clef treble <c'' g'' d'''>2. \ff <g'' ef'''>8 <fs'' d'''>8 | % viola-sonata/pno2 viola-sonata/b2.t23 99.5-100; 25
  <f''! df'''>2 \mf \arpeggioArrowUp<ef'' gf'' b''>2\arpeggio | % viola-sonata/pno2 viola-sonata/b2.t23 102-104; 26
  <bf~ g'~>1 \p | % viola-sonata/pno2 viola-sonata/b2.t23 104-110; 27
  <bf g'>2 <bf~ g'~>2 | % viola-sonata/pno2 viola-sonata/b2.t23 110-117; 28
  <bf~ g'~>1 | % viola-sonata/pno2 viola-sonata/b2.t23 110-117; 29
  <bf g'>4 as''16( gs''16 d''16 ds''16 fs''4~ fs''8. g''16) | % viola-sonata/pno2 viola-sonata/b2.t23 119.75-120; 30
  a''2 r16 g''16( c''16 df''16 f''4~ | % viola-sonata/pno2 viola-sonata/b2.t23 123-125.25; 31
  f''4~ f''16) e''16( b'16 c''16 ef''4~ ef''16) d''16 a'16 e'16 | % viola-sonata/pno2 viola-sonata/b2.t23 127.75-128; 32
  cs'16 f'16 a'16 fs''16~ fs''4 \arpeggioArrowUp<as' fs'' cs'''>2\arpeggio \mf | % viola-sonata/pno2 viola-sonata/b2.t23 130-132; 33
  <b' ds'' g''>2 <b'~ ds''~ fs''~>2 | % viola-sonata/pno2 viola-sonata/b2.t23 134-139; 34
  <b' ds'' fs''>2. \times 2/3 { as''16( gs''16 ds''16 } fs''8~ | % viola-sonata/pno2 viola-sonata/b2.t23 139.5-141; 35
  fs''4) \times 2/3 { e''16( ds''16 g'16 } cs''8~ cs''4) r8 b'8 | % viola-sonata/pno2 viola-sonata/b2.t23 143.5-144; 36
  as'2 \> fs'2 \! | % viola-sonata/pno2 viola-sonata/b2.t23 146-148; 37
  <<
  { \voiceOne
  r4 \p \times 2/3 { e'''16( ds'''16 as''16 } d'''8~ d'''4) \times 2/3 { cs'''16( b''16 g''16 } as''8) | % viola-sonata/pno2 viola-sonata/b2.t23 151.5-152; 38
  } \new Voice { \voiceTwo
  gs'1 | % viola-sonata/pno2 viola-sonata/b2.t26 148-152; 38
  } >> \oneVoice
  \times 4/6 { b''16( as''16 e''16 a''16 gs''16 cs''16) } g''8 e''8 c''8 f'8 gs'8.( \mf \> b'16) | % viola-sonata/pno2 viola-sonata/b2.t23 155.75-156; 39
  g'8.( bf'16) fs'8.( a'16) r4 a4~ \p | % viola-sonata/pno2 viola-sonata/b2.t23 159-161; 40
  a4 c'16( \mf e''16 \> bf'16 g'16) b16( d''16 af'16 d'16) r4 | % viola-sonata/pno2 viola-sonata/b2.t23 162.75-163; 41
  a4( \p g'4) f'4( g'8 af'8) | % viola-sonata/pno2 viola-sonata/b2.t23 167.5-168; 42
  ef'8( ef''8 d'4) ef'8( ef''8 df'8 b8) | % viola-sonata/pno2 viola-sonata/b2.t23 171.5-172; 43
  c'16 c'8.-. a'16 a'8.-. bf16 bf8.-. g'16 g'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 175.25-176; 44
  a16 a8.-. gf'16 gf'8.-. c'16 c'8.-. a'16 a'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 179.25-180; 45
  bf16 bf8.-. g'16 g'8.-. a16 a8.-. gf'16 gf'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 183.25-184; 46
  \clef bass g16 \< g8. ef'16 ef'8. f16 f8. d'16 d'8. | % viola-sonata/pno2 viola-sonata/b2.t23 187.25-188; 47
  ef16 ef8. c'16 c'8. d16 d8. b16 b8. | % viola-sonata/pno2 viola-sonata/b2.t23 191.25-192; 48
  \clef treble g'16( \f d''16 a''16 g''16 d'''16 g''16 a''16 c''16) g''16( d''16 f''16 a''16 bf'16 g''16 a'16 fs''16) | % viola-sonata/pno2 viola-sonata/b2.t23 195.75-196; 49
  g'16( ef''16 a''16 g''16 d'''16 ef''16 a''16 g''16) ef''16( d'''16 f''16 c'''16 ef''16 bf''16 d''16 a''16) | % viola-sonata/pno2 viola-sonata/b2.t23 199.75-200; 50
  g'16( d''16 a''16 g''16 d'''16 f''16 a''16 g''16) \ottava #1 g'''16( d'''16 c'''16 f'''16 bf''16 f'''16 a''16 f'''16) | % viola-sonata/pno2 viola-sonata/b2.t23 203.75-204; 51
  g''16( bf''16 g'''16 f'''16 bf'''16 ef'''16 d'''16 a'''16) g'''8( as''8) f'''8( a''8) | % viola-sonata/pno2 viola-sonata/b2.t23 207.5-208; 52
  ef'''8( g''8) d'''8( f''8) c'''8( ef''8) bf''8( d''8) | % viola-sonata/pno2 viola-sonata/b2.t23 211.5-212; 53
  \ottava #0 g''8( bf'8) f''8( a'8) ef''8( g'8) d''8( f'8) | % viola-sonata/pno2 viola-sonata/b2.t23 215.5-216; 54
  c''8( ef'8) bf'8( d'8) a'8( c'8) g'8( bf8) | % viola-sonata/pno2 viola-sonata/b2.t23 219.5-220; 55
  <a g'>4 a'8( c'8) <g' g''>16( ef''16 bf'16 g'16 ef'16 c'16 g16 ef16) | % viola-sonata/pno2 viola-sonata/b2.t23 223.75-224; 56
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t23 223.75-224; 57
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t23 223.75-224; 58
  e''16 \p e''8.-. f'16 f'8.-. d''16 d''8.-. e'16 e'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 235.25-236; 59
  df''16 df''8.-. d'16 d'8.-. b'16 b'8.-. df'16 df'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 239.25-240; 60
  e''16 e''8.-. f'16 f'8.-. d''16 d''8.-. e'16 e'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 243.25-244; 61
  df''16 df''8.-. d'16 d'8.-. b'16 b'8.-. df'16 df'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 247.25-248; 62
  e''16 \f e''8.-. f'16 f'8.-. d''16 d''8.-. e'16 e'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 251.25-252; 63
  df''16 df''8.-. d'16 d'8.-. b'16 b'8.-. df'16 df'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 255.25-256; 64
  d''16 d''8.-. f'16 f'8.-. a'16 a'8.-. a16 a8.-. | \time 2/4 % viola-sonata/pno2 viola-sonata/b2.t23 259.25-260; 65
  f'16 f'8.-. r4 | \time 4/4 % viola-sonata/pno2 viola-sonata/b2.t23 260.25-261; 66
  <c'' g'' d'''>2. <g'' ef'''>8( <d''' gf''>8) | % viola-sonata/pno2 viola-sonata/b2.t23 265.5-266; 67
  <f'' df'''>2 \arpeggioArrowUp<ds'' fs'' b''>2\arpeggio | % viola-sonata/pno2 viola-sonata/b2.t23 268-270; 68
  \clef bass c,16( g,16 d16 c16 g16 c16 d16 c'16) f16( g16 ef'16 f16 d'16 ef16 c'16 d16) | % viola-sonata/pno2 viola-sonata/b2.t23 273.75-274; 69
  c16( g16 d'16 c'16 \clef treble g'16 d'16 c''16 ef'16) g'16( ef''16 f'16 d''16 ef'16 c''16 d'16 b'16) | % viola-sonata/pno2 viola-sonata/b2.t23 277.75-278; 70
  <ef' b'>2 \arpeggioArrowUp<c'' ef'' af'' c'''>2\arpeggio | % viola-sonata/pno2 viola-sonata/b2.t23 280-282; 71
  <b af'>4 \ff <c' g'>4 <af c' f'>4 <g c' g'>4 | % viola-sonata/pno2 viola-sonata/b2.t23 285-286; 72
  <c' f' a'>4 <d' f' b'>4 <ef' g' c''>4 <d' g' b'>4 | % viola-sonata/pno2 viola-sonata/b2.t23 289-290; 73
  <d' g' bf'>4 <c' f' a'>4 <c' f' af'>4 <c' ef' g'>4 | % viola-sonata/pno2 viola-sonata/b2.t23 293-294; 74
  <c' d' f'>4 <c' ef'>4 <b d'>4 <b ef'>4 | % viola-sonata/pno2 viola-sonata/b2.t23 297-298; 75
  <c' f'>4 <c' g'>4 <c' a'>4 <d' b'>4 | % viola-sonata/pno2 viola-sonata/b2.t23 301-302; 76
  <ef' c''>4-. <g g'>4-. g4-. r4 \bar "|." 
} }

\new Staff = "down" {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 4/4 \key c \major \clef bass R4*4 | % viola-sonata/pno2 viola-sonata/b2.t23 304-305; 1
  r2 r8 \ottava #-1 \times 2/3 { d,,16( cs,16 f,,16 } d,,8) \ottava #0 r8 | % viola-sonata/pno2 viola-sonata/b2.t9 7-7.5; 2
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 7-7.5; 3
  r2 r8 \ottava #-1 \times 2/3 { d,,16( d,16 a,,16 } d,,8) \ottava #0 r8 | % viola-sonata/pno2 viola-sonata/b2.t9 15-15.5; 4
  \ottava #-1 <d,,~ fs,~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 16-22; 5
  <d,, fs,>2 <d,,~ fs,~>2 | % viola-sonata/pno2 viola-sonata/b2.t9 22-32; 6
  <d,,~ fs,~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 22-32; 7
  <d,, fs,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 22-32; 8
  \ottava #0 R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 22-32; 9
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 22-32; 10
  r2 \arpeggioArrowUp<ds as f'>2\arpeggio | % viola-sonata/pno2 viola-sonata/b2.t9 42-44; 11
  <b, fs cs'>2 <gs,~ ds~ as~>2 | % viola-sonata/pno2 viola-sonata/b2.t9 46-52; 12
  <gs, ds as>1 | % viola-sonata/pno2 viola-sonata/b2.t9 46-52; 13
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 46-52; 14
  <cs' b, fs>2 <fs as gs, ds>2 | % viola-sonata/pno2 viola-sonata/b2.t9 58-60; 15
  <ds~ cs,~ gs,~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 60-68; 16
  <ds cs, gs,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 60-68; 17
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 60-68; 18
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 60-68; 19
  \times 2/3 { cs,16( cs16 e,16 } cs,8) r4 gs2 | % viola-sonata/pno2 viola-sonata/b2.t9 78-80; 20
  fs4( gs8 a8) e8( e'8 ds4) | % viola-sonata/pno2 viola-sonata/b2.t9 83-83.93; 21
  \times 2/3 { cs16( cs'16 e16 } cs8) \times 2/3 { gs16( ds'16 bs16 } fs8) \times 2/3 { e16( cs'16 gs16 } ds8) <g b>8( <f a>8) | % viola-sonata/pno2 viola-sonata/b2.t9 87.5-88; 22
  <ds fs>8( <e gs>8 <fs a>8 c'8 df'4) df'16 df'8.-. | % viola-sonata/pno2 viola-sonata/b2.t9 91.25-92; 23
  \clef treble r2 \clef bass \times 4/6 { cs'16( gs16 cs16 cs'16 gs16 cs16) } r4 | % viola-sonata/pno2 viola-sonata/b2.t9 91.25-92; 24
  <c, g, e>1 | % viola-sonata/pno2 viola-sonata/b2.t9 96-100; 25
  <c, g, ef>1 | % viola-sonata/pno2 viola-sonata/b2.t9 100-104; 26
  <c,~ e~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 104-110; 27
  <c, e>2 <c,~ e~>2 | % viola-sonata/pno2 viola-sonata/b2.t9 110-120; 28
  <c,~ e~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 110-120; 29
  <c, e>1 | % viola-sonata/pno2 viola-sonata/b2.t9 110-120; 30
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 110-120; 31
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 110-120; 32
  r2 \arpeggioArrowUp<ds as f'>2\arpeggio | % viola-sonata/pno2 viola-sonata/b2.t9 130-132; 33
  <b, fs cs'>2 <gs,~ ds~ as~>2 | % viola-sonata/pno2 viola-sonata/b2.t9 134-140; 34
  <gs, ds as>1 | % viola-sonata/pno2 viola-sonata/b2.t9 134-140; 35
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 134-140; 36
  <cs' b, fs>2 <fs as gs, ds>2 | % viola-sonata/pno2 viola-sonata/b2.t9 146-148; 37
  <gs, ds cs,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 148-152; 38
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 148-152; 39
  r2 r4 \ottava #-1 <fs,~ d,,~>4 | % viola-sonata/pno2 viola-sonata/b2.t9 159-163; 40
  <fs, d,,>2. r4 | % viola-sonata/pno2 viola-sonata/b2.t9 159-163; 41
  <fs,~ d,,~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 164-172; 42
  <fs, d,,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 164-172; 43
  <g,~ g,,~ d,~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 172-184; 44
  <g,~ g,,~ d,~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 172-184; 45
  <g, g,, d,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 172-184; 46
  <g,,~ g,~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 184-192; 47
  <g,, g,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 184-192; 48
  \ottava #0 g16 g8. ef'16 ef'8. f16 f8. d'16 d'8. | % viola-sonata/pno2 viola-sonata/b2.t9 195.25-196; 49
  ef16 ef8. c'16 c'8. d16 d8. bf16 bf8. | % viola-sonata/pno2 viola-sonata/b2.t9 199.25-200; 50
  c16 c8. a16 a8. bf,16 bf,8. g16 g8. | % viola-sonata/pno2 viola-sonata/b2.t9 203.25-204; 51
  c16 c8. a8. \ottava #-1 a,16 g,2~ | % viola-sonata/pno2 viola-sonata/b2.t9 206-212; 52
  <g, f,, c,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 206-212; 53
  g,2~ <g, f,, c,>2 | % viola-sonata/pno2 viola-sonata/b2.t9 212-216; 54
  <f, c as,,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 216-220; 55
  <d, g, g,,>4 <c,, c,>4 <g,, d, g,>2 | % viola-sonata/pno2 viola-sonata/b2.t9 222-224; 56
  \ottava #0 <g,~ d~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 224-232; 57
  <g, d>1 | % viola-sonata/pno2 viola-sonata/b2.t9 224-232; 58
  \ottava #-1 <d,~ d,,~ a,,~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 232-240; 59
  <d, d,, a,,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 232-240; 60
  \ottava #0 R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 232-240; 61
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 232-240; 62
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 232-240; 63
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 232-240; 64
  R4*4 | \time 2/4 % viola-sonata/pno2 viola-sonata/b2.t9 232-240; 65
  r4 d16 d8.-. | \time 4/4 % viola-sonata/pno2 viola-sonata/b2.t9 261.25-262; 66
  <c, g, e>1 | % viola-sonata/pno2 viola-sonata/b2.t9 262-266; 67
  <g, ds c,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 266-270; 68
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 266-270; 69
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 266-270; 70
  \ottava #-1 <af,, ef, b,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 278-282; 71
  \ottava #0 <b, ef af af,>4 <c ef g g,>4 <f, c f>4 <ef, c>4 | % viola-sonata/pno2 viola-sonata/b2.t9 285-286; 72
  <d, d>4 <g, d>4 <ef, c>4 <g, d>4 | % viola-sonata/pno2 viola-sonata/b2.t9 289-290; 73
  <bf, g>4 <c a>4 <ef af>4 <ef g>4 | % viola-sonata/pno2 viola-sonata/b2.t9 293-294; 74
  f4 fs4 g4 gf4 | % viola-sonata/pno2 viola-sonata/b2.t9 297-298; 75
  <af f>4 e4 ef4 <g d>4 | % viola-sonata/pno2 viola-sonata/b2.t9 301-302; 76
  <g ef' c>4-. <g, ef c,>4-. \ottava #-1 <c,, c,>4-. \ottava #0 r4 \bar "|." 
} }

>>

>>
}

