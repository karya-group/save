\version "2.14.2"
\language "english"
\pointAndClickOff
\header { title = "vla3-2" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\score {
<<
\new Staff  {
\set Staff.instrumentName = "viola"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 4/4 \key af \major af8-. bf8-. c'8-. df'8-. ef'4 af4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 3-4; 1
 f'4 ef'8-. d'8-. ef'4.( f'8) | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 7.5-8; 2
 gf'4 f'8( ef'8) df'4( ef'4) | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 11-12; 3
 ff'4 ef'4 ff'4 ef'4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 15-16; 4
 ef'8^"pizz." af'8 ef'8 af'8 ef'8 af'8 ef'8 af'8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 19.5-20; 5
 ef'8 af'8 ef'8 af'8 e'8 g'8 e'8 g'8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 23.5-24; 6
 ef'8 ef''8 ef''8 ef'8 ef''8 ef''8 d'8 ef'8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 27.5-28; 7
 r8 d'8 ef'8 d'8 ef'8 e'8 r8 d'8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 31.5-32; 8
 ef'8 d'8 ef'8 df'8 cf'8 bf8 af8 bf8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 35.5-36; 9
 c'8 df'8 ef'4 af4 d'4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 39-40; 10
 c'8 bf8 c'4 ef8 f8 gf4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 43-44; 11
 gf'4 gf4 gf'4 ef8 ff8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 47.5-48; 12
 b8 ef'8 \clef treble e'8 as'8 b'8 e''8 af''8 df''8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 51.5-52; 13
 gf'8 df'8 \clef alto bf8 gf8 df8 gf8 <d b>16-.^"arco" <d b>16-. <d b>16-. <d b>16-. | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 55.75-56; 14
 <d b>8-. r8 <bf ef'>16 <bf ef'>16 <bf ef'>16 <bf ef'>16 <ef' bf'>8-. r8 r4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 58-58.5; 15
 r4 d'4( a4) af'4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 63-64; 16
 gf'8( f'8) gf'8( d'8 a4) \clef treble df''8( ef''8) | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 67.5-68; 17
 gf''16( ff''16 ef''16 df''16) ef''8( df''16 cf''16) bff'8-. \clef alto af'4( gf'16 ff'16) | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 71.75-72; 18
 gf'8( ff'16 ef'16) ff'8( ef'16 df'16) b8 b8 \key c \major a4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 75-76; 19
 r4 c'8-. d'8-. e'8-. f'8-. g'4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 79-80; 20
 c'4 a'4 g'8( fs'8) g'8 e'8( | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 83.5-84; 21
 g'8 a'8) bf'8( a'16 g'16) f'8-. ef'8-. \times 4/6 { df'16( e'16 a'16) a'16( e'16 df'16) } | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 86.5-87; 22
 \times 4/6 { df'16 e'16 a'16 a'16 e'16 df'16 } \times 4/6 { c'16 e'16 a'16 a'16 e'16 c'16 } \times 4/6 { c'16 e'16 a'16 a'16 e'16 c'16 } \times 4/6 { df'16 e'16 a'16 a'16 e'16 df'16 } | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 86.5-87; 23
 \times 4/6 { df'16 e'16 a'16 a'16 e'16 df'16 } \times 4/6 { c'16 e'16 a'16 a'16 e'16 c'16 } \times 4/6 { c'16 e'16 a'16 a'16 e'16 c'16 } b4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 95-96; 24
 c'4 d'4 e'4 f'4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 99-100; 25
 gs'4 a'4 b'4 cs''8 b'16( a'16) | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 103.75-104; 26
 g'8-. f'8-. g'8( f'8) ds'8( e'8) \times 4/6 { ef'16 a'16 c''16 c''16 a'16 ef'16 } | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 106.5-107; 27
 \times 4/6 { ef'16 a'16 c''16 c''16 a'16 ef'16 } \times 4/6 { a8 e'8 a'8 a'8 e'8 a8 } \times 4/6 { d'8 a'8 d''8 d''8 a'8 d'8 } | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 106.5-107; 28
  \times 4/6 { d'8 a'8 d''8 d''8 a'8 d'8 } d'16 d''16 a'16 d'16 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 115.75-116; 29
 d''16 a'16 d'16 d''16 a'16 d'16 d''16 a'16 d'16 d''16 a'16 d'16 b'16 af'16 f'16 e'16 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 119.75-120; 30
 d'16 b16 af16 f16 e16 a16 cs'16 e'16 a'16 cs''16 e''16 cs''16 \clef treble d''8-. e''8-. | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 123.5-124; 31
 fs''8-. g''8-. a''4 d''4 b'4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 127-128; 32
 a'8-. gs'8-. a'8 fs'8 a'8 d''8 g''4~( | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 131-132.5; 33
 g''8 fs''8) g''2 g''4~( | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 135-136.5; 34
 g''8 e''8) \acciaccatura { g''8 } fs''8 e''8 d''8 e''8 g''4~( | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 139-140.5; 35
 g''8 fs''8) g''2 g''4~( | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 143-144.5; 36
 g''8 e''8) fs''4 e''4 d''4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 147-148; 37
 r4 \clef alto <d a fs' d''>4 r4 d'4~ | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 151-155; 38
 d'2. r4 \bar "|."
} }

>>
}

