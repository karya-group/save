\version "2.18.2"
\language "english"
\pointAndClickOff
\include "ly/lib.ily"
\header { title = "vla2" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\bookpart {
\score {
<<
\new Staff  {
\set Staff.instrumentName = "viola"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 4/4 \key c \major R4*4 | % 1
 R4*4 | % 2
 R4*4 | % 3
 R4*4 | % 4
 R4*4 | % 5
 R4*4 | % 6
 R4*4 | % 7
 R4*4 | % 8
 R4*4 | % 9
 R4*4 | % 10
 R4*4 | % 11
 R4*4 | % 12
 R4*4 | % 13
 R4*4 | % 14
 R4*4 | % 15
 R4*4 | % 16
 R4*4 | % 17
 R4*4 | % 18
 R4*4 | % 19
 R4*4 | % 20
 R4*4 | % 21
 R4*4 | % 22
 R4*4 | % 23
 R4*4 | % 24
<<
  { \voiceOne
   r2 bf'2 | % viola-sonata/vla2 viola-sonata/b2.t38 98-100; 25
} \new Voice { \voiceTwo
   r2 d'2 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 25
} >> \oneVoice
   R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 26
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 27
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 28
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 29
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 30
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 31
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 32
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 33
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 34
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 35
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 36
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 37
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 38
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 39
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 40
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 41
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 42
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 43
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 44
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 45
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 46
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 47
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 48
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 49
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 50
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 51
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 52
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 53
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 54
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 55
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 56
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 57
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 58
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 59
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 60
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 61
 R4*4 | % viola-sonata/vla2 viola-sonata/b2.t41 98-100; 62
<<
  { \voiceOne
   a'4} \new Voice { \voiceTwo
   f'4} >> \oneVoice
   r4 r2 \bar "|."
} }

>>
}
}

