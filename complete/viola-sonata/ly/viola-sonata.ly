\version "2.14.2"
\language "english"
\pointAndClickOff
\header { title = "viola sonata" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\score {
<<
\new Staff  {
\set Staff.instrumentName = "viola"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 6/8 \key c \major \clef alto R8*6 | % 1
  R8*6 | % 2
  R8*6 | % 3
  R8*6 | % 4
  R8*6 | % 5
  R8*6 | % 6
  R8*6 | % 7
  R8*6 | % 8
  R8*6 | % 9
  R8*6 | % 10
  R8*6 | % 11
  R8*6 | % 12
  R8*6 | % 13
  R8*6 | % 14
  R8*6 | % 15
  R8*6 | % 16
  R8*6 | % 17
  R8*6 | % 18
  R8*6 | % 19
  R8*6 | % 20
  R8*6 | % 21
  R8*6 | % 22
  r4. cs8-. \mf <a e'>8-. <a e'>8-. | % viola-sonata/vla1 viola-sonata/vla1.t2 0-.5; 23
  cs8-. <a e'>8-. <a e'>8-. d16( a16) <d' f'>8-. <d' a'>8-. | % viola-sonata/vla1 viola-sonata/vla1.t2 0-.5; 24
  as8-. <f' as'>8-. g8-. <d' g'>8-. ds8-. <as ds'>8-. | % viola-sonata/vla1 viola-sonata/vla1.t2 0-.5; 25
  a16( b16 cs'16 d'16 e'16 f'16 g'4) bf'8 | % viola-sonata/vla1 viola-sonata/vla1.t2 77.5-78; 26
  a'16( gs'16 a'16 e'16 cs'16 d'16) e'16( d'16 e'16 cs'16 a16 as16) | % viola-sonata/vla1 viola-sonata/vla1.t2 0-.25; 27
  a16( e'16 a'16 gs'16 a'16 b'16) cs''8. d''16~ d''8 | % viola-sonata/vla1 viola-sonata/vla1.t2 83.25-84; 28
  cs''4. d''8. \ff f''16~ f''8 | % viola-sonata/vla1 viola-sonata/vla1.t2 86.25-87; 29
  e''4.:32 r4. | % viola-sonata/vla1 viola-sonata/vla1.t2 0-1.5; 30
  <d' a'>2( <d'' a''>4)-\flageolet | % viola-sonata/vla1 viola-sonata/vla1.t2 0-1; 31
  r4. cs8-. \mf <a e'>8-. <a e'>8-. | % viola-sonata/vla1 viola-sonata/vla1.t2 0-.5; 32
  cs8-. <a e'>8-. <a e'>8-. d16( a16) <d' f'>8-. <d' a'>8-. | % viola-sonata/vla1 viola-sonata/vla1.t2 0-.5; 33
  cs16-. a16-. <e' cs''>8-. <e' cs''>8-. \times 4/5 { d''16-. a'16-. f'16-. e'16-. d'16-. } <a f'>8 | % viola-sonata/vla1 viola-sonata/vla1.t2 101.5-102; 34
  a'16( \p bf'16 a'8) c''16( bf'16 a'4) r8 | % viola-sonata/vla1 viola-sonata/vla1.t2 .5-1.43; 35
  a'16( \mf gs'16 a'16 f'16 d'16 e'16) f'16( e'16 f'16 d'16 a16 as16) | % viola-sonata/vla1 viola-sonata/vla1.t2 0-.25; 36
  c'16( a16 fs16 ef'16 d'16 c'16) bf16( a16 bf16 g16 d16 g16) | % viola-sonata/vla1 viola-sonata/vla1.t2 0-.25; 37
  f4( g8) a8( d'8 f'8) | % viola-sonata/vla1 viola-sonata/vla1.t2 0-.5; 38
  af'8( \> g'8 f'8) e'8( d'8 cs'8) \p | % viola-sonata/vla1 viola-sonata/vla1.t2 1-1.5; 39
  d'8-. \f <a f'>8-. <a f'>8-. f8-. <a d'>8-. <a d'>8-. | % viola-sonata/vla1 viola-sonata/vla1.t2 0-.5; 40
  <d' a'>2( <d'' a''>4)-\flageolet | % viola-sonata/vla1 viola-sonata/vla1.t2 0-1; 41
  a'8^"pizz." \fp f'8 \< d'8 a8 f8 d8 | % viola-sonata/vla1 viola-sonata/vla1.t2 0-.5; 42
  cs8 \f a8 \mf a8 cs8 a8 a8 | % viola-sonata/vla1 viola-sonata/vla1.t2 0-.5; 43
  d8 <a f'>8 <a f'>8 c8 <a e'>8 <a e'>8 | % viola-sonata/vla1 viola-sonata/vla1.t2 0-.5; 44
  <f c'>8 <c' af'>8 <c' af'>8 <ef c'>8 <c' g'>8 <c' g'>8 | % viola-sonata/vla1 viola-sonata/vla1.t2 0-.5; 45
  \clef treble <ef' bf'>8 <bf' gf''>8 <bf' gf''>8 bf''8 f''8 df''8 | % viola-sonata/vla1 viola-sonata/vla1.t2 0-.5; 46
  \clef alto e'2.^"arco" \sfz | % viola-sonata/vla1 viola-sonata/vla1.t2 0-3; 47
  a'16( \f gs'16 a'16 f'16 d'16 e'16) f'16( e'16 f'16 d'16 a16 bf16) | % viola-sonata/vla1 viola-sonata/vla1.t2 .25-.5; 48
  c'16( a16 fs16 ef'16 d'16 c'16) bf16( a16 bf16 g16 d16 g16) | % viola-sonata/vla1 viola-sonata/vla1.t2 1.25-1.5; 49
  f4( \mf g8) \< a8 d'8 f'8 | % viola-sonata/vla1 viola-sonata/vla1.t2 149.5-150; 50
  c''8( \f bf'8 a'8) bf'16( a'16 g'8 d'8) | % viola-sonata/vla1 viola-sonata/vla1.t2 1-1.5; 51
  b'16( a'16 b'16 g'16 d'16 g'16) ef'8 g'8 c''8 | % viola-sonata/vla1 viola-sonata/vla1.t2 155.5-156; 52
  ef''8( d''8 c''8) ef''16( d''16 c''16 b'16 c''8) | % viola-sonata/vla1 viola-sonata/vla1.t2 .75-1.25; 53
  a'16( a16) g'16( bf16) gf'16( c'16) ef'16( g16) d'16( a16) c'16( fs16) | % viola-sonata/vla1 viola-sonata/vla1.t2 0-.25; 54
  ef'2 r4 | % viola-sonata/vla1 viola-sonata/vla1.t2 162-164; 55
  d'4. c'4. | % viola-sonata/vla1 viola-sonata/vla1.t2 166.5-168; 56
  bf2. | % viola-sonata/vla1 viola-sonata/vla1.t2 168-171; 57
  \ottava #1 g''4.^"pizz." \ottava #0 <g g'>4. \bar "|." 
} }

\new PianoStaff <<
\set PianoStaff.instrumentName = ""
\set PianoStaff.shortInstrumentName = ""
\new Staff = "up" {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 6/8 \key c \major a''16( \f gs''16 a''16 f''16 d''16 e''16) f''16( e''16 f''16 d''16 a'16 bf'16) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.25; 1
  c''16( a'16 fs'16 ef''16 d''16 c''16) bf'16( a'16 bf'16 g'16 d'16 g'16) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.25; 2
  f'4( g'8) a'8( d''8 f''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.5; 3
  c'''8( bf''8 a''8) bf''16( a''16 g''8 d''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.5; 4
  b''16( a''16 b''16 g''16 d''16 g''16) ef''8( g''8 c'''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.5; 5
  ef'''8( d'''8 c'''8) ef'''16( d'''16 c'''16 b''16 c'''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.5; 6
  d'''16( c'''16 b''8 g''8) d'''16( b''16 g''16 d''16 b'16 g'16) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.25; 7
  c''4~ \mf c''16 a'16( bf'4~ bf'16) g'16 | % viola-sonata/pno1 viola-sonata/pno1.t26 23.75-24; 8
  a'16( \< d''16 cs''16 d''16 a'16 fs'16) e'16( g'16 fs'16 g'16 e'16 c'16) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.25; 9
  d'8 \f b'8 g''8 ef'8 c''8 g''8 | % viola-sonata/pno1 viola-sonata/pno1.t26 29.5-30; 10
  fs''4.( g''4.) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-1.5; 11
  g''16( d''16 bf'16 a'16 bf'16 d''16) bf''16( g''16 d''16 cs''16 d''16 g''16) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.25; 12
  \ottava #1 g'''4 f'''8 ef'''8 d'''8 c'''8 | % viola-sonata/pno1 viola-sonata/pno1.t26 38.5-39; 13
  \ottava #0 bf''8 a''8 g''8 cs'''8( \p a''16 g''16 a''16 e'''16) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.25; 14
  cs'''8( a''16 g''16 a''16 e'''16) f'''8( e'''8 d'''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.5; 15
  c'''8( bf''8 a''8) g''8( f''8 e''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.5; 16
  f''8( e''8 d''8) \stemUp g''8( f''8 e''8) \stemNeutral | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.5; 17
  d''2 a'16( a''16 d''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.5; 18
  f''8 \f e''8 d''8 c''8 bf'8 a'8 | % viola-sonata/pno1 viola-sonata/pno1.t26 56.5-57; 19
  gs'16 \p fs'16 gs'16 b'16 e'16 b'16 gs'16 fs'16 gs'16 b'16 e'16 b'16 | % viola-sonata/pno1 viola-sonata/pno1.t26 59.75-60; 20
  c''16( \< b'16 c''16 a'16 e'16 a'16) c''16( b'16 c''16 a'16 e'16 a'16) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.25; 21
  e''16( ds''16 e''16 c''16 a'16 c''16) <e'' a''>4 \f a''8 \p | % viola-sonata/pno1 viola-sonata/pno1.t26 65.5-66; 22
  bf''16( a''16 g''16 f''16 e''16 d''16) cs''4. | % viola-sonata/pno1 viola-sonata/pno1.t26 67.5-69; 23
  a'4 a'16( bf'16 a'8) c''16( bf'16 a'8) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.5; 24
  f''16( e''16 d''8) g''16( f''16 e''8) bf''16( a''16 g''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.5; 25
  cs'''16( b''16 a''16 g''16 f''16 e''16 ds''4) g''8^"melody" | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.5; 26
  a''8 \acciaccatura { bf''8[ c'''8] } bf''4~ bf''8 \acciaccatura { d'''8[ c'''8] } bf''8. gs''16 | % viola-sonata/pno1 viola-sonata/pno1.t26 80.75-81; 27
  <e'' a''>8 \acciaccatura { f''8[ g''8] } f''4 r8 \times 4/6 { \acciaccatura { a''8[ g''8] } f''16 e''16 d''16 c''16 as'16 gs'16 } | % viola-sonata/pno1 viola-sonata/pno1.t26 81.5-82.5; 28
  a'4. f'8. \ff d'16~ d'8 | % viola-sonata/pno1 viola-sonata/pno1.t26 86.25-87; 29
  \ottava #1 <a'' a''' e''' cs'''>4.:32 r4. | % viola-sonata/pno1 viola-sonata/pno1.t26 0-1.5; 30
  d'''16( cs'''16 d'''16 a''16 f''16 a''16) f'''16( e'''16 f'''16 d'''16 a''16 d'''16) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.25; 31
  \ottava #0 bf''16( \> a''16 g''16 f''16 e''16 d''16 cs''4.) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-1.43; 32
  a'4 \p a'16( bf'16 a'8) c''16( bf'16 a'16 g'16 | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.25; 33
  a'8) c''16( bf'16 a'16 g'16 f'16 g'16 a'4) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.93; 34
  <a' f' cs'' f''>4. r4. | % viola-sonata/pno1 viola-sonata/pno1.t26 102-103.5; 35
  R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t26 102-103.5; 36
  R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t26 102-103.5; 37
  a'16( gs'16 a'16 f'16 d'16 e'16) f'16( e'16 f'8 a16 bf16) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.25; 38
  d'16( \> e'16) af'16( g'16) bf'16( a'16) e''16( d''16) f''16( e''16) bf''16( \p a''16) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.25; 39
  d'''8 \f r4 r4. | % viola-sonata/pno1 viola-sonata/pno1.t26 117-117.5; 40
  \ottava #1 \acciaccatura { d''''8[ a'''8] } d'''16( cs'''16 d'''16 a''16 f''16 a''16) f'''16( e'''16 f'''16 d'''16 a''16 d'''16) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.25; 41
  a'''16 \fp e'''16 \< f'''16 cs'''16 d'''16 gs''16 \ottava #0 a''16 e''16 f''16 cs''16 d''16 a'16 \f | % viola-sonata/pno1 viola-sonata/pno1.t26 125.75-126; 42
  cs''8( \p a''16 g''16 a''16 e'''16) cs'''8( a''16 g''16 a''16 e'''16) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.25; 43
  \ottava #1 f'''8( e'''8 d'''8) e'''8( f'''8 g'''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.5; 44
  af'''8( g'''8 f'''8) ef'''8( d'''8 c'''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.5; 45
  gf'''8( f'''8 ef'''8) df'''8( c'''8 bf''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.5; 46
  <g'' bf'' d'''>4. <a'' cs''' e'''>4. | % viola-sonata/pno1 viola-sonata/pno1.t26 139.5-141; 47
  <f''' a'' d'''>4. \f <d''' f'' a''>4. | % viola-sonata/pno1 viola-sonata/pno1.t26 142.5-144; 48
  <d''' a'' c'''>8. <ef'''~ a''~ c'''~>16 <ef''' a'' c'''>8 <bf'' d''' a''>8. <d'''~ g''~ bf''~>16 <d''' g'' bf''>8 | % viola-sonata/pno1 viola-sonata/pno1.t26 146.25-147; 49
  \ottava #0 <a'' g''>8( <f'' a''>4) <bf'' e''>8( <a'' f''>8) \times 2/3 { d''16 e''16 g''16 } | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.5; 50
  a''8( g''8 fs''8) d''16( ef''16 d''8 g''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.5; 51
  d''16( e''?16 d''8 g''8) c''16( d''16 c''8 g''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.5; 52
  a'16( bf'16 a'8 ef''8) a'16( b'16 a'8 ef''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 0-.5; 53
  a'16 b'16 a'8 ef''8 a'16 c''16 a'8 d''8 | % viola-sonata/pno1 viola-sonata/pno1.t26 161.5-162; 54
  <ef' bf' ef''>2 r4 | % viola-sonata/pno1 viola-sonata/pno1.t26 162-164; 55
  <d' bf' d''>4. <d' a' d''>4. | % viola-sonata/pno1 viola-sonata/pno1.t26 166.5-168; 56
  <bf' d'' g'>2. | % viola-sonata/pno1 viola-sonata/pno1.t26 168-171; 57
  <g''' g'' bf''>8 r4 <g'' bf'' bf' d''>8 r4 \bar "|." 
} }

\new Staff = "down" {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 6/8 \key c \major R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t26 172.5-173; 1
  R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t26 172.5-173; 2
  a'16( gs'16 a'16 f'16 d'16 e'16) f'16( e'16 f'16 d'16 a16 bf16) | % viola-sonata/pno1 viola-sonata/pno1.t2 0-.25; 3
  \clef bass c'16( a16 fs16 ef'16 d'16 c'16) bf16( a16 bf16 g16 d16 g16) | % viola-sonata/pno1 viola-sonata/pno1.t2 0-.25; 4
  f4( g8) ef16( d16 ef16 c16 g,16 c16) | % viola-sonata/pno1 viola-sonata/pno1.t2 0-.25; 5
  g16( fs16 g16 ef16 c16 ef16) c'16( b16 c'16 g16 ef16 g16) | % viola-sonata/pno1 viola-sonata/pno1.t2 0-.25; 6
  g,16( d16 g16 b16 d'16 g'16) g8( b8 c'16 d'16) | % viola-sonata/pno1 viola-sonata/pno1.t2 0-.25; 7
  \clef treble ef'16( g'16 fs'16 g'16 ef'16 c'16) d'16( g'16 fs'16 g'16 d'16 bf16) | % viola-sonata/pno1 viola-sonata/pno1.t2 0-.25; 8
  d'4~ d'16 b16( c'4~ c'16) a16 | % viola-sonata/pno1 viola-sonata/pno1.t2 26.75-27; 9
  b16 d'16 g'16 fs'16 g'16 d'16 c'16 ef'16 g'16 fs'16 g'16 a16 | % viola-sonata/pno1 viola-sonata/pno1.t2 29.75-30; 10
  \clef bass d'16( d16 e16 fs16 g16 a16) \clef treble b16( c'16 d'16 e'16 fs'16 g'16) | % viola-sonata/pno1 viola-sonata/pno1.t2 0-.25; 11
  a'16( bf'16 g'16 d'16 a16 bf16) \clef bass g16( d16 cs16 d16 bf,16 g,16) | % viola-sonata/pno1 viola-sonata/pno1.t2 0-.25; 12
  g4 <f f'>8 <ef ef'>8 <d d'>8 <c c'>8 | % viola-sonata/pno1 viola-sonata/pno1.t2 38.5-39; 13
  <bf, bf>8 <a, a>8 <g, g>8 cs,16 cs16 <a, e>16 bf,16 <a, e>16 <g a>16 | % viola-sonata/pno1 viola-sonata/pno1.t2 41.75-42; 14
  cs,16 cs16 <a, e>16 bf,16 <a, e>16 <g a>16 d,16 d16 <a, f>16 e16 <a, f>16 a16 | % viola-sonata/pno1 viola-sonata/pno1.t2 44.75-45; 15
  g,16 d16 <g d'>16 cs'16 <bf d'>16 g'16 c16 g16 <c' e'>16 b16 <c' g'>16 e'16 | % viola-sonata/pno1 viola-sonata/pno1.t2 47.75-48; 16
  <d' f'>8( <a e'>8 <f d'>8) c16_( g16 c'16 \change Staff = "up" e'16 g'16 c''16) | % viola-sonata/pno1 viola-sonata/pno1.t2 0-.25; 17
  \change Staff = "down" a,16( d16 f16 a16 d'16 f'16) d,16( a,16 d16 f16 a16 d'16) | % viola-sonata/pno1 viola-sonata/pno1.t2 0-.25; 18
  <f f'>8 <e e'>8 <d d'>8 <c c'>8 <bf, bf>8 <a, a>8 | % viola-sonata/pno1 viola-sonata/pno1.t2 56.5-57; 19
  e,16 fs,16 e,16 b,16 <e gs>16 b,16 e16 ds16 e16 b,16 gs,16 b,16 | % viola-sonata/pno1 viola-sonata/pno1.t2 59.75-60; 20
  <c' c>8 <b b,>8 <a, a>8 <g g,>8 <f f,>8 <e, e>8 | % viola-sonata/pno1 viola-sonata/pno1.t2 62.5-63; 21
  <d d,>8 <c c,>8 <b,, b,>8 <a,, a,>4 r8 | % viola-sonata/pno1 viola-sonata/pno1.t2 64.5-65.5; 22
  R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t2 64.5-65.5; 23
  R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t2 64.5-65.5; 24
  R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t2 64.5-65.5; 25
  R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t2 64.5-65.5; 26
  a,8-. <e cs'>8-. <e cs'>8-. a,8-. <f d'>8-. <f d'>8-. | % viola-sonata/pno1 viola-sonata/pno1.t2 0-.5; 27
  a,8-. <e cs'>8-. <e cs'>8-. a,8-. <d' f>8-. <d' f>8-. | % viola-sonata/pno1 viola-sonata/pno1.t2 0-.5; 28
  <e cs' a,>4. <d a f,>8. <a,~ f~ d,~>16 <a, f d,>8 | % viola-sonata/pno1 viola-sonata/pno1.t2 86.25-87; 29
  \ottava #-1 <a,, a,>4. \ottava #0 r4. | % viola-sonata/pno1 viola-sonata/pno1.t2 87-88.5; 30
  d,16 a,16 <a d>16 gs16 <a f>16 d'16 f,16 d16 <f a>16 e16 <d' f>16 a'16 | % viola-sonata/pno1 viola-sonata/pno1.t2 92.75-93; 31
  \clef treble bf'16( a'16 g'16 f'16 e'16 d'16 cs'4.) | % viola-sonata/pno1 viola-sonata/pno1.t2 0-1.43; 32
  \clef bass a4 a16( bf16 a8) c'16( bf16 a16 bf16 | % viola-sonata/pno1 viola-sonata/pno1.t2 0-.25; 33
  a8) c'16( bf16 a16 bf16 a16 bf16 a4) | % viola-sonata/pno1 viola-sonata/pno1.t2 0-.93; 34
  <f, cs a>4. a16 \mf g16 f16 e16 d16 bf,16 | % viola-sonata/pno1 viola-sonata/pno1.t2 104.75-105; 35
  g,16 g16 <bf d'>16 cs'16 <g d'>16 g'16 a,16 d16 <f a>16 e16 <f d'>16 d16 | % viola-sonata/pno1 viola-sonata/pno1.t2 107.75-108; 36
  c16 a16 c'16 b16 c'16 fs'16 bf,16 d16 g16 bf16 d'16 g'16 | % viola-sonata/pno1 viola-sonata/pno1.t2 110.75-111; 37
  d,8-. <f a,>8-. <f a,>8-. <a, f>8-. <a d>8-. d'8-. | % viola-sonata/pno1 viola-sonata/pno1.t2 0-.5; 38
  <b f'>8( <as e'>8 <a d'>8) <g e'>8( <f d'>8 <e cs'>8) | % viola-sonata/pno1 viola-sonata/pno1.t2 0-.5; 39
  d,16 a,16 <d a>16 gs16 <f a>16 d'16 f,16 d16 <a f>16 e16 <d' f>16 a'16 | % viola-sonata/pno1 viola-sonata/pno1.t2 119.75-120; 40
  d,16 a,16 <d a>16 gs16 <f a>16 d'16 f,16 d16 <f a>16 e16 <f d'>16 a'16 | % viola-sonata/pno1 viola-sonata/pno1.t2 122.75-123; 41
  \clef treble a'16( a''16) f'16( f''16) d'16( d''16) \clef bass a16( a'16) f16( f'16) d16( d'16) | % viola-sonata/pno1 viola-sonata/pno1.t2 0-.25; 42
  cs,16 cs16 <a, e>16 bf,16 <a, e>16 <g a>16 cs,16 cs16 <a, e>16 bf,16 <a, e>16 <g a>16 | % viola-sonata/pno1 viola-sonata/pno1.t2 128.75-129; 43
  d,16 d16 <a, f>16 e16 <a, f>16 a16 c,16 c16 <a, e>16 d16 <c e>16 a16 | % viola-sonata/pno1 viola-sonata/pno1.t2 131.75-132; 44
  f,16 f16 <af c'>16 g16 <af f'>16 c'16 c,16 c16 <ef c'>16 d16 <g c'>16 g'16 | % viola-sonata/pno1 viola-sonata/pno1.t2 134.75-135; 45
  ef,16( ef16) df,16( df16) c,16( c16) bf,,16( bf,16 f16 bf16 df'16 f'16) | % viola-sonata/pno1 viola-sonata/pno1.t2 0-.25; 46
  g'16( e'16 d'16 bf16 g16 e16) e'16( cs'16 a16 g16 e16 cs16) | % viola-sonata/pno1 viola-sonata/pno1.t2 0-.25; 47
  <d d'>4. r4. | % viola-sonata/pno1 viola-sonata/pno1.t2 141-142.5; 48
  R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t2 141-142.5; 49
  \clef treble a'16 gs'16 a'16 f'16 d'16 e'16 f'16 e'16 f'16 d'16 a16 bf16 | % viola-sonata/pno1 viola-sonata/pno1.t2 149.75-150; 50
  \clef bass c'16 a16 fs16 ef'16 d'16 c'16 bf16 a16 bf16 g16 d16 g16 | % viola-sonata/pno1 viola-sonata/pno1.t2 152.75-153; 51
  f4( g8) ef16 d16 ef16 c16 g,16 c16 | % viola-sonata/pno1 viola-sonata/pno1.t2 155.75-156; 52
  g16 fs16 g16 ef16 c16 ef16 c'16 b16 c'16 g16 ef16 g16 | % viola-sonata/pno1 viola-sonata/pno1.t2 158.75-159; 53
  ef'16 d'16 ef'16 c'16 g16 c'16 fs'16 e'16 fs'16 d'16 a16 d'16 | % viola-sonata/pno1 viola-sonata/pno1.t2 161.75-162; 54
  <ef g bf df'>2 r4 | % viola-sonata/pno1 viola-sonata/pno1.t2 162-164; 55
  <d g bf>4. <d fs a>4. | % viola-sonata/pno1 viola-sonata/pno1.t2 166.5-168; 56
  g,16( d16 g16 bf,16 d16 bf16) d'16( g16 d'16 \change Staff = "up" bf'16 g'16 d''16) | % viola-sonata/pno1 viola-sonata/pno1.t2 0-.25; 57
  \change Staff = "down" <bf, g, bf d>8 r4 \ottava #-1 <g,, bf,, d, bf,>8 r4 \bar "|." 
} }

>>

>>
\header { piece = "I" }
}

\score {
<<
\new Staff  {
\set Staff.instrumentName = "viola"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 4/4 \key c \major \clef treble e''4^"pizz." \f f'4 d''4 e'4 | % viola-sonata/vla2 viola-sonata/b2.t38 0-1; 1
  cs''4 d'4 b'4 cs'4 | % viola-sonata/vla2 viola-sonata/b2.t38 0-1; 2
  e''4 f'4 f''4 e'4 | % viola-sonata/vla2 viola-sonata/b2.t38 0-1; 3
  c''4 ef'4 bf'4 d'4 | % viola-sonata/vla2 viola-sonata/b2.t38 0-1; 4
  \clef alto c'1~^"arco" \p | % viola-sonata/vla2 viola-sonata/b2.t38 16-20.25; 5
  c'16 bf'16( c'16) a'16( df'16) g'16 r8 c'2 | % viola-sonata/vla2 viola-sonata/b2.t38 22-24; 6
  bf'4( c'4) a'4( bf4) | % viola-sonata/vla2 viola-sonata/b2.t38 0-1; 7
  g'4( a4) gf'4( g4) | % viola-sonata/vla2 viola-sonata/b2.t38 0-1; 8
  a'4-+ \f r8 a'8_~^"nv" \pp <a'_~ bf'>2 | % viola-sonata/vla2 viola-sonata/b2.t41 0-8.5; 9
  <a'_~ b'!^~>2 <a'_~ b'>8 <a'_~ c''~>4. | % viola-sonata/vla2 viola-sonata/b2.t41 0-8.5; 10
  <a' c''>2 <gs' cs''>2^"vib" \mf | % viola-sonata/vla2 viola-sonata/b2.t38 42-44; 11
  b'2 as'2~( | % viola-sonata/vla2 viola-sonata/b2.t38 0-2.5; 12
  as'8 b8) gs'2 as'8( b8) | % viola-sonata/vla2 viola-sonata/b2.t38 0-.5; 13
  gs'8( as8) g'8( af8) f'8( g8) r8 b'8 | % viola-sonata/vla2 viola-sonata/b2.t38 55.5-56; 14
  cs''2 \> ds''2 | % viola-sonata/vla2 viola-sonata/b2.t38 58-60; 15
  e''2~( \p e''8 fs'8) ds''4( | % viola-sonata/vla2 viola-sonata/b2.t38 0-1; 16
  e'8) cs''8( ds'8) bs'8~ bs'4. cs''16(-- ds''16)-- | % viola-sonata/vla2 viola-sonata/b2.t38 .25-.5; 17
  e''4^"pizz." \f fs'4 ds''4 e'4 | % viola-sonata/vla2 viola-sonata/b2.t38 0-1; 18
  cs''4 ds'4 bs'4 cs'4 | % viola-sonata/vla2 viola-sonata/b2.t38 0-1; 19
  e''4 fs'4 ds''4 e'4 | % viola-sonata/vla2 viola-sonata/b2.t38 0-1; 20
  cs''4 ds'4 bs'4 cs'4 | % viola-sonata/vla2 viola-sonata/b2.t38 0-1; 21
  gs2^"arco" \p \< fs4 b8( a8) | % viola-sonata/vla2 viola-sonata/b2.t38 0-.5; 22
  ds8( e8 fs8) <c a>8( df8 gs8 fs8 f8) | % viola-sonata/vla2 viola-sonata/b2.t38 0-.5; 23
  e2 \f <gs e'>4 <cs cs'>4 | % viola-sonata/vla2 viola-sonata/b2.t38 95-96; 24
  <c g>16 \ff <e'~ c''~>8. <e' c''>4 <<
  { \voiceOne
  bf'8( af'8) g'4 | % viola-sonata/vla2 viola-sonata/b2.t38 0-1; 25
  } \new Voice { \voiceTwo
  d'4 bf8( a8) | % viola-sonata/vla2 viola-sonata/b2.t41 0-.5; 25
  } >> \oneVoice
  <c g>16 \mf <g ef'>8. bf'16( af'16 g'8)-. g'16( f'16 ef'8)-. ef'16( df'16 b16 a16) | % viola-sonata/vla2 viola-sonata/b2.t38 0-.25; 26
  g1~ \p | % viola-sonata/vla2 viola-sonata/b2.t38 104-108.25; 27
  g16 ef'16( g16) d'16( a16) df'16 r8 g2 | % viola-sonata/vla2 viola-sonata/b2.t38 110-112; 28
  ef'4( f4) d'4( ef4) | % viola-sonata/vla2 viola-sonata/b2.t38 0-1; 29
  c'4( d4) b4( c4) | % viola-sonata/vla2 viola-sonata/b2.t38 0-1; 30
  a'4-+ \f r8 a'8_~^"nv" \pp <a'_~ bf'>2 | % viola-sonata/vla2 viola-sonata/b2.t41 0-8.5; 31
  <a'_~ b'!^~>2 <a'_~ b'>8 <a'_~ c''~>4. | % viola-sonata/vla2 viola-sonata/b2.t41 0-8.5; 32
  <a' c''>2 <gs' cs''>2^"vib" \mf | % viola-sonata/vla2 viola-sonata/b2.t38 130-132; 33
  b'2 as'2~( | % viola-sonata/vla2 viola-sonata/b2.t38 0-2.5; 34
  as'8 b8) gs'2 as'8( b8) | % viola-sonata/vla2 viola-sonata/b2.t38 0-.5; 35
  gs'8( as8) g'8( af8) f'8( g8) r8 b'8 | % viola-sonata/vla2 viola-sonata/b2.t38 143.5-144; 36
  cs''2 \> ds''2 | % viola-sonata/vla2 viola-sonata/b2.t38 146-148; 37
  e''2 \p ef''2 | % viola-sonata/vla2 viola-sonata/b2.t38 150-152; 38
  df''4 e''8 c''8 af'8 d'8 df'16( \mf b'8.) | % viola-sonata/vla2 viola-sonata/b2.t38 0-.75; 39
  c'16( \> bf'8.) b16( a'8.) r4 c'4~ \p | % viola-sonata/vla2 viola-sonata/b2.t38 159-161; 40
  c'4 g'8.( \mf \> bf'16) gf'8.( a'16) r4 | % viola-sonata/vla2 viola-sonata/b2.t38 0-.25; 41
  c'1 \p | % viola-sonata/vla2 viola-sonata/b2.t38 164-168; 42
  c'4.( b8) c'4.( bf8) | % viola-sonata/vla2 viola-sonata/b2.t38 0-.5; 43
  a2 d8 a4( bf8) | % viola-sonata/vla2 viola-sonata/b2.t38 0-.5; 44
  d8( a8 bf8 c'8) bf8( a4 d8) | % viola-sonata/vla2 viola-sonata/b2.t38 0-.5; 45
  bf8( a4 d8) c'8( bf8) bf'8( a8) | % viola-sonata/vla2 viola-sonata/b2.t38 0-.5; 46
  g16 \< g8. ef'16 ef'8. f16 f8. d'16 d'8. | % viola-sonata/vla2 viola-sonata/b2.t38 187.25-188; 47
  ef16 ef8. c'16 c'8. d16 d8. b16 b8. | % viola-sonata/vla2 viola-sonata/b2.t38 191.25-192; 48
  g16 \f g8. ef'16 ef'8. f16 f8. d'16 d'8. | % viola-sonata/vla2 viola-sonata/b2.t38 195.25-196; 49
  ef16 ef8. c'16 c'8. d16 d8. bf16 bf8. | % viola-sonata/vla2 viola-sonata/b2.t38 199.25-200; 50
  c16 c8. a16 a8. d16 d8. bf16 bf8. | % viola-sonata/vla2 viola-sonata/b2.t38 203.25-204; 51
  ef16 ef8. c'16 c'8 a16 <g~ d'~>2 | % viola-sonata/vla2 viola-sonata/b2.t38 206-212; 52
  <g d'>1 | % viola-sonata/vla2 viola-sonata/b2.t38 206-212; 53
  <g d'>16 <d' a'>8. <c g>16 <g f'>8. <ef g>16 <g ef'>8. <c g>16 <g d'>8. | % viola-sonata/vla2 viola-sonata/b2.t38 215.25-216; 54
  c16( a16 c'16 f'16 \clef treble bf'16 f''16 g''16 c''16) f''8 ef''8 d''8 c''8 | % viola-sonata/vla2 viola-sonata/b2.t38 219.5-220; 55
  d''4 f''8 ef''8 d''2 | % viola-sonata/vla2 viola-sonata/b2.t38 222-224; 56
  \clef alto d'2 \p c'4( d'8 ef'8) | % viola-sonata/vla2 viola-sonata/b2.t38 0-.5; 57
  bf4( a4) bf4( a4) | % viola-sonata/vla2 viola-sonata/b2.t38 0-1; 58
  d2 e4( f8 g8) | % viola-sonata/vla2 viola-sonata/b2.t38 0-.5; 59
  bf8( g'8) a4 b8( f'8) af8( gs'8) | % viola-sonata/vla2 viola-sonata/b2.t38 0-.5; 60
  a'2 bf'8( a'8) c''8( b'8) | % viola-sonata/vla2 viola-sonata/b2.t38 0-.5; 61
  bf'4( a'8 g'8) b8( cs'8 e'8 g'8) | % viola-sonata/vla2 viola-sonata/b2.t38 0-.5; 62
  <<
  { \voiceOne
  a'4. \f r8 bf'8 r8 a'8( g'8) | % viola-sonata/vla2 viola-sonata/b2.t41 0-.5; 63
  f'4 r4 a'4.( bf'8) | % viola-sonata/vla2 viola-sonata/b2.t41 0-.5; 64
  d''4.( cs''8) } \new Voice { \voiceTwo
  f'32( g'32 f'32 e'32 f'4 e'8) d'8( f'8) df'4 | % viola-sonata/vla2 viola-sonata/b2.t38 0-1; 63
  bf8( g8) a8( bf8) f'8( e'8 g'4~ | % viola-sonata/vla2 viola-sonata/b2.t38 1-2.5; 64
  g'8) e'8( f'4) } >> \oneVoice
  <d'~ d''~>2 \> | \time 2/4 % viola-sonata/vla2 viola-sonata/b2.t38 258-262; 65
  <d' d''>2 | \time 4/4 % viola-sonata/vla2 viola-sonata/b2.t38 258-262; 66
  <c g>16 \f <e'~ c''~>8. <e' c''>4 <<
  { \voiceOne
  bf'8( af'8) g'4 | % viola-sonata/vla2 viola-sonata/b2.t38 0-1; 67
  } \new Voice { \voiceTwo
  d'4 bf8( a8) | % viola-sonata/vla2 viola-sonata/b2.t41 0-.5; 67
  } >> \oneVoice
  <c g>16 <g ef'>8. bf'16( af'16 g'8)-. g'16( f'16 ef'8)-. ef'16( df'16 b16 a16) | % viola-sonata/vla2 viola-sonata/b2.t38 0-.25; 68
  <c g>16 <g ef'>8. <g ef'>16 <ef' c''>8. <c g>16 <g ef'>8. <g ef'>16 <ef' c''>8. | % viola-sonata/vla2 viola-sonata/b2.t38 273.25-274; 69
  \clef treble ef'16 g'16 c''16 g'16 ef''16 c''16 g'16 g''16 ef''16 c''16 g'16 ef''16 c''16 g'16 ef'16 af'16 | % viola-sonata/vla2 viola-sonata/b2.t38 277.75-278; 70
  <b af'>16 <af' ef''>8. af''16( g''16 f''8)-. f''16( ef''16 d''8)-. d''16( c''16 b'16 a'16) | % viola-sonata/vla2 viola-sonata/b2.t38 0-.25; 71
  af'4-- \ff g'4-- f'4-- ef'4-- | % viola-sonata/vla2 viola-sonata/b2.t38 3-4; 72
  c'8 g'8 d''8 g''8 c'''4-- b''4-- | % viola-sonata/vla2 viola-sonata/b2.t38 1-2; 73
  bf''4-- a''4-- af''4-- g''4-- | % viola-sonata/vla2 viola-sonata/b2.t38 5-6; 74
  f'8 af'8 c''8 f''8 g''4-- gf''4-- | % viola-sonata/vla2 viola-sonata/b2.t38 1-2; 75
  f''4-- e''4-- ef''4-- d''4-- | % viola-sonata/vla2 viola-sonata/b2.t38 5-6; 76
  c''4-. \clef alto <g c>16( <g ef'>8.)-. c4-. r4 \bar "|." 
} }

\new PianoStaff <<
\set PianoStaff.instrumentName = ""
\set PianoStaff.shortInstrumentName = ""
\new Staff = "up" {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 4/4 \key c \major e''16 \mf e''8.-. f'16 f'8.-. d''16 d''8.-. e'16 e'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 3.25-4; 1
  cs''16 cs''8.-. d'16 d'8.-. b'16 b'8.-. cs'16 cs'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 7.25-8; 2
  e''16 e''8.-. f'16 f'8.-. d''16 d''8.-. e'16 e'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 11.25-12; 3
  bf'16 bf'8.-. c'16 c'8.-. a'16 a'8.-. bf16 bf8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 15.25-16; 4
  a1~ \p | % viola-sonata/pno2 viola-sonata/b2.t23 16-22; 5
  a2 a2~ | % viola-sonata/pno2 viola-sonata/b2.t23 22-29; 6
  a1~ | % viola-sonata/pno2 viola-sonata/b2.t23 22-29; 7
  a4 c'''16( bf''16 e''16 f''16) a''4~( a''8. bf''16) | % viola-sonata/pno2 viola-sonata/b2.t23 0-.25; 8
  a''2 r16 g''16( c''16 df''16 f''4~ | % viola-sonata/pno2 viola-sonata/b2.t23 0-2.25; 9
  f''4~ f''16) e''16( b'16 c''16 ef''4~ ef''16) d''16 a'16 e'16 | % viola-sonata/pno2 viola-sonata/b2.t23 39.75-40; 10
  cs'16 f'16 a'16 fs''16~ fs''4 \arpeggioArrowUp<as' fs'' cs'''>2\arpeggio \mf | % viola-sonata/pno2 viola-sonata/b2.t23 0-2; 11
  <b' ds'' g''>2 <b'~ ds''~ fs''~>2 | % viola-sonata/pno2 viola-sonata/b2.t23 46-51; 12
  <b' ds'' fs''>2. \times 2/3 { as''16( gs''16 ds''16 } fs''8~ | % viola-sonata/pno2 viola-sonata/b2.t23 51.5-53; 13
  fs''4) \times 2/3 { e''16( ds''16 g'16 } cs''8~ cs''4.) b'8 | % viola-sonata/pno2 viola-sonata/b2.t23 55.5-56; 14
  as'2 \> fs'2 | % viola-sonata/pno2 viola-sonata/b2.t23 58-60; 15
  gs'1~ \p | % viola-sonata/pno2 viola-sonata/b2.t23 60-68; 16
  gs'1 | % viola-sonata/pno2 viola-sonata/b2.t23 60-68; 17
  e''16 \mf e''8.-. fs'16 fs'8.-. ds''16 ds''8.-. e'16 e'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 71.25-72; 18
  cs''16 cs''8.-. ds'16 ds'8.-. c''16 c''8.-. cs'16 cs'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 75.25-76; 19
  e''16 e''8.-. fs'16 fs'8.-. ds''16 ds''8.-. e'16 e'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 79.25-80; 20
  cs''16 cs''8.-. ds'16 ds'8.-. c''16 c''8.-. cs'16 cs'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 83.25-84; 21
  e''16 \< e''8.-. fs'16 fs'8.-. ds''16 ds''8.-. e'16 e'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 87.25-88; 22
  cs''16 \mf cs''8.-. ds'16 ds'8.-. <a' c''>16 <a' c''>8.-. r16 cs''16 c'''16 f''16 | % viola-sonata/pno2 viola-sonata/b2.t23 91.75-92; 23
  \times 4/6 { cs'''16( \f e''16 cs''16 \change Staff = "down" c''16 e'16 cs'16) } \times 4/5 { \change Staff = "up" e''16( cs''16 bs'16 gs'16 e'16) } r4 \clef bass \times 2/3 { e,16( cs16 gs,16 } cs,8) | % viola-sonata/pno2 viola-sonata/b2.t23 95.5-96; 24
  \clef treble <c'' g'' d'''>2. \ff <g'' ef'''>8 <fs'' d'''>8 | % viola-sonata/pno2 viola-sonata/b2.t23 99.5-100; 25
  <f''! df'''>2 \mf \arpeggioArrowUp<ef'' gf'' b''>2\arpeggio | % viola-sonata/pno2 viola-sonata/b2.t23 0-2; 26
  <bf~ g'~>1 \p | % viola-sonata/pno2 viola-sonata/b2.t23 104-110; 27
  <bf g'>2 <bf~ g'~>2 | % viola-sonata/pno2 viola-sonata/b2.t23 110-117; 28
  <bf~ g'~>1 | % viola-sonata/pno2 viola-sonata/b2.t23 110-117; 29
  <bf g'>4 as''16( gs''16 d''16 ds''16 fs''4~ fs''8. g''16) | % viola-sonata/pno2 viola-sonata/b2.t23 0-.25; 30
  a''2 r16 g''16( c''16 df''16 f''4~ | % viola-sonata/pno2 viola-sonata/b2.t23 0-2.25; 31
  f''4~ f''16) e''16( b'16 c''16 ef''4~ ef''16) d''16 a'16 e'16 | % viola-sonata/pno2 viola-sonata/b2.t23 127.75-128; 32
  cs'16 f'16 a'16 fs''16~ fs''4 \arpeggioArrowUp<as' fs'' cs'''>2\arpeggio \mf | % viola-sonata/pno2 viola-sonata/b2.t23 0-2; 33
  <b' ds'' g''>2 <b'~ ds''~ fs''~>2 | % viola-sonata/pno2 viola-sonata/b2.t23 134-139; 34
  <b' ds'' fs''>2. \times 2/3 { as''16( gs''16 ds''16 } fs''8~ | % viola-sonata/pno2 viola-sonata/b2.t23 139.5-141; 35
  fs''4) \times 2/3 { e''16( ds''16 g'16 } cs''8~ cs''4) r8 b'8 | % viola-sonata/pno2 viola-sonata/b2.t23 143.5-144; 36
  as'2 \> fs'2 \! | % viola-sonata/pno2 viola-sonata/b2.t23 146-148; 37
  <<
  { \voiceOne
  r4 \p \times 2/3 { e'''16( ds'''16 as''16 } d'''8~ d'''4) \times 2/3 { cs'''16( b''16 g''16 } as''8) | % viola-sonata/pno2 viola-sonata/b2.t23 0-.5; 38
  } \new Voice { \voiceTwo
  gs'1 | % viola-sonata/pno2 viola-sonata/b2.t26 0-4; 38
  } >> \oneVoice
  \times 4/6 { b''16( as''16 e''16 a''16 gs''16 cs''16) } g''8 e''8 c''8 f'8 gs'8.( \mf \> b'16) | % viola-sonata/pno2 viola-sonata/b2.t23 0-.25; 39
  g'8.( bf'16) fs'8.( a'16) r4 a4~ \p | % viola-sonata/pno2 viola-sonata/b2.t23 159-161; 40
  a4 c'16( \mf e''16 \> bf'16 g'16) b16( d''16 af'16 d'16) r4 | % viola-sonata/pno2 viola-sonata/b2.t23 0-.25; 41
  a4( \p g'4) f'4( g'8 af'8) | % viola-sonata/pno2 viola-sonata/b2.t23 0-.5; 42
  ef'8( ef''8 d'4) ef'8( ef''8 df'8 b8) | % viola-sonata/pno2 viola-sonata/b2.t23 0-.5; 43
  c'16 c'8.-. a'16 a'8.-. bf16 bf8.-. g'16 g'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 175.25-176; 44
  a16 a8.-. gf'16 gf'8.-. c'16 c'8.-. a'16 a'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 179.25-180; 45
  bf16 bf8.-. g'16 g'8.-. a16 a8.-. gf'16 gf'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 183.25-184; 46
  \clef bass g16 \< g8. ef'16 ef'8. f16 f8. d'16 d'8. | % viola-sonata/pno2 viola-sonata/b2.t23 187.25-188; 47
  ef16 ef8. c'16 c'8. d16 d8. b16 b8. | % viola-sonata/pno2 viola-sonata/b2.t23 191.25-192; 48
  \clef treble g'16( \f d''16 a''16 g''16 d'''16 g''16 a''16 c''16) g''16( d''16 f''16 a''16 bf'16 g''16 a'16 fs''16) | % viola-sonata/pno2 viola-sonata/b2.t23 0-.25; 49
  g'16( ef''16 a''16 g''16 d'''16 ef''16 a''16 g''16) ef''16( d'''16 f''16 c'''16 ef''16 bf''16 d''16 a''16) | % viola-sonata/pno2 viola-sonata/b2.t23 0-.25; 50
  g'16( d''16 a''16 g''16 d'''16 f''16 a''16 g''16) \ottava #1 g'''16( d'''16 c'''16 f'''16 bf''16 f'''16 a''16 f'''16) | % viola-sonata/pno2 viola-sonata/b2.t23 0-.25; 51
  g''16( bf''16 g'''16 f'''16 bf'''16 ef'''16 d'''16 a'''16) g'''8( as''8) f'''8( a''8) | % viola-sonata/pno2 viola-sonata/b2.t23 0-.5; 52
  ef'''8( g''8) d'''8( f''8) c'''8( ef''8) bf''8( d''8) | % viola-sonata/pno2 viola-sonata/b2.t23 0-.5; 53
  \ottava #0 g''8( bf'8) f''8( a'8) ef''8( g'8) d''8( f'8) | % viola-sonata/pno2 viola-sonata/b2.t23 0-.5; 54
  c''8( ef'8) bf'8( d'8) a'8( c'8) g'8( bf8) | % viola-sonata/pno2 viola-sonata/b2.t23 0-.5; 55
  <a g'>4 a'8( c'8) <g' g''>16( ef''16 bf'16 g'16 ef'16 c'16 g16 ef16) | % viola-sonata/pno2 viola-sonata/b2.t23 0-.25; 56
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t23 0-.25; 57
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t23 0-.25; 58
  e''16 \p e''8.-. f'16 f'8.-. d''16 d''8.-. e'16 e'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 235.25-236; 59
  df''16 df''8.-. d'16 d'8.-. b'16 b'8.-. df'16 df'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 239.25-240; 60
  e''16 e''8.-. f'16 f'8.-. d''16 d''8.-. e'16 e'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 243.25-244; 61
  df''16 df''8.-. d'16 d'8.-. b'16 b'8.-. df'16 df'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 247.25-248; 62
  e''16 \f e''8.-. f'16 f'8.-. d''16 d''8.-. e'16 e'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 251.25-252; 63
  df''16 df''8.-. d'16 d'8.-. b'16 b'8.-. df'16 df'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 255.25-256; 64
  d''16 d''8.-. f'16 f'8.-. a'16 a'8.-. a16 a8.-. | \time 2/4 % viola-sonata/pno2 viola-sonata/b2.t23 259.25-260; 65
  f'16 f'8.-. r4 | \time 4/4 % viola-sonata/pno2 viola-sonata/b2.t23 260.25-261; 66
  <c'' g'' d'''>2. <g'' ef'''>8( <d''' gf''>8) | % viola-sonata/pno2 viola-sonata/b2.t23 0-.5; 67
  <f'' df'''>2 \arpeggioArrowUp<ds'' fs'' b''>2\arpeggio | % viola-sonata/pno2 viola-sonata/b2.t23 0-2; 68
  \clef bass c,16( g,16 d16 c16 g16 c16 d16 c'16) f16( g16 ef'16 f16 d'16 ef16 c'16 d16) | % viola-sonata/pno2 viola-sonata/b2.t23 0-.25; 69
  c16( g16 d'16 c'16 \clef treble g'16 d'16 c''16 ef'16) g'16( ef''16 f'16 d''16 ef'16 c''16 d'16 b'16) | % viola-sonata/pno2 viola-sonata/b2.t23 0-.25; 70
  <ef' b'>2 \arpeggioArrowUp<c'' ef'' af'' c'''>2\arpeggio | % viola-sonata/pno2 viola-sonata/b2.t23 0-2; 71
  <b af'>4 \ff <c' g'>4 <af c' f'>4 <g c' g'>4 | % viola-sonata/pno2 viola-sonata/b2.t23 285-286; 72
  <c' f' a'>4 <d' f' b'>4 <ef' g' c''>4 <d' g' b'>4 | % viola-sonata/pno2 viola-sonata/b2.t23 289-290; 73
  <d' g' bf'>4 <c' f' a'>4 <c' f' af'>4 <c' ef' g'>4 | % viola-sonata/pno2 viola-sonata/b2.t23 293-294; 74
  <c' d' f'>4 <c' ef'>4 <b d'>4 <b ef'>4 | % viola-sonata/pno2 viola-sonata/b2.t23 297-298; 75
  <c' f'>4 <c' g'>4 <c' a'>4 <d' b'>4 | % viola-sonata/pno2 viola-sonata/b2.t23 301-302; 76
  <ef' c''>4-. <g g'>4-. g4-. r4 \bar "|." 
} }

\new Staff = "down" {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 4/4 \key c \major \clef bass R4*4 | % viola-sonata/pno2 viola-sonata/b2.t23 304-305; 1
  r2 r8 \ottava #-1 \times 2/3 { d,,16( cs,16 f,,16 } d,,8) \ottava #0 r8 | % viola-sonata/pno2 viola-sonata/b2.t9 7-7.5; 2
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 7-7.5; 3
  r2 r8 \ottava #-1 \times 2/3 { d,,16( d,16 a,,16 } d,,8) \ottava #0 r8 | % viola-sonata/pno2 viola-sonata/b2.t9 15-15.5; 4
  \ottava #-1 <d,,~ fs,~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 16-22; 5
  <d,, fs,>2 <d,,~ fs,~>2 | % viola-sonata/pno2 viola-sonata/b2.t9 22-32; 6
  <d,,~ fs,~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 22-32; 7
  <d,, fs,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 22-32; 8
  \ottava #0 R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 22-32; 9
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 22-32; 10
  r2 \arpeggioArrowUp<ds as f'>2\arpeggio | % viola-sonata/pno2 viola-sonata/b2.t9 0-2; 11
  <b, fs cs'>2 <gs,~ ds~ as~>2 | % viola-sonata/pno2 viola-sonata/b2.t9 46-52; 12
  <gs, ds as>1 | % viola-sonata/pno2 viola-sonata/b2.t9 46-52; 13
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 46-52; 14
  <cs' b, fs>2 <fs as gs, ds>2 | % viola-sonata/pno2 viola-sonata/b2.t9 58-60; 15
  <ds~ cs,~ gs,~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 60-68; 16
  <ds cs, gs,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 60-68; 17
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 60-68; 18
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 60-68; 19
  \times 2/3 { cs,16( cs16 e,16 } cs,8) r4 gs2 | % viola-sonata/pno2 viola-sonata/b2.t9 78-80; 20
  fs4( gs8 a8) e8( e'8 ds4) | % viola-sonata/pno2 viola-sonata/b2.t9 0-.93; 21
  \times 2/3 { cs16( cs'16 e16 } cs8) \times 2/3 { gs16( ds'16 bs16 } fs8) \times 2/3 { e16( cs'16 gs16 } ds8) <g b>8( <f a>8) | % viola-sonata/pno2 viola-sonata/b2.t9 0-.5; 22
  <ds fs>8( <e gs>8 <fs a>8 c'8 df'4) df'16 df'8.-. | % viola-sonata/pno2 viola-sonata/b2.t9 91.25-92; 23
  \clef treble r2 \clef bass \times 4/6 { cs'16( gs16 cs16 cs'16 gs16 cs16) } r4 | % viola-sonata/pno2 viola-sonata/b2.t9 91.25-92; 24
  <c, g, e>1 | % viola-sonata/pno2 viola-sonata/b2.t9 96-100; 25
  <c, g, ef>1 | % viola-sonata/pno2 viola-sonata/b2.t9 100-104; 26
  <c,~ e~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 104-110; 27
  <c, e>2 <c,~ e~>2 | % viola-sonata/pno2 viola-sonata/b2.t9 110-120; 28
  <c,~ e~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 110-120; 29
  <c, e>1 | % viola-sonata/pno2 viola-sonata/b2.t9 110-120; 30
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 110-120; 31
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 110-120; 32
  r2 \arpeggioArrowUp<ds as f'>2\arpeggio | % viola-sonata/pno2 viola-sonata/b2.t9 0-2; 33
  <b, fs cs'>2 <gs,~ ds~ as~>2 | % viola-sonata/pno2 viola-sonata/b2.t9 134-140; 34
  <gs, ds as>1 | % viola-sonata/pno2 viola-sonata/b2.t9 134-140; 35
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 134-140; 36
  <cs' b, fs>2 <fs as gs, ds>2 | % viola-sonata/pno2 viola-sonata/b2.t9 146-148; 37
  <gs, ds cs,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 148-152; 38
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 148-152; 39
  r2 r4 \ottava #-1 <fs,~ d,,~>4 | % viola-sonata/pno2 viola-sonata/b2.t9 159-163; 40
  <fs, d,,>2. r4 | % viola-sonata/pno2 viola-sonata/b2.t9 159-163; 41
  <fs,~ d,,~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 164-172; 42
  <fs, d,,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 164-172; 43
  <g,~ g,,~ d,~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 172-184; 44
  <g,~ g,,~ d,~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 172-184; 45
  <g, g,, d,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 172-184; 46
  <g,,~ g,~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 184-192; 47
  <g,, g,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 184-192; 48
  \ottava #0 g16 g8. ef'16 ef'8. f16 f8. d'16 d'8. | % viola-sonata/pno2 viola-sonata/b2.t9 195.25-196; 49
  ef16 ef8. c'16 c'8. d16 d8. bf16 bf8. | % viola-sonata/pno2 viola-sonata/b2.t9 199.25-200; 50
  c16 c8. a16 a8. bf,16 bf,8. g16 g8. | % viola-sonata/pno2 viola-sonata/b2.t9 203.25-204; 51
  c16 c8. a8. \ottava #-1 a,16 g,2~ | % viola-sonata/pno2 viola-sonata/b2.t9 206-212; 52
  <g, f,, c,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 206-212; 53
  g,2~ <g, f,, c,>2 | % viola-sonata/pno2 viola-sonata/b2.t9 212-216; 54
  <f, c as,,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 216-220; 55
  <d, g, g,,>4 <c,, c,>4 <g,, d, g,>2 | % viola-sonata/pno2 viola-sonata/b2.t9 222-224; 56
  \ottava #0 <g,~ d~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 224-232; 57
  <g, d>1 | % viola-sonata/pno2 viola-sonata/b2.t9 224-232; 58
  \ottava #-1 <d,~ d,,~ a,,~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 232-240; 59
  <d, d,, a,,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 232-240; 60
  \ottava #0 R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 232-240; 61
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 232-240; 62
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 232-240; 63
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 232-240; 64
  R4*4 | \time 2/4 % viola-sonata/pno2 viola-sonata/b2.t9 232-240; 65
  r4 d16 d8.-. | \time 4/4 % viola-sonata/pno2 viola-sonata/b2.t9 261.25-262; 66
  <c, g, e>1 | % viola-sonata/pno2 viola-sonata/b2.t9 262-266; 67
  <g, ds c,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 266-270; 68
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 266-270; 69
  R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 266-270; 70
  \ottava #-1 <af,, ef, b,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 278-282; 71
  \ottava #0 <b, ef af af,>4 <c ef g g,>4 <f, c f>4 <ef, c>4 | % viola-sonata/pno2 viola-sonata/b2.t9 285-286; 72
  <d, d>4 <g, d>4 <ef, c>4 <g, d>4 | % viola-sonata/pno2 viola-sonata/b2.t9 289-290; 73
  <bf, g>4 <c a>4 <ef af>4 <ef g>4 | % viola-sonata/pno2 viola-sonata/b2.t9 293-294; 74
  f4 fs4 g4 gf4 | % viola-sonata/pno2 viola-sonata/b2.t9 297-298; 75
  <af f>4 e4 ef4 <g d>4 | % viola-sonata/pno2 viola-sonata/b2.t9 301-302; 76
  <g ef' c>4-. <g, ef c,>4-. \ottava #-1 <c,, c,>4-. \ottava #0 r4 \bar "|." 
} }

>>

>>
\header { piece = "II" }
}

\score {
<<
\new Staff  {
\set Staff.instrumentName = "viola"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 4/4 \key d \major \clef treble d'8-. e'8-. fs'8-. g'8-. a'4 d'4 | % viola-sonata/vla3-1 viola-sonata/b2.t76 3-4; 1
  b'4 a'8-. gs'8-. a'8 fs'8 a'8 d''8 | % viola-sonata/vla3-1 viola-sonata/b2.t76 7.5-8; 2
  g''4. fs''8 g''2 | % viola-sonata/vla3-1 viola-sonata/b2.t76 10-12; 3
  g''4. e''8 \acciaccatura { g''8 } fs''8 e''8 d''8 e''8 | % viola-sonata/vla3-1 viola-sonata/b2.t76 15.5-16; 4
  d''8( e''16 d''16) c''8( f''16 e''16) d''8( c''16 d''16) e''16-. d''16-. c''16-. bf'16-. | % viola-sonata/vla3-1 viola-sonata/b2.t76 0-.25; 5
  \clef alto a'16 d''8. af'8^"pizz." f'8 d'8 af8 f8 d8 | % viola-sonata/vla3-1 viola-sonata/b2.t76 0-.5; 6
  f'8 d''8 ef''16(^"arco" d''8) ef''16( d''8) f8-^ d'8 ef'16( d'16~ | \time 3/4 % viola-sonata/vla3-1 viola-sonata/b2.t76 0-.5; 7
  d'16) ef'16( d'8) cs'16-. cs'16-. cs''16-. cs'16~ cs'16 cs'16-. cs''16-. cs'16 | \time 4/4 % viola-sonata/vla3-1 viola-sonata/b2.t76 30.75-31; 8
  a'8 d'16 a'16~ a'16 d'16 a'8 bf'16 a'16 d'16 ef'16 d'8 ef'16 d'16 | % viola-sonata/vla3-1 viola-sonata/b2.t76 34.75-35; 9
  g'8 d'16 a'16~ a'16 d'16 g'8 bf'16 a'16 d'16 ef'16 d'8 c'16 d'16 | \time 2/4 % viola-sonata/vla3-1 viola-sonata/b2.t76 38.75-39; 10
  r8 <ef g>8^"pizz." <ef g>8 <ef g>8 | \time 4/4 % viola-sonata/vla3-1 viola-sonata/b2.t76 0-.5; 11
  d'8 e'8 fs'8 g'8 a'4 d'4 | % viola-sonata/vla3-1 viola-sonata/b2.t76 0-1; 12
  b'4 a'8 gs'8 a'8 fs'8(^"arco" a'8 d''8) | % viola-sonata/vla3-1 viola-sonata/b2.t76 0-.5; 13
  \clef treble g''4 fs''8( e''8) d''8 fs'8( a'8 d''8) | % viola-sonata/vla3-1 viola-sonata/b2.t76 1-1.5; 14
  g''4 fs''8( e''8) d''4. e''8 | % viola-sonata/vla3-1 viola-sonata/b2.t76 56.5-57; 15
  d''8 e''16( d''16) c''8 f''16( e''16) d''8 c''16( d''16) e''16-. d''16-. c''16-. as'16-. | % viola-sonata/vla3-1 viola-sonata/b2.t76 0-.25; 16
  f''8 d''16( f''16) e''8 c''16( e''16) f''8 d''16( bf'16) af'16 c''16 ef''16 af'16 | % viola-sonata/vla3-1 viola-sonata/b2.t76 64.75-65; 17
  f'8 f'32( gf'32 f'32 gf'32 f'32 gf'32 f'16) bf'16( f''16) f'8 c''16( e''16) e'8 c''16( e''16) | % viola-sonata/vla3-1 viola-sonata/b2.t76 0-.25; 18
  \clef alto d'8 e'8 fs'8 g'8 a'4 d'4 | % viola-sonata/vla3-1 viola-sonata/b2.t76 72-73; 19
  g'8 a'8 bf'8 c''8 d''4 g'4 | % viola-sonata/vla3-1 viola-sonata/b2.t76 76-77; 20
  \clef treble ef''4 d''8( c''8) d''8 g'8( bf'8 d''8) | % viola-sonata/vla3-1 viola-sonata/b2.t76 0-.5; 21
  f''4 ef''4 d''4 cs''4 | % viola-sonata/vla3-1 viola-sonata/b2.t76 84-85; 22
  c''8-. bf'8-. a'8-. g'8-. fs'4-. \acciaccatura { cs''8 } d''4-. | % viola-sonata/vla3-1 viola-sonata/b2.t76 0-1; 23
  \clef alto ef'2 d'4 c'4 | % viola-sonata/vla3-1 viola-sonata/b2.t76 92-93; 24
  bf4 a4 g2 | % viola-sonata/vla3-1 viola-sonata/b2.t76 95-97; 25
  ef'8 f'16( ef'16) d'8-. c'8-. d'8 ef'16( d'16) c'8-. bf8-. | % viola-sonata/vla3-1 viola-sonata/b2.t76 0-.5; 26
  c'8 d'16( c'16) bf8-. a8-. g2 | % viola-sonata/vla3-1 viola-sonata/b2.t76 103-105; 27
  d'8 ef'16( d'16) c'8-. bf8-. c'8 d'16( c'16) bf8-. a8-. | % viola-sonata/vla3-1 viola-sonata/b2.t76 0-.5; 28
  g2 g8-. a8-. b8-. c'8-. | % viola-sonata/vla3-1 viola-sonata/b2.t76 0-.5; 29
  d'4 g4 f'4 e'4 | % viola-sonata/vla3-1 viola-sonata/b2.t76 116-117; 30
  ef'4 d'4 c'4 bf4 | % viola-sonata/vla3-1 viola-sonata/b2.t76 120-121; 31
  a2~ a8 a4 a8 | % viola-sonata/vla3-1 viola-sonata/b2.t76 124.5-125; 32
  af2~ af8 af4 af8 | % viola-sonata/vla3-1 viola-sonata/b2.t76 128.5-129; 33
  f'16 f''8 f'16 f''8 f'16 f''16 c''16 c'16 c''8 c'16 c''8 c'16 | % viola-sonata/vla3-1 viola-sonata/b2.t76 132.75-133; 34
  af'16 c'16 f'16 af'16 c'16 af16 c'16 f16 af16 f16 <f' c''>4. | \time 1/4 % viola-sonata/vla3-1 viola-sonata/b2.t76 135.5-137; 35
  g'4 | \time 4/4 % viola-sonata/vla3-1 viola-sonata/b2.t76 137-138; 36
  \key af \major af8-. bf8-. c'8-. df'8-. ef'4 af4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 3-4; 37
  f'4 ef'8-. d'8-. ef'4.( f'8) | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 0-.5; 38
  gf'4 f'8( ef'8) df'4( ef'4) | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 0-1; 39
  ff'4 ef'4 ff'4 ef'4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 15-16; 40
  ef'8^"pizz." af'8 ef'8 af'8 ef'8 af'8 ef'8 af'8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 0-.5; 41
  ef'8 af'8 ef'8 af'8 e'8 g'8 e'8 g'8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 0-.5; 42
  ef'8 ef''8 ef''8 ef'8 ef''8 ef''8 d'8 ef'8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 0-.5; 43
  r8 d'8 ef'8 d'8 ef'8 e'8 r8 d'8 | \time 3/4 % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 0-.5; 44
  ef'8 d'8 ef'8 df'8 cf'8 bf8 | \time 4/4 % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 0-.5; 45
  af8 bf8 c'8 df'8 ef'4 af4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 0-1; 46
  d'4 c'8 bf8 c'4 ef8 f8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 0-.5; 47
  gf4 gf'4 gf4 gf'4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 0-1; 48
  ef8 ff8 b8 ef'8 \clef treble e'8 as'8 b'8 e''8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 0-.5; 49
  af''8 df''8 gf'8 df'8 \clef alto bf8 gf8 df8 gf8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 0-.5; 50
  <d b>16-.^"arco" <d b>16-. <d b>16-. <d b>16-. <d b>8-. r8 <bf ef'>16 <bf ef'>16 <bf ef'>16 <bf ef'>16 <ef' bf'>8-. r8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 58-58.5; 51
  r2 d'4( a4) | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 0-1; 52
  af'4 gf'8( f'8) gf'8( d'8 a4) | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 0-1; 53
  \clef treble df''8( ef''8) gf''16( ff''16 ef''16 df''16) ef''8( df''16 cf''16) bff'8-. \clef alto af'8~( | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 0-1; 54
  af'8 gf'16 ff'16) gf'8( ff'16 ef'16) ff'8( ef'16 df'16) b8 b8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 74.5-75; 55
  \key c \major a4 r4 c'8-. d'8-. e'8-. f'8-. | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 0-.5; 56
  g'4 c'4 a'4 g'8( fs'8) | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 0-.5; 57
  g'8 e'8( g'8 a'8) bf'8( a'16 g'16) f'8-. ef'8-. | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 0-.5; 58
  df'32 e'32 a'32 r32 a'32 e'32 df'32 r32 df'32 e'32 a'32 r32 a'32 e'32 df'32 r32 c'32 e'32 a'32 r32 a'32 e'32 c'32 r32 c'32 e'32 a'32 r32 a'32 e'32 c'32 r32 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 .62-.75; 59
  df'32 e'32 a'32 r32 a'32 e'32 df'32 r32 df'32 e'32 a'32 r32 a'32 e'32 df'32 r32 c'32 e'32 a'32 r32 a'32 e'32 c'32 r32 c'32 e'32 a'32 r32 a'32 e'32 c'32 r32 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 .62-.75; 60
  b4 c'4 d'4 e'4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 98-99; 61
  f'4 gs'4 a'4 b'4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 102-103; 62
  cs''8 b'16( a'16) g'8-. f'8-. g'8( f'8) ds'8( e'8) | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 0-.5; 63
  ef'32 a'32 c''32 r32 c''32 a'32 ef'32 r32 ef'32 a'32 c''32 r32 c''32 a'32 ef'32 r32 \times 4/6 { a8 e'8 a'8 a'8 e'8 a8 } | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 .62-.75; 64
  \times 4/6 { d'8 a'8 d''8 d''8 a'8 d'8 } \times 4/6 { d'8 a'8 d''8 d''8 a'8 d'8 } | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 .62-.75; 65
  d'16 d''16 a'16 d'16 d''16 a'16 d'16 d''16 a'16 d'16 d''16 a'16 d'16 d''16 a'16 d'16 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 118.75-119; 66
  b'16 af'16 f'16 e'16 d'16 b16 af16 f16 e16 a16 cs'16 e'16 a'16 cs''16 e''16 cs''16 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 122.75-123; 67
  \clef treble d''8-. e''8-. fs''8-. g''8-. a''4 d''4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 126-127; 68
  b'4 a'8-. gs'8-. a'8 fs'8 a'8 d''8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 130.5-131; 69
  g''4.( fs''8) g''2 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 133-135; 70
  g''4.( e''8) \acciaccatura { g''8 } fs''8 e''8 d''8 e''8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 138.5-139; 71
  g''4.( fs''8) g''2 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 141-143; 72
  g''4.( e''8) fs''4 e''4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 146-147; 73
  d''4 r4 \clef alto <d a fs' d''>4 r4 | % viola-sonata/vla3-2-chord-ly viola-sonata/b2.t1 0-1; 74
  d'1 \bar "|." 
} }

\new PianoStaff <<
\set PianoStaff.instrumentName = ""
\set PianoStaff.shortInstrumentName = ""
\new Staff = "up" {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 4/4 \key d \major \clef treble d'8-. \f e'8-. fs'8-. g'8-. a'4 d'4 | % viola-sonata/pno3-1 viola-sonata/b2.t61 3-4; 1
  b'4 a'8-. gs'8-. a'8 d'8( fs'8 a'8) | % viola-sonata/pno3-1 viola-sonata/b2.t61 0-.5; 2
  c''4.( d''8) b'4. g'16( a'16) | % viola-sonata/pno3-1 viola-sonata/b2.t61 0-.25; 3
  bf'4.( c''8) <fs' a'>8( <e' gs'>8 <d' fs'>4) | % viola-sonata/pno3-1 viola-sonata/b2.t61 0-1; 4
  f'16-. f'16-. bf8-. bf'16-. bf'16-. f'16-. bf'16-. f''16-. f''16-. e''16-. g''16-. bf''16-. bf''16-. f''16-. bf''16-. | % viola-sonata/pno3-1 viola-sonata/b2.t61 0-.25; 5
  d'''16-. d'''16-. a''16-. d'''16-. d''16-. d''16-. f''16-. d''16-. a''16-. a''16-. a'16-. a''16-. d''16-. d'16-. fs'16-. d'16-. | % viola-sonata/pno3-1 viola-sonata/b2.t61 0-.25; 6
  d'16-. a'16-. d''16-. a'16-. c''16( a'16 g'16 c''16 a'16 g'16) c'16-. g'16-. c''16-. g'16-. c'16( a16 | \time 3/4 % viola-sonata/pno3-1 viola-sonata/b2.t61 0-.25; 7
  \clef bass g16 c'16 a16 g16) <ds fs>8-. \mf <ds fs>8-. <ds fs>8-. <ds fs>8-. | \time 4/4 % viola-sonata/pno3-1 viola-sonata/b2.t61 0-.5; 8
  \clef treble d'16-. \f d'16-. a'16-. d'16-. d''16-. d''16-. a'16-. d''16-. a''16-. a''16-. d''16-. ef'''16-. d'''16-. a''16-. ef''16-. d''16-. | % viola-sonata/pno3-1 viola-sonata/b2.t61 0-.25; 9
  d'16-. d'16-. a'16-. d'16-. d''16-. d''16-. a'16-. d''16-. a''16-. a''16-. d''16-. ef'''16-. d'''16-. a''16-. ef''16-. d''16-. | \time 2/4 % viola-sonata/pno3-1 viola-sonata/b2.t61 0-.25; 10
  R4*2 \mf | \time 4/4 % viola-sonata/pno3-1 viola-sonata/b2.t61 0-.25; 11
  d'8-. \f e'8-. fs'8-. g'8-. a'4 d'4 | % viola-sonata/pno3-1 viola-sonata/b2.t61 44-45; 12
  b'4 a'8-. gs'8-. a'8 <a d'>8( <fs' d'>8 <fs' a'>8) | % viola-sonata/pno3-1 viola-sonata/b2.t61 0-.5; 13
  <g' c''>4 <c'' e''>4 <a' d''>8 <a d' fs'>8( <d' fs' a'>8 <fs' a' d''>8) | % viola-sonata/pno3-1 viola-sonata/b2.t61 0-.5; 14
  <g' df' bf'>4( <bf e' g'>4 <a d' fs'>2) | % viola-sonata/pno3-1 viola-sonata/b2.t61 0-2; 15
  bf'16 bf'16 bf''16 bf'16~ bf'16 bf'16 bf''16 bf'16 bf'16 bf'16 bf''16 bf'16~ bf'16 bf'16 bf''8 | % viola-sonata/pno3-1 viola-sonata/b2.t61 60.5-61; 16
  f'16 f'16 f''16 f'16~ f'16 f'16 f''16 f'16 f'16 f'16 f''16 f'16~ f'16 f'16 f''8 | % viola-sonata/pno3-1 viola-sonata/b2.t61 64.5-65; 17
  bf'16 bf'16 bf''16 bf'16 f'16 f'16 f''16 f'16 e'16 e'16 e''16 e'16~ e'16 e'16 e''8 | % viola-sonata/pno3-1 viola-sonata/b2.t61 68.5-69; 18
  d'16( d''16) e'16( e''16) fs'16( fs''16) g'16( g''16) a'16( a''16 fs''16 d''16) fs'16( fs''16 d''16 d'16) | % viola-sonata/pno3-1 viola-sonata/b2.t61 0-.25; 19
  bf'16( bf''16 g''16 d''16) g'16( g''16 d''16 bf'16) a'16 a'16 a''16 a'16~ a'16 a'16 a''16 a'16 | % viola-sonata/pno3-1 viola-sonata/b2.t61 76.75-77; 20
  af'16 af'16 af''16 af'16~ af'16 af'16 af''16 af'16 g'16 g''16 g'8 g'16 g''8. | % viola-sonata/pno3-1 viola-sonata/b2.t61 80.25-81; 21
  gf'8 gf''8 gf'8 gf''8 f'8 f''8 f'8 f''8 | % viola-sonata/pno3-1 viola-sonata/b2.t61 84.5-85; 22
  r2 \> r4 d'''16( \mf a''16 d''16) d'''16( | % viola-sonata/pno3-1 viola-sonata/b2.t61 0-.25; 23
  a''16 d''16) d'''16( a''16 d''16) d'''16( a''16 d''16) d'''16( a''16 d''16) d'''16( a''16 d''16) d'''16( a''16 | % viola-sonata/pno3-1 viola-sonata/b2.t61 0-.25; 24
  d''16) \> g''16( d''16 g'16) g''16( d''16 g'16) g''16( d''16 g'16) g''16( d''16 g'4) \p | % viola-sonata/pno3-1 viola-sonata/b2.t61 0-1; 25
  g'8-. a'8-. bf'8-. c''8-. d''4 g'4 | % viola-sonata/pno3-1 viola-sonata/b2.t61 100-101; 26
  a'8( bf'16 a'16) g'8-. fs'8-. g'8( f'16 ef'16) d'8-. c'8-. | % viola-sonata/pno3-1 viola-sonata/b2.t61 0-.5; 27
  bf8( c'16 bf16) a8-. bf8-. g4 \mf r4 | % viola-sonata/pno3-1 viola-sonata/b2.t61 107-108; 28
  R4*4 | % viola-sonata/pno3-1 viola-sonata/b2.t61 107-108; 29
  R4*4 | % viola-sonata/pno3-1 viola-sonata/b2.t61 107-108; 30
  R4*4 | % viola-sonata/pno3-1 viola-sonata/b2.t61 107-108; 31
  <c'' f''>16-. \f <c'' f''>16-. <c'' f''>8-. <c'' f''>8-. <a' c''>8-. <a' c''>8-. <f' a'>8-. <f' a'>8-. <c' g'>8-. | % viola-sonata/pno3-1 viola-sonata/b2.t61 0-.5; 32
  <c'' f''>16-. <c'' f''>16-. <c'' f''>8-. <c'' f''>8-. <af' c''>8-. <af' c''>8-. <f' af'>8-. <f' af'>8-. <cs' e'>8-. | % viola-sonata/pno3-1 viola-sonata/b2.t61 0-.5; 33
  <cs' e'>8( <c' f'>4) <cs' e'>8( <c' f'>4) <c' e'>8( <f' c'>8) | % viola-sonata/pno3-1 viola-sonata/b2.t61 0-.5; 34
  <c' e'>8-. <c' f'>8-. <c' e'>8-. <c' f'>8-. <c' e'>8-. <c' f'>4. \ff | \time 1/4 % viola-sonata/pno3-1 viola-sonata/b2.t61 135.5-137; 35
  <g bf ef'>4 | \time 4/4 % viola-sonata/pno3-1 viola-sonata/b2.t61 137-138; 36
  \key af \major af'8-. \f bf'8-. c''8-. df''8-. ef''4 af'4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 3-4; 37
  f''8 df'''16( f''16 df''16 df'''16 af''16 f''16) ef''8 af''16( ef''16 af'16 af''16 ef''16 bf'16) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 0-.25; 38
  b'16( b''16 gf''16 ef''16) bf'16( bf''16 f''16 d''16) a'16( a''16 e''16 df''16) af'16( af''16 ef''16 c''16) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 0-.25; 39
  g'16( g''16 ef''16 b'16) af'16( af''16 ef''16 c''16) g'16( g''16 ef''16 b'16 af'4) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 0-1; 40
  <ef'' af''>16-. <ef'' af''>16-. <ef'' af''>8-. <ef'' af''>8-. <c'' ef''>8-. <c'' ef''>8-. <af' c''>8-. <af' c''>8-. <ef' bf'>8-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 0-.5; 41
  <ef'' af''>16-. <ef'' af''>16-. <ef'' af''>8-. <ef'' af''>8-. <b' ef''>8-. <b' ef''>8-. <g' b'>8-. <g' b'>8-. <e' g'>8-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 0-.5; 42
  \ottava #1 ef''16( bf''16 ef'''16 ff'''16 ef'''16 bf''16) ef''16( bf''16 ef'''16 ff'''16 ef'''16 bf''16) \ottava #0 d''8-> ef''8-> | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 0-.5; 43
  r8 d''8-> ef''8-> d''8-> ef''8-> e''8-> r8 d'8-> | \time 3/4 % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 0-.5; 44
  ef'8-> d'8-> ef'8-> e'8-> f'8-> g'8-> | \time 4/4 % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 0-.5; 45
  af8-. \mf bf8-. c'8-. df'8-. ef'4 af4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 38-39; 46
  f'4 \acciaccatura { ef'8[ f'8] } ef'8 d'8 ef'4. f'8 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 42.5-43; 47
  <ef'' af''>8 <ef'' af''>16 <ef'' af''>16 <ef'' g''>16 <ef'' g''>16 r16 <ef'' gf''>16 r8 <ef'' gf''>8-. <ef'' f''>8-. <ef'' f''>8-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 0-.5; 48
  <ef'' af''>8 <ef'' af''>16 <ef'' af''>16 <ef'' gf''>16 <ef'' gf''>16 r16 <df'' ff''>16 r8 <df'' ff''>8-. <df'' ef''>8-. <df'' ef''>8-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 0-.5; 49
  <ef'' af''>8 <ef'' af''>16 <ef'' af''>16 <ef'' g''>16 <ef'' g''>16 r16 <ef'' gf''>16 r8 <ef'' gf''>8-. <ef'' f''>8-. <ef'' f''>8-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 0-.5; 50
  <ef'' af''>16 \f <ef'' af''>16 <ef'' af''>16 <ef'' af''>16 <ef'' af''>8-. r8 <bf' g''>16 <bf' g''>16 <bf' g''>16 <bf' g''>16 <bf' g''>8-. r8 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 0-.5; 51
  af8 \mf bf8 c'8 df'8 d'8 a'16^( d'16 \change Staff = "down" a16 \change Staff = "up" gf'16 d'16 \change Staff = "down" d16) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 0-.25; 52
  \change Staff = "up" f'4( ef'8 d'8) a'8( e'8 d'8 a8) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 0-.5; 53
  ff'16 ff'16 ff'16 ff'16 ff'8-. gf'8-. ef'16 ef'16 ef'16 ef'16 ef'8-. df'16 ef'16 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 70.75-71; 54
  <e' a'>16 \> <e' a'>16 <e' a'>16 <e' a'>16 <e' a'>8 <e' a'>16 <e' a'>16 <e' a'>16 <e' a'>16 <e' a'>8 <g' a'>8 \p <g' a'>8 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 74.5-75; 55
  \key c \major <fs' a'>8-. \< <g' b'>8-. <a' c''>8-. <b' d''>8-. <c'' e''>4 \f <g' c''>4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 78-79; 56
  <d'' f''>4( <c'' e''>8 <b' d''>8) <c'' e''>2 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 81-83; 57
  <<
  { \voiceOne
  g''4.( a''8) bf''4.( c'''8) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 0-.5; 58
  } \new Voice { \voiceTwo
  e''2 ef''2 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t18 85-87; 58
  } >> \oneVoice
  <df'' df'''>8 <b' b''>16 <a' a''>16 <g' g''>8-. <f' f''>8-. <ds' ds''>8( <e' e''>8) <ds' ds''>8( <e' e''>8) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 0-.5; 59
  cs''16( \p b'16 d''16 \< cs''16 b'16 a'16 gs'16 fs'16) e'16( d'16 fs'16 e'16 d'16 c'16 b16 a16) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 0-.25; 60
  gs2 \ff a2 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 97-99; 61
  b2 c'4 d'4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 102-103; 62
  a8-. \f b8-. cs'8-. d'8-. e'4 a4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 106-107; 63
  fs'4( e'8 ds'8) e'8 a8( cs'8 e'8) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 0-.5; 64
  g'4.( a'8) fs'4. d'16( e'16) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 0-.25; 65
  <d' f'>4 <c' e'>4 <b d'>4 <a c'>4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 118-119; 66
  <gs b e'>2 <e' a cs'>2 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 121-123; 67
  d'8-. e'8-. fs'8-. g'8-. a'4 d'4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 126-127; 68
  b'4 a'8-. gs'8-. a'8 d'8( fs'8 a'8) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 0-.5; 69
  c''4.( d''8) b'4. g'16( a'16) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 0-.25; 70
  bf'4.( c''8) <fs' a'>8( <e' gs'>8 <fs' d'>4) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 0-1; 71
  <<
  { \voiceOne
  c''4.( \mf d''8) } \new Voice { \voiceTwo
  g'2 } >> \oneVoice
  <d' g' b'>2 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 141-143; 72
  <d' g' bf'>2 <d' a' c''>4 \p <d' g' bf'>4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 146-147; 73
  <d' fs' a'>4 r4 \clef bass <fs a d'>4 r4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 149-150; 74
  <a, d fs>1 \bar "|." 
} }

\new Staff = "down" {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
{
\time 4/4 \key d \major \clef bass d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 0-1; 1
  d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 0-1; 2
  d8-. d8-. d,4-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 0-1; 3
  d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 0-1; 4
  \ottava #-1 as,4 as,,4 as,4 as,,4 | % viola-sonata/pno3-1 viola-sonata/b2.t46 19-20; 5
  \ottava #0 d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 0-1; 6
  d,16-. d,16-. d,16-. d,16-. d,4-. d4-. d,16-. d,16-. d,16-. d,16-. | \time 3/4 % viola-sonata/pno3-1 viola-sonata/b2.t46 0-.25; 7
  d4 b,4 as,4 | \time 4/4 % viola-sonata/pno3-1 viola-sonata/b2.t46 30-31; 8
  d,4-. d4-. d,4-. d4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 0-1; 9
  d,4-. d4-. d,4-. d4-. | \time 2/4 % viola-sonata/pno3-1 viola-sonata/b2.t46 0-1; 10
  <b, ds fs>4 <as, ds fs>4 | \time 4/4 % viola-sonata/pno3-1 viola-sonata/b2.t46 40-41; 11
  d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 0-1; 12
  d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 0-1; 13
  d8-. d8-. d,4-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 0-1; 14
  d4-. d,4-. d4-. d,8 c,8 | % viola-sonata/pno3-1 viola-sonata/b2.t46 56.5-57; 15
  bf,,8-. f,8-. bf,8-. f,8-. af,,8-. ef,8-. bf,8-. f,8-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 0-.5; 16
  bf,,8-. f,8-. bf,8-. f,8-. af,,8-. ef,8-. bf,8-. f,8-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 0-.5; 17
  bf,,8-. f,8-. bf,8-. f,8-. c,8-. g,8-. e8-. g,8-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 0-.5; 18
  d,8-. a,8-. fs8-. fs,8-. d8-. a8-. fs8-. c'8-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 0-.5; 19
  bf8-. d'8-. d8-. g8-. g,8-. g,16-. a,16-. bf,16-. a,16-. g,8-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 0-.5; 20
  c8 c16( d16) ef16( d16 c8) bf,16( d16 g16 bf,16) d16( g16 bf16 d16) | % viola-sonata/pno3-1 viola-sonata/b2.t46 0-.25; 21
  ef16( d'16 bf16 gf16) d16( ef16 c'16 g16) bf,16( d16 f16 bf16) bf,16( df16 f16 bf16) | % viola-sonata/pno3-1 viola-sonata/b2.t46 0-.25; 22
  a,8-. bf,8-. c8-. cs8-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 0-1; 23
  \ottava #-1 <c, g,>2 <bf,, g,>4 <a,, a,>4 | % viola-sonata/pno3-1 viola-sonata/b2.t46 92-93; 24
  <g,, bf,>1 | % viola-sonata/pno3-1 viola-sonata/b2.t46 93-97; 25
  <c, g,>2 <bf,, g,>2 | % viola-sonata/pno3-1 viola-sonata/b2.t46 99-101; 26
  <a,, a,>4. <d,, d,>8 <g,, bf,>2 | % viola-sonata/pno3-1 viola-sonata/b2.t46 103-105; 27
  \ottava #0 r2 c8-. d8-. ef8-. f8-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 0-.5; 28
  g4 c4 a4 g8 fs8 | % viola-sonata/pno3-1 viola-sonata/b2.t46 112.5-113; 29
  g8 c8( e8 g8) bf4.( c'8) | % viola-sonata/pno3-1 viola-sonata/b2.t46 0-.5; 30
  a4.( bf8) af4 gf4 | % viola-sonata/pno3-1 viola-sonata/b2.t46 120-121; 31
  f2 f4 f4 | % viola-sonata/pno3-1 viola-sonata/b2.t46 124-125; 32
  f2 f,8 f8 f,8 f8 | % viola-sonata/pno3-1 viola-sonata/b2.t46 128.5-129; 33
  f,16( c16 f16 gf16 f16 c16) f,16( c16 f16 gf16 f16 c16) gf16( f16 c16 af,16) | % viola-sonata/pno3-1 viola-sonata/b2.t46 0-.25; 34
  <c, c>8-. <f, f>8-. <c, c>8-. <f, f>8-. <c, c>8-. <f, f>4. | \time 1/4 % viola-sonata/pno3-1 viola-sonata/b2.t46 135.5-137; 35
  <ef, ef>4 | \time 4/4 % viola-sonata/pno3-1 viola-sonata/b2.t46 137-138; 36
  \key af \major \ottava #-1 af,4-. af,,4-. af,4-. af,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-1; 37
  af,4-. af,,4-. af,4-. af,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-1; 38
  af,4-. af,,4-. af,4-. af,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-1; 39
  g,4-> af,4 g,4-> af,4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 15-16; 40
  <f,, f,>4 r4 r4 <f,, f,>4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 19-20; 41
  <e,, e,>4 r4 r4 <e,, e,>4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 23-24; 42
  <ef,, ef,>4 r4 r4 <d,, d,>8-> <ef,, ef,>8-> | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-.5; 43
  r8 <d,, d,>8-> <ef,, ef,>8-> <d,, d,>8-> <ef,, ef,>8-> <e,, e,>8-> r8 <d,, d,>8-> | \time 3/4 % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-.5; 44
  <ef,, ef,>8-> <d,, d,>8-> <ef,, ef,>8-> <e,, e,>8-> <f,, f,>8-> <g,, g,>8-> | \time 4/4 % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-.5; 45
  <ef, af,>4-. af,,4-. <ef, af,>4-. af,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-1; 46
  <ef, af,>8-. <ef, af,>8-. af,,4-. <ef, g,>8( af,8) af,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-1; 47
  \ottava #0 gf,8( df8 gf8 af8 gf8 df8 gf,8 df8) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-.5; 48
  e,8( b,8 ff8 gf8 ff8 b,8 e,8 b,8) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-.5; 49
  gf,8( df8 gf8 af8 gf8 df8 gf,8 df8) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-.5; 50
  ff,16 ff16 ff,16 ff16 gf8-. cs,16 d,16 ef,?16 e16 df,16 ef16 bf8-. cf'16( bf16) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-.25; 51
  af,4-. af,,4-. d4-. d,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-1; 52
  af4-. af,4-. d4-. d,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-1; 53
  \ottava #-1 a,4-. a,,4-. af,4-. af,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-1; 54
  gf,4-. gf,,4-. e,4-. e,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-1; 55
  \ottava #0 \key c \major d4-. d,4-. c4-. c,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-1; 56
  c4-. c,4-. c4-. c,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-1; 57
  c4-. c,4-. c4-. c,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-1; 58
  a,4-. a,,4-. e4-. e,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-1; 59
  a,4-. a,,4-. e4-. e,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-1; 60
  \ottava #-1 <f,, f,>2 <e,, e,>2 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 97-99; 61
  <d,, d,>2 <c, c>4 <b,, b,>4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 102-103; 62
  a,4-. a,,4-. a,4-. a,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-1; 63
  c4-. c,4-. a,4-. a,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-1; 64
  \ottava #0 d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-1; 65
  <b, f>4 <c e>4 d4 <c ef>4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 118-119; 66
  e2 a,2 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 121-123; 67
  d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-1; 68
  d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-1; 69
  d8-. d8-. d,4-. d4-. d,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-1; 70
  d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-1; 71
  d8-. d8-. d,4-. d4-. d,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 0-1; 72
  d4-. d,4-. d2 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 145-147; 73
  d4 r4 d,4 r4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 149-150; 74
  \ottava #-1 <d,, d,>1 \bar "|." 
} }

>>

>>
\header { piece = "III" }
}

