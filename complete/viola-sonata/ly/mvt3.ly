\version "2.14.2"
\language "english"
\pointAndClickOff
\header { title = "mvt3" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\score {
<<
\new Staff  {
\set Staff.instrumentName = "viola"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 4/4 \key d \major \clef treble d'8-. e'8-. fs'8-. g'8-. a'4 d'4 | % viola-sonata/vla3-1 viola-sonata/b2.t76 3-4; 1
 b'4 a'8-. gs'8-. a'8 fs'8 a'8 d''8 | % viola-sonata/vla3-1 viola-sonata/b2.t76 7.5-8; 2
 g''4. fs''8 g''2 | % viola-sonata/vla3-1 viola-sonata/b2.t76 10-12; 3
 g''4. e''8 \acciaccatura { g''8 } fs''8 e''8 d''8 e''8 | % viola-sonata/vla3-1 viola-sonata/b2.t76 15.5-16; 4
 d''8( e''16 d''16) c''8( f''16 e''16) d''8( c''16 d''16) e''16-. d''16-. c''16-. bf'16-. | % viola-sonata/vla3-1 viola-sonata/b2.t76 19.75-20; 5
 \clef alto a'16 d''8. af'8^"pizz." f'8 d'8 af8 f8 d8 | % viola-sonata/vla3-1 viola-sonata/b2.t76 23.5-24; 6
 f'8 d''8 ef''16(^"arco" d''8) ef''16( d''8) f8-^ d'8 ef'16( d'16~ | \time 3/4 % viola-sonata/vla3-1 viola-sonata/b2.t76 27.75-28.25; 7
 d'16) ef'16( d'8) cs'16-. cs'16-. cs''16-. cs'16~ cs'16 cs'16-. cs''16-. cs'16 | \time 4/4 % viola-sonata/vla3-1 viola-sonata/b2.t76 30.75-31; 8
 a'8 d'16 a'16~ a'16 d'16 a'8 bf'16 a'16 d'16 ef'16 d'8 ef'16 d'16 | % viola-sonata/vla3-1 viola-sonata/b2.t76 34.75-35; 9
 g'8 d'16 a'16~ a'16 d'16 g'8 bf'16 a'16 d'16 ef'16 d'8 c'16 d'16 | \time 2/4 % viola-sonata/vla3-1 viola-sonata/b2.t76 38.75-39; 10
 r8 <ef g>8^"pizz." <ef g>8 <ef g>8 | \time 4/4 % viola-sonata/vla3-1 viola-sonata/b2.t76 40.5-41; 11
 d'8 e'8 fs'8 g'8 a'4 d'4 | % viola-sonata/vla3-1 viola-sonata/b2.t76 44-45; 12
 b'4 a'8 gs'8 a'8 fs'8(^"arco" a'8 d''8) | % viola-sonata/vla3-1 viola-sonata/b2.t76 48.5-49; 13
 \clef treble g''4 fs''8( e''8) d''8 fs'8( a'8 d''8) | % viola-sonata/vla3-1 viola-sonata/b2.t76 52.5-53; 14
 g''4 fs''8( e''8) d''4. e''8 | % viola-sonata/vla3-1 viola-sonata/b2.t76 56.5-57; 15
 d''8 e''16( d''16) c''8 f''16( e''16) d''8 c''16( d''16) e''16-. d''16-. c''16-. as'16-. | % viola-sonata/vla3-1 viola-sonata/b2.t76 60.75-61; 16
 f''8 d''16( f''16) e''8 c''16( e''16) f''8 d''16( bf'16) af'16 c''16 ef''16 af'16 | % viola-sonata/vla3-1 viola-sonata/b2.t76 64.75-65; 17
 f'8 f'32( gf'32 f'32 gf'32 f'32 gf'32 f'16) bf'16( f''16) f'8 c''16( e''16) e'8 c''16( e''16) | % viola-sonata/vla3-1 viola-sonata/b2.t76 68.75-69; 18
 \clef alto d'8 e'8 fs'8 g'8 a'4 d'4 | % viola-sonata/vla3-1 viola-sonata/b2.t76 72-73; 19
 g'8 a'8 bf'8 c''8 d''4 g'4 | % viola-sonata/vla3-1 viola-sonata/b2.t76 76-77; 20
 \clef treble ef''4 d''8( c''8) d''8 g'8( bf'8 d''8) | % viola-sonata/vla3-1 viola-sonata/b2.t76 80.5-81; 21
 f''4 ef''4 d''4 cs''4 | % viola-sonata/vla3-1 viola-sonata/b2.t76 84-85; 22
 c''8-. bf'8-. a'8-. g'8-. fs'4-. \acciaccatura { cs''8 } d''4-. | % viola-sonata/vla3-1 viola-sonata/b2.t76 88-89; 23
 \clef alto ef'2 d'4 c'4 | % viola-sonata/vla3-1 viola-sonata/b2.t76 92-93; 24
 bf4 a4 g2 | % viola-sonata/vla3-1 viola-sonata/b2.t76 95-97; 25
 ef'8 f'16( ef'16) d'8-. c'8-. d'8 ef'16( d'16) c'8-. bf8-. | % viola-sonata/vla3-1 viola-sonata/b2.t76 100.5-101; 26
 c'8 d'16( c'16) bf8-. a8-. g2 | % viola-sonata/vla3-1 viola-sonata/b2.t76 103-105; 27
 d'8 ef'16( d'16) c'8-. bf8-. c'8 d'16( c'16) bf8-. a8-. | % viola-sonata/vla3-1 viola-sonata/b2.t76 108.5-109; 28
 g2 g8-. a8-. b8-. c'8-. | % viola-sonata/vla3-1 viola-sonata/b2.t76 112.5-113; 29
 d'4 g4 f'4 e'4 | % viola-sonata/vla3-1 viola-sonata/b2.t76 116-117; 30
 ef'4 d'4 c'4 bf4 | % viola-sonata/vla3-1 viola-sonata/b2.t76 120-121; 31
 a2~ a8 a4 a8 | % viola-sonata/vla3-1 viola-sonata/b2.t76 124.5-125; 32
 af2~ af8 af4 af8 | % viola-sonata/vla3-1 viola-sonata/b2.t76 128.5-129; 33
 f'16 f''8 f'16 f''8 f'16 f''16 c''16 c'16 c''8 c'16 c''8 c'16 | % viola-sonata/vla3-1 viola-sonata/b2.t76 132.75-133; 34
 af'16 c'16 f'16 af'16 c'16 af16 c'16 f16 af16 f16 <f' c''>4. | \time 1/4 % viola-sonata/vla3-1 viola-sonata/b2.t76 135.5-137; 35
 g'4 | \time 4/4 % viola-sonata/vla3-1 viola-sonata/b2.t76 137-138; 36
 \key af \major af8-. bf8-. c'8-. df'8-. ef'4 af4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 3-4; 37
 f'4 ef'8-. d'8-. ef'4.( f'8) | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 7.5-8; 38
 gf'4 f'8( ef'8) df'4( ef'4) | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 11-12; 39
 ff'4 ef'4 ff'4 ef'4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 15-16; 40
 ef'8^"pizz." af'8 ef'8 af'8 ef'8 af'8 ef'8 af'8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 19.5-20; 41
 ef'8 af'8 ef'8 af'8 e'8 g'8 e'8 g'8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 23.5-24; 42
 ef'8 ef''8 ef''8 ef'8 ef''8 ef''8 d'8 ef'8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 27.5-28; 43
 r8 d'8 ef'8 d'8 ef'8 e'8 r8 d'8 | \time 3/4 % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 31.5-32; 44
 ef'8 d'8 ef'8 df'8 cf'8 bf8 | \time 4/4 % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 34.5-35; 45
 af8 bf8 c'8 df'8 ef'4 af4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 38-39; 46
 d'4 c'8 bf8 c'4 ef8 f8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 42.5-43; 47
 gf4 gf'4 gf4 gf'4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 46-47; 48
 ef8 ff8 b8 ef'8 \clef treble e'8 as'8 b'8 e''8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 50.5-51; 49
 af''8 df''8 gf'8 df'8 \clef alto bf8 gf8 df8 gf8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 54.5-55; 50
 <d b>16-.^"arco" <d b>16-. <d b>16-. <d b>16-. <d b>8-. r8 <bf ef'>16 <bf ef'>16 <bf ef'>16 <bf ef'>16 <ef' bf'>8-. r8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 58-58.5; 51
 r2 d'4( a4) | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 62-63; 52
 af'4 gf'8( f'8) gf'8( d'8 a4) | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 66-67; 53
 \clef treble df''8( ef''8) gf''16( ff''16 ef''16 df''16) ef''8( df''16 cf''16) bff'8-. \clef alto af'8~( | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 70.5-71.5; 54
 af'8 gf'16 ff'16) gf'8( ff'16 ef'16) ff'8( ef'16 df'16) b8 b8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 74.5-75; 55
 \key c \major a4 r4 c'8-. d'8-. e'8-. f'8-. | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 78.5-79; 56
 g'4 c'4 a'4 g'8( fs'8) | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 82.5-83; 57
 g'8 e'8( g'8 a'8) bf'8( a'16 g'16) f'8-. ef'8-. | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 86.5-87; 58
 \times 4/6 { df'16( e'16 a'16) a'16( e'16 df'16) } \times 4/6 { df'16( e'16 a'16) a'16( e'16 df'16) } \times 4/6 { c'16( e'16 a'16) a'16( e'16 c'16) } \times 4/6 { c'16( e'16 a'16) a'16( e'16 c'16) } | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 86.5-87; 59
 \times 4/6 { df'16( e'16 a'16) a'16( e'16 df'16) } \times 4/6 { df'16( e'16 a'16) a'16( e'16 df'16) } \times 4/6 { c'16( e'16 a'16) a'16( e'16 c'16) } \times 4/6 { c'16( e'16 a'16) a'16( e'16 c'16) } | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 86.5-87; 60
 b4 c'4 d'4 e'4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 98-99; 61
 f'4 gs'4 a'4 b'4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 102-103; 62
 cs''8 b'16( a'16) g'8-. f'8-. g'8( f'8) ds'8( e'8) | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 106.5-107; 63
 \times 4/6 { ef'16( a'16 c''16) c''16( a'16 ef'16) } \times 4/6 { ef'16( a'16 c''16) c''16( a'16 ef'16) } \times 4/6 { a8( e'8 a'8) a'8( e'8 a8) } | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 106.5-107; 64
 \times 4/6 { d'8( a'8 d''8) d''8( a'8 d'8) } \times 4/6 { d'8( a'8 d''8) d''8( a'8 d'8) } | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 106.5-107; 65
 d'16 d''16 a'16 d'16 d''16 a'16 d'16 d''16 a'16 d'16 d''16 a'16 d'16 d''16 a'16 d'16 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 118.75-119; 66
 b'16 af'16 f'16 e'16 d'16 b16 af16 f16 e16 a16 cs'16 e'16 a'16 cs''16 e''16 cs''16 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 122.75-123; 67
 \clef treble d''8-. e''8-. fs''8-. g''8-. a''4 d''4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 126-127; 68
 b'4 a'8-. gs'8-. a'8 fs'8 a'8 d''8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 130.5-131; 69
 g''4.( fs''8) g''2 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 133-135; 70
 g''4.( e''8) \acciaccatura { g''8 } fs''8 e''8 d''8 e''8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 138.5-139; 71
 g''4.( fs''8) g''2 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 141-143; 72
 g''4.( e''8) fs''4 e''4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 146-147; 73
 d''4 r4 \clef alto <d a fs' d''>4 r4 | % viola-sonata/vla3-2-chord-ly viola-sonata/b2.t1 0-1; 74
 d'1 \bar "|."
} }

\new PianoStaff <<
\set PianoStaff.instrumentName = "piano"
\set PianoStaff.shortInstrumentName = ""
\new Staff = "up" {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 4/4 \key d \major \clef treble d'8-. \f e'8-. fs'8-. g'8-. a'4 d'4 | % viola-sonata/pno3-1 viola-sonata/b2.t61 3-4; 1
 b'4 a'8-. gs'8-. a'8 d'8( fs'8 a'8) | % viola-sonata/pno3-1 viola-sonata/b2.t61 7.5-8; 2
 c''4.( d''8) b'4. g'16( a'16) | % viola-sonata/pno3-1 viola-sonata/b2.t61 11.75-12; 3
 bf'4.( c''8) <fs' a'>8( <e' gs'>8 <d' fs'>4) | % viola-sonata/pno3-1 viola-sonata/b2.t61 15-16; 4
 f'16-. f'16-. bf8-. bf'16-. bf'16-. f'16-. bf'16-. f''16-. f''16-. e''16-. g''16-. bf''16-. bf''16-. f''16-. bf''16-. | % viola-sonata/pno3-1 viola-sonata/b2.t61 19.75-20; 5
 d'''16-. d'''16-. a''16-. d'''16-. d''16-. d''16-. f''16-. d''16-. a''16-. a''16-. a'16-. a''16-. d''16-. d'16-. fs'16-. d'16-. | % viola-sonata/pno3-1 viola-sonata/b2.t61 23.75-24; 6
 d'16-. a'16-. d''16-. a'16-. c''16( a'16 g'16 c''16 a'16 g'16) c'16-. g'16-. c''16-. g'16-. c'16( a16 | \time 3/4 % viola-sonata/pno3-1 viola-sonata/b2.t61 27.75-28; 7
 \clef bass g16 c'16 a16 g16) <ds fs>8-. \mf <ds fs>8-. <ds fs>8-. <ds fs>8-. | \time 4/4 % viola-sonata/pno3-1 viola-sonata/b2.t61 30.5-31; 8
 \clef treble d'16-. \f d'16-. a'16-. d'16-. d''16-. d''16-. a'16-. d''16-. a''16-. a''16-. d''16-. ef'''16-. d'''16-. a''16-. ef''16-. d''16-. | % viola-sonata/pno3-1 viola-sonata/b2.t61 34.75-35; 9
 d'16-. d'16-. a'16-. d'16-. d''16-. d''16-. a'16-. d''16-. a''16-. a''16-. d''16-. ef'''16-. d'''16-. a''16-. ef''16-. d''16-. | \time 2/4 % viola-sonata/pno3-1 viola-sonata/b2.t61 38.75-39; 10
 R4*2 \mf | \time 4/4 % viola-sonata/pno3-1 viola-sonata/b2.t61 38.75-39; 11
 d'8-. \f e'8-. fs'8-. g'8-. a'4 d'4 | % viola-sonata/pno3-1 viola-sonata/b2.t61 44-45; 12
 b'4 a'8-. gs'8-. a'8 <a d'>8( <fs' d'>8 <fs' a'>8) | % viola-sonata/pno3-1 viola-sonata/b2.t61 48.5-49; 13
 <g' c''>4 <c'' e''>4 <a' d''>8 <a d' fs'>8( <d' fs' a'>8 <fs' a' d''>8) | % viola-sonata/pno3-1 viola-sonata/b2.t61 52.5-53; 14
 <g' df' bf'>4( <bf e' g'>4 <a d' fs'>2) | % viola-sonata/pno3-1 viola-sonata/b2.t61 55-57; 15
 bf'16 bf'16 bf''16 bf'16~ bf'16 bf'16 bf''16 bf'16 bf'16 bf'16 bf''16 bf'16~ bf'16 bf'16 bf''8 | % viola-sonata/pno3-1 viola-sonata/b2.t61 60.5-61; 16
 f'16 f'16 f''16 f'16~ f'16 f'16 f''16 f'16 f'16 f'16 f''16 f'16~ f'16 f'16 f''8 | % viola-sonata/pno3-1 viola-sonata/b2.t61 64.5-65; 17
 bf'16 bf'16 bf''16 bf'16 f'16 f'16 f''16 f'16 e'16 e'16 e''16 e'16~ e'16 e'16 e''8 | % viola-sonata/pno3-1 viola-sonata/b2.t61 68.5-69; 18
 d'16( d''16) e'16( e''16) fs'16( fs''16) g'16( g''16) a'16( a''16 fs''16 d''16) fs'16( fs''16 d''16 d'16) | % viola-sonata/pno3-1 viola-sonata/b2.t61 72.75-73; 19
 bf'16( bf''16 g''16 d''16) g'16( g''16 d''16 bf'16) a'16 a'16 a''16 a'16~ a'16 a'16 a''16 a'16 | % viola-sonata/pno3-1 viola-sonata/b2.t61 76.75-77; 20
 af'16 af'16 af''16 af'16~ af'16 af'16 af''16 af'16 g'16 g''16 g'8 g'16 g''8. | % viola-sonata/pno3-1 viola-sonata/b2.t61 80.25-81; 21
 gf'8 gf''8 gf'8 gf''8 f'8 f''8 f'8 f''8 | % viola-sonata/pno3-1 viola-sonata/b2.t61 84.5-85; 22
 r2 \> r4 d'''16( \mf a''16 d''16) d'''16( | % viola-sonata/pno3-1 viola-sonata/b2.t61 88.75-89; 23
 a''16 d''16) d'''16( a''16 d''16) d'''16( a''16 d''16) d'''16( a''16 d''16) d'''16( a''16 d''16) d'''16( a''16 | % viola-sonata/pno3-1 viola-sonata/b2.t61 92.75-93; 24
 d''16) \> g''16( d''16 g'16) g''16( d''16 g'16) g''16( d''16 g'16) g''16( d''16 g'4) \p | % viola-sonata/pno3-1 viola-sonata/b2.t61 96-97; 25
 g'8-. a'8-. bf'8-. c''8-. d''4 g'4 | % viola-sonata/pno3-1 viola-sonata/b2.t61 100-101; 26
 a'8( bf'16 a'16) g'8-. fs'8-. g'8( f'16 ef'16) d'8-. c'8-. | % viola-sonata/pno3-1 viola-sonata/b2.t61 104.5-105; 27
 bf8( c'16 bf16) a8-. bf8-. g4 \mf r4 | % viola-sonata/pno3-1 viola-sonata/b2.t61 107-108; 28
 R4*4 | % viola-sonata/pno3-1 viola-sonata/b2.t61 107-108; 29
 R4*4 | % viola-sonata/pno3-1 viola-sonata/b2.t61 107-108; 30
 R4*4 | % viola-sonata/pno3-1 viola-sonata/b2.t61 107-108; 31
 <c'' f''>16-. \f <c'' f''>16-. <c'' f''>8-. <c'' f''>8-. <a' c''>8-. <a' c''>8-. <f' a'>8-. <f' a'>8-. <c' g'>8-. | % viola-sonata/pno3-1 viola-sonata/b2.t61 124.5-125; 32
 <c'' f''>16-. <c'' f''>16-. <c'' f''>8-. <c'' f''>8-. <af' c''>8-. <af' c''>8-. <f' af'>8-. <f' af'>8-. <cs' e'>8-. | % viola-sonata/pno3-1 viola-sonata/b2.t61 128.5-129; 33
 <cs' e'>8( <c' f'>4) <cs' e'>8( <c' f'>4) <c' e'>8( <f' c'>8) | % viola-sonata/pno3-1 viola-sonata/b2.t61 132.5-133; 34
 <c' e'>8-. <c' f'>8-. <c' e'>8-. <c' f'>8-. <c' e'>8-. <c' f'>4. \ff | \time 1/4 % viola-sonata/pno3-1 viola-sonata/b2.t61 135.5-137; 35
 <g bf ef'>4 | \time 4/4 % viola-sonata/pno3-1 viola-sonata/b2.t61 137-138; 36
 \key af \major af'8-. \f bf'8-. c''8-. df''8-. ef''4 af'4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 3-4; 37
 f''8 df'''16( f''16 df''16 df'''16 af''16 f''16) ef''8 af''16( ef''16 af'16 af''16 ef''16 bf'16) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 7.75-8; 38
 b'16( b''16 gf''16 ef''16) bf'16( bf''16 f''16 d''16) a'16( a''16 e''16 df''16) af'16( af''16 ef''16 c''16) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 11.75-12; 39
 g'16( g''16 ef''16 b'16) af'16( af''16 ef''16 c''16) g'16( g''16 ef''16 b'16 af'4) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 15-16; 40
 <ef'' af''>16-. <ef'' af''>16-. <ef'' af''>8-. <ef'' af''>8-. <c'' ef''>8-. <c'' ef''>8-. <af' c''>8-. <af' c''>8-. <ef' bf'>8-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 19.5-20; 41
 <ef'' af''>16-. <ef'' af''>16-. <ef'' af''>8-. <ef'' af''>8-. <b' ef''>8-. <b' ef''>8-. <g' b'>8-. <g' b'>8-. <e' g'>8-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 23.5-24; 42
 \ottava #1 ef''16( bf''16 ef'''16 ff'''16 ef'''16 bf''16) ef''16( bf''16 ef'''16 ff'''16 ef'''16 bf''16) \ottava #0 d''8-> ef''8-> | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 27.5-28; 43
 r8 d''8-> ef''8-> d''8-> ef''8-> e''8-> r8 d'8-> | \time 3/4 % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 31.5-32; 44
 ef'8-> d'8-> ef'8-> e'8-> f'8-> g'8-> | \time 4/4 % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 34.5-35; 45
 af8-. \mf bf8-. c'8-. df'8-. ef'4 af4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 38-39; 46
 f'4 \acciaccatura { ef'8[ f'8] } ef'8 d'8 ef'4. f'8 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 42.5-43; 47
 <ef'' af''>8 <ef'' af''>16 <ef'' af''>16 <ef'' g''>16 <ef'' g''>16 r16 <ef'' gf''>16 r8 <ef'' gf''>8-. <ef'' f''>8-. <ef'' f''>8-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 46.5-47; 48
 <ef'' af''>8 <ef'' af''>16 <ef'' af''>16 <ef'' gf''>16 <ef'' gf''>16 r16 <df'' ff''>16 r8 <df'' ff''>8-. <df'' ef''>8-. <df'' ef''>8-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 50.5-51; 49
 <ef'' af''>8 <ef'' af''>16 <ef'' af''>16 <ef'' g''>16 <ef'' g''>16 r16 <ef'' gf''>16 r8 <ef'' gf''>8-. <ef'' f''>8-. <ef'' f''>8-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 54.5-55; 50
 <ef'' af''>16 \f <ef'' af''>16 <ef'' af''>16 <ef'' af''>16 <ef'' af''>8-. r8 <bf' g''>16 <bf' g''>16 <bf' g''>16 <bf' g''>16 <bf' g''>8-. r8 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 58-58.5; 51
 af8 \mf bf8 c'8 df'8 d'8 a'16^( d'16 \change Staff = "down" a16 \change Staff = "up" gf'16 d'16 \change Staff = "down" d16) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 62.75-63; 52
 \change Staff = "up" f'4( ef'8 d'8) a'8( e'8 d'8 a8) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 66.5-67; 53
 ff'16 ff'16 ff'16 ff'16 ff'8-. gf'8-. ef'16 ef'16 ef'16 ef'16 ef'8-. df'16 ef'16 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 70.75-71; 54
 <e' a'>16 \> <e' a'>16 <e' a'>16 <e' a'>16 <e' a'>8 <e' a'>16 <e' a'>16 <e' a'>16 <e' a'>16 <e' a'>8 <g' a'>8 \p <g' a'>8 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 74.5-75; 55
 \key c \major <fs' a'>8-. \< <g' b'>8-. <a' c''>8-. <b' d''>8-. <c'' e''>4 \f <g' c''>4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 78-79; 56
 <d'' f''>4( <c'' e''>8 <b' d''>8) <c'' e''>2 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 81-83; 57
<<
  { \voiceOne
   g''4.( a''8) bf''4.( c'''8) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 86.5-87; 58
} \new Voice { \voiceTwo
   e''2 ef''2 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t18 85-87; 58
} >> \oneVoice
   <df'' df'''>8 <b' b''>16 <a' a''>16 <g' g''>8-. <f' f''>8-. <ds' ds''>8( <e' e''>8) <ds' ds''>8( <e' e''>8) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 90.5-91; 59
 cs''16( \p b'16 d''16 \< cs''16 b'16 a'16 gs'16 fs'16) e'16( d'16 fs'16 e'16 d'16 c'16 b16 a16) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 94.75-95; 60
 gs2 \ff a2 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 97-99; 61
 b2 c'4 d'4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 102-103; 62
 a8-. \f b8-. cs'8-. d'8-. e'4 a4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 106-107; 63
 fs'4( e'8 ds'8) e'8 a8( cs'8 e'8) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 110.5-111; 64
 g'4.( a'8) fs'4. d'16( e'16) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 114.75-115; 65
 <d' f'>4 <c' e'>4 <b d'>4 <a c'>4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 118-119; 66
 <gs b e'>2 <e' a cs'>2 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 121-123; 67
 d'8-. e'8-. fs'8-. g'8-. a'4 d'4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 126-127; 68
 b'4 a'8-. gs'8-. a'8 d'8( fs'8 a'8) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 130.5-131; 69
 c''4.( d''8) b'4. g'16( a'16) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 134.75-135; 70
 bf'4.( c''8) <fs' a'>8( <e' gs'>8 <fs' d'>4) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 138-139; 71
<<
  { \voiceOne
   c''4.( \mf d''8)} \new Voice { \voiceTwo
   g'2} >> \oneVoice
   <d' g' b'>2 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 141-143; 72
 <d' g' bf'>2 <d' a' c''>4 \p <d' g' bf'>4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 146-147; 73
 <d' fs' a'>4 r4 \clef bass <fs a d'>4 r4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 149-150; 74
 <a, d fs>1 \bar "|."
} }

\new Staff = "down" {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 4/4 \key d \major \clef bass d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 3-4; 1
 d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 7-8; 2
 d8-. d8-. d,4-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 11-12; 3
 d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 15-16; 4
 \ottava #-1 as,4 as,,4 as,4 as,,4 | % viola-sonata/pno3-1 viola-sonata/b2.t46 19-20; 5
 \ottava #0 d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 23-24; 6
 d,16-. d,16-. d,16-. d,16-. d,4-. d4-. d,16-. d,16-. d,16-. d,16-. | \time 3/4 % viola-sonata/pno3-1 viola-sonata/b2.t46 27.75-28; 7
 d4 b,4 as,4 | \time 4/4 % viola-sonata/pno3-1 viola-sonata/b2.t46 30-31; 8
 d,4-. d4-. d,4-. d4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 34-35; 9
 d,4-. d4-. d,4-. d4-. | \time 2/4 % viola-sonata/pno3-1 viola-sonata/b2.t46 38-39; 10
 <b, ds fs>4 <as, ds fs>4 | \time 4/4 % viola-sonata/pno3-1 viola-sonata/b2.t46 40-41; 11
 d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 44-45; 12
 d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 48-49; 13
 d8-. d8-. d,4-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 52-53; 14
 d4-. d,4-. d4-. d,8 c,8 | % viola-sonata/pno3-1 viola-sonata/b2.t46 56.5-57; 15
 bf,,8-. f,8-. bf,8-. f,8-. af,,8-. ef,8-. bf,8-. f,8-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 60.5-61; 16
 bf,,8-. f,8-. bf,8-. f,8-. af,,8-. ef,8-. bf,8-. f,8-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 64.5-65; 17
 bf,,8-. f,8-. bf,8-. f,8-. c,8-. g,8-. e8-. g,8-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 68.5-69; 18
 d,8-. a,8-. fs8-. fs,8-. d8-. a8-. fs8-. c'8-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 72.5-73; 19
 bf8-. d'8-. d8-. g8-. g,8-. g,16-. a,16-. bf,16-. a,16-. g,8-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 76.5-77; 20
 c8 c16( d16) ef16( d16 c8) bf,16( d16 g16 bf,16) d16( g16 bf16 d16) | % viola-sonata/pno3-1 viola-sonata/b2.t46 80.75-81; 21
 ef16( d'16 bf16 gf16) d16( ef16 c'16 g16) bf,16( d16 f16 bf16) bf,16( df16 f16 bf16) | % viola-sonata/pno3-1 viola-sonata/b2.t46 84.75-85; 22
 a,8-. bf,8-. c8-. cs8-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 88-89; 23
 \ottava #-1 <c, g,>2 <bf,, g,>4 <a,, a,>4 | % viola-sonata/pno3-1 viola-sonata/b2.t46 92-93; 24
 <g,, bf,>1 | % viola-sonata/pno3-1 viola-sonata/b2.t46 93-97; 25
 <c, g,>2 <bf,, g,>2 | % viola-sonata/pno3-1 viola-sonata/b2.t46 99-101; 26
 <a,, a,>4. <d,, d,>8 <g,, bf,>2 | % viola-sonata/pno3-1 viola-sonata/b2.t46 103-105; 27
 \ottava #0 r2 c8-. d8-. ef8-. f8-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 108.5-109; 28
 g4 c4 a4 g8 fs8 | % viola-sonata/pno3-1 viola-sonata/b2.t46 112.5-113; 29
 g8 c8( e8 g8) bf4.( c'8) | % viola-sonata/pno3-1 viola-sonata/b2.t46 116.5-117; 30
 a4.( bf8) af4 gf4 | % viola-sonata/pno3-1 viola-sonata/b2.t46 120-121; 31
 f2 f4 f4 | % viola-sonata/pno3-1 viola-sonata/b2.t46 124-125; 32
 f2 f,8 f8 f,8 f8 | % viola-sonata/pno3-1 viola-sonata/b2.t46 128.5-129; 33
 f,16( c16 f16 gf16 f16 c16) f,16( c16 f16 gf16 f16 c16) gf16( f16 c16 af,16) | % viola-sonata/pno3-1 viola-sonata/b2.t46 132.75-133; 34
 <c, c>8-. <f, f>8-. <c, c>8-. <f, f>8-. <c, c>8-. <f, f>4. | \time 1/4 % viola-sonata/pno3-1 viola-sonata/b2.t46 135.5-137; 35
 <ef, ef>4 | \time 4/4 % viola-sonata/pno3-1 viola-sonata/b2.t46 137-138; 36
 \key af \major \ottava #-1 af,4-. af,,4-. af,4-. af,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 3-4; 37
 af,4-. af,,4-. af,4-. af,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 7-8; 38
 af,4-. af,,4-. af,4-. af,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 11-12; 39
 g,4-> af,4 g,4-> af,4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 15-16; 40
 <f,, f,>4 r4 r4 <f,, f,>4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 19-20; 41
 <e,, e,>4 r4 r4 <e,, e,>4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 23-24; 42
 <ef,, ef,>4 r4 r4 <d,, d,>8-> <ef,, ef,>8-> | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 27.5-28; 43
 r8 <d,, d,>8-> <ef,, ef,>8-> <d,, d,>8-> <ef,, ef,>8-> <e,, e,>8-> r8 <d,, d,>8-> | \time 3/4 % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 31.5-32; 44
 <ef,, ef,>8-> <d,, d,>8-> <ef,, ef,>8-> <e,, e,>8-> <f,, f,>8-> <g,, g,>8-> | \time 4/4 % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 34.5-35; 45
 <ef, af,>4-. af,,4-. <ef, af,>4-. af,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 38-39; 46
 <ef, af,>8-. <ef, af,>8-. af,,4-. <ef, g,>8( af,8) af,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 42-43; 47
 \ottava #0 gf,8( df8 gf8 af8 gf8 df8 gf,8 df8) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 46.5-47; 48
 e,8( b,8 ff8 gf8 ff8 b,8 e,8 b,8) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 50.5-51; 49
 gf,8( df8 gf8 af8 gf8 df8 gf,8 df8) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 54.5-55; 50
 ff,16 ff16 ff,16 ff16 gf8-. cs,16 d,16 ef,?16 e16 df,16 ef16 bf8-. cf'16( bf16) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 58.75-59; 51
 af,4-. af,,4-. d4-. d,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 62-63; 52
 af4-. af,4-. d4-. d,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 66-67; 53
 \ottava #-1 a,4-. a,,4-. af,4-. af,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 70-71; 54
 gf,4-. gf,,4-. e,4-. e,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 74-75; 55
 \ottava #0 \key c \major d4-. d,4-. c4-. c,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 78-79; 56
 c4-. c,4-. c4-. c,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 82-83; 57
 c4-. c,4-. c4-. c,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 86-87; 58
 a,4-. a,,4-. e4-. e,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 90-91; 59
 a,4-. a,,4-. e4-. e,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 94-95; 60
 \ottava #-1 <f,, f,>2 <e,, e,>2 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 97-99; 61
 <d,, d,>2 <c, c>4 <b,, b,>4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 102-103; 62
 a,4-. a,,4-. a,4-. a,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 106-107; 63
 c4-. c,4-. a,4-. a,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 110-111; 64
 \ottava #0 d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 114-115; 65
 <b, f>4 <c e>4 d4 <c ef>4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 118-119; 66
 e2 a,2 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 121-123; 67
 d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 126-127; 68
 d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 130-131; 69
 d8-. d8-. d,4-. d4-. d,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 134-135; 70
 d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 138-139; 71
 d8-. d8-. d,4-. d4-. d,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 142-143; 72
 d4-. d,4-. d2 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 145-147; 73
 d4 r4 d,4 r4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 149-150; 74
 \ottava #-1 <d,, d,>1 \bar "|."
} }

>>

>>
}

