\version "2.14.2"
\language "english"
\pointAndClickOff
\header { title = "sonata - vla" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\bookpart {
\header { piece = "I" }
\score {
<<
\new Staff  {
\set Staff.instrumentName = "viola"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 6/8 \key c \major \clef alto R8*6 | % 1
 R8*6 | % 2
 R8*6 | % 3
 R8*6 | % 4
 R8*6 | % 5
 R8*6 | % 6
 R8*6 | % 7
 R8*6 | % 8
 R8*6 | % 9
 R8*6 | % 10
 R8*6 | % 11
 R8*6 | % 12
 R8*6 | % 13
 R8*6 | % 14
 R8*6 | % 15
 R8*6 | % 16
 R8*6 | % 17
 R8*6 | % 18
 R8*6 | % 19
 R8*6 | % 20
 R8*6 | % 21
 R8*6 | % 22
 r4. cs8-. \mf <a e'>8-. <a e'>8-. | % viola-sonata/vla1 viola-sonata/vla1.t2 68.5-69; 23
 cs8-. <a e'>8-. <a e'>8-. d16( a16) <d' f'>8-. <d' a'>8-. | % viola-sonata/vla1 viola-sonata/vla1.t2 71.5-72; 24
 as8-. <f' as'>8-. g8-. <d' g'>8-. ds8-. <as ds'>8-. | % viola-sonata/vla1 viola-sonata/vla1.t2 74.5-75; 25
 a16( b16 cs'16 d'16 e'16 f'16 g'4) bf'8 | % viola-sonata/vla1 viola-sonata/vla1.t2 77.5-78; 26
 a'16( gs'16 a'16 e'16 cs'16 d'16) e'16( d'16 e'16 cs'16 a16 as16) | % viola-sonata/vla1 viola-sonata/vla1.t2 80.75-81; 27
 a16( e'16 a'16 gs'16 a'16 b'16) cs''8. d''16~ d''8 | % viola-sonata/vla1 viola-sonata/vla1.t2 83.25-84; 28
 cs''4. d''8. \ff f''16~ f''8 | % viola-sonata/vla1 viola-sonata/vla1.t2 86.25-87; 29
 e''4.:32 r4. | % viola-sonata/vla1 viola-sonata/vla1.t2 87-88.5; 30
 <d' a'>2( <d'' a''>4)-\flageolet | % viola-sonata/vla1 viola-sonata/vla1.t2 92-93; 31
 r4. cs8-. \mf <a e'>8-. <a e'>8-. | % viola-sonata/vla1 viola-sonata/vla1.t2 95.5-96; 32
 cs8-. <a e'>8-. <a e'>8-. d16( a16) <d' f'>8-. <d' a'>8-. | % viola-sonata/vla1 viola-sonata/vla1.t2 98.5-99; 33
 cs16-. a16-. <e' cs''>8-. <e' cs''>8-. \times 4/5 { d''16-. a'16-. f'16-. e'16-. d'16-. } <a f'>8 | % viola-sonata/vla1 viola-sonata/vla1.t2 101.5-102; 34
 a'16( \p bf'16 a'8) c''16( bf'16 a'4) r8 | % viola-sonata/vla1 viola-sonata/vla1.t2 103.5-104.44; 35
 a'16( \mf gs'16 a'16 f'16 d'16 e'16) f'16( e'16 f'16 d'16 a16 as16) | % viola-sonata/vla1 viola-sonata/vla1.t2 107.75-108; 36
 c'16( a16 fs16 ef'16 d'16 c'16) bf16( a16 bf16 g16 d16 g16) | % viola-sonata/vla1 viola-sonata/vla1.t2 110.75-111; 37
 f4( g8) a8( d'8 f'8) | % viola-sonata/vla1 viola-sonata/vla1.t2 113.5-114; 38
 af'8( \> g'8 f'8) e'8( d'8 cs'8) \p | % viola-sonata/vla1 viola-sonata/vla1.t2 116.5-117; 39
 d'8-. \f <a f'>8-. <a f'>8-. f8-. <a d'>8-. <a d'>8-. | % viola-sonata/vla1 viola-sonata/vla1.t2 119.5-120; 40
 <d' a'>2( <d'' a''>4)-\flageolet | % viola-sonata/vla1 viola-sonata/vla1.t2 122-123; 41
 a'8^"pizz." \fp f'8 \< d'8 a8 f8 d8 | % viola-sonata/vla1 viola-sonata/vla1.t2 125.5-126; 42
 cs8 \f a8 \mf a8 cs8 a8 a8 | % viola-sonata/vla1 viola-sonata/vla1.t2 128.5-129; 43
 d8 <a f'>8 <a f'>8 c8 <a e'>8 <a e'>8 | % viola-sonata/vla1 viola-sonata/vla1.t2 131.5-132; 44
 <f c'>8 <c' af'>8 <c' af'>8 <ef c'>8 <c' g'>8 <c' g'>8 | % viola-sonata/vla1 viola-sonata/vla1.t2 134.5-135; 45
 \clef treble <ef' bf'>8 <bf' gf''>8 <bf' gf''>8 bf''8 f''8 df''8 | % viola-sonata/vla1 viola-sonata/vla1.t2 137.5-138; 46
 \clef alto e'2.^"arco" \sfz | % viola-sonata/vla1 viola-sonata/vla1.t2 138-141; 47
 a'16( \f gs'16 a'16 f'16 d'16 e'16) f'16( e'16 f'16 d'16 a16 bf16) | % viola-sonata/vla1 viola-sonata/vla1.t2 143.75-144; 48
 c'16( a16 fs16 ef'16 d'16 c'16) bf16( a16 bf16 g16 d16 g16) | % viola-sonata/vla1 viola-sonata/vla1.t2 146.75-147; 49
 f4( \mf g8) \< a8 d'8 f'8 | % viola-sonata/vla1 viola-sonata/vla1.t2 149.5-150; 50
 c''8( \f bf'8 a'8) bf'16( a'16 g'8 d'8) | % viola-sonata/vla1 viola-sonata/vla1.t2 152.5-153; 51
 b'16( a'16 b'16 g'16 d'16 g'16) ef'8 g'8 c''8 | % viola-sonata/vla1 viola-sonata/vla1.t2 155.5-156; 52
 ef''8( d''8 c''8) ef''16( d''16 c''16 b'16 c''8) | % viola-sonata/vla1 viola-sonata/vla1.t2 158.5-159; 53
 a'16( a16) g'16( bf16) gf'16( c'16) ef'16( g16) d'16( a16) c'16( fs16) | % viola-sonata/vla1 viola-sonata/vla1.t2 161.75-162; 54
 ef'2 r4 | % viola-sonata/vla1 viola-sonata/vla1.t2 162-164; 55
 d'4. c'4. | % viola-sonata/vla1 viola-sonata/vla1.t2 166.5-168; 56
 bf2. | % viola-sonata/vla1 viola-sonata/vla1.t2 168-171; 57
 \ottava #1 g''4.^"pizz." \ottava #0 <g g'>4. \bar "|."
} }

>>
}
}

\bookpart {
\header { piece = "II" }
\score {
<<
\new Staff  {
\set Staff.instrumentName = "viola"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 4/4 \key c \major \clef treble e''4^"pizz." \f f'4 d''4 e'4 | % viola-sonata/vla2 viola-sonata/b2.t38 3-4; 1
 cs''4 d'4 b'4 cs'4 | % viola-sonata/vla2 viola-sonata/b2.t38 7-8; 2
 e''4 f'4 f''4 e'4 | % viola-sonata/vla2 viola-sonata/b2.t38 11-12; 3
 c''4 ef'4 bf'4 d'4 | % viola-sonata/vla2 viola-sonata/b2.t38 15-16; 4
 \clef alto c'1~^"arco" \p | % viola-sonata/vla2 viola-sonata/b2.t38 16-20.25; 5
 c'16 bf'16( c'16) a'16( df'16) g'16 r8 c'2 | % viola-sonata/vla2 viola-sonata/b2.t38 22-24; 6
 bf'4( c'4) a'4( bf4) | % viola-sonata/vla2 viola-sonata/b2.t38 27-28; 7
 g'4( a4) gf'4( g4) | % viola-sonata/vla2 viola-sonata/b2.t38 31-32; 8
 a'4-+ \f r8 a'8_~^"nv" \pp <a'_~ bf'>2 | % viola-sonata/vla2 viola-sonata/b2.t41 33.5-42; 9
 <a'_~ b'!^~>2 <a'_~ b'>8 <a'_~ c''~>4. | % viola-sonata/vla2 viola-sonata/b2.t41 33.5-42; 10
 <a' c''>2 <gs' cs''>2^"vib" \mf | % viola-sonata/vla2 viola-sonata/b2.t38 42-44; 11
 b'2 as'2~( | % viola-sonata/vla2 viola-sonata/b2.t38 46-48.5; 12
 as'8 b8) gs'2 as'8( b8) | % viola-sonata/vla2 viola-sonata/b2.t38 51.5-52; 13
 gs'8( as8) g'8( af8) f'8( g8) r8 b'8 | % viola-sonata/vla2 viola-sonata/b2.t38 55.5-56; 14
 cs''2 \> ds''2 | % viola-sonata/vla2 viola-sonata/b2.t38 58-60; 15
 e''2~( \p e''8 fs'8) ds''4( | % viola-sonata/vla2 viola-sonata/b2.t38 63-64; 16
 e'8) cs''8( ds'8) bs'8~ bs'4. cs''16(-- ds''16)-- | % viola-sonata/vla2 viola-sonata/b2.t38 67.75-68; 17
 e''4^"pizz." \f fs'4 ds''4 e'4 | % viola-sonata/vla2 viola-sonata/b2.t38 71-72; 18
 cs''4 ds'4 bs'4 cs'4 | % viola-sonata/vla2 viola-sonata/b2.t38 75-76; 19
 e''4 fs'4 ds''4 e'4 | % viola-sonata/vla2 viola-sonata/b2.t38 79-80; 20
 cs''4 ds'4 bs'4 cs'4 | % viola-sonata/vla2 viola-sonata/b2.t38 83-84; 21
 gs2^"arco" \p \< fs4 b8( a8) | % viola-sonata/vla2 viola-sonata/b2.t38 87.5-88; 22
 ds8( e8 fs8) <c a>8( df8 gs8 fs8 f8) | % viola-sonata/vla2 viola-sonata/b2.t38 91.5-92; 23
 e2 \f <gs e'>4 <cs cs'>4 | % viola-sonata/vla2 viola-sonata/b2.t38 95-96; 24
 <c g>16 \ff <e'~ c''~>8. <e' c''>4<<
  { \voiceOne
   bf'8( af'8) g'4 | % viola-sonata/vla2 viola-sonata/b2.t38 99-100; 25
} \new Voice { \voiceTwo
   d'4 bf8( a8) | % viola-sonata/vla2 viola-sonata/b2.t41 99.5-100; 25
} >> \oneVoice
   <c g>16 \mf <g ef'>8. bf'16( af'16 g'8)-. g'16( f'16 ef'8)-. ef'16( df'16 b16 a16) | % viola-sonata/vla2 viola-sonata/b2.t38 103.75-104; 26
 g1~ \p | % viola-sonata/vla2 viola-sonata/b2.t38 104-108.25; 27
 g16 ef'16( g16) d'16( a16) df'16 r8 g2 | % viola-sonata/vla2 viola-sonata/b2.t38 110-112; 28
 ef'4( f4) d'4( ef4) | % viola-sonata/vla2 viola-sonata/b2.t38 115-116; 29
 c'4( d4) b4( c4) | % viola-sonata/vla2 viola-sonata/b2.t38 119-120; 30
 a'4-+ \f r8 a'8_~^"nv" \pp <a'_~ bf'>2 | % viola-sonata/vla2 viola-sonata/b2.t41 121.5-130; 31
 <a'_~ b'!^~>2 <a'_~ b'>8 <a'_~ c''~>4. | % viola-sonata/vla2 viola-sonata/b2.t41 121.5-130; 32
 <a' c''>2 <gs' cs''>2^"vib" \mf | % viola-sonata/vla2 viola-sonata/b2.t38 130-132; 33
 b'2 as'2~( | % viola-sonata/vla2 viola-sonata/b2.t38 134-136.5; 34
 as'8 b8) gs'2 as'8( b8) | % viola-sonata/vla2 viola-sonata/b2.t38 139.5-140; 35
 gs'8( as8) g'8( af8) f'8( g8) r8 b'8 | % viola-sonata/vla2 viola-sonata/b2.t38 143.5-144; 36
 cs''2 \> ds''2 | % viola-sonata/vla2 viola-sonata/b2.t38 146-148; 37
 e''2 \p ef''2 | % viola-sonata/vla2 viola-sonata/b2.t38 150-152; 38
 df''4 e''8 c''8 af'8 d'8 df'16( \mf b'8.) | % viola-sonata/vla2 viola-sonata/b2.t38 155.25-156; 39
 c'16( \> bf'8.) b16( a'8.) r4 c'4~ \p | % viola-sonata/vla2 viola-sonata/b2.t38 159-161; 40
 c'4 g'8.( \mf \> bf'16) gf'8.( a'16) r4 | % viola-sonata/vla2 viola-sonata/b2.t38 162.75-163; 41
 c'1 \p | % viola-sonata/vla2 viola-sonata/b2.t38 164-168; 42
 c'4.( b8) c'4.( bf8) | % viola-sonata/vla2 viola-sonata/b2.t38 171.5-172; 43
 a2 d8 a4( bf8) | % viola-sonata/vla2 viola-sonata/b2.t38 175.5-176; 44
 d8( a8 bf8 c'8) bf8( a4 d8) | % viola-sonata/vla2 viola-sonata/b2.t38 179.5-180; 45
 bf8( a4 d8) c'8( bf8) bf'8( a8) | % viola-sonata/vla2 viola-sonata/b2.t38 183.5-184; 46
 g16 \< g8. ef'16 ef'8. f16 f8. d'16 d'8. | % viola-sonata/vla2 viola-sonata/b2.t38 187.25-188; 47
 ef16 ef8. c'16 c'8. d16 d8. b16 b8. | % viola-sonata/vla2 viola-sonata/b2.t38 191.25-192; 48
 g16 \f g8. ef'16 ef'8. f16 f8. d'16 d'8. | % viola-sonata/vla2 viola-sonata/b2.t38 195.25-196; 49
 ef16 ef8. c'16 c'8. d16 d8. bf16 bf8. | % viola-sonata/vla2 viola-sonata/b2.t38 199.25-200; 50
 c16 c8. a16 a8. d16 d8. bf16 bf8. | % viola-sonata/vla2 viola-sonata/b2.t38 203.25-204; 51
 ef16 ef8. c'16 c'8 a16 <g~ d'~>2 | % viola-sonata/vla2 viola-sonata/b2.t38 206-212; 52
 <g d'>1 | % viola-sonata/vla2 viola-sonata/b2.t38 206-212; 53
 <g d'>16 <d' a'>8. <c g>16 <g f'>8. <ef g>16 <g ef'>8. <c g>16 <g d'>8. | % viola-sonata/vla2 viola-sonata/b2.t38 215.25-216; 54
 c16( a16 c'16 f'16 \clef treble bf'16 f''16 g''16 c''16) f''8 ef''8 d''8 c''8 | % viola-sonata/vla2 viola-sonata/b2.t38 219.5-220; 55
 d''4 f''8 ef''8 d''2 | % viola-sonata/vla2 viola-sonata/b2.t38 222-224; 56
 \clef alto d'2 \p c'4( d'8 ef'8) | % viola-sonata/vla2 viola-sonata/b2.t38 227.5-228; 57
 bf4( a4) bf4( a4) | % viola-sonata/vla2 viola-sonata/b2.t38 231-232; 58
 d2 e4( f8 g8) | % viola-sonata/vla2 viola-sonata/b2.t38 235.5-236; 59
 bf8( g'8) a4 b8( f'8) af8( gs'8) | % viola-sonata/vla2 viola-sonata/b2.t38 239.5-240; 60
 a'2 bf'8( a'8) c''8( b'8) | % viola-sonata/vla2 viola-sonata/b2.t38 243.5-244; 61
 bf'4( a'8 g'8) b8( cs'8 e'8 g'8) | % viola-sonata/vla2 viola-sonata/b2.t38 247.5-248; 62
<<
  { \voiceOne
   a'4. \f r8 bf'8 r8 a'8( g'8) | % viola-sonata/vla2 viola-sonata/b2.t41 251.5-252; 63
 f'4 r4 a'4.( bf'8) | % viola-sonata/vla2 viola-sonata/b2.t41 255.5-256; 64
 d''4.( cs''8)} \new Voice { \voiceTwo
   f'32( g'32 f'32 e'32 f'4 e'8) d'8( f'8) df'4 | % viola-sonata/vla2 viola-sonata/b2.t38 251-252; 63
 bf8( g8) a8( bf8) f'8( e'8 g'4~ | % viola-sonata/vla2 viola-sonata/b2.t38 255-256.5; 64
 g'8) e'8( f'4)} >> \oneVoice
   <d'~ d''~>2 \> | \time 2/4 % viola-sonata/vla2 viola-sonata/b2.t38 258-262; 65
 <d' d''>2 | \time 4/4 % viola-sonata/vla2 viola-sonata/b2.t38 258-262; 66
 <c g>16 \f <e'~ c''~>8. <e' c''>4<<
  { \voiceOne
   bf'8( af'8) g'4 | % viola-sonata/vla2 viola-sonata/b2.t38 265-266; 67
} \new Voice { \voiceTwo
   d'4 bf8( a8) | % viola-sonata/vla2 viola-sonata/b2.t41 265.5-266; 67
} >> \oneVoice
   <c g>16 <g ef'>8. bf'16( af'16 g'8)-. g'16( f'16 ef'8)-. ef'16( df'16 b16 a16) | % viola-sonata/vla2 viola-sonata/b2.t38 269.75-270; 68
 <c g>16 <g ef'>8. <g ef'>16 <ef' c''>8. <c g>16 <g ef'>8. <g ef'>16 <ef' c''>8. | % viola-sonata/vla2 viola-sonata/b2.t38 273.25-274; 69
 \clef treble ef'16 g'16 c''16 g'16 ef''16 c''16 g'16 g''16 ef''16 c''16 g'16 ef''16 c''16 g'16 ef'16 af'16 | % viola-sonata/vla2 viola-sonata/b2.t38 277.75-278; 70
 <b af'>16 <af' ef''>8. af''16( g''16 f''8)-. f''16( ef''16 d''8)-. d''16( c''16 b'16 a'16) | % viola-sonata/vla2 viola-sonata/b2.t38 281.75-282; 71
 af'4-- \ff g'4-- f'4-- ef'4-- | % viola-sonata/vla2 viola-sonata/b2.t38 285-286; 72
 c'8 g'8 d''8 g''8 c'''4-- b''4-- | % viola-sonata/vla2 viola-sonata/b2.t38 289-290; 73
 bf''4-- a''4-- af''4-- g''4-- | % viola-sonata/vla2 viola-sonata/b2.t38 293-294; 74
 f'8 af'8 c''8 f''8 g''4-- gf''4-- | % viola-sonata/vla2 viola-sonata/b2.t38 297-298; 75
 f''4-- e''4-- ef''4-- d''4-- | % viola-sonata/vla2 viola-sonata/b2.t38 301-302; 76
 c''4-. \clef alto <g c>16( <g ef'>8.)-. c4-. r4 \bar "|."
} }

>>
}
}

\bookpart {
\header { piece = "III" }
\score {
<<
\new Staff  {
\set Staff.instrumentName = "viola"
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 4/4 \key d \major \clef treble d'8-. e'8-. fs'8-. g'8-. a'4 d'4 | % viola-sonata/vla3-1 viola-sonata/b2.t76 3-4; 1
 b'4 a'8-. gs'8-. a'8 fs'8 a'8 d''8 | % viola-sonata/vla3-1 viola-sonata/b2.t76 7.5-8; 2
 g''4. fs''8 g''2 | % viola-sonata/vla3-1 viola-sonata/b2.t76 10-12; 3
 g''4. e''8 \acciaccatura { g''8 } fs''8 e''8 d''8 e''8 | % viola-sonata/vla3-1 viola-sonata/b2.t76 15.5-16; 4
 d''8( e''16 d''16) c''8( f''16 e''16) d''8( c''16 d''16) e''16-. d''16-. c''16-. bf'16-. | % viola-sonata/vla3-1 viola-sonata/b2.t76 19.75-20; 5
 \clef alto a'16 d''8. af'8^"pizz." f'8 d'8 af8 f8 d8 | % viola-sonata/vla3-1 viola-sonata/b2.t76 23.5-24; 6
 f'8 d''8 ef''16(^"arco" d''8) ef''16( d''8) f8-^ d'8 ef'16( d'16~ | \time 3/4 % viola-sonata/vla3-1 viola-sonata/b2.t76 27.75-28.25; 7
 d'16) ef'16( d'8) cs'16-. cs'16-. cs''16-. cs'16~ cs'16 cs'16-. cs''16-. cs'16 | \time 4/4 % viola-sonata/vla3-1 viola-sonata/b2.t76 30.75-31; 8
 a'8 d'16 a'16~ a'16 d'16 a'8 bf'16 a'16 d'16 ef'16 d'8 ef'16 d'16 | % viola-sonata/vla3-1 viola-sonata/b2.t76 34.75-35; 9
 g'8 d'16 a'16~ a'16 d'16 g'8 bf'16 a'16 d'16 ef'16 d'8 c'16 d'16 | \time 2/4 % viola-sonata/vla3-1 viola-sonata/b2.t76 38.75-39; 10
 r8 <ef g>8^"pizz." <ef g>8 <ef g>8 | \time 4/4 % viola-sonata/vla3-1 viola-sonata/b2.t76 40.5-41; 11
 d'8 e'8 fs'8 g'8 a'4 d'4 | % viola-sonata/vla3-1 viola-sonata/b2.t76 44-45; 12
 b'4 a'8 gs'8 a'8 fs'8(^"arco" a'8 d''8) | % viola-sonata/vla3-1 viola-sonata/b2.t76 48.5-49; 13
 \clef treble g''4 fs''8( e''8) d''8 fs'8( a'8 d''8) | % viola-sonata/vla3-1 viola-sonata/b2.t76 52.5-53; 14
 g''4 fs''8( e''8) d''4. e''8 | % viola-sonata/vla3-1 viola-sonata/b2.t76 56.5-57; 15
 d''8 e''16( d''16) c''8 f''16( e''16) d''8 c''16( d''16) e''16-. d''16-. c''16-. as'16-. | % viola-sonata/vla3-1 viola-sonata/b2.t76 60.75-61; 16
 f''8 d''16( f''16) e''8 c''16( e''16) f''8 d''16( bf'16) af'16 c''16 ef''16 af'16 | % viola-sonata/vla3-1 viola-sonata/b2.t76 64.75-65; 17
 f'8 f'32( gf'32 f'32 gf'32 f'32 gf'32 f'16) bf'16( f''16) f'8 c''16( e''16) e'8 c''16( e''16) | % viola-sonata/vla3-1 viola-sonata/b2.t76 68.75-69; 18
 \clef alto d'8 e'8 fs'8 g'8 a'4 d'4 | % viola-sonata/vla3-1 viola-sonata/b2.t76 72-73; 19
 g'8 a'8 bf'8 c''8 d''4 g'4 | % viola-sonata/vla3-1 viola-sonata/b2.t76 76-77; 20
 \clef treble ef''4 d''8( c''8) d''8 g'8( bf'8 d''8) | % viola-sonata/vla3-1 viola-sonata/b2.t76 80.5-81; 21
 f''4 ef''4 d''4 cs''4 | % viola-sonata/vla3-1 viola-sonata/b2.t76 84-85; 22
 c''8-. bf'8-. a'8-. g'8-. fs'4-. \acciaccatura { cs''8 } d''4-. | % viola-sonata/vla3-1 viola-sonata/b2.t76 88-89; 23
 \clef alto ef'2 d'4 c'4 | % viola-sonata/vla3-1 viola-sonata/b2.t76 92-93; 24
 bf4 a4 g2 | % viola-sonata/vla3-1 viola-sonata/b2.t76 95-97; 25
 ef'8 f'16( ef'16) d'8-. c'8-. d'8 ef'16( d'16) c'8-. bf8-. | % viola-sonata/vla3-1 viola-sonata/b2.t76 100.5-101; 26
 c'8 d'16( c'16) bf8-. a8-. g2 | % viola-sonata/vla3-1 viola-sonata/b2.t76 103-105; 27
 d'8 ef'16( d'16) c'8-. bf8-. c'8 d'16( c'16) bf8-. a8-. | % viola-sonata/vla3-1 viola-sonata/b2.t76 108.5-109; 28
 g2 g8-. a8-. b8-. c'8-. | % viola-sonata/vla3-1 viola-sonata/b2.t76 112.5-113; 29
 d'4 g4 f'4 e'4 | % viola-sonata/vla3-1 viola-sonata/b2.t76 116-117; 30
 ef'4 d'4 c'4 bf4 | % viola-sonata/vla3-1 viola-sonata/b2.t76 120-121; 31
 a2~ a8 a4 a8 | % viola-sonata/vla3-1 viola-sonata/b2.t76 124.5-125; 32
 af2~ af8 af4 af8 | % viola-sonata/vla3-1 viola-sonata/b2.t76 128.5-129; 33
 f'16 f''8 f'16 f''8 f'16 f''16 c''16 c'16 c''8 c'16 c''8 c'16 | % viola-sonata/vla3-1 viola-sonata/b2.t76 132.75-133; 34
 af'16 c'16 f'16 af'16 c'16 af16 c'16 f16 af16 f16 <f' c''>4. | \time 1/4 % viola-sonata/vla3-1 viola-sonata/b2.t76 135.5-137; 35
 g'4 | \time 4/4 % viola-sonata/vla3-1 viola-sonata/b2.t76 137-138; 36
 \key af \major af8-. bf8-. c'8-. df'8-. ef'4 af4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 3-4; 37
 f'4 ef'8-. d'8-. ef'4.( f'8) | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 7.5-8; 38
 gf'4 f'8( ef'8) df'4( ef'4) | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 11-12; 39
 ff'4 ef'4 ff'4 ef'4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 15-16; 40
 ef'8^"pizz." af'8 ef'8 af'8 ef'8 af'8 ef'8 af'8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 19.5-20; 41
 ef'8 af'8 ef'8 af'8 e'8 g'8 e'8 g'8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 23.5-24; 42
 ef'8 ef''8 ef''8 ef'8 ef''8 ef''8 d'8 ef'8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 27.5-28; 43
 r8 d'8 ef'8 d'8 ef'8 e'8 r8 d'8 | \time 3/4 % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 31.5-32; 44
 ef'8 d'8 ef'8 df'8 cf'8 bf8 | \time 4/4 % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 34.5-35; 45
 af8 bf8 c'8 df'8 ef'4 af4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 38-39; 46
 d'4 c'8 bf8 c'4 ef8 f8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 42.5-43; 47
 gf4 gf'4 gf4 gf'4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 46-47; 48
 ef8 ff8 b8 ef'8 \clef treble e'8 as'8 b'8 e''8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 50.5-51; 49
 af''8 df''8 gf'8 df'8 \clef alto bf8 gf8 df8 gf8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 54.5-55; 50
 <d b>16-.^"arco" <d b>16-. <d b>16-. <d b>16-. <d b>8-. r8 <bf ef'>16 <bf ef'>16 <bf ef'>16 <bf ef'>16 <ef' bf'>8-. r8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 58-58.5; 51
 r2 d'4( a4) | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 62-63; 52
 af'4 gf'8( f'8) gf'8( d'8 a4) | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 66-67; 53
 \clef treble df''8( ef''8) gf''16( ff''16 ef''16 df''16) ef''8( df''16 cf''16) bff'8-. \clef alto af'8~( | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 70.5-71.5; 54
 af'8 gf'16 ff'16) gf'8( ff'16 ef'16) ff'8( ef'16 df'16) b8 b8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 74.5-75; 55
 \key c \major a4 r4 c'8-. d'8-. e'8-. f'8-. | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 78.5-79; 56
 g'4 c'4 a'4 g'8( fs'8) | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 82.5-83; 57
 g'8 e'8( g'8 a'8) bf'8( a'16 g'16) f'8-. ef'8-. | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 86.5-87; 58
 \times 4/6 { df'16( e'16 a'16) a'16( e'16 df'16) } \times 4/6 { df'16( e'16 a'16) a'16( e'16 df'16) } \times 4/6 { c'16( e'16 a'16) a'16( e'16 c'16) } \times 4/6 { c'16( e'16 a'16) a'16( e'16 c'16) } | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 86.5-87; 59
 \times 4/6 { df'16( e'16 a'16) a'16( e'16 df'16) } \times 4/6 { df'16( e'16 a'16) a'16( e'16 df'16) } \times 4/6 { c'16( e'16 a'16) a'16( e'16 c'16) } \times 4/6 { c'16( e'16 a'16) a'16( e'16 c'16) } | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 86.5-87; 60
 b4 c'4 d'4 e'4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 98-99; 61
 f'4 gs'4 a'4 b'4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 102-103; 62
 cs''8 b'16( a'16) g'8-. f'8-. g'8( f'8) ds'8( e'8) | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 106.5-107; 63
 \times 4/6 { ef'16( a'16 c''16) c''16( a'16 ef'16) } \times 4/6 { ef'16( a'16 c''16) c''16( a'16 ef'16) } \times 4/6 { a8( e'8 a'8) a'8( e'8 a8) } | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 106.5-107; 64
 \times 4/6 { d'8( a'8 d''8) d''8( a'8 d'8) } \times 4/6 { d'8( a'8 d''8) d''8( a'8 d'8) } | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 106.5-107; 65
 d'16 d''16 a'16 d'16 d''16 a'16 d'16 d''16 a'16 d'16 d''16 a'16 d'16 d''16 a'16 d'16 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 118.75-119; 66
 b'16 af'16 f'16 e'16 d'16 b16 af16 f16 e16 a16 cs'16 e'16 a'16 cs''16 e''16 cs''16 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 122.75-123; 67
 \clef treble d''8-. e''8-. fs''8-. g''8-. a''4 d''4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 126-127; 68
 b'4 a'8-. gs'8-. a'8 fs'8 a'8 d''8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 130.5-131; 69
 g''4.( fs''8) g''2 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 133-135; 70
 g''4.( e''8) \acciaccatura { g''8 } fs''8 e''8 d''8 e''8 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 138.5-139; 71
 g''4.( fs''8) g''2 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 141-143; 72
 g''4.( e''8) fs''4 e''4 | % viola-sonata/vla3-2 viola-sonata/vla3-2.t4 146-147; 73
 d''4 r4 \clef alto <d a fs' d''>4 r4 | % viola-sonata/vla3-2-chord-ly viola-sonata/b2.t1 0-1; 74
 d'1 \bar "|."
} }

>>
}
}

