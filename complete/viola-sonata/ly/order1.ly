\version "2.14.2"
\language "english"
\header { title = "order1" tagline = "" }
\score { <<

\new Staff  {
\numericTimeSignature
\set Staff.instrumentName = "viola"
\set Staff.shortInstrumentName = ""
{
\time 6/8 \key c \major \clef alto R2. | % 1
  R2. | % 2
  R2. | % 3
  R2. | % 4
  R2. | % 5
  R2. | % 6
  R2. | % 7
  R2. | % 8
  R2. | % 9
  R2. | % 10
  R2. | % 11
  R2. | % 12
  R2. | % 13
  R2. | % 14
  R2. | % 15
  R2. | % 16
  R2. | % 17
  R2. | % 18
  R2. | % 19
  R2. | % 20
  R2. | % 21
  R2. | % 22
  r4. cs8-. \mf <a e'>8-. <a e'>8-. | % 23
  cs8-. <a e'>8-. <a e'>8-. d16( a16) <d' f'>8-. <d' a'>8-. | % 24
  as8-. <f' as'>8-. g8-. <d' g'>8-. ds8-. <as ds'>8-. | % 25
  a16( b16 cs'16 d'16 e'16 f'16 g'8.) r16 bf'8 | % 26
  a'16( gs'16 a'16 e'16 cs'16 d'16) e'16( d'16 e'16 cs'16 a16 as16) | % 27
  a16( e'16 a'16 gs'16 a'16 b'16) cs''8. d''16~ d''8 | % 28
  cs''4. d''8. \ff f''16~ f''8 | % 29
  e''4.:32 r4. | % 30
  <d' a'>2( <d'' a''>4)-\flageolet | % 31
  r4. cs8-. \mf <a e'>8-. <a e'>8-. | % 32
  cs8-. <a e'>8-. <a e'>8-. d16( a16) <d' f'>8-. <d' a'>8-. | % 33
  cs16-. a16-. <e' cs''>8-. <e' cs''>8-. \times 4/5 { d''16-. a'16-. f'16-. e'16-. d'16-. } <a f'>8 | % 34
  a'16( \p bf'16 a'8) c''16( bf'16 a'4) r8 | % 35
  a'16( \mf gs'16 a'16 f'16 d'16 e'16) f'16( e'16 f'16 d'16 a16 as16) | % 36
  c'16( a16 fs16 ef'16 d'16 c'16) bf16( a16 bf16 g16 d16 g16) | % 37
  f4( g8) a8( d'8 f'8) | % 38
  af'8( \> g'8 f'8) e'8( d'8 cs'8) \p | % 39
  d'8-. \f <a f'>8-. <a f'>8-. f8-. <a d'>8-. <a d'>8-. | % 40
  <d' a'>2( <d'' a''>4)-\flageolet | % 41
  a'8^"pizz." \fp f'8 \< d'8 a8 f8 d8 | % 42
  cs8 \f a8 \mf a8 cs8 a8 a8 | % 43
  d8 <a f'>8 <a f'>8 c8 <a e'>8 <a e'>8 | % 44
  <f c'>8 <c' af'>8 <c' af'>8 <ef c'>8 <c' g'>8 <c' g'>8 | % 45
  \clef treble <ef' bf'>8 <bf' gf''>8 <bf' gf''>8 bf''8 f''8 df''8 | % 46
  \clef alto e'2.^"arco" \sfz | % 47
  a'16( \f gs'16 a'16 f'16 d'16 e'16) f'16( e'16 f'16 d'16 a16 bf16) | % 48
  c'16( a16 fs16 ef'16 d'16 c'16) bf16( a16 bf16 g16 d16 g16) | % 49
  f4( \mf g8) \< a8 d'8 f'8 | % 50
  c''8( \f bf'8 a'8) bf'16( a'16 g'8 d'8) | % 51
  b'16( a'16 b'16 g'16 d'16 g'16) ef'8 g'8 c''8 | % 52
  ef''8( d''8 c''8) ef''16( d''16 c''16 b'16 c''8) | % 53
  a'16( a16) g'16( bf16) gf'16( c'16) ef'16( g16) d'16( a16) c'16( fs16) | % 54
  ef'2 r4 | % 55
  d'4. c'4. | % 56
  bf2. | % 57
  \ottava #1 g''4.^"pizz." \ottava #0 <g g'>4. } }

\new PianoStaff <<
\set PianoStaff.instrumentName = "piano"
\set PianoStaff.shortInstrumentName = ""

\new Staff = "up" {
\numericTimeSignature
{
\time 6/8 \key c \major a''16( \f gs''16 a''16 f''16 d''16 e''16) f''16( e''16 f''16 d''16 a'16 bf'16) | % 1
  c''16( a'16 fs'16 ef''16 d''16 c''16) bf'16( a'16 bf'16 g'16 d'16 g'16) | % 2
  f'4( g'8) a'8( d''8 f''8) | % 3
  c'''8( bf''8 a''8) bf''16( a''16 g''8 d''8) | % 4
  b''16( a''16 b''16 g''16 d''16 g''16) ef''8( g''8 c'''8) | % 5
  ef'''8( d'''8 c'''8) ef'''16( d'''16 c'''16 b''16 c'''8) | % 6
  d'''16( c'''16 b''8 g''8) d'''16( b''16 g''16 d''16 b'16 g'16) | % 7
  c''4~ \mf c''16 a'16( bf'4~ bf'16) g'16 | % 8
  a'16( \< d''16 cs''16 d''16 a'16 fs'16) e'16( g'16 fs'16 g'16 e'16 c'16) | % 9
  d'8 \f b'8 g''8 ef'8 c''8 g''8 | % 10
  fs''4.( g''4.) | % 11
  g''16( d''16 bf'16 a'16 bf'16 d''16) bf''16( g''16 d''16 cs''16 d''16 g''16) | % 12
  \ottava #1 g'''4 f'''8 ef'''8 d'''8 c'''8 | % 13
  \ottava #0 bf''8 a''8 g''8 cs'''8( \p a''16 g''16 a''16 e'''16) | % 14
  cs'''8( a''16 g''16 a''16 e'''16) f'''8( e'''8 d'''8) | % 15
  c'''8( bf''8 a''8) g''8( f''8 e''8) | % 16
  f''8( e''8 d''8) g''8( f''8 e''8) | % 17
  d''2 a'16( a''16 d''8) | % 18
  f''8 \f e''8 d''8 c''8 bf'8 a'8 | % 19
  gs'16 \p fs'16 gs'16 b'16 e'16 b'16 gs'16 fs'16 gs'16 b'16 e'16 b'16 | % 20
  c''16( \< b'16 c''16 a'16 e'16 a'16) c''16( b'16 c''16 a'16 e'16 a'16) | % 21
  e''16( ds''16 e''16 c''16 a'16 c''16) <e'' a''>4 \f a''8 \p | % 22
  bf''16( a''16 g''16 f''16 e''16 d''16) cs''4. | % 23
  a'4 a'16( bf'16 a'8) c''16( bf'16 a'8) | % 24
  f''16( e''16 d''8) g''16( f''16 e''8) bf''16( a''16 g''8) | % 25
  cs'''16( b''16 a''16 g''16 f''16 e''16 ds''4) g''8^"melody" | % 26
  a''8 \acciaccatura { bf''8[ c'''8] } bf''4~ bf''8 \acciaccatura { d'''8[ c'''8] } bf''8. gs''16 | % 27
  <e'' a''>8 \acciaccatura { f''8[ g''8] } f''4 r8 \times 4/6 { \acciaccatura { a''8[ g''8] } f''16 e''16 d''16 c''16 as'16 gs'16 } | % 28
  a'4. f'8. \ff d'16~ d'8 | % 29
  \ottava #1 a''32 <e''' a'''>32 cs'''32 a''32 <e''' a'''>32 cs'''32 a''32 <e''' a'''>32 cs'''32 a''32 <e''' a'''>16 r4. | % 30
  d'''16( cs'''16 d'''16 a''16 f''16 a''16) f'''16( e'''16 f'''16 d'''16 a''16 d'''16) | % 31
  \ottava #0 bf''16( \> a''16 g''16 f''16 e''16 d''16 cs''4.) | % 32
  a'4 \p a'16( bf'16 a'8) c''16( bf'16 a'16 g'16 | % 33
  a'8) c''16( bf'16 a'16 g'16 f'16 g'16 a'4) | % 34
  <a' f' cs'' f''>4. r4. | % 35
  R2. | % 36
  R2. | % 37
  a'16( gs'16 a'16 f'16 d'16 e'16) f'16( e'16 f'8 a16 bf16) | % 38
  d'16( \> e'16) af'16( g'16) bf'16( a'16) e''16( d''16) f''16( e''16) bf''16( \p a''16) | % 39
  d'''8 \f r4 r4. | % 40
  \ottava #1 \acciaccatura { d''''8[ a'''8] } d'''16( cs'''16 d'''16 a''16 f''16 a''16) f'''16( e'''16 f'''16 d'''16 a''16 d'''16) | % 41
  a'''16 \fp e'''16 \< f'''16 cs'''16 d'''16 gs''16 \ottava #0 a''16 e''16 f''16 cs''16 d''16 a'16 \f | % 42
  cs''8( \p a''16 g''16 a''16 e'''16) cs'''8( a''16 g''16 a''16 e'''16) | % 43
  \ottava #1 f'''8( e'''8 d'''8) e'''8( f'''8 g'''8) | % 44
  af'''8( g'''8 f'''8) ef'''8( d'''8 c'''8) | % 45
  gf'''8( f'''8 ef'''8) df'''8( c'''8 bf''8) | % 46
  <g'' bf'' d'''>4. <a'' cs''' e'''>4. | % 47
  <f''' a'' d'''>4. \f <d''' f'' a''>4. | % 48
  <d''' a'' c'''>8. <ef'''~ a''~ c'''~>16 <ef''' a'' c'''>8 \acciaccatura { a''8[ bf''8] } <bf'' d''' a''>8. <d'''~ g''~ bf''~>16 <d''' g'' bf''>8 | % 49
  \ottava #0 <a'' g''>8( <f'' a''>4) <bf'' e''>8( <a'' f''>8) \times 2/3 { d''16 e''16 g''16 } | % 50
  a''8( g''8 fs''8) d''16( ef''16 d''8 g''8) | % 51
  d''16( e''?16 d''8 g''8) c''16( d''16 c''8 g''8) | % 52
  a'16( bf'16 a'8 ef''8) a'16( b'16 a'8 ef''8) | % 53
  a'16 b'16 a'8 ef''8 a'16 c''16 a'8 d''8 | % 54
  <ef' bf' ef''>2 r4 | % 55
  <d' bf' d''>4. <d' a' d''>4. | % 56
  <bf' d'' g'>2. | % 57
  <g''' g'' bf''>8 r4 <g'' bf'' bf' d''>8 r4 } }

\new Staff = "down" {
\numericTimeSignature
{
\time 6/8 \key c \major R2. | % 1
  R2. | % 2
  a'16( gs'16 a'16 f'16 d'16 e'16) f'16( e'16 f'16 d'16 a16 bf16) | % 3
  \clef bass c'16( a16 fs16 ef'16 d'16 c'16) bf16( a16 bf16 g16 d16 g16) | % 4
  f4( g8) ef16( d16 ef16 c16 g,16 c16) | % 5
  g16( fs16 g16 ef16 c16 ef16) c'16( b16 c'16 g16 ef16 g16) | % 6
  g,16( d16 g16 b16 d'16 g'16) g8( b8 c'16 d'16) | % 7
  \clef treble ef'16( g'16 fs'16 g'16 ef'16 c'16) d'16( g'16 fs'16 g'16 d'16 bf16) | % 8
  d'4~ d'16 b16( c'4~ c'16) a16 | % 9
  b16 d'16 g'16 fs'16 g'16 d'16 c'16 ef'16 g'16 fs'16 g'16 a16 | % 10
  \clef bass d'16( d16 e16 fs16 g16 a16) \clef treble b16( c'16 d'16 e'16 fs'16 g'16) | % 11
  a'16( bf'16 g'16 d'16 a16 bf16) \clef bass g16( d16 cs16 d16 bf,16 g,16) | % 12
  g4 <f f'>8 <ef ef'>8 <d d'>8 <c c'>8 | % 13
  <bf, bf>8 <a, a>8 <g, g>8 cs,16 cs16 <a, e>16 bf,16 <a, e>16 <g a>16 | % 14
  cs,16 cs16 <a, e>16 bf,16 <a, e>16 <g a>16 d,16 d16 <a, f>16 e16 <a, f>16 a16 | % 15
  g,16 d16 <g d'>16 cs'16 <bf d'>16 g'16 c16 g16 <c' e'>16 b16 <c' g'>16 e'16 | % 16
  <d' f'>8( <a e'>8 <f d'>8) c16_( g16 c'16 e'16 \change Staff = "up" g'16 c''16) | % 17
  \change Staff = "down" a,16( d16 f16 a16 d'16 f'16) d,16( a,16 d16 f16 a16 d'16) | % 18
  <f f'>8 <e e'>8 <d d'>8 <c c'>8 <bf, bf>8 <a, a>8 | % 19
  e,16 fs,16 e,16 b,16 <e gs>16 b,16 e16 ds16 e16 b,16 gs,16 b,16 | % 20
  <c' c>8 <b b,>8 <a, a>8 <g g,>8 <f f,>8 <e, e>8 | % 21
  <d d,>8 <c c,>8 <b,, b,>8 <a,, a,>4 r8 | % 22
  R2. | % 23
  R2. | % 24
  R2. | % 25
  R2. | % 26
  a,8-. <e cs'>8-. <e cs'>8-. a,8-. <f d'>8-. <f d'>8-. | % 27
  a,8-. <e cs'>8-. <e cs'>8-. a,8-. <d' f>8-. <d' f>8-. | % 28
  <e cs' a,>4. <d a f,>8. <a,~ f~ d,~>16 <a, f d,>8 | % 29
  \ottava #-1 <a,, a,>4. \ottava #0 r4. | % 30
  d,16 a,16 <a d>16 gs16 <a f>16 d'16 f,16 d16 <f a>16 e16 <d' f>16 a'16 | % 31
  \clef treble bf'16( a'16 g'16 f'16 e'16 d'16 cs'4.) | % 32
  \clef bass a4 a16( bf16 a8) c'16( bf16 a16 bf16 | % 33
  a8) c'16( bf16 a16 bf16 a16 bf16 a4) | % 34
  <f, cs a>4. a16 \mf g16 f16 e16 d16 bf,16 | % 35
  g,16 g16 <bf d'>16 cs'16 <g d'>16 g'16 a,16 d16 <f a>16 e16 <f d'>16 d16 | % 36
  c16 a16 c'16 b16 c'16 fs'16 bf,16 d16 g16 bf16 d'16 g'16 | % 37
  d,8-. <f a,>8-. <f a,>8-. <a, f>8-. <a d>8-. d'8-. | % 38
  <b f'>8( <as e'>8 <a d'>8) <g e'>8( <f d'>8 <e cs'>8) | % 39
  d,16 a,16 <d a>16 gs16 <f a>16 d'16 f,16 d16 <a f>16 e16 <d' f>16 a'16 | % 40
  d,16 a,16 <d a>16 gs16 <f a>16 d'16 f,16 d16 <f a>16 e16 <f d'>16 a'16 | % 41
  \clef treble a'16( a''16) f'16( f''16) d'16( d''16) \clef bass a16( a'16) f16( f'16) d16( d'16) | % 42
  cs,16 cs16 <a, e>16 bf,16 <a, e>16 <g a>16 cs,16 cs16 <a, e>16 bf,16 <a, e>16 <g a>16 | % 43
  d,16 d16 <a, f>16 e16 <a, f>16 a16 c,16 c16 <a, e>16 d16 <c e>16 a16 | % 44
  f,16 f16 <af c'>16 g16 <af f'>16 c'16 c,16 c16 <ef c'>16 d16 <g c'>16 g'16 | % 45
  ef,16( ef16) df,16( df16) c,16( c16) bf,,16( bf,16 f16 bf16 df'16 f'16) | % 46
  g'16( e'16 d'16 bf16 g16 e16) e'16( cs'16 a16 g16 e16 cs16) | % 47
  <d d'>4. r4. | % 48
  R2. | % 49
  \clef treble a'16 gs'16 a'16 f'16 d'16 e'16 f'16 e'16 f'16 d'16 a16 bf16 | % 50
  \clef bass c'16 a16 fs16 ef'16 d'16 c'16 bf16 a16 bf16 g16 d16 g16 | % 51
  f4( g8) ef16 d16 ef16 c16 g,16 c16 | % 52
  g16 fs16 g16 ef16 c16 ef16 c'16 b16 c'16 g16 ef16 g16 | % 53
  ef'16 d'16 ef'16 c'16 g16 c'16 fs'16 e'16 fs'16 d'16 a16 d'16 | % 54
  <ef g bf df'>2 r4 | % 55
  <d g bf>4. <d fs a>4. | % 56
  g,16( d16 g16 bf,16 d16 bf16) d'16( g16 d'16 \change Staff = "up" bf'16 g'16 d''16) | % 57
  \change Staff = "down" <bf, g, bf d>8 r4 \ottava #-1 <g,, bf,, d, bf,>8 r4 } }
>>
>> }
