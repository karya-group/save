\version "2.14.2"
\language "english"
\header { title = "pno2" tagline = "" }
\score { <<

\new PianoStaff <<
\set PianoStaff.instrumentName = "piano"
\set PianoStaff.shortInstrumentName = ""

\new Staff= "up" {
{
  \clef treble \key c \major \time 4/4 e''16 e''8.-. f'16 f'8.-. d''16 d''8.-. e'16 e'8.-. | cs''16 cs''8.-. d'16 d'8.-. b'16 b'8.-. cs'16 cs'8.-. | % 0
  e''16 e''8.-. f'16 f'8.-. d''16 d''8.-. e'16 e'8.-. | as'16 as'8.-. c'16 c'8.-. a'16 a'8.-. as16 as8.-. | % 2
  a1~ | a2 a2~ | % 4
  a1~ | a4 c'''16 as''16 e''16 f''16 a''4~ a''8. as''16 | % 6
  a''2 r16 g''16 c''16 cs''16 f''4~ | f''4~ f''32 r32 e''16 b'16 c''16 ds''4~ ds''16 d''16 a'16 e'16 | % 8
  cs'16 f'16 a'16 fs''16~ fs''4 \arpeggioArrowUp<as' fs'' cs'''>2\arpeggio | <b' ds'' g''>2 <b' ds'' fs''>2~ | % 10
  <b' ds'' fs''>2. \times 2/3 { as''16 gs''16 ds''16 } fs''8~ | fs''4 \times 2/3 { e''16 ds''16 g'16 } cs''8~ cs''4~ cs''16. r32 b'8 | % 12
  as'2 fs'2 | gs'1~ | % 14
  gs'1 | e''16 e''16. r32 r16 fs'16 fs'16. r32 r16 ds''16 ds''16. r32 r16 e'16 e'16. r32 r16 | % 16
  cs''16 cs''16. r32 r16 ds'16 ds'16. r32 r16 c''16 c''16. r32 r16 cs'16 cs'16. r32 r16 | e''16 e''16. r32 r16 fs'16 fs'16. r32 r16 ds''16 ds''16. r32 r16 e'16 e'16. r32 r16 | % 18
  cs''16 cs''16. r32 r16 ds'16 ds'16. r32 r16 c''16 c''16. r32 r16 cs'16 cs'16. r32 r16 | e''16 e''16. r32 r16 fs'16 fs'16. r32 r16 ds''16 ds''16. r32 r16 e'16 e'16. r32 r16 | % 20
  cs''16 cs''16. r32 r16 ds'16 ds'16. r32 r16 <a' c''>16 <a' c''>16. r32 r16 r16 cs''16 c'''16 f''16 | cs'''32 e''32 cs''32 r32 \change Staff = "down" c''32 e'32 cs'32 r32 \change Staff = "up" \times 4/5 { e''16 cs''16 c''16 gs'16 e'16 } r4 r32 cs32 gs,32 r32 cs,8 | % 22
  <c'' g'' d'''>2. <g'' ef'''>8 <gf'' d'''>8 | <f'' df'''>2 \arpeggioArrowUp<ef'' gf'' b''>2\arpeggio | % 24
  <bf g'>1~ | <bf g'>2 <bf g'>2~ | % 26
  <bf g'>1~ | <bf g'>4 as''16 gs''16 d''16 ds''16 fs''4~ fs''8. g''16 | % 28
  a''2 r16 g''16 c''16 cs''16 f''4~ | f''4~ f''32 r32 e''16 b'16 c''16 ds''4~ ds''16 d''16 a'16 e'16 | % 30
  cs'16 f'16 a'16 fs''16~ fs''4 as'32~ <as' fs''>32~ <as' fs'' cs'''>8.~ <as' fs'' cs'''>4 | <b' ds'' g''>2 <b' ds'' fs''>2~ | % 32
  <b' ds'' fs''>2. \times 2/3 { as''16 gs''16 ds''16 } fs''8~ | fs''4 \times 2/3 { e''16 ds''16 g'16 } cs''8~ cs''4~ cs''16. r32 b'8 | % 34
  as'2 fs'2 | gs'4~ gs'8~ <gs' d'''>8~ <gs' d'''>4~ gs'8~ <gs' as''>8 | % 36
  \times 4/6 { b''16 as''16 e''16 a''16 gs''16 cs''16 } g''8 e''8 c''8 f'8 gs'8. b'16 | g'8. as'16 fs'8. a'16 r4 a4~ | % 38
  a4 c'16 e''16 as'16 g'16 b16 d''16 gs'16 d'16 r4 | a4 g'4 f'4 g'8 gs'8 | % 40
  ds'8 ds''8 d'4 ds'8 ds''8 cs'8 b8 | c'16 c'16. r32 r16 a'16 a'16. r32 r16 as16 as16. r32 r16 g'16 g'16. r32 r16 | % 42
  a16 a16. r32 r16 fs'16 fs'16. r32 r16 c'16 c'16. r32 r16 a'16 a'16. r32 r16 | as16 as16. r32 r16 g'16 g'16. r32 r16 a16 a16. r32 r16 fs'16 fs'16. r32 r16 | % 44
  g16 g8. ds'16 ds'8. f16 f8. d'16 d'8. | ds16 ds8. c'16 c'8. d16 d8. b16 b8. | % 46
  g'16 d''16 a''16 g''16 d'''16 g''16 a''16 c''16 g''16 d''16 f''16 a''16 as'16 g''16 a'16 fs''16 | g'16 ds''16 a''16 g''16 d'''16 ds''16 a''16 g''16 ds''16 d'''16 f''16 c'''16 ds''16 as''16 d''16 a''16 | % 48
  g'16 d''16 a''16 g''16 d'''16 f''16 a''16 g''16 g'''16 d'''16 c'''16 f'''16 as''16 f'''16 a''16 f'''16 | g''16 as''16 g'''16 f'''16 as'''16 ds'''16 d'''16 a'''16 g'''8 as''8 f'''8 a''8 | % 50
  ds'''8 g''8 d'''8 f''8 c'''8 ds''8 as''8 d''8 | g''8 as'8 f''8 a'8 ds''8 g'8 d''8 f'8 | % 52
  c''8 ds'8 as'8 d'8 a'8 c'8 g'8 as8 | <a g'>4 a'8 c'8 <g' g''>16 ds''16 as'16 g'16 ds'16 c'16 g16 ds16 | % 54
  R1 | R1 | % 56
  e''16 e''16. r32 r16 f'16 f'16. r32 r16 d''16 d''16. r32 r16 e'16 e'16. r32 r16 | cs''16 cs''16. r32 r16 d'16 d'16. r32 r16 b'16 b'16. r32 r16 cs'16 cs'16. r32 r16 | % 58
  e''16 e''16. r32 r16 f'16 f'16. r32 r16 d''16 d''16. r32 r16 e'16 e'16. r32 r16 | cs''16 cs''16. r32 r16 d'16 d'16. r32 r16 b'16 b'16. r32 r16 cs'16 cs'16. r32 r16 | % 60
  e''16 e''16. r32 r16 f'16 f'16. r32 r16 d''16 d''16. r32 r16 e'16 e'16. r32 r16 | cs''16 cs''16. r32 r16 d'16 d'16. r32 r16 b'16 b'16. r32 r16 cs'16 cs'16. r32 r16 | % 62
  d''16 d''16. r32 r16 f'16 f'16. r32 r16 a'16 a'16. r32 r16 a16 a16. r32 r16 | f'16 f'16. r32 r16 r4 <c'' g'' d'''>2~ | % 64
  <c'' g'' d'''>4 <g'' ef'''>8 <d''' gf''>8 <f'' df'''>2 | ds''32~ <ds'' fs''>32~ <ds'' fs'' b''>8.~ <ds'' fs'' b''>4 c,16 g,16 d16 c16 g16 c16 d16 c'16 | % 66
  f16 g16 ds'16 f16 d'16 ds16 c'16 d16 c16 g16 d'16 c'16 g'16 d'16 c''16 ds'16 | g'16 ds''16 f'16 d''16 ds'16 c''16 d'16 b'16 <ds' b'>2 | % 68
  c''32~ <c'' ds''>32~ <c'' ds'' gs'' c'''>8.~ <c'' ds'' gs'' c'''>4 <b af'>4 <c' g'>4 | <af c' f'>4 <g c' g'>4 <c' f' a'>4 <d' f' b'>4 | % 70
  <ef' g' c''>4 <d' g' b'>4 <d' g' bf'>4 <c' f' a'>4 | <c' f' af'>4 <c' ef' g'>4 <c' d' f'>4 <c' ef'>4 | % 72
  <b d'>4 <b ef'>4 <c' f'>4 <c' g'>4 | <c' a'>4 <d' b'>4 <ef' c''>8 r8 <g g'>8 r8 | % 74
  g8 r8 r4 r2 | % 76
} }

\new Staff= "down" {
{
  \clef bass \key c \major \time 4/4 R1 | r2 r8 \times 2/3 { d,,16 cs,16 f,,16 } d,,8 r8 | % 0
  R1 | r2 r8 \times 2/3 { d,,16 d,16 a,,16 } d,,8 r8 | % 2
  <d,, fs,>1~ | <d,, fs,>2 <d,, fs,>2~ | % 4
  <d,, fs,>1~ | <d,, fs,>1 | % 6
  R1 | R1 | % 8
  r2 \arpeggioArrowUp<ds as f'>2\arpeggio | <b, fs cs'>2 <gs, ds as>2~ | % 10
  <gs, ds as>1 | R1 | % 12
  <cs' b, fs>2 <fs as gs, ds>2 | <ds cs, gs,>1~ | % 14
  <ds cs, gs,>1 | R1 | % 16
  R1 | \times 2/3 { cs,16 cs16 e,16 } cs,8 r4 gs2 | % 18
  fs4 gs8 a8 e8 e'8 ds4 | \times 2/3 { cs16 cs'16 e16 } cs8 \times 2/3 { gs16 ds'16 c'16 } fs8 \times 2/3 { e16 cs'16 gs16 } ds8 <g b>8 <f a>8 | % 20
  <ds fs>8 <e gs>8 <fs a>8 c'8 df'4 cs'16 cs'16. r32 r16 | r2 \times 4/6 { cs'16 gs16 cs16 cs'16 gs16 cs16 } r4 | % 22
  <c, g, e>1 | <c, g, ef>1 | % 24
  <c, e>1~ | <c, e>2 <c, e>2~ | % 26
  <c, e>1~ | <c, e>1 | % 28
  R1 | R1 | % 30
  r2 ds32~ <ds as>32~ <ds as f'>8.~ <ds as f'>4 | <b, fs cs'>2 <gs, ds as>2~ | % 32
  <gs, ds as>1 | R1 | % 34
  <cs' b, fs>2 <fs as gs, ds>2 | <gs, ds cs,>1 | % 36
  R1 | r2 r4 <fs, d,,>4~ | % 38
  <fs, d,,>2. r4 | <fs, d,,>1~ | % 40
  <fs, d,,>1 | <g, g,, d,>1~ | % 42
  <g, g,, d,>1~ | <g, g,, d,>1 | % 44
  <g,, g,>1~ | <g,, g,>1 | % 46
  g16 g8. ds'16 ds'8. f16 f8. d'16 d'8. | ds16 ds8. c'16 c'8. d16 d8. as16 as8. | % 48
  c16 c8. a16 a8. as,16 as,8. g16 g8. | c16 c8. a8. a,16 g,2~ | % 50
  <g, f,, c,>1 | g,2~ <g, f,, c,>2 | % 52
  <f, c as,,>1 | <d, g, g,,>4 <c,, c,>4 <g,, d, g,>2 | % 54
  <g, d>1~ | <g, d>1 | % 56
  <d, d,, a,,>1~ | <d, d,, a,,>1 | % 58
  R1 | R1 | % 60
  R1 | R1 | % 62
  R1 | r4 d16 d16. r32 r16 <c, g, e>2~ | % 64
  <c, g, e>2 <g, ds c,>2~ | <g, ds c,>2~ c,16 r16 r8 r4 | % 66
  R1 | r2 <gs,, ds, b,>2~ | % 68
  <gs,, ds, b,>2 <b, ef af af,>4 <c ef g g,>4 | <f, c f>4 <ef, c>4 <d, d>4 <g, d>4 | % 70
  <ef, c>4 <g, d>4 <bf, g>4 <c a>4 | <ef af>4 <ef g>4 f4 fs4 | % 72
  g4 gf4 <af f>4 e4 | ef4 <g d>4 <g ef' c>8 r8 <g, ef c,>8 r8 | % 74
  <c,, c,>8 r8 r4 r2 | % 76
} }
>>
>> }
