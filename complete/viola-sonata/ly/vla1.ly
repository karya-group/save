\version "2.14.2"
\language "english"
\header { title = "vla1" tagline = "" }
\score { <<

\new Staff {
\set Staff.instrumentName = "viola"
\set Staff.shortInstrumentName = ""
{
  \clef alto \key c \major \time 4/4 cs8-. <e' a>8-. <e' a>8-. cs8-. <e' a>8-. <e' a>8-. d16( a16) <f' d'>8-. | <a' d'>8-. as8-. <as' f'>8-. g8-. <g' d'>8-. ds8-. <ds' as>8-. a16( b16 | cs'16 d'16 e'16 f'16 g'8.) r16 bf'8 a'16( gs'16 a'16 e'16 cs'16 d'16) | e'16( d'16 e'16 cs'16 a16 as16) a16( e'16 a'16 gs'16 a'16 b'16) cs''8. d''16~ | % 0
  d''8 cs''4. d''8.-^ f''16~-^ f''8-^ e''8~:32 | e''4:32 r4 r8 <d' a'>4.~ | <d' a'>8 <a'' d''>4-\flageolet r8 r4 cs8-. <e' a>8-. | <e' a>8-. cs8-. <e' a>8-. <e' a>8-. d16( a16) <f' d'>8-. <a' d'>8-. cs16-. a16-. | % 4
  <cs'' e'>8-. <cs'' e'>8-. \times 4/5 { d''16-. a'16-. f'16-. e'16-. d'16-. } <a f'>8 a'16( bf'16 a'8) c''16( bf'16 | a'4) r8 a'16( gs'16 a'16 f'16 d'16 e'16) f'16( e'16 f'16 d'16 | a16 as16) c'16( a16 fs16 ef'16 d'16 c'16) bf16( a16 bf16 g16 d16 g16) f8~( | f8( g8) a8( d'8 f'8) af'8( g'8 f'8) | % 8
  e'8( d'8 cs'8) d'8-. <f' a>8-. <f' a>8-. f8-. <d' a>8-. | <d' a>8-. <d' a'>4.~ <d' a'>8 <a'' d''>4-\flageolet a'8^"pizz." | f'8 d'8 a8 f8 d8 cs8 a8 a8 | cs8 a8 a8 d8 <f' a>8 <f' a>8 c8 <e' a>8 | % 12
  <e' a>8 <c' f>8 <af' c'>8 <af' c'>8 <c' ef>8 <g' c'>8 <g' c'>8 \clef treble <ef' bf'>8 | <bf' gf''>8 <bf' gf''>8 bf''8 f''8 df''8 \clef alto e'4.~^"arco" | e'4. a'16( gs'16 a'16 f'16 d'16 e'16) f'16( e'16 f'16 d'16 | a16 bf16) c'16( a16 fs16 ef'16 d'16 c'16) bf16( a16 bf16 g16 d16 g16) f8~( | % 16
  f8( g8) a8 d'8 f'8 c''8( bf'8 a'8) | bf'16( a'16 g'8 d'8) b'16( a'16 b'16 g'16 d'16 g'16) ef'8 g'8 | c''8 ef''8( d''8 c''8) ef''16( d''16 c''16 b'16 c''8) a'16( a16) | g'16( bf16) gf'16( c'16) ef'16( g16) d'16( a16) c'16( fs16) ef'4.~ | % 20
  ef'8 r8 r8 d'8~ d'4 c'4~ | c'8 bf4.~ bf4. \ottava #1 g''8~^"pizz." | g''4 \ottava #0 <g' g>4. r8 r4 | % 24
} }
>> }
