\version "2.14.2"
\language "english"
\pointAndClickOff
\header { title = "sonata - pno" tagline = "" }

\paper {
print-page-number = ##t
print-first-page-number = ##t
oddHeaderMarkup = \markup \null
evenHeaderMarkup = \markup \null
oddFooterMarkup = \markup {
\fill-line {
    \on-the-fly #print-page-number-check-first
    \fromproperty #'page:page-number-string
    }
}
evenFooterMarkup = \oddFooterMarkup
}

\bookpart {
\header { piece = "I" }
\score {
<<
\new PianoStaff <<
\set PianoStaff.instrumentName = "piano"
\set PianoStaff.shortInstrumentName = ""
\new Staff = "up" {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 6/8 \key c \major a''16( \f gs''16 a''16 f''16 d''16 e''16) f''16( e''16 f''16 d''16 a'16 bf'16) | % viola-sonata/pno1 viola-sonata/pno1.t26 2.75-3; 1
 c''16( a'16 fs'16 ef''16 d''16 c''16) bf'16( a'16 bf'16 g'16 d'16 g'16) | % viola-sonata/pno1 viola-sonata/pno1.t26 5.75-6; 2
 f'4( g'8) a'8( d''8 f''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 8.5-9; 3
 c'''8( bf''8 a''8) bf''16( a''16 g''8 d''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 11.5-12; 4
 b''16( a''16 b''16 g''16 d''16 g''16) ef''8( g''8 c'''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 14.5-15; 5
 ef'''8( d'''8 c'''8) ef'''16( d'''16 c'''16 b''16 c'''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 17.5-18; 6
 d'''16( c'''16 b''8 g''8) d'''16( b''16 g''16 d''16 b'16 g'16) | % viola-sonata/pno1 viola-sonata/pno1.t26 20.75-21; 7
 c''4~ \mf c''16 a'16( bf'4~ bf'16) g'16 | % viola-sonata/pno1 viola-sonata/pno1.t26 23.75-24; 8
 a'16( \< d''16 cs''16 d''16 a'16 fs'16) e'16( g'16 fs'16 g'16 e'16 c'16) | % viola-sonata/pno1 viola-sonata/pno1.t26 26.75-27; 9
 d'8 \f b'8 g''8 ef'8 c''8 g''8 | % viola-sonata/pno1 viola-sonata/pno1.t26 29.5-30; 10
 fs''4.( g''4.) | % viola-sonata/pno1 viola-sonata/pno1.t26 31.5-33; 11
 g''16( d''16 bf'16 a'16 bf'16 d''16) bf''16( g''16 d''16 cs''16 d''16 g''16) | % viola-sonata/pno1 viola-sonata/pno1.t26 35.75-36; 12
 \ottava #1 g'''4 f'''8 ef'''8 d'''8 c'''8 | % viola-sonata/pno1 viola-sonata/pno1.t26 38.5-39; 13
 \ottava #0 bf''8 a''8 g''8 cs'''8( \p a''16 g''16 a''16 e'''16) | % viola-sonata/pno1 viola-sonata/pno1.t26 41.75-42; 14
 cs'''8( a''16 g''16 a''16 e'''16) f'''8( e'''8 d'''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 44.5-45; 15
 c'''8( bf''8 a''8) g''8( f''8 e''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 47.5-48; 16
 f''8( e''8 d''8) \stemUp g''8( f''8 e''8) \stemNeutral | % viola-sonata/pno1 viola-sonata/pno1.t26 50.5-51; 17
 d''2 a'16( a''16 d''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 53.5-54; 18
 f''8 \f e''8 d''8 c''8 bf'8 a'8 | % viola-sonata/pno1 viola-sonata/pno1.t26 56.5-57; 19
 gs'16 \p fs'16 gs'16 b'16 e'16 b'16 gs'16 fs'16 gs'16 b'16 e'16 b'16 | % viola-sonata/pno1 viola-sonata/pno1.t26 59.75-60; 20
 c''16( \< b'16 c''16 a'16 e'16 a'16) c''16( b'16 c''16 a'16 e'16 a'16) | % viola-sonata/pno1 viola-sonata/pno1.t26 62.75-63; 21
 e''16( ds''16 e''16 c''16 a'16 c''16) <e'' a''>4 \f a''8 \p | % viola-sonata/pno1 viola-sonata/pno1.t26 65.5-66; 22
 bf''16( a''16 g''16 f''16 e''16 d''16) cs''4. | % viola-sonata/pno1 viola-sonata/pno1.t26 67.5-69; 23
 a'4 a'16( bf'16 a'8) c''16( bf'16 a'8) | % viola-sonata/pno1 viola-sonata/pno1.t26 71.5-72; 24
 f''16( e''16 d''8) g''16( f''16 e''8) bf''16( a''16 g''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 74.5-75; 25
 cs'''16( b''16 a''16 g''16 f''16 e''16 ds''4) g''8^"melody" | % viola-sonata/pno1 viola-sonata/pno1.t26 77.5-78; 26
 a''8 \acciaccatura { bf''8[ c'''8] } bf''4~ bf''8 \acciaccatura { d'''8[ c'''8] } bf''8. gs''16 | % viola-sonata/pno1 viola-sonata/pno1.t26 80.75-81; 27
 <e'' a''>8 \acciaccatura { f''8[ g''8] } f''4 r8 \times 4/6 { \acciaccatura { a''8[ g''8] } f''16 e''16 d''16 c''16 as'16 gs'16 } | % viola-sonata/pno1 viola-sonata/pno1.t26 81.5-82.5; 28
 a'4. f'8. \ff d'16~ d'8 | % viola-sonata/pno1 viola-sonata/pno1.t26 86.25-87; 29
 \ottava #1 <a'' a''' e''' cs'''>4.:32 r4. | % viola-sonata/pno1 viola-sonata/pno1.t26 87-88.5; 30
 d'''16( cs'''16 d'''16 a''16 f''16 a''16) f'''16( e'''16 f'''16 d'''16 a''16 d'''16) | % viola-sonata/pno1 viola-sonata/pno1.t26 92.75-93; 31
 \ottava #0 bf''16( \> a''16 g''16 f''16 e''16 d''16 cs''4.) | % viola-sonata/pno1 viola-sonata/pno1.t26 94.5-95.94; 32
 a'4 \p a'16( bf'16 a'8) c''16( bf'16 a'16 g'16 | % viola-sonata/pno1 viola-sonata/pno1.t26 98.75-99; 33
 a'8) c''16( bf'16 a'16 g'16 f'16 g'16 a'4) | % viola-sonata/pno1 viola-sonata/pno1.t26 101-101.94; 34
 <a' f' cs'' f''>4. r4. | % viola-sonata/pno1 viola-sonata/pno1.t26 102-103.5; 35
 R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t26 102-103.5; 36
 R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t26 102-103.5; 37
 a'16( gs'16 a'16 f'16 d'16 e'16) f'16( e'16 f'8 a16 bf16) | % viola-sonata/pno1 viola-sonata/pno1.t26 113.75-114; 38
 d'16( \> e'16) af'16( g'16) bf'16( a'16) e''16( d''16) f''16( e''16) bf''16( \p a''16) | % viola-sonata/pno1 viola-sonata/pno1.t26 116.75-117; 39
 d'''8 \f r4 r4. | % viola-sonata/pno1 viola-sonata/pno1.t26 117-117.5; 40
 \ottava #1 \acciaccatura { d''''8[ a'''8] } d'''16( cs'''16 d'''16 a''16 f''16 a''16) f'''16( e'''16 f'''16 d'''16 a''16 d'''16) | % viola-sonata/pno1 viola-sonata/pno1.t26 122.75-123; 41
 a'''16 \fp e'''16 \< f'''16 cs'''16 d'''16 gs''16 \ottava #0 a''16 e''16 f''16 cs''16 d''16 a'16 \f | % viola-sonata/pno1 viola-sonata/pno1.t26 125.75-126; 42
 cs''8( \p a''16 g''16 a''16 e'''16) cs'''8( a''16 g''16 a''16 e'''16) | % viola-sonata/pno1 viola-sonata/pno1.t26 128.75-129; 43
 \ottava #1 f'''8( e'''8 d'''8) e'''8( f'''8 g'''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 131.5-132; 44
 af'''8( g'''8 f'''8) ef'''8( d'''8 c'''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 134.5-135; 45
 gf'''8( f'''8 ef'''8) df'''8( c'''8 bf''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 137.5-138; 46
 <g'' bf'' d'''>4. <a'' cs''' e'''>4. | % viola-sonata/pno1 viola-sonata/pno1.t26 139.5-141; 47
 <f''' a'' d'''>4. \f <d''' f'' a''>4. | % viola-sonata/pno1 viola-sonata/pno1.t26 142.5-144; 48
 <d''' a'' c'''>8. <ef'''~ a''~ c'''~>16 <ef''' a'' c'''>8 <bf'' d''' a''>8. <d'''~ g''~ bf''~>16 <d''' g'' bf''>8 | % viola-sonata/pno1 viola-sonata/pno1.t26 146.25-147; 49
 \ottava #0 <a'' g''>8( <f'' a''>4) <bf'' e''>8( <a'' f''>8) \times 2/3 { d''16 e''16 g''16 } | % viola-sonata/pno1 viola-sonata/pno1.t26 149-149.5; 50
 a''8( g''8 fs''8) d''16( ef''16 d''8 g''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 152.5-153; 51
 d''16( e''?16 d''8 g''8) c''16( d''16 c''8 g''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 155.5-156; 52
 a'16( bf'16 a'8 ef''8) a'16( b'16 a'8 ef''8) | % viola-sonata/pno1 viola-sonata/pno1.t26 158.5-159; 53
 a'16 b'16 a'8 ef''8 a'16 c''16 a'8 d''8 | % viola-sonata/pno1 viola-sonata/pno1.t26 161.5-162; 54
 <ef' bf' ef''>2 r4 | % viola-sonata/pno1 viola-sonata/pno1.t26 162-164; 55
 <d' bf' d''>4. <d' a' d''>4. | % viola-sonata/pno1 viola-sonata/pno1.t26 166.5-168; 56
 <bf' d'' g'>2. | % viola-sonata/pno1 viola-sonata/pno1.t26 168-171; 57
 <g''' g'' bf''>8 r4 <g'' bf'' bf' d''>8 r4 \bar "|."
} }

\new Staff = "down" {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 6/8 \key c \major R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t26 172.5-173; 1
 R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t26 172.5-173; 2
 a'16( gs'16 a'16 f'16 d'16 e'16) f'16( e'16 f'16 d'16 a16 bf16) | % viola-sonata/pno1 viola-sonata/pno1.t2 8.75-9; 3
 \clef bass c'16( a16 fs16 ef'16 d'16 c'16) bf16( a16 bf16 g16 d16 g16) | % viola-sonata/pno1 viola-sonata/pno1.t2 11.75-12; 4
 f4( g8) ef16( d16 ef16 c16 g,16 c16) | % viola-sonata/pno1 viola-sonata/pno1.t2 14.75-15; 5
 g16( fs16 g16 ef16 c16 ef16) c'16( b16 c'16 g16 ef16 g16) | % viola-sonata/pno1 viola-sonata/pno1.t2 17.75-18; 6
 g,16( d16 g16 b16 d'16 g'16) g8( b8 c'16 d'16) | % viola-sonata/pno1 viola-sonata/pno1.t2 20.75-21; 7
 \clef treble ef'16( g'16 fs'16 g'16 ef'16 c'16) d'16( g'16 fs'16 g'16 d'16 bf16) | % viola-sonata/pno1 viola-sonata/pno1.t2 23.75-24; 8
 d'4~ d'16 b16( c'4~ c'16) a16 | % viola-sonata/pno1 viola-sonata/pno1.t2 26.75-27; 9
 b16 d'16 g'16 fs'16 g'16 d'16 c'16 ef'16 g'16 fs'16 g'16 a16 | % viola-sonata/pno1 viola-sonata/pno1.t2 29.75-30; 10
 \clef bass d'16( d16 e16 fs16 g16 a16) \clef treble b16( c'16 d'16 e'16 fs'16 g'16) | % viola-sonata/pno1 viola-sonata/pno1.t2 32.75-33; 11
 a'16( bf'16 g'16 d'16 a16 bf16) \clef bass g16( d16 cs16 d16 bf,16 g,16) | % viola-sonata/pno1 viola-sonata/pno1.t2 35.75-36; 12
 g4 <f f'>8 <ef ef'>8 <d d'>8 <c c'>8 | % viola-sonata/pno1 viola-sonata/pno1.t2 38.5-39; 13
 <bf, bf>8 <a, a>8 <g, g>8 cs,16 cs16 <a, e>16 bf,16 <a, e>16 <g a>16 | % viola-sonata/pno1 viola-sonata/pno1.t2 41.75-42; 14
 cs,16 cs16 <a, e>16 bf,16 <a, e>16 <g a>16 d,16 d16 <a, f>16 e16 <a, f>16 a16 | % viola-sonata/pno1 viola-sonata/pno1.t2 44.75-45; 15
 g,16 d16 <g d'>16 cs'16 <bf d'>16 g'16 c16 g16 <c' e'>16 b16 <c' g'>16 e'16 | % viola-sonata/pno1 viola-sonata/pno1.t2 47.75-48; 16
 <d' f'>8( <a e'>8 <f d'>8) c16_( g16 c'16 \change Staff = "up" e'16 g'16 c''16) | % viola-sonata/pno1 viola-sonata/pno1.t2 50.75-51; 17
 \change Staff = "down" a,16( d16 f16 a16 d'16 f'16) d,16( a,16 d16 f16 a16 d'16) | % viola-sonata/pno1 viola-sonata/pno1.t2 53.75-54; 18
 <f f'>8 <e e'>8 <d d'>8 <c c'>8 <bf, bf>8 <a, a>8 | % viola-sonata/pno1 viola-sonata/pno1.t2 56.5-57; 19
 e,16 fs,16 e,16 b,16 <e gs>16 b,16 e16 ds16 e16 b,16 gs,16 b,16 | % viola-sonata/pno1 viola-sonata/pno1.t2 59.75-60; 20
 <c' c>8 <b b,>8 <a, a>8 <g g,>8 <f f,>8 <e, e>8 | % viola-sonata/pno1 viola-sonata/pno1.t2 62.5-63; 21
 <d d,>8 <c c,>8 <b,, b,>8 <a,, a,>4 r8 | % viola-sonata/pno1 viola-sonata/pno1.t2 64.5-65.5; 22
 R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t2 64.5-65.5; 23
 R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t2 64.5-65.5; 24
 R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t2 64.5-65.5; 25
 R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t2 64.5-65.5; 26
 a,8-. <e cs'>8-. <e cs'>8-. a,8-. <f d'>8-. <f d'>8-. | % viola-sonata/pno1 viola-sonata/pno1.t2 80.5-81; 27
 a,8-. <e cs'>8-. <e cs'>8-. a,8-. <d' f>8-. <d' f>8-. | % viola-sonata/pno1 viola-sonata/pno1.t2 83.5-84; 28
 <e cs' a,>4. <d a f,>8. <a,~ f~ d,~>16 <a, f d,>8 | % viola-sonata/pno1 viola-sonata/pno1.t2 86.25-87; 29
 \ottava #-1 <a,, a,>4. \ottava #0 r4. | % viola-sonata/pno1 viola-sonata/pno1.t2 87-88.5; 30
 d,16 a,16 <a d>16 gs16 <a f>16 d'16 f,16 d16 <f a>16 e16 <d' f>16 a'16 | % viola-sonata/pno1 viola-sonata/pno1.t2 92.75-93; 31
 \clef treble bf'16( a'16 g'16 f'16 e'16 d'16 cs'4.) | % viola-sonata/pno1 viola-sonata/pno1.t2 94.5-95.94; 32
 \clef bass a4 a16( bf16 a8) c'16( bf16 a16 bf16 | % viola-sonata/pno1 viola-sonata/pno1.t2 98.75-99; 33
 a8) c'16( bf16 a16 bf16 a16 bf16 a4) | % viola-sonata/pno1 viola-sonata/pno1.t2 101-101.94; 34
 <f, cs a>4. a16 \mf g16 f16 e16 d16 bf,16 | % viola-sonata/pno1 viola-sonata/pno1.t2 104.75-105; 35
 g,16 g16 <bf d'>16 cs'16 <g d'>16 g'16 a,16 d16 <f a>16 e16 <f d'>16 d16 | % viola-sonata/pno1 viola-sonata/pno1.t2 107.75-108; 36
 c16 a16 c'16 b16 c'16 fs'16 bf,16 d16 g16 bf16 d'16 g'16 | % viola-sonata/pno1 viola-sonata/pno1.t2 110.75-111; 37
 d,8-. <f a,>8-. <f a,>8-. <a, f>8-. <a d>8-. d'8-. | % viola-sonata/pno1 viola-sonata/pno1.t2 113.5-114; 38
 <b f'>8( <as e'>8 <a d'>8) <g e'>8( <f d'>8 <e cs'>8) | % viola-sonata/pno1 viola-sonata/pno1.t2 116.5-117; 39
 d,16 a,16 <d a>16 gs16 <f a>16 d'16 f,16 d16 <a f>16 e16 <d' f>16 a'16 | % viola-sonata/pno1 viola-sonata/pno1.t2 119.75-120; 40
 d,16 a,16 <d a>16 gs16 <f a>16 d'16 f,16 d16 <f a>16 e16 <f d'>16 a'16 | % viola-sonata/pno1 viola-sonata/pno1.t2 122.75-123; 41
 \clef treble a'16( a''16) f'16( f''16) d'16( d''16) \clef bass a16( a'16) f16( f'16) d16( d'16) | % viola-sonata/pno1 viola-sonata/pno1.t2 125.75-126; 42
 cs,16 cs16 <a, e>16 bf,16 <a, e>16 <g a>16 cs,16 cs16 <a, e>16 bf,16 <a, e>16 <g a>16 | % viola-sonata/pno1 viola-sonata/pno1.t2 128.75-129; 43
 d,16 d16 <a, f>16 e16 <a, f>16 a16 c,16 c16 <a, e>16 d16 <c e>16 a16 | % viola-sonata/pno1 viola-sonata/pno1.t2 131.75-132; 44
 f,16 f16 <af c'>16 g16 <af f'>16 c'16 c,16 c16 <ef c'>16 d16 <g c'>16 g'16 | % viola-sonata/pno1 viola-sonata/pno1.t2 134.75-135; 45
 ef,16( ef16) df,16( df16) c,16( c16) bf,,16( bf,16 f16 bf16 df'16 f'16) | % viola-sonata/pno1 viola-sonata/pno1.t2 137.75-138; 46
 g'16( e'16 d'16 bf16 g16 e16) e'16( cs'16 a16 g16 e16 cs16) | % viola-sonata/pno1 viola-sonata/pno1.t2 140.75-141; 47
 <d d'>4. r4. | % viola-sonata/pno1 viola-sonata/pno1.t2 141-142.5; 48
 R8*6 | % viola-sonata/pno1 viola-sonata/pno1.t2 141-142.5; 49
 \clef treble a'16 gs'16 a'16 f'16 d'16 e'16 f'16 e'16 f'16 d'16 a16 bf16 | % viola-sonata/pno1 viola-sonata/pno1.t2 149.75-150; 50
 \clef bass c'16 a16 fs16 ef'16 d'16 c'16 bf16 a16 bf16 g16 d16 g16 | % viola-sonata/pno1 viola-sonata/pno1.t2 152.75-153; 51
 f4( g8) ef16 d16 ef16 c16 g,16 c16 | % viola-sonata/pno1 viola-sonata/pno1.t2 155.75-156; 52
 g16 fs16 g16 ef16 c16 ef16 c'16 b16 c'16 g16 ef16 g16 | % viola-sonata/pno1 viola-sonata/pno1.t2 158.75-159; 53
 ef'16 d'16 ef'16 c'16 g16 c'16 fs'16 e'16 fs'16 d'16 a16 d'16 | % viola-sonata/pno1 viola-sonata/pno1.t2 161.75-162; 54
 <ef g bf df'>2 r4 | % viola-sonata/pno1 viola-sonata/pno1.t2 162-164; 55
 <d g bf>4. <d fs a>4. | % viola-sonata/pno1 viola-sonata/pno1.t2 166.5-168; 56
 g,16( d16 g16 bf,16 d16 bf16) d'16( g16 d'16 \change Staff = "up" bf'16 g'16 d''16) | % viola-sonata/pno1 viola-sonata/pno1.t2 170.75-171; 57
 \change Staff = "down" <bf, g, bf d>8 r4 \ottava #-1 <g,, bf,, d, bf,>8 r4 \bar "|."
} }

>>

>>
}
}

\bookpart {
\header { piece = "II" }
\score {
<<
\new PianoStaff <<
\set PianoStaff.instrumentName = "piano"
\set PianoStaff.shortInstrumentName = ""
\new Staff = "up" {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 4/4 \key c \major e''16 \mf e''8.-. f'16 f'8.-. d''16 d''8.-. e'16 e'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 3.25-4; 1
 cs''16 cs''8.-. d'16 d'8.-. b'16 b'8.-. cs'16 cs'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 7.25-8; 2
 e''16 e''8.-. f'16 f'8.-. d''16 d''8.-. e'16 e'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 11.25-12; 3
 bf'16 bf'8.-. c'16 c'8.-. a'16 a'8.-. bf16 bf8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 15.25-16; 4
 a1~ \p | % viola-sonata/pno2 viola-sonata/b2.t23 16-22; 5
 a2 a2~ | % viola-sonata/pno2 viola-sonata/b2.t23 22-29; 6
 a1~ | % viola-sonata/pno2 viola-sonata/b2.t23 22-29; 7
 a4 c'''16( bf''16 e''16 f''16) a''4~( a''8. bf''16) | % viola-sonata/pno2 viola-sonata/b2.t23 31.75-32; 8
 a''2 r16 g''16( c''16 df''16 f''4~ | % viola-sonata/pno2 viola-sonata/b2.t23 35-37.25; 9
 f''4~ f''16) e''16( b'16 c''16 ef''4~ ef''16) d''16 a'16 e'16 | % viola-sonata/pno2 viola-sonata/b2.t23 39.75-40; 10
 cs'16 f'16 a'16 fs''16~ fs''4 \arpeggioArrowUp<as' fs'' cs'''>2\arpeggio \mf | % viola-sonata/pno2 viola-sonata/b2.t23 42-44; 11
 <b' ds'' g''>2 <b'~ ds''~ fs''~>2 | % viola-sonata/pno2 viola-sonata/b2.t23 46-51; 12
 <b' ds'' fs''>2. \times 2/3 { as''16( gs''16 ds''16 } fs''8~ | % viola-sonata/pno2 viola-sonata/b2.t23 51.5-53; 13
 fs''4) \times 2/3 { e''16( ds''16 g'16 } cs''8~ cs''4.) b'8 | % viola-sonata/pno2 viola-sonata/b2.t23 55.5-56; 14
 as'2 \> fs'2 | % viola-sonata/pno2 viola-sonata/b2.t23 58-60; 15
 gs'1~ \p | % viola-sonata/pno2 viola-sonata/b2.t23 60-68; 16
 gs'1 | % viola-sonata/pno2 viola-sonata/b2.t23 60-68; 17
 e''16 \mf e''8.-. fs'16 fs'8.-. ds''16 ds''8.-. e'16 e'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 71.25-72; 18
 cs''16 cs''8.-. ds'16 ds'8.-. c''16 c''8.-. cs'16 cs'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 75.25-76; 19
 e''16 e''8.-. fs'16 fs'8.-. ds''16 ds''8.-. e'16 e'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 79.25-80; 20
 cs''16 cs''8.-. ds'16 ds'8.-. c''16 c''8.-. cs'16 cs'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 83.25-84; 21
 e''16 \< e''8.-. fs'16 fs'8.-. ds''16 ds''8.-. e'16 e'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 87.25-88; 22
 cs''16 \mf cs''8.-. ds'16 ds'8.-. <a' c''>16 <a' c''>8.-. r16 cs''16 c'''16 f''16 | % viola-sonata/pno2 viola-sonata/b2.t23 91.75-92; 23
 \times 4/6 { cs'''16( \f e''16 cs''16 c''16 e'16 cs'16) } \times 4/5 { e''16( cs''16 bs'16 gs'16 e'16) } r4 \clef bass \times 2/3 { e,16( cs16 gs,16 } cs,8) | % viola-sonata/pno2 viola-sonata/b2.t23 95.5-96; 24
 \clef treble <c'' g'' d'''>2. \ff <g'' ef'''>8 <fs'' d'''>8 | % viola-sonata/pno2 viola-sonata/b2.t23 99.5-100; 25
 <f''! df'''>2 \mf \arpeggioArrowUp<ef'' gf'' b''>2\arpeggio | % viola-sonata/pno2 viola-sonata/b2.t23 102-104; 26
 <bf~ g'~>1 \p | % viola-sonata/pno2 viola-sonata/b2.t23 104-110; 27
 <bf g'>2 <bf~ g'~>2 | % viola-sonata/pno2 viola-sonata/b2.t23 110-117; 28
 <bf~ g'~>1 | % viola-sonata/pno2 viola-sonata/b2.t23 110-117; 29
 <bf g'>4 as''16( gs''16 d''16 ds''16 fs''4~ fs''8. g''16) | % viola-sonata/pno2 viola-sonata/b2.t23 119.75-120; 30
 a''2 r16 g''16( c''16 df''16 f''4~ | % viola-sonata/pno2 viola-sonata/b2.t23 123-125.25; 31
 f''4~ f''16) e''16( b'16 c''16 ef''4~ ef''16) d''16 a'16 e'16 | % viola-sonata/pno2 viola-sonata/b2.t23 127.75-128; 32
 cs'16 f'16 a'16 fs''16~ fs''4 \arpeggioArrowUp<as' fs'' cs'''>2\arpeggio \mf | % viola-sonata/pno2 viola-sonata/b2.t23 130-132; 33
 <b' ds'' g''>2 <b'~ ds''~ fs''~>2 | % viola-sonata/pno2 viola-sonata/b2.t23 134-139; 34
 <b' ds'' fs''>2. \times 2/3 { as''16( gs''16 ds''16 } fs''8~ | % viola-sonata/pno2 viola-sonata/b2.t23 139.5-141; 35
 fs''4) \times 2/3 { e''16( ds''16 g'16 } cs''8~ cs''4) r8 b'8 | % viola-sonata/pno2 viola-sonata/b2.t23 143.5-144; 36
 as'2 \> fs'2 \! | % viola-sonata/pno2 viola-sonata/b2.t23 146-148; 37
<<
  { \voiceOne
   r4 \p \times 2/3 { e'''16( ds'''16 as''16 } d'''8~ d'''4) \times 2/3 { cs'''16( b''16 g''16 } as''8) | % viola-sonata/pno2 viola-sonata/b2.t23 151.5-152; 38
} \new Voice { \voiceTwo
   gs'1 | % viola-sonata/pno2 viola-sonata/b2.t26 148-152; 38
} >> \oneVoice
   \times 4/6 { b''16( as''16 e''16 a''16 gs''16 cs''16) } g''8 e''8 c''8 f'8 gs'8.( \mf \> b'16) | % viola-sonata/pno2 viola-sonata/b2.t23 155.75-156; 39
 g'8.( bf'16) fs'8.( a'16) r4 a4~ \p | % viola-sonata/pno2 viola-sonata/b2.t23 159-161; 40
 a4 c'16( \mf e''16 \> bf'16 g'16) b16( d''16 af'16 d'16) r4 | % viola-sonata/pno2 viola-sonata/b2.t23 162.75-163; 41
 a4( \p g'4) f'4( g'8 af'8) | % viola-sonata/pno2 viola-sonata/b2.t23 167.5-168; 42
 ef'8( ef''8 d'4) ef'8( ef''8 df'8 b8) | % viola-sonata/pno2 viola-sonata/b2.t23 171.5-172; 43
 c'16 c'8.-. a'16 a'8.-. bf16 bf8.-. g'16 g'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 175.25-176; 44
 a16 a8.-. gf'16 gf'8.-. c'16 c'8.-. a'16 a'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 179.25-180; 45
 bf16 bf8.-. g'16 g'8.-. a16 a8.-. gf'16 gf'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 183.25-184; 46
 \clef bass g16 \< g8. ef'16 ef'8. f16 f8. d'16 d'8. | % viola-sonata/pno2 viola-sonata/b2.t23 187.25-188; 47
 ef16 ef8. c'16 c'8. d16 d8. b16 b8. | % viola-sonata/pno2 viola-sonata/b2.t23 191.25-192; 48
 \clef treble g'16( \f d''16 a''16 g''16 d'''16 g''16 a''16 c''16) g''16( d''16 f''16 a''16 bf'16 g''16 a'16 fs''16) | % viola-sonata/pno2 viola-sonata/b2.t23 195.75-196; 49
 g'16( ef''16 a''16 g''16 d'''16 ef''16 a''16 g''16) ef''16( d'''16 f''16 c'''16 ef''16 bf''16 d''16 a''16) | % viola-sonata/pno2 viola-sonata/b2.t23 199.75-200; 50
 g'16( d''16 a''16 g''16 d'''16 f''16 a''16 g''16) \ottava #1 g'''16( d'''16 c'''16 f'''16 bf''16 f'''16 a''16 f'''16) | % viola-sonata/pno2 viola-sonata/b2.t23 203.75-204; 51
 g''16( bf''16 g'''16 f'''16 bf'''16 ef'''16 d'''16 a'''16) g'''8( as''8) f'''8( a''8) | % viola-sonata/pno2 viola-sonata/b2.t23 207.5-208; 52
 ef'''8( g''8) d'''8( f''8) c'''8( ef''8) bf''8( d''8) | % viola-sonata/pno2 viola-sonata/b2.t23 211.5-212; 53
 \ottava #0 g''8( bf'8) f''8( a'8) ef''8( g'8) d''8( f'8) | % viola-sonata/pno2 viola-sonata/b2.t23 215.5-216; 54
 c''8( ef'8) bf'8( d'8) a'8( c'8) g'8( bf8) | % viola-sonata/pno2 viola-sonata/b2.t23 219.5-220; 55
 <a g'>4 a'8( c'8) <g' g''>16( ef''16 bf'16 g'16 ef'16 c'16 g16 ef16) | % viola-sonata/pno2 viola-sonata/b2.t23 223.75-224; 56
 R4*4 | % viola-sonata/pno2 viola-sonata/b2.t23 223.75-224; 57
 R4*4 | % viola-sonata/pno2 viola-sonata/b2.t23 223.75-224; 58
 e''16 \p e''8.-. f'16 f'8.-. d''16 d''8.-. e'16 e'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 235.25-236; 59
 df''16 df''8.-. d'16 d'8.-. b'16 b'8.-. df'16 df'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 239.25-240; 60
 e''16 e''8.-. f'16 f'8.-. d''16 d''8.-. e'16 e'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 243.25-244; 61
 df''16 df''8.-. d'16 d'8.-. b'16 b'8.-. df'16 df'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 247.25-248; 62
 e''16 \f e''8.-. f'16 f'8.-. d''16 d''8.-. e'16 e'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 251.25-252; 63
 df''16 df''8.-. d'16 d'8.-. b'16 b'8.-. df'16 df'8.-. | % viola-sonata/pno2 viola-sonata/b2.t23 255.25-256; 64
 d''16 d''8.-. f'16 f'8.-. a'16 a'8.-. a16 a8.-. | \time 2/4 % viola-sonata/pno2 viola-sonata/b2.t23 259.25-260; 65
 f'16 f'8.-. r4 | \time 4/4 % viola-sonata/pno2 viola-sonata/b2.t23 260.25-261; 66
 <c'' g'' d'''>2. <g'' ef'''>8( <d''' gf''>8) | % viola-sonata/pno2 viola-sonata/b2.t23 265.5-266; 67
 <f'' df'''>2 \arpeggioArrowUp<ds'' fs'' b''>2\arpeggio | % viola-sonata/pno2 viola-sonata/b2.t23 268-270; 68
 \clef bass c,16( g,16 d16 c16 g16 c16 d16 c'16) f16( g16 ef'16 f16 d'16 ef16 c'16 d16) | % viola-sonata/pno2 viola-sonata/b2.t23 273.75-274; 69
 c16( g16 d'16 c'16 \clef treble g'16 d'16 c''16 ef'16) g'16( ef''16 f'16 d''16 ef'16 c''16 d'16 b'16) | % viola-sonata/pno2 viola-sonata/b2.t23 277.75-278; 70
 <ef' b'>2 \arpeggioArrowUp<c'' ef'' af'' c'''>2\arpeggio | % viola-sonata/pno2 viola-sonata/b2.t23 280-282; 71
 <b af'>4 \ff <c' g'>4 <af c' f'>4 <g c' g'>4 | % viola-sonata/pno2 viola-sonata/b2.t23 285-286; 72
 <c' f' a'>4 <d' f' b'>4 <ef' g' c''>4 <d' g' b'>4 | % viola-sonata/pno2 viola-sonata/b2.t23 289-290; 73
 <d' g' bf'>4 <c' f' a'>4 <c' f' af'>4 <c' ef' g'>4 | % viola-sonata/pno2 viola-sonata/b2.t23 293-294; 74
 <c' d' f'>4 <c' ef'>4 <b d'>4 <b ef'>4 | % viola-sonata/pno2 viola-sonata/b2.t23 297-298; 75
 <c' f'>4 <c' g'>4 <c' a'>4 <d' b'>4 | % viola-sonata/pno2 viola-sonata/b2.t23 301-302; 76
 <ef' c''>4-. <g g'>4-. g4-. r4 \bar "|."
} }

\new Staff = "down" {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 4/4 \key c \major \clef bass R4*4 | % viola-sonata/pno2 viola-sonata/b2.t23 304-305; 1
 r2 r8 \ottava #-1 \times 2/3 { d,,16( cs,16 f,,16 } d,,8) \ottava #0 r8 | % viola-sonata/pno2 viola-sonata/b2.t9 7-7.5; 2
 R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 7-7.5; 3
 r2 r8 \ottava #-1 \times 2/3 { d,,16( d,16 a,,16 } d,,8) \ottava #0 r8 | % viola-sonata/pno2 viola-sonata/b2.t9 15-15.5; 4
 \ottava #-1 <d,,~ fs,~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 16-22; 5
 <d,, fs,>2 <d,,~ fs,~>2 | % viola-sonata/pno2 viola-sonata/b2.t9 22-32; 6
 <d,,~ fs,~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 22-32; 7
 <d,, fs,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 22-32; 8
 \ottava #0 R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 22-32; 9
 R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 22-32; 10
 r2 \arpeggioArrowUp<ds as f'>2\arpeggio | % viola-sonata/pno2 viola-sonata/b2.t9 42-44; 11
 <b, fs cs'>2 <gs,~ ds~ as~>2 | % viola-sonata/pno2 viola-sonata/b2.t9 46-52; 12
 <gs, ds as>1 | % viola-sonata/pno2 viola-sonata/b2.t9 46-52; 13
 R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 46-52; 14
 <cs' b, fs>2 <fs as gs, ds>2 | % viola-sonata/pno2 viola-sonata/b2.t9 58-60; 15
 <ds~ cs,~ gs,~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 60-68; 16
 <ds cs, gs,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 60-68; 17
 R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 60-68; 18
 R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 60-68; 19
 \times 2/3 { cs,16( cs16 e,16 } cs,8) r4 gs2 | % viola-sonata/pno2 viola-sonata/b2.t9 78-80; 20
 fs4( gs8 a8) e8( e'8 ds4) | % viola-sonata/pno2 viola-sonata/b2.t9 83-83.94; 21
 \times 2/3 { cs16( cs'16 e16 } cs8) \times 2/3 { gs16( ds'16 bs16 } fs8) \times 2/3 { e16( cs'16 gs16 } ds8) <g b>8( <f a>8) | % viola-sonata/pno2 viola-sonata/b2.t9 87.5-88; 22
 <ds fs>8( <e gs>8 <fs a>8 c'8 df'4) df'16 df'8.-. | % viola-sonata/pno2 viola-sonata/b2.t9 91.25-92; 23
 \clef treble r2 \clef bass \times 4/6 { cs'16( gs16 cs16 cs'16 gs16 cs16) } r4 | % viola-sonata/pno2 viola-sonata/b2.t9 91.25-92; 24
 <c, g, e>1 | % viola-sonata/pno2 viola-sonata/b2.t9 96-100; 25
 <c, g, ef>1 | % viola-sonata/pno2 viola-sonata/b2.t9 100-104; 26
 <c,~ e~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 104-110; 27
 <c, e>2 <c,~ e~>2 | % viola-sonata/pno2 viola-sonata/b2.t9 110-120; 28
 <c,~ e~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 110-120; 29
 <c, e>1 | % viola-sonata/pno2 viola-sonata/b2.t9 110-120; 30
 R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 110-120; 31
 R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 110-120; 32
 r2 \arpeggioArrowUp<ds as f'>2\arpeggio | % viola-sonata/pno2 viola-sonata/b2.t9 130-132; 33
 <b, fs cs'>2 <gs,~ ds~ as~>2 | % viola-sonata/pno2 viola-sonata/b2.t9 134-140; 34
 <gs, ds as>1 | % viola-sonata/pno2 viola-sonata/b2.t9 134-140; 35
 R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 134-140; 36
 <cs' b, fs>2 <fs as gs, ds>2 | % viola-sonata/pno2 viola-sonata/b2.t9 146-148; 37
 <gs, ds cs,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 148-152; 38
 R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 148-152; 39
 r2 r4 \ottava #-1 <fs,~ d,,~>4 | % viola-sonata/pno2 viola-sonata/b2.t9 159-163; 40
 <fs, d,,>2. r4 | % viola-sonata/pno2 viola-sonata/b2.t9 159-163; 41
 <fs,~ d,,~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 164-172; 42
 <fs, d,,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 164-172; 43
 <g,~ g,,~ d,~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 172-184; 44
 <g,~ g,,~ d,~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 172-184; 45
 <g, g,, d,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 172-184; 46
 <g,,~ g,~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 184-192; 47
 <g,, g,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 184-192; 48
 \ottava #0 g16 g8. ef'16 ef'8. f16 f8. d'16 d'8. | % viola-sonata/pno2 viola-sonata/b2.t9 195.25-196; 49
 ef16 ef8. c'16 c'8. d16 d8. bf16 bf8. | % viola-sonata/pno2 viola-sonata/b2.t9 199.25-200; 50
 c16 c8. a16 a8. bf,16 bf,8. g16 g8. | % viola-sonata/pno2 viola-sonata/b2.t9 203.25-204; 51
 c16 c8. a8. \ottava #-1 a,16 g,2~ | % viola-sonata/pno2 viola-sonata/b2.t9 206-212; 52
 <g, f,, c,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 206-212; 53
 g,2~ <g, f,, c,>2 | % viola-sonata/pno2 viola-sonata/b2.t9 212-216; 54
 <f, c as,,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 216-220; 55
 <d, g, g,,>4 <c,, c,>4 <g,, d, g,>2 | % viola-sonata/pno2 viola-sonata/b2.t9 222-224; 56
 \ottava #0 <g,~ d~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 224-232; 57
 <g, d>1 | % viola-sonata/pno2 viola-sonata/b2.t9 224-232; 58
 \ottava #-1 <d,~ d,,~ a,,~>1 | % viola-sonata/pno2 viola-sonata/b2.t9 232-240; 59
 <d, d,, a,,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 232-240; 60
 \ottava #0 R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 232-240; 61
 R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 232-240; 62
 R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 232-240; 63
 R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 232-240; 64
 R4*4 | \time 2/4 % viola-sonata/pno2 viola-sonata/b2.t9 232-240; 65
 r4 d16 d8.-. | \time 4/4 % viola-sonata/pno2 viola-sonata/b2.t9 261.25-262; 66
 <c, g, e>1 | % viola-sonata/pno2 viola-sonata/b2.t9 262-266; 67
 <g, ds c,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 266-270; 68
 R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 266-270; 69
 R4*4 | % viola-sonata/pno2 viola-sonata/b2.t9 266-270; 70
 \ottava #-1 <af,, ef, b,>1 | % viola-sonata/pno2 viola-sonata/b2.t9 278-282; 71
 \ottava #0 <b, ef af af,>4 <c ef g g,>4 <f, c f>4 <ef, c>4 | % viola-sonata/pno2 viola-sonata/b2.t9 285-286; 72
 <d, d>4 <g, d>4 <ef, c>4 <g, d>4 | % viola-sonata/pno2 viola-sonata/b2.t9 289-290; 73
 <bf, g>4 <c a>4 <ef af>4 <ef g>4 | % viola-sonata/pno2 viola-sonata/b2.t9 293-294; 74
 f4 fs4 g4 gf4 | % viola-sonata/pno2 viola-sonata/b2.t9 297-298; 75
 <af f>4 e4 ef4 <g d>4 | % viola-sonata/pno2 viola-sonata/b2.t9 301-302; 76
 <g ef' c>4-. <g, ef c,>4-. \ottava #-1 <c,, c,>4-. \ottava #0 r4 \bar "|."
} }

>>

>>
}
}

\bookpart {
\header { piece = "III" }
\score {
<<
\new PianoStaff <<
\set PianoStaff.instrumentName = "piano"
\set PianoStaff.shortInstrumentName = ""
\new Staff = "up" {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 4/4 \key d \major \clef treble d'8-. \f e'8-. fs'8-. g'8-. a'4 d'4 | % viola-sonata/pno3-1 viola-sonata/b2.t61 3-4; 1
 b'4 a'8-. gs'8-. a'8 d'8( fs'8 a'8) | % viola-sonata/pno3-1 viola-sonata/b2.t61 7.5-8; 2
 c''4.( d''8) b'4. g'16( a'16) | % viola-sonata/pno3-1 viola-sonata/b2.t61 11.75-12; 3
 bf'4.( c''8) <fs' a'>8( <e' gs'>8 <d' fs'>4) | % viola-sonata/pno3-1 viola-sonata/b2.t61 15-16; 4
 f'16-. f'16-. bf8-. bf'16-. bf'16-. f'16-. bf'16-. f''16-. f''16-. e''16-. g''16-. bf''16-. bf''16-. f''16-. bf''16-. | % viola-sonata/pno3-1 viola-sonata/b2.t61 19.75-20; 5
 d'''16-. d'''16-. a''16-. d'''16-. d''16-. d''16-. f''16-. d''16-. a''16-. a''16-. a'16-. a''16-. d''16-. d'16-. fs'16-. d'16-. | % viola-sonata/pno3-1 viola-sonata/b2.t61 23.75-24; 6
 d'16-. a'16-. d''16-. a'16-. c''16( a'16 g'16 c''16 a'16 g'16) c'16-. g'16-. c''16-. g'16-. c'16( a16 | \time 3/4 % viola-sonata/pno3-1 viola-sonata/b2.t61 27.75-28; 7
 \clef bass g16 c'16 a16 g16) <ds fs>8-. \mf <ds fs>8-. <ds fs>8-. <ds fs>8-. | \time 4/4 % viola-sonata/pno3-1 viola-sonata/b2.t61 30.5-31; 8
 \clef treble d'16-. \f d'16-. a'16-. d'16-. d''16-. d''16-. a'16-. d''16-. a''16-. a''16-. d''16-. ef'''16-. d'''16-. a''16-. ef''16-. d''16-. | % viola-sonata/pno3-1 viola-sonata/b2.t61 34.75-35; 9
 d'16-. d'16-. a'16-. d'16-. d''16-. d''16-. a'16-. d''16-. a''16-. a''16-. d''16-. ef'''16-. d'''16-. a''16-. ef''16-. d''16-. | \time 2/4 % viola-sonata/pno3-1 viola-sonata/b2.t61 38.75-39; 10
 R4*2 \mf | \time 4/4 % viola-sonata/pno3-1 viola-sonata/b2.t61 38.75-39; 11
 d'8-. \f e'8-. fs'8-. g'8-. a'4 d'4 | % viola-sonata/pno3-1 viola-sonata/b2.t61 44-45; 12
 b'4 a'8-. gs'8-. a'8 <a d'>8( <fs' d'>8 <fs' a'>8) | % viola-sonata/pno3-1 viola-sonata/b2.t61 48.5-49; 13
 <g' c''>4 <c'' e''>4 <a' d''>8 <a d' fs'>8( <d' fs' a'>8 <fs' a' d''>8) | % viola-sonata/pno3-1 viola-sonata/b2.t61 52.5-53; 14
 <g' df' bf'>4( <bf e' g'>4 <a d' fs'>2) | % viola-sonata/pno3-1 viola-sonata/b2.t61 55-57; 15
 bf'16 bf'16 bf''16 bf'16~ bf'16 bf'16 bf''16 bf'16 bf'16 bf'16 bf''16 bf'16~ bf'16 bf'16 bf''8 | % viola-sonata/pno3-1 viola-sonata/b2.t61 60.5-61; 16
 f'16 f'16 f''16 f'16~ f'16 f'16 f''16 f'16 f'16 f'16 f''16 f'16~ f'16 f'16 f''8 | % viola-sonata/pno3-1 viola-sonata/b2.t61 64.5-65; 17
 bf'16 bf'16 bf''16 bf'16 f'16 f'16 f''16 f'16 e'16 e'16 e''16 e'16~ e'16 e'16 e''8 | % viola-sonata/pno3-1 viola-sonata/b2.t61 68.5-69; 18
 d'16( d''16) e'16( e''16) fs'16( fs''16) g'16( g''16) a'16( a''16 fs''16 d''16) fs'16( fs''16 d''16 d'16) | % viola-sonata/pno3-1 viola-sonata/b2.t61 72.75-73; 19
 bf'16( bf''16 g''16 d''16) g'16( g''16 d''16 bf'16) a'16 a'16 a''16 a'16~ a'16 a'16 a''16 a'16 | % viola-sonata/pno3-1 viola-sonata/b2.t61 76.75-77; 20
 af'16 af'16 af''16 af'16~ af'16 af'16 af''16 af'16 g'16 g''16 g'8 g'16 g''8. | % viola-sonata/pno3-1 viola-sonata/b2.t61 80.25-81; 21
 gf'8 gf''8 gf'8 gf''8 f'8 f''8 f'8 f''8 | % viola-sonata/pno3-1 viola-sonata/b2.t61 84.5-85; 22
 r2 \> r4 d'''16( \mf a''16 d''16) d'''16( | % viola-sonata/pno3-1 viola-sonata/b2.t61 88.75-89; 23
 a''16 d''16) d'''16( a''16 d''16) d'''16( a''16 d''16) d'''16( a''16 d''16) d'''16( a''16 d''16) d'''16( a''16 | % viola-sonata/pno3-1 viola-sonata/b2.t61 92.75-93; 24
 d''16) \> g''16( d''16 g'16) g''16( d''16 g'16) g''16( d''16 g'16) g''16( d''16 g'4) \p | % viola-sonata/pno3-1 viola-sonata/b2.t61 96-97; 25
 g'8-. a'8-. bf'8-. c''8-. d''4 g'4 | % viola-sonata/pno3-1 viola-sonata/b2.t61 100-101; 26
 a'8( bf'16 a'16) g'8-. fs'8-. g'8( f'16 ef'16) d'8-. c'8-. | % viola-sonata/pno3-1 viola-sonata/b2.t61 104.5-105; 27
 bf8( c'16 bf16) a8-. bf8-. g4 \mf r4 | % viola-sonata/pno3-1 viola-sonata/b2.t61 107-108; 28
 R4*4 | % viola-sonata/pno3-1 viola-sonata/b2.t61 107-108; 29
 R4*4 | % viola-sonata/pno3-1 viola-sonata/b2.t61 107-108; 30
 R4*4 | % viola-sonata/pno3-1 viola-sonata/b2.t61 107-108; 31
 <c'' f''>16-. \f <c'' f''>16-. <c'' f''>8-. <c'' f''>8-. <a' c''>8-. <a' c''>8-. <f' a'>8-. <f' a'>8-. <c' g'>8-. | % viola-sonata/pno3-1 viola-sonata/b2.t61 124.5-125; 32
 <c'' f''>16-. <c'' f''>16-. <c'' f''>8-. <c'' f''>8-. <af' c''>8-. <af' c''>8-. <f' af'>8-. <f' af'>8-. <cs' e'>8-. | % viola-sonata/pno3-1 viola-sonata/b2.t61 128.5-129; 33
 <cs' e'>8( <c' f'>4) <cs' e'>8( <c' f'>4) <c' e'>8( <f' c'>8) | % viola-sonata/pno3-1 viola-sonata/b2.t61 132.5-133; 34
 <c' e'>8-. <c' f'>8-. <c' e'>8-. <c' f'>8-. <c' e'>8-. <c' f'>4. \ff | \time 1/4 % viola-sonata/pno3-1 viola-sonata/b2.t61 135.5-137; 35
 <g bf ef'>4 | \time 4/4 % viola-sonata/pno3-1 viola-sonata/b2.t61 137-138; 36
 \key af \major af'8-. \f bf'8-. c''8-. df''8-. ef''4 af'4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 3-4; 37
 f''8 df'''16( f''16 df''16 df'''16 af''16 f''16) ef''8 af''16( ef''16 af'16 af''16 ef''16 bf'16) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 7.75-8; 38
 b'16( b''16 gf''16 ef''16) bf'16( bf''16 f''16 d''16) a'16( a''16 e''16 df''16) af'16( af''16 ef''16 c''16) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 11.75-12; 39
 g'16( g''16 ef''16 b'16) af'16( af''16 ef''16 c''16) g'16( g''16 ef''16 b'16 af'4) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 15-16; 40
 <ef'' af''>16-. <ef'' af''>16-. <ef'' af''>8-. <ef'' af''>8-. <c'' ef''>8-. <c'' ef''>8-. <af' c''>8-. <af' c''>8-. <ef' bf'>8-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 19.5-20; 41
 <ef'' af''>16-. <ef'' af''>16-. <ef'' af''>8-. <ef'' af''>8-. <b' ef''>8-. <b' ef''>8-. <g' b'>8-. <g' b'>8-. <e' g'>8-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 23.5-24; 42
 \ottava #1 ef''16( bf''16 ef'''16 ff'''16 ef'''16 bf''16) ef''16( bf''16 ef'''16 ff'''16 ef'''16 bf''16) \ottava #0 d''8-> ef''8-> | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 27.5-28; 43
 r8 d''8-> ef''8-> d''8-> ef''8-> e''8-> r8 d'8-> | \time 3/4 % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 31.5-32; 44
 ef'8-> d'8-> ef'8-> e'8-> f'8-> g'8-> | \time 4/4 % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 34.5-35; 45
 af8-. \mf bf8-. c'8-. df'8-. ef'4 af4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 38-39; 46
 f'4 \acciaccatura { ef'8[ f'8] } ef'8 d'8 ef'4. f'8 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 42.5-43; 47
 <ef'' af''>8 <ef'' af''>16 <ef'' af''>16 <ef'' g''>16 <ef'' g''>16 r16 <ef'' gf''>16 r8 <ef'' gf''>8-. <ef'' f''>8-. <ef'' f''>8-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 46.5-47; 48
 <ef'' af''>8 <ef'' af''>16 <ef'' af''>16 <ef'' gf''>16 <ef'' gf''>16 r16 <df'' ff''>16 r8 <df'' ff''>8-. <df'' ef''>8-. <df'' ef''>8-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 50.5-51; 49
 <ef'' af''>8 <ef'' af''>16 <ef'' af''>16 <ef'' g''>16 <ef'' g''>16 r16 <ef'' gf''>16 r8 <ef'' gf''>8-. <ef'' f''>8-. <ef'' f''>8-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 54.5-55; 50
 <ef'' af''>16 \f <ef'' af''>16 <ef'' af''>16 <ef'' af''>16 <ef'' af''>8-. r8 <bf' g''>16 <bf' g''>16 <bf' g''>16 <bf' g''>16 <bf' g''>8-. r8 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 58-58.5; 51
 af8 \mf bf8 c'8 df'8 d'8 a'16^( d'16 \change Staff = "down" a16 \change Staff = "up" gf'16 d'16 \change Staff = "down" d16) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 62.75-63; 52
 \change Staff = "up" f'4( ef'8 d'8) a'8( e'8 d'8 a8) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 66.5-67; 53
 ff'16 ff'16 ff'16 ff'16 ff'8-. gf'8-. ef'16 ef'16 ef'16 ef'16 ef'8-. df'16 ef'16 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 70.75-71; 54
 <e' a'>16 \> <e' a'>16 <e' a'>16 <e' a'>16 <e' a'>8 <e' a'>16 <e' a'>16 <e' a'>16 <e' a'>16 <e' a'>8 <g' a'>8 \p <g' a'>8 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 74.5-75; 55
 \key c \major <fs' a'>8-. \< <g' b'>8-. <a' c''>8-. <b' d''>8-. <c'' e''>4 \f <g' c''>4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 78-79; 56
 <d'' f''>4( <c'' e''>8 <b' d''>8) <c'' e''>2 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 81-83; 57
<<
  { \voiceOne
   g''4.( a''8) bf''4.( c'''8) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 86.5-87; 58
} \new Voice { \voiceTwo
   e''2 ef''2 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t18 85-87; 58
} >> \oneVoice
   <df'' df'''>8 <b' b''>16 <a' a''>16 <g' g''>8-. <f' f''>8-. <ds' ds''>8( <e' e''>8) <ds' ds''>8( <e' e''>8) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 90.5-91; 59
 cs''16( \p b'16 d''16 \< cs''16 b'16 a'16 gs'16 fs'16) e'16( d'16 fs'16 e'16 d'16 c'16 b16 a16) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 94.75-95; 60
 gs2 \ff a2 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 97-99; 61
 b2 c'4 d'4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 102-103; 62
 a8-. \f b8-. cs'8-. d'8-. e'4 a4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 106-107; 63
 fs'4( e'8 ds'8) e'8 a8( cs'8 e'8) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 110.5-111; 64
 g'4.( a'8) fs'4. d'16( e'16) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 114.75-115; 65
 <d' f'>4 <c' e'>4 <b d'>4 <a c'>4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 118-119; 66
 <gs b e'>2 <e' a cs'>2 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 121-123; 67
 d'8-. e'8-. fs'8-. g'8-. a'4 d'4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 126-127; 68
 b'4 a'8-. gs'8-. a'8 d'8( fs'8 a'8) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 130.5-131; 69
 c''4.( d''8) b'4. g'16( a'16) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 134.75-135; 70
 bf'4.( c''8) <fs' a'>8( <e' gs'>8 <fs' d'>4) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 138-139; 71
<<
  { \voiceOne
   c''4.( \mf d''8)} \new Voice { \voiceTwo
   g'2} >> \oneVoice
   <d' g' b'>2 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 141-143; 72
 <d' g' bf'>2 <d' a' c''>4 \p <d' g' bf'>4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 146-147; 73
 <d' fs' a'>4 r4 \clef bass <fs a d'>4 r4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t15 149-150; 74
 <a, d fs>1 \bar "|."
} }

\new Staff = "down" {
\numericTimeSignature
\set Staff.printKeyCancellation = ##f
\override Score.BarNumber.break-visibility = ##(#f #t #t)
{
 \time 4/4 \key d \major \clef bass d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 3-4; 1
 d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 7-8; 2
 d8-. d8-. d,4-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 11-12; 3
 d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 15-16; 4
 \ottava #-1 as,4 as,,4 as,4 as,,4 | % viola-sonata/pno3-1 viola-sonata/b2.t46 19-20; 5
 \ottava #0 d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 23-24; 6
 d,16-. d,16-. d,16-. d,16-. d,4-. d4-. d,16-. d,16-. d,16-. d,16-. | \time 3/4 % viola-sonata/pno3-1 viola-sonata/b2.t46 27.75-28; 7
 d4 b,4 as,4 | \time 4/4 % viola-sonata/pno3-1 viola-sonata/b2.t46 30-31; 8
 d,4-. d4-. d,4-. d4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 34-35; 9
 d,4-. d4-. d,4-. d4-. | \time 2/4 % viola-sonata/pno3-1 viola-sonata/b2.t46 38-39; 10
 <b, ds fs>4 <as, ds fs>4 | \time 4/4 % viola-sonata/pno3-1 viola-sonata/b2.t46 40-41; 11
 d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 44-45; 12
 d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 48-49; 13
 d8-. d8-. d,4-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 52-53; 14
 d4-. d,4-. d4-. d,8 c,8 | % viola-sonata/pno3-1 viola-sonata/b2.t46 56.5-57; 15
 bf,,8-. f,8-. bf,8-. f,8-. af,,8-. ef,8-. bf,8-. f,8-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 60.5-61; 16
 bf,,8-. f,8-. bf,8-. f,8-. af,,8-. ef,8-. bf,8-. f,8-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 64.5-65; 17
 bf,,8-. f,8-. bf,8-. f,8-. c,8-. g,8-. e8-. g,8-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 68.5-69; 18
 d,8-. a,8-. fs8-. fs,8-. d8-. a8-. fs8-. c'8-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 72.5-73; 19
 bf8-. d'8-. d8-. g8-. g,8-. g,16-. a,16-. bf,16-. a,16-. g,8-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 76.5-77; 20
 c8 c16( d16) ef16( d16 c8) bf,16( d16 g16 bf,16) d16( g16 bf16 d16) | % viola-sonata/pno3-1 viola-sonata/b2.t46 80.75-81; 21
 ef16( d'16 bf16 gf16) d16( ef16 c'16 g16) bf,16( d16 f16 bf16) bf,16( df16 f16 bf16) | % viola-sonata/pno3-1 viola-sonata/b2.t46 84.75-85; 22
 a,8-. bf,8-. c8-. cs8-. d4-. d,4-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 88-89; 23
 \ottava #-1 <c, g,>2 <bf,, g,>4 <a,, a,>4 | % viola-sonata/pno3-1 viola-sonata/b2.t46 92-93; 24
 <g,, bf,>1 | % viola-sonata/pno3-1 viola-sonata/b2.t46 93-97; 25
 <c, g,>2 <bf,, g,>2 | % viola-sonata/pno3-1 viola-sonata/b2.t46 99-101; 26
 <a,, a,>4. <d,, d,>8 <g,, bf,>2 | % viola-sonata/pno3-1 viola-sonata/b2.t46 103-105; 27
 \ottava #0 r2 c8-. d8-. ef8-. f8-. | % viola-sonata/pno3-1 viola-sonata/b2.t46 108.5-109; 28
 g4 c4 a4 g8 fs8 | % viola-sonata/pno3-1 viola-sonata/b2.t46 112.5-113; 29
 g8 c8( e8 g8) bf4.( c'8) | % viola-sonata/pno3-1 viola-sonata/b2.t46 116.5-117; 30
 a4.( bf8) af4 gf4 | % viola-sonata/pno3-1 viola-sonata/b2.t46 120-121; 31
 f2 f4 f4 | % viola-sonata/pno3-1 viola-sonata/b2.t46 124-125; 32
 f2 f,8 f8 f,8 f8 | % viola-sonata/pno3-1 viola-sonata/b2.t46 128.5-129; 33
 f,16( c16 f16 gf16 f16 c16) f,16( c16 f16 gf16 f16 c16) gf16( f16 c16 af,16) | % viola-sonata/pno3-1 viola-sonata/b2.t46 132.75-133; 34
 <c, c>8-. <f, f>8-. <c, c>8-. <f, f>8-. <c, c>8-. <f, f>4. | \time 1/4 % viola-sonata/pno3-1 viola-sonata/b2.t46 135.5-137; 35
 <ef, ef>4 | \time 4/4 % viola-sonata/pno3-1 viola-sonata/b2.t46 137-138; 36
 \key af \major \ottava #-1 af,4-. af,,4-. af,4-. af,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 3-4; 37
 af,4-. af,,4-. af,4-. af,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 7-8; 38
 af,4-. af,,4-. af,4-. af,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 11-12; 39
 g,4-> af,4 g,4-> af,4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 15-16; 40
 <f,, f,>4 r4 r4 <f,, f,>4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 19-20; 41
 <e,, e,>4 r4 r4 <e,, e,>4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 23-24; 42
 <ef,, ef,>4 r4 r4 <d,, d,>8-> <ef,, ef,>8-> | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 27.5-28; 43
 r8 <d,, d,>8-> <ef,, ef,>8-> <d,, d,>8-> <ef,, ef,>8-> <e,, e,>8-> r8 <d,, d,>8-> | \time 3/4 % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 31.5-32; 44
 <ef,, ef,>8-> <d,, d,>8-> <ef,, ef,>8-> <e,, e,>8-> <f,, f,>8-> <g,, g,>8-> | \time 4/4 % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 34.5-35; 45
 <ef, af,>4-. af,,4-. <ef, af,>4-. af,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 38-39; 46
 <ef, af,>8-. <ef, af,>8-. af,,4-. <ef, g,>8( af,8) af,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 42-43; 47
 \ottava #0 gf,8( df8 gf8 af8 gf8 df8 gf,8 df8) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 46.5-47; 48
 e,8( b,8 ff8 gf8 ff8 b,8 e,8 b,8) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 50.5-51; 49
 gf,8( df8 gf8 af8 gf8 df8 gf,8 df8) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 54.5-55; 50
 ff,16 ff16 ff,16 ff16 gf8-. cs,16 d,16 ef,?16 e16 df,16 ef16 bf8-. cf'16( bf16) | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 58.75-59; 51
 af,4-. af,,4-. d4-. d,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 62-63; 52
 af4-. af,4-. d4-. d,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 66-67; 53
 \ottava #-1 a,4-. a,,4-. af,4-. af,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 70-71; 54
 gf,4-. gf,,4-. e,4-. e,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 74-75; 55
 \ottava #0 \key c \major d4-. d,4-. c4-. c,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 78-79; 56
 c4-. c,4-. c4-. c,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 82-83; 57
 c4-. c,4-. c4-. c,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 86-87; 58
 a,4-. a,,4-. e4-. e,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 90-91; 59
 a,4-. a,,4-. e4-. e,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 94-95; 60
 \ottava #-1 <f,, f,>2 <e,, e,>2 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 97-99; 61
 <d,, d,>2 <c, c>4 <b,, b,>4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 102-103; 62
 a,4-. a,,4-. a,4-. a,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 106-107; 63
 c4-. c,4-. a,4-. a,,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 110-111; 64
 \ottava #0 d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 114-115; 65
 <b, f>4 <c e>4 d4 <c ef>4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 118-119; 66
 e2 a,2 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 121-123; 67
 d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 126-127; 68
 d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 130-131; 69
 d8-. d8-. d,4-. d4-. d,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 134-135; 70
 d4-. d,4-. d4-. d,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 138-139; 71
 d8-. d8-. d,4-. d4-. d,4-. | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 142-143; 72
 d4-. d,4-. d2 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 145-147; 73
 d4 r4 d,4 r4 | % viola-sonata/pno3-2 viola-sonata/pno3-2.t3 149-150; 74
 \ottava #-1 <d,, d,>1 \bar "|."
} }

>>

>>
}
}

